const electronInstaller = require('electron-winstaller');

try {
  electronInstaller.createWindowsInstaller({
    appDirectory: 'out/curecall-win32-x64',
    outputDirectory: 'curecall-installers',
    certificateFile: 'user.crt',
    authors: 'Curecall',
    exe: 'curecall.exe',
    version: '1.0.0-beta',
    loadingGif: 'loading.gif',
    iconUrl: 'https://dhygie.curecall.com/favicon.ico',
    setupIcon: './favicon.ico',
  }).then(() => {
	console.log("The installers of your application were succesfully created !");
  }, (e) => {
	console.log(`Well, sometimes you are not so lucky: ${e.message}`)
  });
  console.log('It worked!');
} catch (e) {
  console.log(`No dice: ${e.message}`);
}