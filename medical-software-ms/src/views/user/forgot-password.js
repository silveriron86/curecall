import React, { useState } from 'react';
import { Row, Card, CardTitle, Label, FormGroup, Button } from 'reactstrap';
import { NavLink, useHistory } from 'react-router-dom';
import { Formik, Form, Field } from 'formik';
import { Colxx } from '../../components/common/CustomBootstrap';
import { NotificationManager } from '../../components/common/react-notifications';
import { AccountApi } from '../../redux/services';

const validateEmail = (value) => {
  let error;
  if (!value) {
    error = 'Merci de saisir votre adresse email';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = 'Adresse email invalide';
  }
  return error;
};

const ForgotPassword = () => {
  const [email] = useState('');
  const [loading, setLoading] = useState(false);

  const history = useHistory();

  const onGetPin = (values) => {
    if (values.email !== '') {
      setLoading(true);
      AccountApi.getPin({ email: values.email })
        .then(({ data }) => {
          if (data === 'ok') {
            NotificationManager.success(
              'Code pin envoyé avec succès',
              'Merci de consulter vos SMS.',
              13000,
              null,
              null,
              ''
            );
          } else {
            NotificationManager.warning(
              `${data}`,
              'Merci de consulter vos SMS.',
              13000,
              null,
              null,
              ''
            );
          }
          history.push(`/user/login-with-pin?email=${values.email}`);
        })
        .catch((error) => {
          console.log('Erreur', error);
          NotificationManager.warning(
            "Le SMS contenant le code pin n'a pas pu être envoyé, contactez hello@curecall.com.",
            'Mot de passe oublié',
            13000,
            null,
            null,
            ''
          );
          setLoading(false);
        });
    }
  };

  const initialValues = { email };

  return (
    <Row className="h-100">
      <Colxx xxs="12" md="10" className="mx-auto my-auto">
        <Card className="auth-card">
          <div className="position-relative image-side ">
            <p className="text-white h2">CURECALL</p>
            <p className="text-white h2">Surgery</p>
            <p className="white mb-0">
              Accompagnez automatiquement vos patients en chirurgie ambulatoire
              avant et après leur opération.
            </p>
          </div>
          <div className="form-side">
            <NavLink to="/" className="white">
              <span className="logo-single" />
            </NavLink>
            <CardTitle className="mb-4">
              Recevez immédiatement un code par SMS pour vous connecter.
            </CardTitle>

            <Formik initialValues={initialValues} onSubmit={onGetPin}>
              {({ errors, touched }) => (
                <Form className="av-tooltip tooltip-label-bottom">
                  <FormGroup className="form-group has-float-label">
                    <Label>Email</Label>
                    <Field
                      className="form-control"
                      name="email"
                      validate={validateEmail}
                    />
                    {errors.email && touched.email && (
                      <div className="invalid-feedback d-block">
                        {errors.email}
                      </div>
                    )}
                  </FormGroup>

                  <div className="d-flex justify-content-between align-items-center">
                    <NavLink to="/user/login">Se connecter</NavLink>
                    <Button
                      color="primary"
                      className={`btn-shadow btn-multiple-state ${
                        loading ? 'show-spinner' : ''
                      }`}
                      size="lg"
                    >
                      <span className="spinner d-inline-block">
                        <span className="bounce1" />
                        <span className="bounce2" />
                        <span className="bounce3" />
                      </span>
                      <span className="label">Recevoir mon code par SMS</span>
                    </Button>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </Card>
      </Colxx>
    </Row>
  );
};

export default ForgotPassword;
