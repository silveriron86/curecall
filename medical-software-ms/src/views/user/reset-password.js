/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import { Row, Card, CardTitle, Label, FormGroup, Button } from 'reactstrap';
import { NavLink, useHistory } from 'react-router-dom';
import { Formik, Form, Field } from 'formik';
import { Colxx } from '../../components/common/CustomBootstrap';
import { NotificationManager } from '../../components/common/react-notifications';
import { adminRoot } from '../../constants/defaultValues';
import { UsersApi } from '../../redux/services';

const hasNumber = /\d/;
const validatePassword = (value) => {
  let error;
  if (!value) {
    error = 'Merci de saisir un mot de passe';
  } else if (value.length < 7) {
    error = 'Le mot de passe doit contenir au moins 8 caractères';
  } else if (value.toLowerCase() === value) {
    error = 'Le mot de passe doit contenir au moins 1 majuscule';
  } else if (!hasNumber.test(value)) {
    error = 'Le mot de passe doit contenir au moins 1 chiffre';
  }
  return error;
};
const validateRepassword = (password) => (value) => {
  let error;
  if (value !== password) {
    error = 'Les mots de passe ne sont pas identiques';
  }
  return error;
};

const isFormValid = ({ password, repassword }) =>
  password !== '' && repassword !== '' && password === repassword;

const ResetPassword = () => {
  const [password] = useState('');
  const [repassword] = useState('');
  const [loading, setLoading] = useState(false);

  const history = useHistory();

  const onResetPassword = async (values) => {
    if (isFormValid(values)) {
      setLoading(true);

      UsersApi.updateMe({ password: values.password })
        .then(() => {
          NotificationManager.success(
            'Mot de passe modifié avec succès',
            'Mot de passe modifié',
            13000,
            null,
            null,
            ''
          );
          history.push(adminRoot);
        })
        .catch(() => {
          NotificationManager.warning(
            "Votre mot de passe n'a pas pu être modifié, contactez hello@curecall.com.",
            'Mot de passe modifié',
            13000,
            null,
            null,
            ''
          );
          setLoading(false);
        });
    }
  };

  const initialValues = { password, repassword };

  return (
    <Row className="h-100">
      <Colxx xxs="12" md="10" className="mx-auto my-auto">
        <Card className="auth-card">
          <div className="position-relative image-side ">
            <p className="text-white h2">CURECALL</p>
            <p className="text-white h2">Surgery</p>
            <p className="white mb-0">
              Accompagnez automatiquement vos patients en chirurgie ambulatoire
              avant et après leur opération.
            </p>
          </div>
          <div className="form-side">
            <NavLink to="/" className="white">
              <span className="logo-single" />
            </NavLink>
            <CardTitle className="mb-4">
              Merci de saisir votre nouveau mot de passe :
              <ul style={{ marginTop: '0.5rem' }}>
                <li>8 caractères minimum</li>
                <li>au moins une majuscule et un chiffre</li>
              </ul>
            </CardTitle>

            <Formik initialValues={initialValues} onSubmit={onResetPassword}>
              {({ errors, touched, values }) => (
                <Form className="av-tooltip tooltip-label-bottom">
                  <FormGroup className="form-group has-float-label">
                    <Label>Mot de passe</Label>
                    <Field
                      type="password"
                      className="form-control"
                      name="password"
                      validate={validatePassword}
                    />
                    {errors.password && touched.password && (
                      <div className="invalid-feedback d-block">
                        {errors.password}
                      </div>
                    )}
                  </FormGroup>
                  <FormGroup className="form-group has-float-label">
                    <Label>Confirmez votre mot de passe</Label>
                    <Field
                      type="password"
                      className="form-control"
                      name="repassword"
                      validate={validateRepassword(values.password)}
                    />
                    {errors.repassword && touched.repassword && (
                      <div className="invalid-feedback d-block">
                        {errors.repassword}
                      </div>
                    )}
                  </FormGroup>

                  <div className="d-flex justify-content-between align-items-center">
                    <NavLink to="/user/login">Se connecter</NavLink>
                    <Button
                      color="primary"
                      className={`btn-shadow btn-multiple-state ${
                        loading ? 'show-spinner' : ''
                      }`}
                      size="lg"
                    >
                      <span className="spinner d-inline-block">
                        <span className="bounce1" />
                        <span className="bounce2" />
                        <span className="bounce3" />
                      </span>
                      <span className="label">Enregistrer</span>
                    </Button>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </Card>
      </Colxx>
    </Row>
  );
};

export default ResetPassword;

// without redux page
