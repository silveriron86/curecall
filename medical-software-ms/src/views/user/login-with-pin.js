import React, { useState } from 'react';
import { Row, Card, CardTitle, Label, FormGroup, Button } from 'reactstrap';
import { NavLink, useHistory } from 'react-router-dom';
import { Formik, Form, Field } from 'formik';
import { Colxx } from '../../components/common/CustomBootstrap';
import { NotificationManager } from '../../components/common/react-notifications';
import { currentUser } from '../../constants/defaultValues';
import { setCurrentUser } from '../../helpers/Utils';
import { AccountApi } from '../../redux/services';
import useQuery from '../../hooks/user-query';

const validateEmail = (value) => {
  let error;
  if (!value) {
    error = 'Merci de saisir votre adresse email';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = 'Adresse email invalide';
  }
  return error;
};

const validatePin = (value) => {
  let error;
  if (!value) {
    error = 'Merci de saisir le code PIN reçu par SMS';
  }
  return error;
};

const isFormValid = (values) => values.email !== '' && values.pin !== '';

const LoginWithPin = () => {
  const query = useQuery();

  const [email] = useState(query.has('email') ? query.get('email') : '');
  const [pin] = useState('');
  const [loading, setLoading] = useState(false);

  const history = useHistory();

  const onLoginWithPin = async (values) => {
    if (!loading && isFormValid(values)) {
      setLoading(true);

      AccountApi.loginWithPin(values)
        .then(({ data }) => {
          localStorage.setItem('access_token', data.access_token);
          const item = { uid: data.access_token, ...currentUser };
          setCurrentUser(item);
          history.push('/user/reset-password');
        })
        .catch(() => {
          NotificationManager.warning(
            "Le code PIN ou l'adresse mail que vous indiqué n'ont pas pu être validée, vérifiez votre adresse et le code pin ou contactez hello@curecall.com.",
            'Code PIN ou Email invalide',
            13000,
            null,
            null,
            ''
          );
          setLoading(false);
        });
    }
  };

  const initialValues = { email, pin };

  return (
    <Row className="h-100">
      <Colxx xxs="12" md="10" className="mx-auto my-auto">
        <Card className="auth-card">
          <div className="position-relative image-side ">
            <p className="text-white h2">CURECALL</p>
            <p className="text-white h2">Surgery</p>
            <p className="white mb-0">
              Accompagnez automatiquement vos patients en chirurgie ambulatoire
              avant et après leur opération.
            </p>
          </div>
          <div className="form-side">
            <NavLink to="/" className="white">
              <span className="logo-single" />
            </NavLink>
            <CardTitle className="mb-4">
              {/* eslint-disable-next-line react/no-unescaped-entities */}
              Indiquer votre addresse email ainsi que le code pin que vous avez
              reçu par SMS pour pouvoir vous connecter.
            </CardTitle>

            <Formik initialValues={initialValues} onSubmit={onLoginWithPin}>
              {({ errors, touched }) => (
                <Form className="av-tooltip tooltip-label-bottom">
                  <FormGroup className="form-group has-float-label">
                    <Label>Email</Label>
                    <Field
                      className="form-control"
                      name="email"
                      validate={validateEmail}
                    />
                    {errors.email && touched.email && (
                      <div className="invalid-feedback d-block">
                        {errors.email}
                      </div>
                    )}
                  </FormGroup>

                  <FormGroup className="form-group has-float-label">
                    <Label>Code PIN reçu par SMS</Label>
                    <Field
                      className="form-control"
                      name="pin"
                      validate={validatePin}
                    />
                    {errors.pin && touched.pin && (
                      <div className="invalid-feedback d-block">
                        {errors.pin}
                      </div>
                    )}
                  </FormGroup>

                  <div className="d-flex justify-content-between align-items-center">
                    <NavLink to="/user/login">
                      Se connecter avec un mot de passe
                    </NavLink>
                    <Button
                      type="submit"
                      color="primary"
                      className={`btn-shadow btn-multiple-state ${
                        loading ? 'show-spinner' : ''
                      }`}
                      size="lg"
                    >
                      <span className="spinner d-inline-block">
                        <span className="bounce1" />
                        <span className="bounce2" />
                        <span className="bounce3" />
                      </span>
                      <span className="label">Se connecter</span>
                    </Button>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </Card>
      </Colxx>
    </Row>
  );
};

export default LoginWithPin;
