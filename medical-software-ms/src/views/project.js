import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';

import { ProjectApi } from '../redux/services';

const Projects = ({ match }) => {
  const [projectsList, setProjectsList] = useState(null);

  useEffect(() => {
    ProjectApi.getAll().then((response) => {
      setProjectsList(response.data);
    });
  }, []);

  if (projectsList === null) return <div className="loading" />;

  if (projectsList.length === 0)
    return <div>Vous n&apos;avez accès à aucun projet pour le moment</div>;

  console.log('project-id', projectsList[0].id);

  return (
    <Redirect
      exact
      from={`${match.url}/`}
      to={`${match.url}/${projectsList[0].id}`}
    />
  );
};

export default Projects;
