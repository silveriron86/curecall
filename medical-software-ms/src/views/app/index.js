import React, { Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { adminRoot } from '../../constants/defaultValues';

import AppLayout from '../../layout/AppLayout';

const Patients = React.lazy(() =>
  import(/* webpackChunkName: "viwes-gogo" */ './patients')
);
const Reviews = React.lazy(() =>
  import(/* webpackChunkName: "viwes-reviews" */ './reviews')
);
const Account = React.lazy(() =>
  import(/* webpackChunkName: "viwes-account" */ './account')
);
const BlankPage = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ './blank-page')
);

const App = ({ match }) => {
  return (
    <AppLayout>
      <div className="dashboard-wrapper">
        <Suspense fallback={<div className="loading" />}>
          <Switch>
            <Redirect
              exact
              from={`${match.url}/`}
              to={`${match.url}/patients`}
            />
            <Route
              path={`${match.url}/patients`}
              render={(props) => <Patients {...props} />}
            />
            <Route
              path={`${match.url}/reviews`}
              render={(props) => <Reviews {...props} />}
            />
            <Route
              path={`${adminRoot}/:projectId/account`}
              render={(props) => <Account {...props} />}
            />
            <Route
              path={`${match.url}/blank-page`}
              render={(props) => <BlankPage {...props} />}
            />
            <Redirect to="/error" />
          </Switch>
        </Suspense>
      </div>
    </AppLayout>
  );
};

const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(App));
