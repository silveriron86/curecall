/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Button, Row, Label, Card, CardBody } from 'reactstrap';
import { cloneDeep } from 'lodash';
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { getMe, updateMe } from '../../../redux/actions';
// import Breadcrumb from '../../../containers/navs/Breadcrumb';

const Account = (pageProps) => {
  const { getMeAction, updateMeAction, me } = pageProps;

  useEffect(() => {
    getMeAction();
  }, []);

  const onSubmit = (event, errors, values) => {
    console.log(errors);
    console.log(values);
    const data = cloneDeep(values);
    if (errors.length === 0) {
      // submit
      delete data.confirmationPassword;
      if (!data.password) {
        delete data.password;
      }
      updateMeAction(data);
    }
  };

  console.log(me);
  if (!me) {
    return null;
  }

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <h1>Mon compte</h1>
          {/* <Breadcrumb heading="Account" match={match} /> */}
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="12" md="8" xl="6" className="mb-4">
          <p>
            Voici les informations sur votre compte utilisateur, vous pouvez les
            mettre à jour à l’aide de ce formulaire.
          </p>
          <Card className="mb-5">
            <CardBody>
              {/* <h6 className="mb-4">Custom Rules</h6> */}
              <AvForm
                className="av-tooltip tooltip-label-right"
                model={me}
                onSubmit={(event, errors, values) =>
                  onSubmit(event, errors, values)
                }
              >
                <Row>
                  <Colxx xxs="12" md="3">
                    <AvField
                      type="select"
                      name="title"
                      required
                      label="Titre"
                      errorMessage="Veuillez sélectionner une option"
                    >
                      <option value="Dr.">Dr.</option>
                      <option value="M.">M.</option>
                      <option value="Mme.">Mme.</option>
                      <option value="Pr.">Pr.</option>
                    </AvField>
                  </Colxx>
                  <Colxx xxs="12" md="9">
                    <Row>
                      <Colxx xxs="12" md="6">
                        <AvGroup className="error-t-negative">
                          <Label>Prénom</Label>
                          <AvField
                            name="firstName"
                            type="text"
                            validate={{
                              required: {
                                value: true,
                                errorMessage:
                                  "Entrez votre prénom s'il vous plait",
                              },
                            }}
                          />
                        </AvGroup>
                      </Colxx>
                      <Colxx xxs="12" md="6">
                        <AvGroup className="error-t-negative">
                          <Label>Nom</Label>
                          <AvField
                            name="lastName"
                            type="text"
                            validate={{
                              required: {
                                value: true,
                                errorMessage:
                                  'Veuillez entrer votre nom de famille',
                              },
                            }}
                          />
                        </AvGroup>
                      </Colxx>
                    </Row>
                  </Colxx>
                </Row>
                <AvGroup className="error-t-negative">
                  <Label>Email</Label>
                  <AvField
                    name="email"
                    type="email"
                    validate={{
                      required: {
                        value: true,
                        errorMessage: 'Veuillez saisir votre adresse e-mail',
                      },
                      email: {
                        value: true,
                        errorMessage: 'Merci de saisir votre adresse email',
                      },
                    }}
                  />
                </AvGroup>
                <AvGroup className="error-t-negative">
                  <Label>N° RPPS</Label>
                  <AvField name="businessID" type="text" />
                </AvGroup>
                <AvGroup className="error-t-negative">
                  <Label>Numéro de mobile</Label>
                  <AvField name="phone" type="text" />
                </AvGroup>
                <AvGroup className="error-t-negative">
                  <Label>Mot de passe</Label>
                  <AvField
                    name="password"
                    type="password"
                    validate={
                      {
                        // required: {
                        //   value: true,
                        //   errorMessage:
                        //     "S'il vous plait entrez votre mot de passe",
                        // },
                      }
                    }
                  />
                </AvGroup>
                <AvGroup className="error-l-100 error-t-negative">
                  <Label>Confirmez le mot de passe</Label>
                  <AvField
                    name="confirmationPassword"
                    type="password"
                    validate={{
                      match: {
                        value: 'password',
                        errorMessage: 'Le mot de passe ne correspond pas',
                      },
                      // required: {
                      //   value: true,
                      //   errorMessage:
                      //     "s'il vous plait entrez votre mot de passe",
                      // },
                    }}
                  />
                </AvGroup>
                <Button color="primary">Enregistrer</Button>
              </AvForm>
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    </>
  );
};

const mapStateToProps = ({ allUsers }) => {
  const { me, loadingMe, error } = allUsers;
  return { me, loadingMe, error };
};

export default connect(mapStateToProps, {
  getMeAction: getMe,
  updateMeAction: updateMe,
})(Account);
