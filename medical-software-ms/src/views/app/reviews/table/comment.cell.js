import moment from 'moment';
import React from 'react';

const CommentCell = ({ comment, time }) => (
  <div>
    <span style={{ fontFamily: 'Crimson Text, serif', fontSize: '1.3em' }}>
      &quot;{comment}&quot;
    </span>{' '}
    - {moment(time).format('DD/MM/YYYY')}
  </div>
);

export default CommentCell;
