import React from 'react';
import { Flex } from '../_styles';

const PatientNamingCell = ({ firstname, lastname }) => (
  <Flex className="svg-icon">
    <i
      className="iconsminds-male-female"
      alt="user-icon"
      style={{ fontSize: '1.5rem' }}
    />
    <div style={{ marginLeft: '1rem' }}>
      {firstname} {lastname}
    </div>
  </Flex>
);

const DoctorNamingCell = ({ title, firstname, lastname }) => (
  <Flex className="svg-icon">
    <i
      className="iconsminds-medical-sign"
      alt="doctor-icon"
      style={{ fontSize: '1.5rem' }}
    />
    <div style={{ marginLeft: '1rem' }}>
      {title} {firstname} {lastname}
    </div>
  </Flex>
);

export { PatientNamingCell, DoctorNamingCell };
