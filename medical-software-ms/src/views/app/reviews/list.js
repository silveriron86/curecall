/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/display-name */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import moment from 'moment';
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import {
  Row,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
} from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';

import { ReviewsListWrapper } from './_styles';

import { ProjectApi } from '../../../redux/services';

import { NotificationManager } from '../../../components/common/react-notifications';
import TableReviews from './table/index';
import { PatientNamingCell, DoctorNamingCell } from './table/naming.cell';
import CommentCell from './table/comment.cell';

const ReviewListPage = () => {
  const [filterOptions, setFilterOptions] = useState([]);
  const [filterOptionSelected, setFilterOptionSelected] = useState({
    lable: 'Tout',
    value: '*',
  });
  const [search, setSearch] = useState('');
  const [reviewsData, setReviewsData] = useState([]);
  const [error, setError] = useState(null);
  const { projectId } = useParams();

  useEffect(() => {
    if (!error) return;

    let msg =
      error.response && error.response.data
        ? error.response.data.message
        : 'Server failed';
    if (Array.isArray(msg)) {
      // eslint-disable-next-line prefer-destructuring
      msg = msg[0];
    }

    NotificationManager.warning(msg, 'Erreur', 3000, null, null, '');
  }, [error]);

  useEffect(() => {
    ProjectApi.getProjectReviews(projectId)
      .then((response) => {
        const localReviews = response.data.map((review) => ({
          firstname: review.firstname,
          lastname: review.lastname,
          refDoctor: review.User,
          text: review.PatientVariables[0].valueString,
          time: review.PatientVariables[0].updatedAt,
          medEventDate: review.PatientMedEvents[0].date,
          medEventName: review.PatientMedEvents[0].MedEvent.name,
        }));

        const localFilterOptions = localReviews.map((review) => ({
          label: `${review.refDoctor.firstname} ${review.refDoctor.lastname}`,
          value: `${review.refDoctor.firstname} ${review.refDoctor.lastname}`,
        }));

        localFilterOptions.push({
          label: 'Tout',
          value: '*',
        });

        const ids = localFilterOptions.map((review) => review.value);

        setFilterOptions(
          localFilterOptions.filter(
            ({ value }, index) => !ids.includes(value, index + 1)
          )
        );
        setReviewsData(localReviews);
      })
      .catch(setError);
  }, [projectId]);

  const cols = React.useMemo(() => [
    {
      Header: 'Patient',
      accessor: 'firstname',
      align: 'left',
      Cell: ({ value, row }) => (
        <PatientNamingCell firstname={value} lastname={row.original.lastname} />
      ),
    },
    {
      Header: 'Medecin reférent',
      accessor: 'refDoctor',
      align: 'left',
      Cell: ({ value }) => (
        <DoctorNamingCell
          title={value.title}
          firstname={value.firstname}
          lastname={value.lastname}
        />
      ),
    },
    {
      Header: 'Type de chirurgie',
      accessor: 'medEventName',
      align: 'left',
      Cell: ({ value }) => <div>{value}</div>,
    },
    {
      Header: 'Date de chirurgie',
      accessor: 'medEventDate',
      align: 'left',
      Cell: ({ value }) => <div>{moment(value).format('DD/MM/YYYY')}</div>,
    },
    {
      Header: 'Commentaire',
      accessor: 'text',
      align: 'center',
      cellClass: 'text-center',
      Cell: ({ value, row }) => (
        <CommentCell comment={value} time={row.original.time} />
      ),
    },
  ]);

  const filterSearch = (list, value, filterProp) => {
    if (value === '') return list;

    if (filterProp === 'completeName') {
      return list.filter((review) => {
        if (review.firstname === null || review.lastname === null) return false;

        return `${review.firstname} ${review.lastname}`.startsWith(value);
      });
    }

    if (filterProp === 'docRefName') {
      if (value === '*') return list;

      return list.filter((review) => {
        if (review.refDoctor === null) return false;

        return (
          `${review.refDoctor.firstname} ${review.refDoctor.lastname}` === value
        );
      });
    }

    return list;
  };

  const sortList = (list) => {
    return list.sort(
      (review1, review2) => new Date(review2.time) - new Date(review1.time)
    );
  };

  let filteredReviews = filterSearch(reviewsData, search, 'completeName');
  filteredReviews = filterSearch(
    filteredReviews,
    filterOptionSelected.value,
    'docRefName'
  );
  const sortedReviews = sortList(filteredReviews);

  return (
    <ReviewsListWrapper>
      <Row className="mb-2">
        <Colxx xxs="12">
          <h1>Liste des commentaires</h1>
        </Colxx>
      </Row>
      <Row className="mb-2">
        <Colxx xxs="12">
          <div
            className="mb-3 d-block d-md-inline-block"
            id="reviews-list-filter-sorter"
          >
            <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1 mr-2">
              <DropdownToggle caret color="outline-light" size="xs">
                Filter par medecin : {filterOptionSelected.label}
              </DropdownToggle>
              <DropdownMenu>
                {filterOptions.map((filter) => (
                  <DropdownItem
                    key={`filter-option-${filter.label}`}
                    onClick={() => setFilterOptionSelected(filter)}
                  >
                    {filter.label}
                  </DropdownItem>
                ))}
              </DropdownMenu>
            </UncontrolledDropdown>
            <div className="search-sm d-inline-block float-md-left mr-1 mb-1 align-top light-color">
              <input
                style={{ width: '240px' }}
                value={search}
                type="text"
                name="keyword"
                className="search-outline"
                placeholder="Rechercher par patient"
                onChange={(e) => {
                  setSearch(e.target.value);
                }}
              />
            </div>
          </div>
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <TableReviews columns={cols} data={sortedReviews} />
    </ReviewsListWrapper>
  );
};

export default ReviewListPage;
