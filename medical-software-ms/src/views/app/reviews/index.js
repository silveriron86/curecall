import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { adminRoot } from '../../../constants/defaultValues';

const ReviewListPage = React.lazy(() =>
  import(/* webpackChunkName: "start" */ './list')
);

const Reviews = () => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect
        exact
        from={`${adminRoot}/:projectId/reviews/`}
        to={`${adminRoot}/:projectId/reviews/list`}
      />
      <Route
        path={`${adminRoot}/:projectId/reviews/list`}
        render={(props) => <ReviewListPage {...props} />}
      />

      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default Reviews;
