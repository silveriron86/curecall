/* eslint-disable import/prefer-default-export */
import styled from '@emotion/styled';
import { Button } from 'reactstrap';

export const ReviewsListWrapper = styled('div')({
  table: {
    borderCollapse: 'initial',
    WebkitBorderVerticalSpacing: '1rem',
    maxWidth: 1500,
    margin: '0 auto',
    tr: {
      boxShadow: '0 3px 30px rgb(0 0 0 / 10%), 0 3px 20px rgb(0 0 0 / 10%)',
      overflow: 'hidden',
      borderRadius: '.75rem',
      td: {
        // backgroundColor: 'white',
        borderRadius: '.75rem',
        verticalAlign: 'middle',
        h6: {
          marginBottom: '.5rem',
        },
      },
      '&.detail-row': {
        boxShadow: 'none',
        td: {
          padding: 0,
          backgroundColor: 'transparent',
          '.row': {
            marginLeft: 15,
            marginRight: 15,
            '.left-col': {
              padding: 25,
              borderRadius: '0.75rem',
              boxShadow:
                '0 3px 30px rgb(0 0 0 / 10%), 0 3px 20px rgb(0 0 0 / 10%)',
            },
          },
        },
      },
    },
    thead: {
      tr: {
        boxShadow: 'none',
        th: {
          backgroundColor: 'transparent',
          borderBottom: 0,
          paddingTop: 0,
          paddingBottom: 0,
          '&.center': {
            textAlign: 'center',
          },
        },
      },
    },
  },
});

export const Flex = styled('div')({
  display: 'flex',
  alignItems: 'center',
  '&.between': {
    justifyContent: 'space-between',
  },
  '&.top': {
    alignItems: 'flex-start',
  },
  '.svg': {
    width: 20,
    height: 20,
    marginLeft: 10,
  },
});

export const LinkButton = styled(Button)({});
