/* eslint-disable no-template-curly-in-string */
/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  FormGroup,
} from 'reactstrap';
import moment from 'moment';

import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { FormikReactSelect } from '../../../../containers/form-validations/FormikFields';
import 'react-datepicker/dist/react-datepicker.css';
import PatientForm, { genders, locales } from './patient-form';
import EventsForm from './events-form';
import { NotificationManager } from '../../../../components/common/react-notifications';

const customVariableProps = {
  INTEGER: 'valueInt',
  STRING: 'valueString',
  DATE: 'valueDate',
  BOOLEAN: 'valueBool',
  FLOAT: 'valueFloat',
};

const AddNewModal = ({
  projectId,
  modalOpen,
  toggleModal,
  allMedEvents,
  allPathologies,
  customVariables,
  patient,
  detail,
  onSave,
  onDeleteEvent,
}) => {
  const PatientSchema = Yup.object().shape({
    firstname: Yup.string().required('Veuillez entrer le prénom'),
    lastname: Yup.string().required('Veuillez entrer le nom de famille'),
    phone: Yup.string().required('Veuillez entrer le numéro de mobile'),
    gender: Yup.object().required(),
    locale: Yup.object().required(),
    hasCaregiver: Yup.number().required(),
    birthdate: Yup.string().required('la date de naissance est obligatoire'),
    pathologies: Yup.array().nullable(),
    medEvents: Yup.mixed().test(
      'shouldLestContainSur',
      'Doit contenir au minimum un évènement de type chirurgie',
      (value) => {
        return (
          value.filter((event) =>
            allMedEvents
              .filter(({ type }) => type === 'surgery')
              .map(({ id }) => id)
              .includes(event.medEventId)
          ).length > 0
        );
      }
    ),
    customVariables: Yup.array(),
    refDoctor: Yup.object().required('Veuillez indiquer un médecin référent'),
  });

  const pathologiesOptions = allPathologies.map((pathology) => ({
    value: pathology.id,
    label: pathology.name,
  }));

  let initialEvents = [];
  let initPathologies = [];
  let initialCustomVariables = [];
  if (detail) {
    if (detail.pathologies) {
      initPathologies = detail.pathologies.map((data) => ({
        value: data.Pathology.id,
        label: data.Pathology.name,
      }));
    }
    if (detail.medEvents) {
      initialEvents = detail.medEvents.map((data) => ({
        ...data.MedEvent,
        ...{
          id: data.id,
          medEventId: data.MedEventId,
          date: moment(data.date).toDate(),
          participant: data.participant,
        },
      }));
    }
    if (detail.customVariables) {
      initialCustomVariables = detail.customVariables.map((data) => ({
        id: data.CustomVariableId,
        name: data.CustomVariable.name,
        type: data.CustomVariable.type,
        value: data[customVariableProps[data.CustomVariable.type]],
      }));
    }
  }

  const onSubmit = (values, { setSubmitting }) => {
    const { pathologies, medEvents } = values;
    const isValidEvents = true;
    if (!isValidEvents) {
      NotificationManager.warning(
        'Merci de bien vouloir choisir le nom du professionnel de santé.',
        'Erreur',
        3000,
        null,
        null,
        ''
      );
      return;
    }

    const variables = values.customVariables.map((customVariable) => ({
      CustomVariableId: customVariable.id,
      [customVariableProps[customVariable.type]]: customVariable.value,
    }));

    console.log('variables', variables);
    const {
      firstname,
      lastname,
      birthdate,
      gender,
      phone,
      locale,
      hasCaregiver,
      refDoctor,
    } = values;

    let formattedPhone = phone;
    if (phone.substring(0, 1) === '0') {
      formattedPhone = `+33${phone.substring(1)}`;
    }

    console.log('variables to submit', variables);

    onSave(
      {
        firstname,
        lastname,
        birthdate: moment(birthdate, 'DD/MM/YYYY').format('YYYY-MM-DD'),
        gender: gender.value,
        phone: formattedPhone,
        locale: locale.value,
        hasCaregiver,
        UserId: refDoctor.value.id,
      },
      pathologies,
      medEvents,
      variables,
      initPathologies
    );
    setSubmitting(false);
  };
  const formatPhone = (phone) => {
    if (phone.indexOf('+') < 0) {
      return `+${phone}`;
    }
    return phone;
  };

  const initialValues = {
    firstname: patient ? patient.firstname : '',
    lastname: patient ? patient.lastname : '',
    birthdate:
      patient && patient.birthdate
        ? moment(patient.birthdate).format('DD/MM/YYYY')
        : '',
    gender: patient
      ? genders.find((g) => g.value === patient.gender)
      : genders[0],
    phone: patient ? formatPhone(patient.phone) : '',
    hasCaregiver: patient ? Number(patient.hasCaregiver) : 0,
    pathologies: initPathologies,
    medEvents: initialEvents,
    customVariables: initialCustomVariables,
    locale: patient
      ? locales.find((l) => l.value === patient.locale)
      : locales[0],
    refDoctor: patient ? { value: { id: patient.UserId } } : undefined,
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >
      <Formik
        initialValues={initialValues}
        validationSchema={PatientSchema}
        onSubmit={onSubmit}
      >
        {(formProps) => {
          const {
            values,
            errors,
            touched,
            setFieldValue,
            setFieldTouched,
          } = formProps;
          console.log('errors', errors);
          return (
            <Form className="av-tooltip tooltip-label-bottom">
              <ModalHeader toggle={toggleModal}>
                {patient
                  ? 'Modifier les informations du patient'
                  : 'Créer un nouveau patient'}
              </ModalHeader>

              <ModalBody>
                <PatientForm
                  {...formProps}
                  projectId={projectId}
                  refCenterVariable={customVariables.find(
                    (customVariable) =>
                      customVariable.name === 'Centre de référence'
                  )}
                />
                <FormGroup className="form-group">
                  <Label className="d-block">Pathologies</Label>
                  <FormikReactSelect
                    name="pathologies"
                    id="pathologies"
                    value={values.pathologies}
                    isMulti
                    options={pathologiesOptions}
                    onChange={(field, value, shouldValidate) => {
                      setFieldValue(field, value, shouldValidate);
                    }}
                    onBlur={setFieldTouched}
                  />
                  {errors.pathologies && touched.pathologies ? (
                    <div className="invalid-feedback d-block">
                      {errors.pathologies}
                    </div>
                  ) : null}
                </FormGroup>

                <EventsForm
                  {...formProps}
                  projectId={projectId}
                  allMedEvents={allMedEvents}
                  onDeleteEvent={onDeleteEvent}
                />

                <h6 className="mt-4 mb-0">Informations centre médical</h6>
                <hr className="mt-2 mb-3" />
                {customVariables
                  .filter(
                    (customVariable) =>
                      customVariable.name !== 'Centre de référence'
                  )
                  .map((cv) => {
                    const data = values.customVariables.find(
                      (customVariable) => customVariable.id === cv.id
                    );

                    return (
                      <CustomVariableField
                        key={`custom-variable-${cv.id}`}
                        customVariable={cv}
                        customVariablesData={values.customVariables}
                        customVariableData={data}
                        setFieldValue={setFieldValue}
                      />
                    );
                  })}
              </ModalBody>

              <ModalFooter>
                <Button color="secondary" outline onClick={toggleModal}>
                  Annuler
                </Button>
                <Button color="primary" type="submit" disabled={errors === {}}>
                  {patient ? 'Modifier' : 'Créer'}
                </Button>
              </ModalFooter>
            </Form>
          );
        }}
      </Formik>
    </Modal>
  );
};

const CustomVariableField = ({
  customVariable,
  customVariablesData,
  customVariableData,
  setFieldValue,
}) => {
  const onChange = (event) => {
    const newCustomVariablesData = customVariablesData;

    const customVariablesIdentifier = {
      id: customVariable.id,
      name: customVariable.name,
      type: customVariable.type,
      value: parseInt(event.target.value, 10),
    };

    const customVariableIndex = customVariablesData.findIndex(
      (cv) => cv.id === customVariablesIdentifier.id
    );

    if (customVariableIndex !== -1) {
      newCustomVariablesData[customVariableIndex] = customVariablesIdentifier;
    } else {
      newCustomVariablesData.push(customVariablesIdentifier);
    }

    console.log('newCustomVariablesData', newCustomVariablesData);

    setFieldValue('customVariables', newCustomVariablesData);
  };

  return (
    <FormGroup
      className="form-group"
      key={`custom-variable-${customVariable.id}`}
    >
      <Label className="d-block">{customVariable.name}</Label>
      <Field
        onChange={onChange}
        className="form-control"
        value={customVariableData !== undefined ? customVariableData.value : ''}
        type="number"
      />
    </FormGroup>
  );
};

export default AddNewModal;
