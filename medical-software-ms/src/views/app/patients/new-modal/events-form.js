/* eslint-disable no-param-reassign */
/* eslint-disable no-template-curly-in-string */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';
import { Label, FormGroup, Button } from 'reactstrap';
import { cloneDeep } from 'lodash';
import {
  FormikReactSelect,
  FormikDatePicker,
} from '../../../../containers/form-validations/FormikFields';
import 'react-datepicker/dist/react-datepicker.css';
import { Flex, EventFieldSet } from '../_styles';
import { ProjectApi } from '../../../../redux/services';

const EventsForm = ({
  errors,
  values,
  setFieldValue,
  projectId,
  allMedEvents,
  onDeleteEvent,
}) => {
  const [eventType, setEventType] = useState(null);

  const [usersList, setUsersList] = useState([]);
  const [participantsOptions, setParticipantsOptions] = useState([]);
  const [medEventsOptions, setMedEventsOptions] = useState([]);

  useEffect(() => {
    setMedEventsOptions(
      allMedEvents.map((med) => ({
        value: med.id,
        label: med.name,
      }))
    );
  }, [allMedEvents]);
  useEffect(() => {
    ProjectApi.getProjectUsers(projectId).then((usersListData) => {
      const docUsersList = usersListData.filter(
        (user) => user.role === 'doctor'
      );

      const participantsList = docUsersList.map((doc) => ({
        label: `${doc.firstName} ${doc.lastName}`,
        value: `${doc.firstName} ${doc.lastName}`,
      }));

      participantsList.push({
        label: `Non renseigné`,
        value: 'undefined',
      });
      setParticipantsOptions(participantsList);
      setUsersList(docUsersList);
    });
  }, [projectId]);

  const { medEvents } = values;

  const onSelectEventType = (_e, v) => {
    setEventType(v);
  };
  const onSelectParticipant = (medEvent, index) => (name, value) => {
    let medEventUpdated = {
      ...medEvent,
      participant: value.value,
    };

    const userParticipant = usersList.find(
      (user) => `${user.firstName} ${user.lastName}` === value.value
    );
    if (userParticipant !== undefined)
      medEventUpdated = { ...medEventUpdated, userId: userParticipant.id };

    const medEventsUpdated = cloneDeep(medEvents);
    medEventsUpdated[index] = medEventUpdated;
    setFieldValue('medEvents', medEventsUpdated);
  };
  const onSelectDate = (medEvent, index) => (name, value) => {
    const medEventUpdated = {
      ...medEvent,
      date: value,
    };

    const medEventsUpdated = cloneDeep(medEvents);
    medEventsUpdated[index] = medEventUpdated;
    setFieldValue('medEvents', medEventsUpdated);
  };

  const onAddEvent = () => {
    if (eventType == null) return;

    medEvents.unshift({
      medEventId: eventType.value,
      date: new Date(),
      participant: 'undefined',
    });
    setFieldValue('medEvents', medEvents);
    setEventType(null);
  };
  const onDeleteEventHandler = (medEvent) => () => {
    const medEventIndex = medEvents.findIndex(
      (localMedEvent) => localMedEvent === medEvent
    );

    delete medEvents[medEventIndex];
    setFieldValue(
      'medEvents',
      medEvents.filter((localMedEvent) => localMedEvent)
    );

    if ('id' in medEvent) {
      onDeleteEvent(medEvent.id);
    }
  };

  return (
    <>
      <FormGroup className="form-group">
        <Label className="d-block">Ajouter un évènement pour le patient</Label>
        <Flex>
          <div style={{ flex: 1 }}>
            <FormikReactSelect
              placeholder="Select..."
              name="allEvents"
              id="allEvents"
              value={eventType}
              options={medEventsOptions}
              onChange={onSelectEventType}
              onBlur={() => {}}
            />
            {errors.medEvents ? (
              <div className="invalid-feedback d-block">{errors.medEvents}</div>
            ) : null}
          </div>
          <Button
            color="transparent"
            className="default btn btn-link"
            onClick={onAddEvent}
          >
            Ajouter un évènement
          </Button>
        </Flex>
      </FormGroup>
      {medEvents
        .sort((medEvent1, medEvent2) => medEvent1.date - medEvent2.date)
        .map((medEvent, index) => (
          <EventsFields
            data={{
              medEventsOptions,
              participantsOptions,
            }}
            medEvent={medEvent}
            index={index}
            key={`medEvent-${index}`}
            onDateChange={onSelectDate(medEvent, index)}
            onDelete={onDeleteEventHandler(medEvent)}
            onParticipantChange={onSelectParticipant(medEvent, index)}
          />
        ))}
    </>
  );
};

const EventsFields = ({
  data,
  medEvent,
  index,
  onParticipantChange,
  onDateChange,
  onDelete,
}) => {
  const { medEventId, date, participant } = medEvent;
  const { medEventsOptions, participantsOptions } = data;

  const medEventOption = medEventsOptions.find(
    (medEventLocalOption) => medEventLocalOption.value === medEventId
  );

  const participantOption = participantsOptions.find(
    (participantLocalOption) =>
      participantLocalOption.value ===
      (participant === undefined ? 'undefined' : participant)
  );
  const label = medEventOption !== undefined ? medEventOption.label : '';

  return (
    <EventFieldSet key={`medEvent-${index}`}>
      <legend>
        {label}
        <Button
          color="transparent"
          className="default btn btn-link"
          onClick={onDelete}
        >
          Supprimer l'évènement
        </Button>
      </legend>
      <FormGroup className="form-group">
        <Label className="d-block">Nom du professionnel de santé</Label>
        <FormikReactSelect
          name="participant"
          value={participantOption}
          options={participantsOptions}
          onChange={onParticipantChange}
          onBlur={() => {}}
          defaultValue="Non renseigné"
        />
      </FormGroup>
      <FormGroup className="form-group">
        <Label className="d-block">Date de {label}</Label>
        <FormikDatePicker
          showTime
          name="date"
          value={date}
          onChange={onDateChange}
          onBlur={() => {}}
        />
      </FormGroup>
    </EventFieldSet>
  );
};

export default EventsForm;
