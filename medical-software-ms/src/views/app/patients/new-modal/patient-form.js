/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';
import { Label, FormGroup, Row } from 'reactstrap';
import { Field } from 'formik';
import {
  FormikReactSelect,
  FormikRadioButtonGroup,
} from '../../../../containers/form-validations/FormikFields';
import 'react-datepicker/dist/react-datepicker.css';
import { Colxx } from '../../../../components/common/CustomBootstrap';
import { ProjectApi } from '../../../../redux/services';
import IconTooltip from '../../../../components/IconTooltip';

export const genders = [
  { value: 'UNKNOWN', label: 'NC' },
  { value: 'MALE', label: 'Homme' },
  { value: 'FEMALE', label: 'Femme' },
];

export const locales = [
  { value: 'fr', label: 'Français' },
  { value: 'en', label: 'Anglais' },
];

const centerRefOptions = [
  { value: 'FRENAY', label: 'Frenay' },
  { value: 'EXPERTS', label: 'Experts' },
  { value: 'VOIRON', label: 'Voiron' },
  { value: 'BOURGOIN', label: 'Bourgoin' },
];

const PatientForm = ({
  errors,
  touched,
  values,
  setFieldValue,
  setFieldTouched,
  projectId,
  refCenterVariable,
}) => {
  const [doctorsList, setDoctorsList] = useState([]);
  const [doctorsOptions, setDoctorsOptions] = useState([]);

  useEffect(() => {
    ProjectApi.getProjectUsers(projectId).then((usersListData) => {
      const docUsersList = usersListData.filter(
        (user) => user.role === 'doctor'
      );

      setDoctorsList(docUsersList);
    });
  }, [projectId]);
  useEffect(() => {
    const localDoctorsOptions = doctorsList.map((doc) => ({
      label: `${doc.firstName} ${doc.lastName}`,
      value: doc,
    }));

    setDoctorsOptions(localDoctorsOptions);
  }, [doctorsList]);

  const doctorOption = doctorsOptions.find((localDoctorOption) => {
    if (values.refDoctor === undefined) return null;
    return localDoctorOption.value.id === values.refDoctor.value.id;
  });

  const centerRefOption = centerRefOptions.find((centerRef) => {
    const customVariablesCenterRef = values.customVariables.find(
      (customVariable) => customVariable.id === refCenterVariable.id
    );

    if (customVariablesCenterRef === undefined) return null;
    return centerRef.value === customVariablesCenterRef.value;
  });

  const handleCenterRefChange = (name, value) => {
    const { customVariables } = values;

    const customVariablesCenterRef = {
      id: refCenterVariable.id,
      name: refCenterVariable.name,
      type: 'STRING',
      value: value.value,
    };

    const customVariablesCenterRefIndex = customVariables.findIndex(
      (customVariable) => customVariable.id === refCenterVariable.id
    );

    if (customVariablesCenterRefIndex !== -1) {
      customVariables[customVariablesCenterRefIndex] = customVariablesCenterRef;
    } else {
      customVariables.push(customVariablesCenterRef);
    }

    setFieldValue('customVariables', customVariables);
  };

  return (
    <>
      <Row>
        <Colxx xxs="6">
          <FormGroup className="form-group">
            <Label>Prénom</Label>
            <Field className="form-control" name="firstname" />
            {errors.firstname && touched.firstname ? (
              <div className="invalid-feedback d-block">{errors.firstname}</div>
            ) : null}
          </FormGroup>
        </Colxx>
        <Colxx xxs="6">
          <FormGroup className="form-group">
            <Label>Nom</Label>
            <Field className="form-control" name="lastname" />
            {errors.lastname && touched.lastname ? (
              <div className="invalid-feedback d-block">{errors.lastname}</div>
            ) : null}
          </FormGroup>
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="6">
          <FormGroup className="form-group">
            <Label className="d-block">Né(e) le</Label>
            <Field
              className="form-control"
              name="birthdate"
              placeholder="jj/mm/aaaa"
              maxLength="10"
            />
            {errors.birthdate && touched.birthdate ? (
              <div className="invalid-feedback d-block">{errors.birthdate}</div>
            ) : null}
          </FormGroup>
        </Colxx>
        <Colxx xxs="6">
          <FormGroup className="form-group">
            <Label>Genre</Label>
            <FormikReactSelect
              name="gender"
              id="gender"
              value={values.gender}
              options={genders}
              onChange={setFieldValue}
              onBlur={setFieldTouched}
            />
            {errors.gender && touched.gender ? (
              <div className="invalid-feedback d-block">{errors.gender}</div>
            ) : null}
          </FormGroup>
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="6">
          <FormGroup className="form-group">
            <Label className="d-block">Numéro de mobile</Label>
            <Field className="form-control" name="phone" />
            {errors.phone && touched.phone ? (
              <div className="invalid-feedback d-block">{errors.phone}</div>
            ) : null}
          </FormGroup>
        </Colxx>
        <Colxx xxs="6">
          <FormGroup className="form-group">
            <Label>S'agit-il du numéro de l'aidant ?</Label>
            <FormikRadioButtonGroup
              inline
              name="hasCaregiver"
              id="hasCaregiver"
              value={values.hasCaregiver}
              onChange={setFieldValue}
              onBlur={setFieldTouched}
              options={[
                { value: 0, label: 'Non' },
                {
                  value: 1,
                  label: 'Oui',
                },
              ]}
            />
            {errors.hasCaregiver && touched.hasCaregiver ? (
              <div className="invalid-feedback d-block">
                {errors.hasCaregiver}
              </div>
            ) : null}
          </FormGroup>
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="10">
          <FormGroup className="form-group">
            <Label>Médecin référent</Label>
            <FormikReactSelect
              name="refDoctor"
              id="refDoctor"
              value={doctorOption}
              options={doctorsOptions}
              onChange={setFieldValue}
              onBlur={setFieldTouched}
            />
            {errors.refDoctor && touched.refDoctor ? (
              <div className="invalid-feedback d-block">{errors.refDoctor}</div>
            ) : null}
          </FormGroup>
        </Colxx>
        <Colxx xxs="2" style={{ paddingLeft: '0px' }}>
          <IconTooltip
            iconName="simple-icon-info"
            style={{
              marginTop: '35px',
              marginLeft: '5px',
            }}
          >
            Ce médecin sera informé par SMS des alertes sur le patient
          </IconTooltip>
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="10">
          <FormGroup className="form-group">
            <Label>Nom du centre de référence</Label>
            <FormikReactSelect
              name="refCenter"
              id="refCenter"
              value={centerRefOption}
              options={centerRefOptions}
              onChange={handleCenterRefChange}
              onBlur={setFieldTouched}
            />
            {errors.refCenter && touched.refCenter ? (
              <div className="invalid-feedback d-block">{errors.refCenter}</div>
            ) : null}
          </FormGroup>
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="6">
          <FormGroup className="form-group">
            <Label>Langue</Label>
            <FormikReactSelect
              name="locale"
              id="locale"
              value={values.locale}
              options={locales}
              onChange={setFieldValue}
              onBlur={setFieldTouched}
            />
            {errors.locale && touched.locale ? (
              <div className="invalid-feedback d-block">{errors.locale}</div>
            ) : null}
          </FormGroup>
        </Colxx>
      </Row>
    </>
  );
};

export default PatientForm;
