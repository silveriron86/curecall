import React, { useState } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  // Label,
  Input,
} from 'reactstrap';
import { NotificationManager } from '../../../components/common/react-notifications';

const SMSModal = ({ modalOpen, toggleModal, patient, sendSMS }) => {
  const [message, setMessage] = useState('');
  if (!patient) {
    return null;
  }
  const { firstname, lastname } = patient;
  const handleSend = () => {
    toggleModal();
    sendSMS(patient.id, {
      message,
    });
    setMessage('');
    setTimeout(() => {
      NotificationManager.success(
        `Message envoyé à ${firstname} ${lastname}`,
        'SMS envoyé !',
        1500,
        null,
        null,
        'filled'
      );
    }, 500);
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop>
      <ModalHeader>
        Envoyer un SMS à {firstname} {lastname}
      </ModalHeader>
      <ModalBody>
        <p>
          Vous allez envoyer un SMS à votre patient, il ne pourra pas vous
          répondre, il s’agit d’un message d’information.
        </p>
        {/* <Label className="mt-2">Description</Label> */}
        <Input
          type="textarea"
          name="text"
          placeholder="Tapez votre message (160 caractères max)"
          style={{ width: '100%', height: 200 }}
          value={message}
          onChange={(e) => setMessage(e.target.value)}
        />
        <p className="mt-4">Le message est envoyé instantanément.</p>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={handleSend} disabled={!message}>
          Envoyer le SMS
        </Button>{' '}
        <Button color="secondary" onClick={toggleModal}>
          Annuler
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default SMSModal;
