/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/display-name */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import {
  Row,
  Button,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
} from 'reactstrap';
import _ from 'lodash';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';

import AddNewModal from './new-modal/index';
import { PatientsListWrapper } from './_styles';

import {
  getPatients,
  createPatient,
  getPatientCustomVariables,
  getProjectCustomVariables,
  updatePatientCustomVariables,
  updatePatient,
  getPatient,
  getPatientMedEvents,
  getPatientSMS,
  postPatientSMS,
  getAllPathologies,
  getAllMedEvents,
  postPatientMedEvents,
  postPatientPathology,
  getPatientPathology,
  getPatientMedAlerts,
  deletePatientMedEvent,
  deletePatientPathology,
  updatePatientMedEvent,
} from '../../../redux/actions';
import { NotificationManager } from '../../../components/common/react-notifications';

import EventCell from './table/event.cell';
import PhoneCell from './table/phone.cell';
import NamingCell from './table/naming.cell';
import StateCell from './table/state.cell';
import IdCell from './table/id.cell';
import TablePatients from './table/index';

const sortOptions = [
  { label: 'Nom A-Z', value: 'name' },
  { label: 'Date de création', value: 'dateTimeCreation' },
  { label: 'Alerte sur le profil', value: 'medAlerts' },
];

const PatientListPage = (pageProps) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [variablesData, setVariables] = useState(null);
  const [currentPatient, setCurrentPatient] = useState(null);
  const [pathologiesData, setPathologies] = useState([]);
  const [medEventsData, setMedEventsData] = useState([]);
  const [sortOptionSelected, setSortOptionSelected] = useState({
    value: 'medAlerts',
  });
  const [search, setSearch] = useState('');
  const {
    projects,
    patients,
    projectCustomVariables,
    newPatientId,
    patientsDetails,
    allMedEvents,
    allPathologies,
    error,
  } = pageProps;
  const { projectId } = useParams();

  useEffect(() => {
    if (error) {
      let msg =
        error.response && error.response.data
          ? error.response.data.message
          : 'Server failed';
      if (Array.isArray(msg)) {
        // eslint-disable-next-line prefer-destructuring
        msg = msg[0];
      }

      if (msg === 'Patient with that phone already exists') {
        msg = 'Ce numéro de mobile est déjà utilisé par un autre patient';
      } else if (msg === 'phone must be a valid phone number') {
        msg = 'Veuillez utiliser un numéro de mobile valide';
      } else if (msg === 'birthdate must be a valid ISO 8601 date string') {
        msg = 'La date de naissance indiquée est invalide';
      }

      NotificationManager.warning(msg, 'Erreur', 3000, null, null, '');
    }
  }, [error]);

  useEffect(() => {
    pageProps.getAllMedEventsAction();
    pageProps.getAllPathologiesAction();
  }, []);

  useEffect(() => {
    if (typeof newPatientId !== 'undefined' && newPatientId >= 0) {
      if (variablesData !== null) {
        pageProps.updatePatientCustomVariablesAction(
          projectId,
          newPatientId,
          variablesData
        );
        setVariables(null);
      }

      if (pathologiesData && pathologiesData.length > 0) {
        pathologiesData.forEach((pathology) => {
          pageProps.postPatientPathologyAction(
            projectId,
            newPatientId,
            pathology.value
          );
        });
        setPathologies([]);
      }

      if (medEventsData && medEventsData.length > 0) {
        medEventsData.forEach((me) => {
          pageProps.postPatientMedEventsAction(projectId, newPatientId, me);
        });
        setMedEventsData([]);
      }
    }

    // eslint-disable-next-line no-use-before-define
    setModalOpen(false);
  }, [newPatientId]);

  useEffect(() => {
    pageProps.getPatientsAction(projectId);
    pageProps.getProjectCustomVariablesAction(projectId);
  }, [projects]);

  const toggleModal = () => {
    setModalOpen(!modalOpen);
  };

  const updateStatus = (record) => {
    pageProps.updatePatientAction(projectId, record.id, {
      toReview: !record.toReview,
    });
  };

  const updateLocale = (patientId, value) => {
    pageProps.updatePatientAction(projectId, patientId, {
      locale: value,
    });
  };

  const handleSave = (
    data,
    pathologies,
    medEvents,
    variables,
    initPathologies
  ) => {
    const createPathologies = (projectIdArg, patientId) => (pathologyData) => {
      pageProps.postPatientPathologyAction(
        projectIdArg,
        patientId,
        pathologyData
      );
    };
    const deletePathologies = (projectIdArg, patientId) => (pathologyId) => {
      pageProps.deletePatientPathologyAction(
        projectIdArg,
        patientId,
        pathologyId
      );
    };

    if (currentPatient) {
      // Editing or After patient data creation
      const patientId = currentPatient.id;
      pageProps.updatePatientAction(projectId, patientId, data);
      pageProps.updatePatientCustomVariablesAction(
        projectId,
        patientId,
        variables
      );

      if (initPathologies.length === 0) {
        pathologies
          .map((pathology) => pathology.value)
          .forEach(createPathologies(projectId, patientId));
      } else {
        const pathologiesListToDelete = _.difference(
          initPathologies,
          pathologies
        );
        const pathologiesListToAdd = _.difference(pathologies, initPathologies);

        pathologiesListToAdd
          .map((pathology) => pathology.value)
          .forEach(createPathologies(projectId, patientId));
        pathologiesListToDelete
          .map((pathology) => pathology.value)
          .forEach(deletePathologies(projectId, patientId));
      }

      if (medEvents && medEvents.length > 0) {
        medEvents.forEach((medEvent) => {
          if (typeof medEvent.id !== 'undefined') {
            // update
            const medEventResId = medEvent.id;
            let medEventData = {
              participant: medEvent.participant,
              date: medEvent.date,
            };
            if ('userId' in medEvent) {
              medEventData = { ...medEventData, userId: medEventData.userId };
            }
            pageProps.updatePatientMedEventAction(
              projectId,
              patientId,
              medEventResId,
              medEventData
            );
          } else {
            pageProps.postPatientMedEventsAction(
              projectId,
              patientId,
              medEvent
            );
          }
        });
      }
    } else {
      setVariables(variables);
      setPathologies(pathologies);
      setMedEventsData(medEvents);
      pageProps.createPatientAction({
        projectId,
        data,
      });
    }
  };

  const handleDeleteEvent = (id) => {
    const patientId = currentPatient.id;
    pageProps.deletePatientMedEventAction({
      projectId,
      patientId,
      id,
    });
  };

  const onGetPatientDetail = (patientId) => {
    pageProps.getPatientAction(projectId, patientId);
    pageProps.getPatientMedEventsAction(projectId, patientId);
    pageProps.getPatientSMSAction(projectId, patientId);
    pageProps.getPatientPathologyAction(projectId, patientId);
    pageProps.getPatientMedAlertsAction(projectId, patientId);
    pageProps.getPatientCustomVariablesAction(projectId, patientId);
  };

  const sendSMS = (patientId, data) => {
    pageProps.sendSMSAction(projectId, patientId, data);
  };

  const onEditPatient = (patient) => {
    setCurrentPatient(patient);
    setModalOpen(true);
  };

  const cols = React.useMemo(() => [
    {
      Header: '',
      accessor: 'toReview',
      width: 10,
      align: 'center',
      className: 'p-0',
      Cell: ({ value }) => <StateCell isCheck={value} />,
    },
    {
      Header: 'ID Patient',
      accessor: 'title',
      align: 'left',
      Cell: ({ row, value }) => <IdCell id={row.original.id} value={value} />,
    },
    {
      Header: 'Patient',
      accessor: 'firstname',
      align: 'left',
      Cell: ({ value, row }) => (
        <NamingCell
          firstname={value}
          lastname={row.original.lastname}
          gender={row.original.gender}
          birthdate={row.original.birthdate}
        />
      ),
    },
    {
      Header: 'N° de mobile patient / aidant',
      accessor: 'phone',
      Cell: ({ row, value }) => (
        <PhoneCell value={value} hasCaregiver={row.original.hasCaregiver} />
      ),
    },
    {
      Header: 'Évènement(s)',
      accessor: 'PatientMedAlerts',
      align: 'center',
      cellClass: 'text-center',
      Cell: ({ value }) => <EventCell patientsList={value} />,
    },
  ]);

  const filterSearch = (list, value) => {
    if (value === '') return list;

    if (value.includes('@'))
      return list.filter((patient) => {
        if (patient.mail === null) return false;

        return patient.mail.startsWith(value);
      });

    if (
      value.split('')[0].match(/^[0-9]+$/) &&
      value.replaceAll(' ', '').match(/^[0-9]+$/)
    )
      return list.filter((patient) => patient.phone.startsWith(value));

    return list.filter((patient) => {
      if (patient.firstname === null || patient.lastname === null) return false;

      return (
        patient.firstname.toUpperCase().startsWith(value.toUpperCase()) ||
        patient.lastname.toUpperCase().startsWith(value.toUpperCase())
      );
    });
  };
  const sortList = (list, sortedProp) => {
    if (sortedProp === 'dateTimeCreation') {
      return list.sort(
        (patient1, patient2) =>
          new Date(patient2.createdAt) - new Date(patient1.createdAt)
      );
    }

    const sortedByReview = list.sort((patient1, patient2) => {
      if (patient1.toReview === true && patient2.toReview === false) {
        return -1;
      }

      if (patient1.toReview === false && patient2.toReview === true) {
        return 1;
      }

      return 0;
    });

    if (sortedProp === 'name') {
      return sortedByReview
        .filter((patient) => patient.lastname !== null)
        .sort((patient1, patient2) => {
          return patient1.lastname.localeCompare(patient2.lastname, 'fr', {
            ignorePunctuation: true,
            sensitivity: 'case',
            caseFirst: 'upper',
          });
        });
    }

    if (sortedProp === 'medAlerts') {
      const preparedToSortList = sortedByReview.map((patient) => {
        const medsAlertActive = patient.PatientMedAlerts.filter(
          (medAlert) => medAlert.active
        );

        if (medsAlertActive.length === 0) {
          return {
            ...patient,
            medAlertDate: null,
          };
        }

        return {
          ...patient,
          medAlertDate: medsAlertActive.sort(
            (medAlert1, medAlert2) =>
              new Date(medAlert2.date) - new Date(medAlert1.date)
          )[0],
        };
      });
      return preparedToSortList.sort((patient1, patient2) => {
        if (patient1.medAlertDate === null && patient2.medAlertDate === null)
          return 0;

        if (patient1.medAlertDate === null && patient2.medAlertDate !== null)
          return 1;

        if (patient1.medAlertDate !== null && patient2.medAlertDate === null)
          return -1;

        return (
          new Date(patient2.medAlertDate) - new Date(patient1.medAlertDate)
        );
      });
    }

    return sortedByReview;
  };

  const filteredPatients = filterSearch(patients, search);
  const sortedPatients = sortList(filteredPatients, sortOptionSelected.value);

  return (
    <PatientsListWrapper>
      <Row className="mb-2">
        <Colxx xxs="12">
          <h1>Liste de mes patients</h1>
          <div className="text-zero top-right-button-container">
            <Button
              color="primary"
              size="lg"
              className="top-right-button"
              onClick={() => {
                setCurrentPatient(null);
                toggleModal();
              }}
            >
              Créer un nouveau patient
            </Button>
          </div>
        </Colxx>
      </Row>
      <Row className="mb-2">
        <Colxx xxs="12">
          <div
            className="mb-3 d-block d-md-inline-block"
            id="patients-list-filter-sorter"
          >
            <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1 mr-2">
              <DropdownToggle caret color="outline-light" size="xs">
                Classer par : {sortOptionSelected.label}
              </DropdownToggle>
              <DropdownMenu>
                {sortOptions.map((sort) => {
                  return (
                    <DropdownItem
                      key={`sort-option-${sort.label}`}
                      onClick={() => setSortOptionSelected(sort)}
                    >
                      {sort.label}
                    </DropdownItem>
                  );
                })}
              </DropdownMenu>
            </UncontrolledDropdown>
            <div className="search-sm d-inline-block float-md-left mr-1 mb-1 align-top light-color">
              <input
                style={{ width: '240px' }}
                value={search}
                type="text"
                name="keyword"
                className="search-outline"
                placeholder="Rechercher par Nom, Email, Mobile"
                onChange={(e) => {
                  setSearch(e.target.value);
                }}
              />
            </div>
          </div>
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <TablePatients
        columns={cols}
        data={sortedPatients}
        updateStatus={updateStatus}
        updateLocale={updateLocale}
        patientsDetails={patientsDetails}
        onDetail={onGetPatientDetail}
        onEdit={onEditPatient}
        sendSMS={sendSMS}
        divided
      />

      <AddNewModal
        projectId={projects.length > 0 ? projectId : 1}
        patient={currentPatient}
        detail={currentPatient ? patientsDetails[currentPatient.id] : null}
        modalOpen={modalOpen}
        onSave={handleSave}
        onDeleteEvent={handleDeleteEvent}
        toggleModal={() => setModalOpen(!modalOpen)}
        customVariables={projectCustomVariables}
        allMedEvents={allMedEvents}
        allPathologies={allPathologies}
        categories={[]}
      />
    </PatientsListWrapper>
  );
};

const mapStateToProps = ({ allProjects, admin }) => {
  const {
    loadingProjects,
    projects,
    loadingPatients,
    patients,
    loadingProjectCustomVariables,
    projectCustomVariables,
    loadingPatientCustomVariables,
    patientCustomVariables,
    patientsDetails,
    newPatientId,
    error,
  } = allProjects;
  const { allMedEvents, allPathologies } = admin;
  return {
    loadingProjects,
    projects,
    loadingPatients,
    patients,
    loadingProjectCustomVariables,
    projectCustomVariables,
    loadingPatientCustomVariables,
    patientCustomVariables,
    patientsDetails,
    newPatientId,
    error,
    allMedEvents,
    allPathologies,
  };
};

export default connect(mapStateToProps, {
  createPatientAction: createPatient,
  updatePatientAction: updatePatient,
  getPatientsAction: getPatients,
  getPatientAction: getPatient,
  updatePatientCustomVariablesAction: updatePatientCustomVariables,
  getPatientCustomVariablesAction: getPatientCustomVariables,
  getProjectCustomVariablesAction: getProjectCustomVariables,
  getPatientMedEventsAction: getPatientMedEvents,
  getPatientSMSAction: getPatientSMS,
  sendSMSAction: postPatientSMS,
  getAllPathologiesAction: getAllPathologies,
  getAllMedEventsAction: getAllMedEvents,
  getPatientPathologyAction: getPatientPathology,
  postPatientPathologyAction: postPatientPathology,
  deletePatientPathologyAction: deletePatientPathology,
  postPatientMedEventsAction: postPatientMedEvents,
  deletePatientMedEventAction: deletePatientMedEvent,
  updatePatientMedEventAction: updatePatientMedEvent,
  getPatientMedAlertsAction: getPatientMedAlerts,
})(PatientListPage);
