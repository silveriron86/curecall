/* eslint-disable react/no-array-index-key */
/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React, { useEffect, useState, useRef } from 'react';
import {
  Row,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  FormGroup,
} from 'reactstrap';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Colxx } from '../../../components/common/CustomBootstrap';
import MessageCard from '../../../components/applications/MessageCard';
import { ChatsWrapper, Flex } from './_styles';
import SMSModal from './sms-modal';
import { FormikReactSelect } from '../../../containers/form-validations/FormikFields';
import { locales } from './new-modal/patient-form';
import IconTooltip from '../../../components/IconTooltip';

const PatientDetail = (props) => {
  const scrollBarRef = useRef(null);
  const [smsModalOpen, setSMSModalOpen] = useState(false);
  const {
    currentPatient,
    patientsDetails,
    onEdit,
    updateStatus,
    updateLocale,
    sendSMS,
  } = props;

  const focusScrollBottom = () => {
    setTimeout(() => {
      if (scrollBarRef.current) {
        scrollBarRef.current._ps.element.scrollTop =
          scrollBarRef.current._ps.contentHeight;
      }
    }, 200);
  };

  useEffect(() => {
    focusScrollBottom();
  }, []);

  const openSMSModal = () => {
    setSMSModalOpen(true);
  };

  const detail = patientsDetails[currentPatient.id];
  let smsList = [];
  let pathologies = '';
  let patientMedEvents = [];
  let patientMedAlerts = [];
  if (typeof detail !== 'undefined') {
    if (detail.sms) {
      smsList = detail.sms;
    }
    if (detail.pathologies) {
      const arr = [];
      detail.pathologies.forEach((p) => {
        arr.push(p.Pathology.name);
      });
      pathologies = arr.join(', ');
    }
    if (detail.medEvents) {
      patientMedEvents = detail.medEvents;
    }
    if (detail.medAlerts) {
      patientMedAlerts = detail.medAlerts;
    }
  }
  const messages = smsList.map((s) => ({
    ...s,
    sender: {
      name: s.senderId,
    },
    gender: currentPatient.gender,
    time: moment(s.sentAt).format('HH:mm DD-MM-YYYY'),
    text: s.message,
  }));
  const foundLocale = locales.find((l) => l.value === currentPatient.locale);
  const locale = foundLocale || locales[0];
  return (
    <div className="text-left">
      <Row>
        <Colxx xxs="6" className="left-col card">
          <Flex className="between mb-1">
            <h2>Suivi post-opératoire</h2>
            <UncontrolledDropdown>
              <DropdownToggle caret color="secondary" outline>
                Choisissez une action
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem onClick={() => onEdit(currentPatient)}>
                  Modifier les informations du patient
                </DropdownItem>
                <DropdownItem onClick={() => updateStatus(currentPatient)}>
                  Marquer comme{' '}
                  {currentPatient.toReview ? 'traité' : 'à traiter'}
                </DropdownItem>
                {/* <DropdownItem divider /> */}
                <DropdownItem onClick={() => openSMSModal()}>
                  Envoyer un SMS au patient
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Flex>
          <hr className="mt-0 mb-0" />
          <Flex className="between center pt-4 pb-0">
            <div
              style={{
                flex: 1,
                minHeight: 300,
                justifyContent: 'center',
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              <div>Pathologies : {pathologies || 'NC'}</div>
              {patientMedEvents.map((me) => {
                return (
                  <div key={`me-${me.id}`} style={{ marginTop: 15 }}>
                    <div className="mt-1">
                      <strong>{me.MedEvent.name}</strong> :{' '}
                      <strong>
                        {moment(me.date).format('DD/MM/YYYY HH:mm')}
                      </strong>
                    </div>
                    <div className="mt-0">
                      Professionnel de santé :{' '}
                      {me.participant === 'undefined'
                        ? 'Non renseigné'
                        : me.participant}
                    </div>
                  </div>
                );
              })}
            </div>
            <div
              style={{
                width: 1,
                height: '100%',
                backgroundColor: '#eee',
                margin: '0 10px',
              }}
            />
            <div
              style={{
                flex: 1,
              }}
            >
              {patientMedAlerts.map((pa) => {
                if (!pa.active) {
                  return null;
                }
                return (
                  <Flex
                    key={`pa-${pa.id}`}
                    className="mt-2 mb-2"
                    style={{ alignItems: 'center' }}
                  >
                    <div style={{ width: '50%', paddingLeft: 15 }}>
                      {pa.MedAlert.label} :
                    </div>
                    <div style={{ flex: 1 }} className="text-right">
                      <h6 className="mb-0">
                        <span className={`badge ${pa.ui}`}>{pa.label}</span>
                        {/* <Badge color="danger">{pa.label}</Badge> */}
                      </h6>
                    </div>
                  </Flex>
                );
              })}
            </div>
          </Flex>

          {/* <h2>Satisfaction Patient</h2>
          <hr className="mt-0 mb-4" />
          <CardSubtitle className="mb-0">Commentaire du patient :</CardSubtitle>
          <p>
            <i>
              J’ai beaucoup attendu dans la salle d’attente lors de mon
              rendez-vous de contrôle et ça pour seulement 5 minutes de
              consultation, c’est dommage parce que tout le reste est super!
            </i>
          </p> */}
        </Colxx>
        <Colxx xxs="6">
          <div
            style={{
              position: 'absolute',
              right: 0,
              top: 5,
              width: 210,
            }}
          >
            <Row>
              <Colxx xxs="10">
                <FormGroup className="form-group">
                  <FormikReactSelect
                    name="locale"
                    id="locale"
                    value={locale}
                    options={locales}
                    onChange={(field, selected) => {
                      updateLocale(currentPatient.id, selected.value);
                    }}
                    onBlur={() => {}}
                  />
                </FormGroup>
              </Colxx>
              <Colxx xxs="2" style={{ paddingLeft: '0px' }}>
                <IconTooltip iconName="simple-icon-question">
                  Sélection de la langue dans laquelle votre patient souhaite
                  communiquer par SMS. <br />
                  Attention, vous verrez les messages dans la langue utilisée
                  par le patient.
                </IconTooltip>
              </Colxx>
            </Row>
          </div>
          <h4 className="mt-3 mb-4 ml-4">Echange SMS avec le patient</h4>
          <ChatsWrapper>
            <PerfectScrollbar
              ref={scrollBarRef}
              // containerRef={(ref) => {}}
              options={{ suppressScrollX: true, wheelPropagation: false }}
            >
              {messages.map((item, index) => {
                return (
                  <MessageCard
                    key={index}
                    index={index}
                    item={item}
                    currentUserid={0}
                  />
                );
              })}
            </PerfectScrollbar>
          </ChatsWrapper>
        </Colxx>
      </Row>
      <SMSModal
        patient={currentPatient}
        modalOpen={smsModalOpen}
        sendSMS={sendSMS}
        toggleModal={() => setSMSModalOpen(!smsModalOpen)}
      />
    </div>
  );
};
export default PatientDetail;
