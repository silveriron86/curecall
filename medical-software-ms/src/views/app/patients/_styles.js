/* eslint-disable import/prefer-default-export */
import styled from '@emotion/styled';
import { Button } from 'reactstrap';

export const PatientsListWrapper = styled('div')({
  table: {
    borderCollapse: 'initial',
    WebkitBorderVerticalSpacing: '1rem',
    maxWidth: 1500,
    margin: '0 auto',
    tr: {
      boxShadow: '0 3px 30px rgb(0 0 0 / 10%), 0 3px 20px rgb(0 0 0 / 10%)',
      overflow: 'hidden',
      borderRadius: '.75rem',
      td: {
        // backgroundColor: 'white',
        borderRadius: '.75rem',
        verticalAlign: 'middle',
        h6: {
          marginBottom: '.5rem',
        },
      },
      '&:nth-of-type(even)': {
        display: 'none',
      },
      '&.detail-row': {
        boxShadow: 'none',
        td: {
          padding: 0,
          backgroundColor: 'transparent',
          '.row': {
            marginLeft: 15,
            marginRight: 15,
            '.left-col': {
              padding: 25,
              borderRadius: '0.75rem',
              boxShadow:
                '0 3px 30px rgb(0 0 0 / 10%), 0 3px 20px rgb(0 0 0 / 10%)',
            },
          },
        },
      },
    },
    thead: {
      tr: {
        boxShadow: 'none',
        th: {
          backgroundColor: 'transparent',
          borderBottom: 0,
          paddingTop: 0,
          paddingBottom: 0,
          '&.center': {
            textAlign: 'center',
          },
        },
      },
    },
  },
});

export const ChatsWrapper = styled('div')({
  width: '100%',
  position: 'absolute',
  height: 'calc(100% - 62px)',
  padding: '0 10px',
  background: 'transparent',
});

export const CollapseButton = styled(Button)({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  i: {
    marginLeft: 5,
  },
});

export const Flex = styled('div')({
  display: 'flex',
  alignItems: 'center',
  '&.between': {
    justifyContent: 'space-between',
  },
  '&.top': {
    alignItems: 'flex-start',
  },
  '.svg': {
    width: 20,
    height: 20,
    marginLeft: 10,
  },
});

export const EventFieldSet = styled('fieldset')({
  padding: '0 15px',
  border: '1px solid #eee',
  marginBottom: 20,
  legend: {
    fontSize: 14,
    width: '100%',
    fontWeight: 400,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    '.btn-link': {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
});

export const LinkButton = styled(Button)({});
