import React from 'react';

const IdCell = ({ id, value }) => (
  <>
    <div>{value}</div>
    <div>{id}</div>
  </>
);

export default IdCell;
