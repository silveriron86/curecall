import moment from 'moment';
import React from 'react';

const NamingCell = ({ firstname, lastname, gender, birthdate }) => (
  <>
    <div>
      {firstname} {lastname} ({gender.substring(0, 1)})
    </div>
    <div>Né(e) {moment(birthdate).format('DD/MM/YYYY')}</div>
  </>
);

export default NamingCell;
