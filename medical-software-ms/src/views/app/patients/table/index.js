/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-key */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/display-name */
import React, { useState } from 'react';
import { useTable, usePagination, useSortBy } from 'react-table';
import classnames from 'classnames';
import {
  Collapse,
  // Dropdown, DropdownToggle,
  Button,
} from 'reactstrap';
import DatatablePagination from '../../../../components/DatatablePagination';
import PatientDetail from '../patient-detail';

export default function TablePatients({
  columns,
  data,
  updateStatus,
  updateLocale,
  sendSMS,
  divided = false,
  defaultPageSize = 20,
  onDetail,
  onEdit,
  patientsDetails,
}) {
  const {
    getTableProps,
    getTableBodyProps,
    prepareRow,
    headerGroups,
    page,
    canPreviousPage,
    canNextPage,
    pageCount,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: defaultPageSize },
    },
    useSortBy,
    usePagination
  );
  const [collapse, setCollapse] = useState([]);
  const [prevRowIndex, setPrevRowIndex] = useState(-1);

  const onToggle = (index, patientId) => {
    if (typeof collapse[index] !== 'undefined' && collapse[index] === true) {
      const newCollapse = [...collapse];
      newCollapse[index] = false;
      setCollapse(newCollapse);
      setPrevRowIndex(-1);
      setTimeout(() => {
        const el = document.getElementById(`detail-row-${index}`);
        if (el) {
          el.style.display = 'none';
        }
      }, 250);
    } else {
      onDetail(patientId);
      document.getElementById(`detail-row-${index}`).style.display =
        'table-row';
      const newCollapse = [...collapse];
      newCollapse[index] = true;
      setCollapse(newCollapse);
      setPrevRowIndex(index);
    }
  };

  return (
    <>
      <table
        {...getTableProps()}
        className={`r-table table ${classnames({ 'table-divided': divided })}`}
      >
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column, columnIndex) => {
                return (
                  <th
                    key={`th_${columnIndex}`}
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                    className={`
                      ${
                        column.isSorted
                          ? column.isSortedDesc
                            ? 'sorted-desc'
                            : 'sorted-asc'
                          : ''
                      }
                      ${column.align ? column.align : ''}
                    `}
                  >
                    {column.render('Header')}
                    <span />
                  </th>
                );
              })}
            </tr>
          ))}
        </thead>

        <tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row);
            const rowIndex = pageIndex * pageSize + i;
            const isExpanded =
              collapse[rowIndex] !== 'undefined' && collapse[rowIndex] === true;
            return (
              <React.Fragment key={`tr-${rowIndex}`}>
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell, cellIndex) => (
                    <td
                      key={`td_${cellIndex}`}
                      {...cell.getCellProps({
                        className: cell.column.cellClass,
                      })}
                    >
                      {cell.render('Cell')}
                    </td>
                  ))}
                  <td>
                    <Button
                      outline
                      onClick={() => onToggle(rowIndex, row.original.id)}
                      className="dropdown-toggle"
                    >
                      {isExpanded ? 'Voir moins' : 'Voir plus'}
                    </Button>
                  </td>
                </tr>
                <tr id={`detail-row-${rowIndex}`} className="detail-row">
                  <td colSpan={7} className="text-center">
                    <Collapse isOpen={isExpanded}>
                      {isExpanded && (
                        <PatientDetail
                          currentPatient={row.original}
                          patientsDetails={patientsDetails}
                          updateStatus={updateStatus}
                          updateLocale={updateLocale}
                          onEdit={onEdit}
                          sendSMS={sendSMS}
                        />
                      )}
                    </Collapse>
                  </td>
                </tr>
              </React.Fragment>
            );
          })}
        </tbody>
      </table>

      <DatatablePagination
        page={pageIndex}
        pages={pageCount}
        canPrevious={canPreviousPage}
        canNext={canNextPage}
        pageSizeOptions={[20, 50, data.length]}
        showPageSizeOptions
        showPageJump={false}
        defaultPageSize={pageSize}
        onPageChange={(p) => {
          if (prevRowIndex >= 0) {
            onToggle(prevRowIndex, -1);
            setTimeout(() => {
              gotoPage(p);
            }, 300);
          } else {
            gotoPage(p);
          }
        }}
        onPageSizeChange={(s) => setPageSize(s)}
        paginationMaxSize={pageCount}
      />
    </>
  );
}
