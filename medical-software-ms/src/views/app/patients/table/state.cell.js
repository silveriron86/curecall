import React from 'react';

const StateCell = ({ isCheck }) => {
  console.log('patient state', isCheck);
  return (
    <i
      className={`${
        isCheck
          ? 'simple-icon-refresh heading-icon'
          : 'simple-icon-check heading-icon'
      }`}
    />
  );
};

export default StateCell;
