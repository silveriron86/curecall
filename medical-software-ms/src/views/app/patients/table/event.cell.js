import { Badge } from 'reactstrap';
import React from 'react';

const EventCell = ({ patientsList }) => {
  const medAlertPinned = patientsList.find(
    (medAlertData) => medAlertData.MedAlert.type === 'pined'
  );
  return (
    <h3 className="mb-0">
      {medAlertPinned !== undefined && (
        <Badge color={medAlertPinned.ui.split('-')[1]} pill>
          {medAlertPinned.label}
        </Badge>
      )}
    </h3>
  );
};

export default EventCell;
