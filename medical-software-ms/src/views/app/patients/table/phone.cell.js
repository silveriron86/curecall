import React from 'react';
import { Flex } from '../_styles';
import { ReactComponent as PatientUpSvg } from '../../../../assets/img/patient_up.svg';
import { ReactComponent as AidantDownSvg } from '../../../../assets/img/aidant_down.svg';

const PhoneCell = ({ value, hasCaregiver }) => (
  <>
    {!(value !== '' && hasCaregiver === true) && (
      <Flex className="svg-icon">
        <div style={{ width: 108 }}>{value}</div>
        <PatientUpSvg className="svg" />
      </Flex>
    )}

    {hasCaregiver && (
      <Flex style={{ marginTop: 5 }} className="svg-icon">
        <div style={{ width: 108 }}>{value}</div>
        <AidantDownSvg className="svg" />
      </Flex>
    )}
  </>
);

export default PhoneCell;
