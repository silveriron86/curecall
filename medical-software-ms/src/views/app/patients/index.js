import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { adminRoot } from '../../../constants/defaultValues';

const PatientListPage = React.lazy(() =>
  import(/* webpackChunkName: "start" */ './list')
);

const Patients = () => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect
        exact
        from={`${adminRoot}/:projectId/patients/`}
        to={`${adminRoot}/:projectId/patients/list`}
      />
      <Route
        path={`${adminRoot}/:projectId/patients/list`}
        render={(props) => <PatientListPage {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);

export default Patients;
