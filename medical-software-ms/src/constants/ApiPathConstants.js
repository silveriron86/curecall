const inv = {
  DEV: 'https://api.dev.curecall.com/v2/',
  BETA: 'https://api.dev.curecall.com/v2/',
  QA: 'https://api.dev.curecall.com/v2/',
  PRODUCTION: 'https://api.curecall.com/v2/',
};

module.exports = {
  getApiPath: () => {
    switch (process.env.REACT_APP_API_ENV || process.env.NODE_ENV) {
      case 'development':
        return inv.DEV;
      case 'beta':
        return inv.BETA;
      case 'quality':
        return inv.QA;
      case 'production':
        return inv.PRODUCTION;
      default:
        return inv.DEV;
    }
  },

  getTitle: () => {
    switch (process.env.REACT_APP_API_ENV || process.env.NODE_ENV) {
      case 'beta':
        return '[DEV] Curecall';
      case 'quality':
        return '[QA] Curecall';
      case 'production':
        return 'Curecall';
      case 'development':
        return '[LOCAL] Curecall';
      default:
        return 'Curecall';
    }
  },

  getLogoEnv: () => {
    switch (process.env.REACT_APP_API_ENV || process.env.NODE_ENV) {
      case 'beta':
        return 'DEV';
      case 'quality':
        return 'QA';
      case 'production':
        return '';
      case 'development':
        return 'LOCAL';
      default:
        return '';
    }
  },
};
