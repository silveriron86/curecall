import { adminRoot } from './defaultValues';

const data = [
  {
    id: 'patient-section',
    icon: 'iconsminds-male-female',
    label: 'Mes Patients',
    to: `${adminRoot}/{projectId}/patients/list`,
    // subs: [
    //   {
    //     icon: 'simple-icon-list',
    //     label: 'Liste de mes patients',
    //     to: `${adminRoot}/patients/list`,
    //   },
    //   {
    //     icon: 'simple-icon-plus',
    //     label: 'Creer un nouveau Patient',
    //     to: `${adminRoot}/patients/add`,
    //   },
    // ],
  },
  {
    id: 'review-section',
    icon: 'iconsminds-pantone',
    label: 'Commentaires patients',
    to: `${adminRoot}/{projectId}/reviews/list`,
    // roles: [UserRole.Admin, UserRole.Editor],
    // subs: [
    //   {
    //     icon: 'simple-icon-paper-plane',
    //     label: 'menu.second',
    //     to: `${adminRoot}/second-menu/second`,
    //   },
    // ],
  },
  {
    id: 'account-section',
    icon: 'iconsminds-profile',
    label: 'Mon compte',
    to: `${adminRoot}/{projectId}/account`,
    // roles: [UserRole.Admin, UserRole.Editor],
    // subs: [
    //   {
    //     icon: 'simple-icon-paper-plane',
    //     label: 'menu.second',
    //     to: `${adminRoot}/second-menu/second`,
    //   },
    // ],
  },
];
export default data;
