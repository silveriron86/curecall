import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { auth } from '../../helpers/Firebase';
import {
  LOGIN_USER,
  LOGIN_USER_PIN,
  REGISTER_USER,
  LOGOUT_USER,
  FORGOT_PASSWORD,
  RESET_PASSWORD,
} from '../constants';

import {
  loginUserSuccess,
  loginUserError,
  registerUserSuccess,
  registerUserError,
  forgotPasswordSuccess,
  forgotPasswordError,
  resetPasswordSuccess,
  resetPasswordError,
} from './actions';

import { adminRoot, currentUser } from '../../constants/defaultValues';
import { setCurrentUser } from '../../helpers/Utils';
import { AccountApi, UsersApi } from '../services';

export function* watchLoginUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(LOGIN_USER, loginWithEmailPassword);
}

function* loginWithEmailPassword({ payload }) {
  const { email, password } = payload.user;
  const { history } = payload;
  try {
    const response = yield call(() => AccountApi.login({ email, password }));
    if (!response.message) {
      localStorage.setItem('access_token', response.data.access_token);
      const item = { uid: response.data.access_token, ...currentUser };
      setCurrentUser(item);
      yield put(loginUserSuccess(item));
      history.push(adminRoot);
    } else {
      yield put(loginUserError(response.message));
    }
  } catch (error) {
    yield put(loginUserError(error));
  }
}

export function* watchLoginUserWithPin() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(LOGIN_USER_PIN, loginWithPinAction);
}

function* loginWithPinAction({ payload }) {
  const { email, pin } = payload.user;
  const { history } = payload;
  try {
    const response = yield call(() => AccountApi.loginWithPin({ email, pin }));
    if (response.message) {
      yield put(loginUserError(response.message));
      return;
    }
    localStorage.setItem('access_token', response.data.access_token);
    const item = { uid: response.data.access_token, ...currentUser };
    setCurrentUser(item);
    yield put(loginUserSuccess(item));
    history.push('/user/reset-password');
  } catch (error) {
    yield put(loginUserError(error));
  }
}

export function* watchRegisterUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(REGISTER_USER, registerWithEmailPassword);
}

const registerWithEmailPasswordAsync = async (email, password) => {
  // eslint-disable-next-line no-return-await
  return await auth
    .createUserWithEmailAndPassword(email, password)
    .then((user) => user)
    .catch((error) => error);
};

function* registerWithEmailPassword({ payload }) {
  const { email, password } = payload.user;
  const { history } = payload;
  try {
    const registerUser = yield call(
      registerWithEmailPasswordAsync,
      email,
      password
    );
    if (!registerUser.message) {
      const item = { uid: registerUser.user.uid, ...currentUser };
      setCurrentUser(item);
      yield put(registerUserSuccess(item));
      history.push(adminRoot);
    } else {
      yield put(registerUserError(registerUser.message));
    }
  } catch (error) {
    yield put(registerUserError(error));
  }
}

export function* watchLogoutUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(LOGOUT_USER, logout);
}

const logoutAsync = async (history) => {
  await auth
    .signOut()
    .then((user) => user)
    .catch((error) => error);
  history.push(`${adminRoot}/login`);
};

function* logout({ payload }) {
  const { history } = payload;
  setCurrentUser();
  yield call(logoutAsync, history);
}

export function* watchForgotPassword() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(FORGOT_PASSWORD, forgotPassword);
}

function* forgotPassword({ payload }) {
  const { email } = payload.forgotUserMail;
  try {
    const forgotPasswordStatus = yield call(() => AccountApi.getPin({ email }));

    if (!forgotPasswordStatus) {
      yield put(forgotPasswordError(forgotPasswordStatus.message));
      return;
    }

    yield put(forgotPasswordSuccess('success'));
  } catch (error) {
    yield put(forgotPasswordError(error));
  }
}

export function* watchResetPassword() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(RESET_PASSWORD, resetPassword);
}

function* resetPassword({ payload }) {
  const newPassword = payload;
  try {
    const resetPasswordStatus = yield call(() =>
      UsersApi.updateMe({ password: newPassword })
    );

    if (resetPasswordStatus) {
      yield put(resetPasswordSuccess('success'));
      return;
    }

    yield put(resetPasswordError(resetPasswordStatus.message));
  } catch (error) {
    yield put(resetPasswordError(error));
  }
}

export default function* rootSaga() {
  yield all([
    fork(watchLoginUser),
    fork(watchLogoutUser),
    fork(watchRegisterUser),
    fork(watchForgotPassword),
    fork(watchResetPassword),
    fork(watchLoginUserWithPin),
  ]);
}
