// eslint-disable-next-line import/no-cycle
import {
  GET_USERS,
  GET_USERS_SUCCESS,
  GET_USERS_ERROR,
  CREATE_USER,
  CREATE_USER_SUCCESS,
  CREATE_USER_ERROR,
  GET_ME,
  GET_ME_SUCCESS,
  GET_ME_ERROR,
  UPDATE_ME,
  UPDATE_ME_SUCCESS,
  UPDATE_ME_ERROR,
} from '../constants';

export const getUsers = () => ({
  type: GET_USERS,
});

export const getUsersSuccess = (users) => {
  return {
    type: GET_USERS_SUCCESS,
    payload: { users },
  };
};

export const getUsersError = (error) => ({
  type: GET_USERS_ERROR,
  payload: error,
});

export const createUser = (data) => ({
  type: CREATE_USER,
  payload: data,
});

export const createUserSuccess = (users) => {
  return {
    type: CREATE_USER_SUCCESS,
    payload: { users },
  };
};

export const createUserError = (error) => ({
  type: CREATE_USER_ERROR,
  payload: error,
});

export const getMe = () => ({
  type: GET_ME,
  payload: {},
});
export const getMeSuccess = (user) => ({
  type: GET_ME_SUCCESS,
  payload: user,
});
export const getMeError = (message) => ({
  type: GET_ME_ERROR,
  payload: { message },
});

export const updateMe = (data) => ({
  type: UPDATE_ME,
  payload: data,
});
export const updateMeSuccess = (user) => ({
  type: UPDATE_ME_SUCCESS,
  payload: user,
});
export const updateMeError = (message) => ({
  type: UPDATE_ME_ERROR,
  payload: { message },
});
