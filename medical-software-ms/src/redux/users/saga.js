import { all, call, fork, put, takeEvery } from 'redux-saga/effects';

import { GET_USERS, CREATE_USER, GET_ME, UPDATE_ME } from '../constants';

import {
  getUsersSuccess,
  getUsersError,
  createUserError,
  getMeSuccess,
  getMeError,
  updateMeError,
} from './actions';
import { UsersApi } from '../services';

function* getUsers() {
  try {
    const response = yield call(() => UsersApi.getAll());
    yield put(getUsersSuccess(response.data));
  } catch (error) {
    yield put(getUsersError(error));
  }
}

export function* watchGetUsers() {
  yield takeEvery(GET_USERS, getUsers);
}

function* createUser({ payload }) {
  try {
    const { data } = payload;
    yield call(() => UsersApi.create(data));
    const response = yield call(() => UsersApi.getAll());
    yield put(getUsersSuccess(response.data));
  } catch (error) {
    yield put(createUserError(error));
  }
}

export function* watchCreateUser() {
  yield takeEvery(CREATE_USER, createUser);
}

function* getMe() {
  try {
    const response = yield call(() => UsersApi.getMe());
    yield put(getMeSuccess(response.data));
  } catch (error) {
    yield put(getMeError(error));
  }
}

export function* watchGetMe() {
  yield takeEvery(GET_ME, getMe);
}

function* updateMe({ payload }) {
  console.log('*me', payload);
  try {
    yield call(() => UsersApi.updateMe(payload));
    const response = yield call(() => UsersApi.getMe());
    yield put(getMeSuccess(response.data));
  } catch (error) {
    yield put(updateMeError(error));
  }
}

export function* watchUpdateMe() {
  yield takeEvery(UPDATE_ME, updateMe);
}

export default function* rootSaga() {
  yield all([
    fork(watchGetUsers),
    fork(watchCreateUser),
    fork(watchGetMe),
    fork(watchUpdateMe),
  ]);
}
