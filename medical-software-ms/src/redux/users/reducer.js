import {
  GET_USERS,
  GET_USERS_SUCCESS,
  GET_USERS_ERROR,
  CREATE_USER,
  CREATE_USER_ERROR,
  GET_ME,
  GET_ME_SUCCESS,
  GET_ME_ERROR,
  UPDATE_ME,
  UPDATE_ME_ERROR,
} from '../constants';

const INIT_STATE = {
  users: [],
  error: '',
  loadingUsers: false,
  me: null,
  loadingMe: false,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_USERS:
    case CREATE_USER:
      return { ...state, loadingUsers: true };

    case GET_USERS_SUCCESS:
      return {
        ...state,
        loadingUsers: false,
        users: action.payload.users,
      };

    case GET_USERS_ERROR:
    case CREATE_USER_ERROR:
      return { ...state, loadingUsers: false, error: action.payload };

    case GET_ME:
    case UPDATE_ME:
      return { ...state, loadingMe: true };

    case GET_ME_SUCCESS:
      return {
        ...state,
        loadingMe: false,
        me: action.payload,
      };

    case GET_ME_ERROR:
    case UPDATE_ME_ERROR:
      return { ...state, loadingMe: false, error: action.payload };

    default:
      return { ...state };
  }
};
