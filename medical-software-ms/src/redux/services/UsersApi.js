/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/prefer-default-export */
import http from '../../util/http';
import { ApiPathConstants } from '../../constants';

export const UsersApi = {
  getAll: () => {
    return http.get(`${ApiPathConstants.getApiPath()}users`);
  },
  getById: (id) => {
    return http.get(`${ApiPathConstants.getApiPath()}users/${id}`);
  },
  create: (data) => http.post(`${ApiPathConstants.getApiPath()}users`, data),
  getMe: () => {
    return http.get(`${ApiPathConstants.getApiPath()}users/me`);
  },
  updateMe: (data) => {
    return http.put(`${ApiPathConstants.getApiPath()}users/me`, data);
  },
};
