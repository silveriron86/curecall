/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/prefer-default-export */
import http from '../../util/http';
import { ApiPathConstants } from '../../constants';

export const PatientApi = {
  getAll: (projectId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients`
    );
  },
  getById: (projectId, patientId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}`
    );
  },
  getPatientMatrixes: (projectId, patientId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/matrices`
    );
  },
  getPatientSMS: (projectId, patientId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/sms`
    );
  },
  postPatientSMS: (projectId, patientId, data) => {
    return http.post(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/sms/one-way`,
      data
    );
  },
  getMatrixVariables: (projectId, patientId, patientMatrixId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/matrices/${patientMatrixId}/variables`
    );
  },
  create: (projectId, data) => {
    return http.post(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients`,
      data
    );
  },
  update: (projectId, patientId, data) => {
    return http.put(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}`,
      data
    );
  },
  getPatientCustomVariables: (projectId, patientId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/custom-variables`
    );
  },
  updatePatientCustomVariables: (projectId, patientId, data) => {
    return http.put(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/custom-variables`,
      data
    );
  },
  getPatientMedAlerts: (projectId, patientId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/patient-med-alerts`
    );
  },
  getPatientMedEvents: (projectId, patientId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/med-events`
    );
  },
  postPatientMedEvents: (projectId, patientId, data) => {
    // console.log('*** postPatientMedEvents', data);
    return http.post(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/med-events`,
      data
    );
  },
  updatePatientMedEvents: (projectId, patientId, data) => {
    return http.put(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/med-events`,
      data
    );
  },
  deletePatientMedEvent: (projectId, patientId, id) => {
    return http.delete(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/med-events/${id}`
    );
  },
  updatePatientMedEvent: (projectId, patientId, id, data) => {
    return http.put(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/med-events/${id}`,
      data
    );
  },

  getSMS: (projectId, patientId, offset = -1, limit = -1) => {
    let params = '';
    if (offset >= 0) {
      params += `?offset=${offset}`;
    }
    if (limit >= 0) {
      params += `${params ? '&' : '?'}limit=${limit}`;
    }
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/sms${params}`
    );
  },
  postSMS: (projectId, patientId, data) => {
    return http.post(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/sms`,
      data
    );
  },
  getPatientPathologies: (projectId, patientId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/pathologies`
    );
  },
  postPatientPathology: (projectId, patientId, pathologyId) => {
    return http.post(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/pathologies`,
      {
        patientId,
        pathologyId,
      }
    );
  },
  updatePatientPathology: (projectId, patientId, data) => {
    return http.put(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/pathologies`,
      data
    );
  },
  deletePatientPathology: (projectId, patientId, pathologyId) => {
    return http.delete(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/pathologies/${pathologyId}`
    );
  },
};
