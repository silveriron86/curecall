/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/prefer-default-export */
import http from '../../util/http';
import { ApiPathConstants } from '../../constants';

export const AdminApi = {
  getPathologies: () => {
    return http.get(`${ApiPathConstants.getApiPath()}pathologies`);
  },
  getMedEvents: () => {
    return http.get(`${ApiPathConstants.getApiPath()}med-events`);
  },
  getCustomVariables: () => {
    return http.get(`${ApiPathConstants.getApiPath()}custom-variables`);
  },
};
