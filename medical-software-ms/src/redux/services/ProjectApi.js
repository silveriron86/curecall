/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/prefer-default-export */
import http from '../../util/http';
import { ApiPathConstants } from '../../constants';

export const ProjectApi = {
  getAll: () => {
    return http.get(`${ApiPathConstants.getApiPath()}projects`);
  },
  getById: (id) => {
    return http.get(`${ApiPathConstants.getApiPath()}projects/${id}`);
  },
  create: (data) => http.post(`${ApiPathConstants.getApiPath()}projects`, data),
  delete: (id) => http.delete(`${ApiPathConstants.getApiPath()}projects/${id}`),
  update: (id, data) =>
    http.patch(`${ApiPathConstants.getApiPath()}projects/${id}`, data),
  getProjectCustomVariables: (id) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${id}/custom-variables`
    );
  },
  getProjectMedAlerts: (projectId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/med-alerts`
    );
  },
  getProjectMedEvents: (projectId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/med-events`
    );
  },
  getProjectUsers: (projectId) => {
    return http
      .get(`${ApiPathConstants.getApiPath()}projects/${projectId}`)
      .then((response) => response.data.Users);
  },
  getProjectReviews: (projectId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/reviews`
    );
  },
};
