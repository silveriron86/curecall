/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/prefer-default-export */
import http from '../../util/http';
import { ApiPathConstants } from '../../constants';

export const AccountApi = {
  login: (data) =>
    http.post(`${ApiPathConstants.getApiPath()}auth/standard`, data),
  getPin: (data) =>
    http.post(`${ApiPathConstants.getApiPath()}auth/forgot/sms`, data),
  loginWithPin: (data) =>
    http.post(`${ApiPathConstants.getApiPath()}auth/forgot`, data),
};
