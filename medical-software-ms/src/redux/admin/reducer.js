/* eslint-disable no-case-declarations */
import {
  GET_ALL_PATHOLOGIES,
  GET_ALL_PATHOLOGIES_SUCCESS,
  GET_ALL_PATHOLOGIES_ERROR,
  GET_ALL_MED_EVENTS,
  GET_ALL_MED_EVENTS_SUCCESS,
  GET_ALL_MED_EVENTS_ERROR,
} from '../constants';

const INIT_STATE = {
  loadingAllMedEvents: false,
  allMedEvents: [],
  loadingAllPathologies: false,
  allPathologies: [],
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_ALL_PATHOLOGIES:
      return { ...state, loadingAllPathologies: true };

    case GET_ALL_PATHOLOGIES_SUCCESS:
      return {
        ...state,
        loadingAllPathologies: false,
        allPathologies: action.payload.allPathologies,
      };

    case GET_ALL_PATHOLOGIES_ERROR:
      return { ...state, loadingAllPathologies: false, error: action.payload };

    case GET_ALL_MED_EVENTS:
      return { ...state, loadingAllMedEvents: true };

    case GET_ALL_MED_EVENTS_SUCCESS:
      return {
        ...state,
        loadingAllMedEvents: false,
        allMedEvents: action.payload.allMedEvents,
      };

    case GET_ALL_MED_EVENTS_ERROR:
      return { ...state, loadingAllMedEvents: false, error: action.payload };

    default:
      return { ...state };
  }
};
