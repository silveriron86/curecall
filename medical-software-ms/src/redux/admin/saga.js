import { all, call, fork, put, takeEvery } from 'redux-saga/effects';

import { GET_ALL_PATHOLOGIES, GET_ALL_MED_EVENTS } from '../constants';

import {
  getAllPathologiesSuccess,
  getAllPathologiesError,
  getAllMedEventsSuccess,
  getAllMedEventsError,
} from './actions';
import { AdminApi } from '../services';

function* getAllPathologies() {
  try {
    const response = yield call(() => AdminApi.getPathologies());
    yield put(getAllPathologiesSuccess(response.data));
  } catch (error) {
    yield put(getAllPathologiesError(error));
  }
}

export function* watchGetAllPathologies() {
  yield takeEvery(GET_ALL_PATHOLOGIES, getAllPathologies);
}

function* getAllMedEvents() {
  try {
    const response = yield call(() => AdminApi.getMedEvents());
    yield put(getAllMedEventsSuccess(response.data));
  } catch (error) {
    yield put(getAllMedEventsError(error));
  }
}

export function* watchGetAllMedEvents() {
  yield takeEvery(GET_ALL_MED_EVENTS, getAllMedEvents);
}

export default function* rootSaga() {
  yield all([fork(watchGetAllMedEvents), fork(watchGetAllPathologies)]);
}
