// eslint-disable-next-line import/no-cycle
import {
  GET_ALL_PATHOLOGIES,
  GET_ALL_PATHOLOGIES_SUCCESS,
  GET_ALL_PATHOLOGIES_ERROR,
  GET_ALL_MED_EVENTS,
  GET_ALL_MED_EVENTS_SUCCESS,
  GET_ALL_MED_EVENTS_ERROR,
} from '../constants';

export const getAllPathologies = () => ({
  type: GET_ALL_PATHOLOGIES,
});

export const getAllPathologiesSuccess = (allPathologies) => {
  return {
    type: GET_ALL_PATHOLOGIES_SUCCESS,
    payload: { allPathologies },
  };
};

export const getAllPathologiesError = (error) => ({
  type: GET_ALL_PATHOLOGIES_ERROR,
  payload: error,
});

export const getAllMedEvents = () => ({
  type: GET_ALL_MED_EVENTS,
});

export const getAllMedEventsSuccess = (allMedEvents) => {
  return {
    type: GET_ALL_MED_EVENTS_SUCCESS,
    payload: { allMedEvents },
  };
};

export const getAllMedEventsError = (error) => ({
  type: GET_ALL_MED_EVENTS_ERROR,
  payload: error,
});
