/* eslint-disable no-shadow */
/* eslint-disable no-case-declarations */
import { cloneDeep } from 'lodash';
import {
  GET_PROJECTS,
  GET_PROJECTS_SUCCESS,
  GET_PROJECTS_ERROR,
  GET_PATIENTS,
  GET_PATIENTS_SUCCESS,
  GET_PATIENTS_ERROR,
  CREATE_PRO_PATIENT,
  GET_PROJECT_CUSTOM_VARIABLES,
  GET_PROJECT_CUSTOM_VARIABLES_ERROR,
  GET_PROJECT_CUSTOM_VARIABLES_SUCCESS,
  GET_PATIENT_CUSTOM_VARIABLES,
  GET_PATIENT_CUSTOM_VARIABLES_ERROR,
  GET_PATIENT_CUSTOM_VARIABLES_SUCCESS,
  UPDATE_PATIENT_CUSTOM_VARIABLES,
  GET_PATIENT_SUCCESS,
  UPDATE_PATIENT,
  GET_PATIENT,
  GET_PATIENT_ERROR,
  GET_PATIENT_MED_EVENTS,
  // GET_PATIENT_MED_EVENTS_ERROR,
  GET_PATIENT_MED_EVENTS_SUCCESS,
  GET_PATIENT_SMS,
  GET_PATIENT_SMS_SUCCESS,
  GET_PATIENT_SMS_ERROR,
  POST_PATIENT_SMS,
  POST_PATIENT_SMS_ERROR,
  GET_PROJECT_MED_ALERTS_SUCCESS,
  GET_PROJECT_MED_EVENTS_SUCCESS,
  GET_PATIENT_PATHOLOGIES_SUCCESS,
  GET_PATIENT_MED_ALERTS_SUCCESS,
} from '../constants';

const INIT_STATE = {
  projects: [],
  projectMedAlerts: [],
  projectMedEvents: [],
  error: '',
  loadingProjects: false,
  patients: [],
  newPatientId: -1,
  loadingPatients: false,
  loadingPatientCustomVariables: false,
  patientCustomVariables: [],
  loadingProjectCustomVariables: false,
  projectCustomVariables: [],
  loadingPatient: false,
  patient: null,
  medEvents: [],
  patientsDetails: [],
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_PROJECTS:
      return { ...state, loadingProjects: true };

    case GET_PROJECTS_SUCCESS:
      return {
        ...state,
        loadingProjects: false,
        projects: action.payload.projects,
      };

    case GET_PROJECTS_ERROR:
      return { ...state, loadingProjects: false, error: action.payload };

    case GET_PROJECT_MED_ALERTS_SUCCESS:
      return {
        ...state,
        projectMedAlerts: action.payload.projectMedAlerts,
      };

    case GET_PROJECT_MED_EVENTS_SUCCESS:
      return {
        ...state,
        projectMedEvents: action.payload.projectMedEvents,
      };

    case GET_PATIENTS:
    case CREATE_PRO_PATIENT:
      return { ...state, newPatientId: -1, loadingPatients: true };

    case GET_PATIENTS_SUCCESS:
      return {
        ...state,
        loadingPatients: false,
        patients: action.payload.patients,
        newPatientId: action.payload.newPatientId,
      };

    case GET_PATIENTS_ERROR:
      return { ...state, loadingPatients: false, error: action.payload };

    case GET_PATIENT:
    case UPDATE_PATIENT:
      return { ...state, loadingPatient: true };

    case GET_PATIENT_SUCCESS:
      return {
        ...state,
        loadingPatient: false,
        patient: action.payload.patient,
      };
    case GET_PATIENT_ERROR:
      return { ...state, loadingPatient: false, error: action.payload };

    case GET_PROJECT_CUSTOM_VARIABLES:
      return { ...state, loadingProjectCustomVariables: true };

    case GET_PROJECT_CUSTOM_VARIABLES_SUCCESS:
      return {
        ...state,
        loadingProjectCustomVariables: false,
        projectCustomVariables: action.payload,
      };

    case GET_PROJECT_CUSTOM_VARIABLES_ERROR:
      return {
        ...state,
        loadingProjectCustomVariables: false,
        error: action.payload,
      };

    case GET_PATIENT_CUSTOM_VARIABLES:
    case UPDATE_PATIENT_CUSTOM_VARIABLES:
      return {
        ...state,
        loadingPatientCustomVariables: true,
        newPatientId: -1,
      };

    case GET_PATIENT_CUSTOM_VARIABLES_ERROR:
      return {
        ...state,
        loadingPatientCustomVariables: false,
        error: action.payload,
      };

    case GET_PATIENT_MED_EVENTS:
      return {
        ...state,
        medEvents: [],
      };

    case GET_PATIENT_MED_EVENTS_SUCCESS:
      const patientsDetails = cloneDeep(state.patientsDetails);
      const { patientId, medEvents } = action.payload;

      patientsDetails[patientId] = {
        ...patientsDetails[patientId],
        medEvents,
      };
      return {
        ...state,
        patientsDetails,
      };

    // case GET_PATIENT_MED_EVENTS_ERROR:
    case GET_PATIENT_SMS_ERROR:
    case POST_PATIENT_SMS_ERROR:
      return {
        ...state,
        error: action.payload,
      };

    case GET_PATIENT_SMS:
    case POST_PATIENT_SMS:
      return {
        ...state,
      };

    case GET_PATIENT_CUSTOM_VARIABLES_SUCCESS: {
      const patientsDetails = cloneDeep(state.patientsDetails);
      const { patientId, customVariables } = action.payload;
      patientsDetails[patientId] = {
        ...patientsDetails[patientId],
        customVariables,
      };
      return {
        ...state,
        patientsDetails,
      };
    }

    case GET_PATIENT_SMS_SUCCESS: {
      const patientsDetails = cloneDeep(state.patientsDetails);
      const { patientId, sms } = action.payload;
      patientsDetails[patientId] = {
        ...patientsDetails[patientId],
        sms,
      };
      return {
        ...state,
        patientsDetails,
      };
    }

    case GET_PATIENT_PATHOLOGIES_SUCCESS: {
      const patientsDetails = cloneDeep(state.patientsDetails);
      const { patientId, pathologies } = action.payload;
      patientsDetails[patientId] = {
        ...patientsDetails[patientId],
        pathologies,
      };
      return {
        ...state,
        patientsDetails,
      };
    }

    case GET_PATIENT_MED_ALERTS_SUCCESS: {
      const patientsDetails = cloneDeep(state.patientsDetails);
      const { patientId, medAlerts } = action.payload;
      patientsDetails[patientId] = {
        ...patientsDetails[patientId],
        medAlerts,
      };
      return {
        ...state,
        patientsDetails,
      };
    }

    default:
      return { ...state };
  }
};
