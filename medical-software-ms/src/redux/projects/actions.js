// eslint-disable-next-line import/no-cycle
import {
  GET_PROJECTS,
  GET_PROJECTS_SUCCESS,
  GET_PROJECTS_ERROR,
  GET_PATIENTS,
  GET_PATIENTS_SUCCESS,
  GET_PATIENTS_ERROR,
  CREATE_PRO_PATIENT,
  CREATE_PATIENT_ERROR,
  GET_PROJECT_CUSTOM_VARIABLES,
  GET_PROJECT_CUSTOM_VARIABLES_SUCCESS,
  GET_PROJECT_CUSTOM_VARIABLES_ERROR,
  GET_PATIENT_CUSTOM_VARIABLES,
  GET_PATIENT_CUSTOM_VARIABLES_SUCCESS,
  GET_PATIENT_CUSTOM_VARIABLES_ERROR,
  UPDATE_PATIENT_CUSTOM_VARIABLES,
  UPDATE_PATIENT_CUSTOM_VARIABLES_SUCCESS,
  UPDATE_PATIENT_CUSTOM_VARIABLES_ERROR,
  UPDATE_PATIENT,
  UPDATE_PATIENT_ERROR,
  GET_PATIENT,
  GET_PATIENT_SUCCESS,
  GET_PATIENT_ERROR,
  GET_PATIENT_MED_EVENTS,
  GET_PATIENT_MED_EVENTS_SUCCESS,
  GET_PATIENT_MED_EVENTS_ERROR,
  GET_PATIENT_SMS,
  GET_PATIENT_SMS_SUCCESS,
  GET_PATIENT_SMS_ERROR,
  POST_PATIENT_SMS,
  POST_PATIENT_SMS_SUCCESS,
  POST_PATIENT_SMS_ERROR,
  GET_PROJECT_MED_ALERTS,
  GET_PROJECT_MED_ALERTS_SUCCESS,
  GET_PROJECT_MED_ALERTS_ERROR,
  GET_PROJECT_MED_EVENTS_SUCCESS,
  GET_PROJECT_MED_EVENTS,
  GET_PROJECT_MED_EVENTS_ERROR,
  DELETE_PATIENT_PATHOLOGY,
  DELETE_PATIENT_PATHOLOGY_ERROR,
  DELETE_PATIENT_PATHOLOGY_SUCCESS,
  POST_PATIENT_PATHOLOGIES_ERROR,
  POST_PATIENT_PATHOLOGIES_SUCCESS,
  POST_PATIENT_PATHOLOGIES,
  UPDATE_PATIENT_PATHOLOGIES_ERROR,
  UPDATE_PATIENT_PATHOLOGIES_SUCCESS,
  UPDATE_PATIENT_PATHOLOGIES,
  GET_PATIENT_PATHOLOGIES,
  GET_PATIENT_PATHOLOGIES_SUCCESS,
  GET_PATIENT_PATHOLOGIES_ERROR,
  UPDATE_PATIENT_MED_EVENT_SUCCESS,
  UPDATE_PATIENT_MED_EVENT,
  UPDATE_PATIENT_MED_EVENT_ERROR,
  POST_PATIENT_MED_EVENTS,
  POST_PATIENT_MED_EVENTS_SUCCESS,
  POST_PATIENT_MED_EVENTS_ERROR,
  GET_PATIENT_MED_ALERTS,
  GET_PATIENT_MED_ALERTS_SUCCESS,
  GET_PATIENT_MED_ALERTS_ERROR,
  DELETE_PATIENT_MED_EVENT,
  DELETE_PATIENT_MED_EVENT_ERROR,
  GET_REVIEWS,
  GET_REVIEWS_SUCCESS,
  GET_REVIEWS_ERROR,
} from '../constants';

export const getProjects = () => ({
  type: GET_PROJECTS,
});

export const getProjectsSuccess = (projects) => {
  return {
    type: GET_PROJECTS_SUCCESS,
    payload: { projects },
  };
};

export const getProjectsError = (error) => ({
  type: GET_PROJECTS_ERROR,
  payload: error,
});

export const getReviews = (projectId) => ({
  type: GET_REVIEWS,
  payload: projectId,
});

export const getReviewsSuccess = (reviews) => {
  return {
    type: GET_REVIEWS_SUCCESS,
    payload: { reviews },
  };
};

export const getReviewsError = (error) => ({
  type: GET_REVIEWS_ERROR,
  payload: error,
});

export const getProjectMedAlerts = (projectId) => ({
  type: GET_PROJECT_MED_ALERTS,
  payload: projectId,
});
export const getProjectMedAlertsSuccess = (projectMedAlerts) => {
  return {
    type: GET_PROJECT_MED_ALERTS_SUCCESS,
    payload: { projectMedAlerts },
  };
};
export const getProjectMedAlertsError = (error) => ({
  type: GET_PROJECT_MED_ALERTS_ERROR,
  payload: error,
});

export const getProjectMedEvents = (projectId) => ({
  type: GET_PROJECT_MED_EVENTS,
  payload: projectId,
});
export const getProjectMedEventsSuccess = (projectMedEvents) => {
  return {
    type: GET_PROJECT_MED_EVENTS_SUCCESS,
    payload: { projectMedEvents },
  };
};
export const getProjectMedEventsError = (error) => ({
  type: GET_PROJECT_MED_EVENTS_ERROR,
  payload: error,
});

export const getPatients = (projectId) => ({
  type: GET_PATIENTS,
  payload: { projectId },
});

export const getPatientsSuccess = (patients, newPatientId) => {
  return {
    type: GET_PATIENTS_SUCCESS,
    payload: { patients, newPatientId },
  };
};

export const getPatientsError = (error) => ({
  type: GET_PATIENTS_ERROR,
  payload: error,
});

export const getPatient = (projectId, patientId) => ({
  type: GET_PATIENT,
  payload: { projectId, patientId },
});

export const getPatientSuccess = (patients) => {
  return {
    type: GET_PATIENT_SUCCESS,
    payload: { patients },
  };
};

export const getPatientError = (error) => ({
  type: GET_PATIENT_ERROR,
  payload: error,
});

export const createPatient = ({ projectId, data }) => ({
  type: CREATE_PRO_PATIENT,
  payload: { projectId, data },
});

export const createPatientError = (error) => ({
  type: CREATE_PATIENT_ERROR,
  payload: error,
});

export const updatePatient = (projectId, patientId, data) => ({
  type: UPDATE_PATIENT,
  payload: { projectId, patientId, data },
});

export const updatePatientError = (error) => ({
  type: UPDATE_PATIENT_ERROR,
  payload: error,
});

export const getPatientMedAlerts = (projectId, patientId) => ({
  type: GET_PATIENT_MED_ALERTS,
  payload: { projectId, patientId },
});

export const getPatientMedAlertsSuccess = (data) => {
  return {
    type: GET_PATIENT_MED_ALERTS_SUCCESS,
    payload: data,
  };
};

export const getPatientMedAlertsError = (error) => ({
  type: GET_PATIENT_MED_ALERTS_ERROR,
  payload: error,
});

export const getPatientMedEvents = (projectId, patientId) => ({
  type: GET_PATIENT_MED_EVENTS,
  payload: { projectId, patientId },
});

export const getPatientMedEventsSuccess = (data) => {
  return {
    type: GET_PATIENT_MED_EVENTS_SUCCESS,
    payload: data,
  };
};

export const getPatientMedEventsError = (error) => ({
  type: GET_PATIENT_MED_EVENTS_ERROR,
  payload: error,
});

export const postPatientMedEvents = (projectId, patientId, data) => ({
  type: POST_PATIENT_MED_EVENTS,
  payload: { projectId, patientId, data },
});

export const postPatientMedEventsSuccess = (patients) => {
  return {
    type: POST_PATIENT_MED_EVENTS_SUCCESS,
    payload: { patients },
  };
};

export const postPatientMedEventsError = (error) => ({
  type: POST_PATIENT_MED_EVENTS_ERROR,
  payload: error,
});

export const updatePatientMedEvent = (projectId, patientId, id, data) => ({
  type: UPDATE_PATIENT_MED_EVENT,
  payload: { projectId, patientId, id, data },
});

export const updatePatientMedEventSuccess = (patients) => {
  return {
    type: UPDATE_PATIENT_MED_EVENT_SUCCESS,
    payload: { patients },
  };
};

export const updatePatientMedEventError = (error) => ({
  type: UPDATE_PATIENT_MED_EVENT_ERROR,
  payload: error,
});

export const deletePatientMedEvent = ({ projectId, patientId, id }) => ({
  type: DELETE_PATIENT_MED_EVENT,
  payload: { projectId, patientId, id },
});

export const deletePatientMedEventError = (error) => ({
  type: DELETE_PATIENT_MED_EVENT_ERROR,
  payload: error,
});

export const getProjectCustomVariables = (data) => ({
  type: GET_PROJECT_CUSTOM_VARIABLES,
  payload: data,
});

export const getProjectCustomVariablesSuccess = (data) => {
  return {
    type: GET_PROJECT_CUSTOM_VARIABLES_SUCCESS,
    payload: data,
  };
};

export const getProjectCustomVariablesError = (error) => ({
  type: GET_PROJECT_CUSTOM_VARIABLES_ERROR,
  payload: error,
});

export const getPatientCustomVariables = (projectId, patientId) => ({
  type: GET_PATIENT_CUSTOM_VARIABLES,
  payload: { projectId, patientId },
});

export const getPatientCustomVariablesSuccess = (data) => {
  return {
    type: GET_PATIENT_CUSTOM_VARIABLES_SUCCESS,
    payload: data,
  };
};

export const getPatientCustomVariablesError = (error) => ({
  type: GET_PATIENT_CUSTOM_VARIABLES_ERROR,
  payload: error,
});

export const updatePatientCustomVariables = (
  projectId,
  patientId,
  variables
) => ({
  type: UPDATE_PATIENT_CUSTOM_VARIABLES,
  payload: { projectId, patientId, variables },
});

export const updatePatientCustomVariablesSuccess = (data) => ({
  type: UPDATE_PATIENT_CUSTOM_VARIABLES_SUCCESS,
  payload: data,
});

export const updatePatientCustomVariablesError = (message) => ({
  type: UPDATE_PATIENT_CUSTOM_VARIABLES_ERROR,
  payload: { message },
});

export const getPatientSMS = (projectId, patientId) => ({
  type: GET_PATIENT_SMS,
  payload: { projectId, patientId },
});
export const getPatientSMSSuccess = (data) => ({
  type: GET_PATIENT_SMS_SUCCESS,
  payload: data,
});
export const getPatientSMSError = (message) => ({
  type: GET_PATIENT_SMS_ERROR,
  payload: { message },
});

export const postPatientSMS = (projectId, patientId, data) => ({
  type: POST_PATIENT_SMS,
  payload: { projectId, patientId, data },
});
export const postPatientSMSSuccess = (data) => ({
  type: POST_PATIENT_SMS_SUCCESS,
  payload: data,
});
export const postPatientSMSError = (message) => ({
  type: POST_PATIENT_SMS_ERROR,
  payload: { message },
});

export const getPatientPathology = (projectId, patientId) => ({
  type: GET_PATIENT_PATHOLOGIES,
  payload: { projectId, patientId },
});
export const getPatientPathologySuccess = (data) => ({
  type: GET_PATIENT_PATHOLOGIES_SUCCESS,
  payload: data,
});
export const getPatientPathologyError = (message) => ({
  type: GET_PATIENT_PATHOLOGIES_ERROR,
  payload: { message },
});

export const deletePatientPathology = (projectId, patientId, pathologyId) => ({
  type: DELETE_PATIENT_PATHOLOGY,
  payload: { projectId, patientId, pathologyId },
});
export const deletePatientPathologySuccess = (data) => ({
  type: DELETE_PATIENT_PATHOLOGY_SUCCESS,
  payload: data,
});
export const deletePatientPathologyError = (message) => ({
  type: DELETE_PATIENT_PATHOLOGY_ERROR,
  payload: { message },
});

export const postPatientPathology = (projectId, patientId, pathologyId) => ({
  type: POST_PATIENT_PATHOLOGIES,
  payload: { projectId, patientId, pathologyId },
});
export const postPatientPathologySuccess = (data) => ({
  type: POST_PATIENT_PATHOLOGIES_SUCCESS,
  payload: data,
});
export const postPatientPathologyError = (message) => ({
  type: POST_PATIENT_PATHOLOGIES_ERROR,
  payload: { message },
});

export const updatePatientPathology = (projectId, patientId, data) => ({
  type: UPDATE_PATIENT_PATHOLOGIES,
  payload: { projectId, patientId, data },
});
export const updatePatientPathologySuccess = (data) => ({
  type: UPDATE_PATIENT_PATHOLOGIES_SUCCESS,
  payload: data,
});
export const updatePatientPathologyError = (message) => ({
  type: UPDATE_PATIENT_PATHOLOGIES_ERROR,
  payload: { message },
});
