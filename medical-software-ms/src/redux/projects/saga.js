import { all, call, fork, put, takeEvery } from 'redux-saga/effects';

import {
  GET_PROJECTS,
  GET_REVIEWS,
  GET_PATIENTS,
  CREATE_PRO_PATIENT,
  UPDATE_PATIENT,
  GET_PROJECT_CUSTOM_VARIABLES,
  GET_PATIENT_CUSTOM_VARIABLES,
  UPDATE_PATIENT_CUSTOM_VARIABLES,
  GET_PATIENT,
  GET_PATIENT_MED_EVENTS,
  GET_PATIENT_SMS,
  POST_PATIENT_SMS,
  GET_PROJECT_MED_ALERTS,
  GET_PROJECT_MED_EVENTS,
  POST_PATIENT_PATHOLOGIES,
  POST_PATIENT_MED_EVENTS,
  GET_PATIENT_PATHOLOGIES,
  GET_PATIENT_MED_ALERTS,
  DELETE_PATIENT_MED_EVENT,
  DELETE_PATIENT_PATHOLOGY,
  UPDATE_PATIENT_MED_EVENT,
} from '../constants';

import {
  getProjectsSuccess,
  getProjectsError,
  getReviewsSuccess,
  getReviewsError,
  getPatientsSuccess,
  getPatientsError,
  getProjectCustomVariablesSuccess,
  getProjectCustomVariablesError,
  getPatientCustomVariablesSuccess,
  getPatientCustomVariablesError,
  getPatientSuccess,
  getPatientError,
  getPatientMedEventsSuccess,
  getPatientMedEventsError,
  getPatientSMSSuccess,
  getPatientSMSError,
  postPatientSMSError,
  getProjectMedAlertsSuccess,
  getProjectMedAlertsError,
  getProjectMedEventsSuccess,
  getProjectMedEventsError,
  getPatientPathologySuccess,
  getPatientPathologyError,
  postPatientPathologyError,
  getPatientMedAlertsSuccess,
  getPatientMedAlertsError,
} from './actions';
import { ProjectApi, PatientApi } from '../services';

function* getProjects() {
  try {
    const response = yield call(() => ProjectApi.getAll());
    yield put(getProjectsSuccess(response.data));
  } catch (error) {
    yield put(getProjectsError(error));
  }
}

export function* watchGetProjects() {
  yield takeEvery(GET_PROJECTS, getProjects);
}

function* getReviews({ payload }) {
  try {
    const response = yield call(() => ProjectApi.getProjectReviews(payload));
    yield put(getReviewsSuccess(response.data));
  } catch (error) {
    yield put(getReviewsError(error));
  }
}

export function* watchGetReviews() {
  yield takeEvery(GET_REVIEWS, getReviews);
}

function* getProjectMedAlerts({ payload }) {
  try {
    const response = yield call(() => ProjectApi.getProjectMedAlerts(payload));
    yield put(getProjectMedAlertsSuccess(response.data));
  } catch (error) {
    yield put(getProjectMedAlertsError(error));
  }
}

export function* watchGetProjectMedAlerts() {
  yield takeEvery(GET_PROJECT_MED_ALERTS, getProjectMedAlerts);
}

function* getProjectMedEvents({ payload }) {
  try {
    const response = yield call(() => ProjectApi.getProjectMedEvents(payload));
    yield put(getProjectMedEventsSuccess(response.data));
  } catch (error) {
    yield put(getProjectMedEventsError(error));
  }
}

export function* watchGetProjectMedEvents() {
  yield takeEvery(GET_PROJECT_MED_EVENTS, getProjectMedEvents);
}

function* getPatients({ payload }) {
  try {
    const { projectId } = payload;
    const response = yield call(() => PatientApi.getAll(projectId));
    yield put(getPatientsSuccess(response.data, -1));
  } catch (error) {
    yield put(getPatientsError(error));
  }
}

export function* watchGetPatients() {
  yield takeEvery(GET_PATIENTS, getPatients);
}

function* getPatient({ payload }) {
  try {
    const { projectId, patientId } = payload;
    const response = yield call(() => PatientApi.getById(projectId, patientId));
    yield put(getPatientSuccess(response.data));
  } catch (error) {
    yield put(getPatientError(error));
  }
}

export function* watchGetPatient() {
  yield takeEvery(GET_PATIENT, getPatient);
}

function* createPatient({ payload }) {
  try {
    const { projectId, data } = payload;
    yield call(() => PatientApi.create(projectId, data));
    const response = yield call(() => PatientApi.getAll(projectId));
    yield put(
      getPatientsSuccess(
        response.data,
        response.data[response.data.length - 1].id
      )
    );
  } catch (error) {
    yield put(getPatientsError(error));
  }
}

export function* watchCreatePatient() {
  yield takeEvery(CREATE_PRO_PATIENT, createPatient);
}

function* updatePatient({ payload }) {
  try {
    const { projectId, patientId, data } = payload;
    console.log('data', data);
    yield call(() => PatientApi.update(projectId, patientId, data));
    // const response = yield call(() => PatientApi.getById(projectId, patientId));
    // yield put(getPatientSuccess(response.data));
    const response = yield call(() => PatientApi.getAll(projectId));
    yield put(
      getPatientsSuccess(
        response.data,
        response.data[response.data.length - 1].id
      )
    );
  } catch (error) {
    yield put(getPatientError(error));
  }
}

export function* watchUpdatePatient() {
  yield takeEvery(UPDATE_PATIENT, updatePatient);
}

function* getProjectCustomVariables({ payload }) {
  try {
    const response = yield call(() =>
      ProjectApi.getProjectCustomVariables(payload)
    );
    yield put(getProjectCustomVariablesSuccess(response.data));
  } catch (error) {
    yield put(getProjectCustomVariablesError(error));
  }
}

export function* watchGetProjectCustomVariables() {
  yield takeEvery(GET_PROJECT_CUSTOM_VARIABLES, getProjectCustomVariables);
}

function* getPatientCustomVariables({ payload }) {
  try {
    const { projectId, patientId } = payload;
    const response = yield call(() =>
      PatientApi.getPatientCustomVariables(projectId, patientId)
    );
    yield put(
      getPatientCustomVariablesSuccess({
        patientId,
        customVariables: response.data,
      })
    );
  } catch (error) {
    yield put(getPatientCustomVariablesError(error));
  }
}

export function* watchGetPatientCustomVariables() {
  yield takeEvery(GET_PATIENT_CUSTOM_VARIABLES, getPatientCustomVariables);
}

function* updatePatientCustomVariables({ payload }) {
  try {
    const { projectId, patientId, variables } = payload;
    yield call(() =>
      PatientApi.updatePatientCustomVariables(projectId, patientId, variables)
    );
    const response = yield call(() =>
      PatientApi.getPatientCustomVariables(projectId, patientId)
    );
    yield put(
      getPatientCustomVariablesSuccess({
        patientId,
        customVariables: response.data,
      })
    );
  } catch (error) {
    yield put(getPatientCustomVariablesError(error));
  }
}

export function* watchUpdatePatientCustomVariables() {
  yield takeEvery(
    UPDATE_PATIENT_CUSTOM_VARIABLES,
    updatePatientCustomVariables
  );
}

function* getPatientMedAlerts({ payload }) {
  try {
    const { projectId, patientId } = payload;
    const response = yield call(() =>
      PatientApi.getPatientMedAlerts(projectId, patientId)
    );
    yield put(
      getPatientMedAlertsSuccess({
        patientId,
        medAlerts: response.data,
      })
    );
  } catch (error) {
    yield put(getPatientMedAlertsError(error));
  }
}

export function* watchGetPatientMedAlerts() {
  yield takeEvery(GET_PATIENT_MED_ALERTS, getPatientMedAlerts);
}

function* getPatientMedEvents({ payload }) {
  try {
    const { projectId, patientId } = payload;
    const response = yield call(() =>
      PatientApi.getPatientMedEvents(projectId, patientId)
    );
    yield put(
      getPatientMedEventsSuccess({
        patientId,
        medEvents: response.data,
      })
    );
  } catch (error) {
    yield put(getPatientMedEventsError(error));
  }
}

export function* watchGetPatientMedEvents() {
  yield takeEvery(GET_PATIENT_MED_EVENTS, getPatientMedEvents);
}

function* postPatientMedEvents({ payload }) {
  try {
    // console.log('function* postPatientMedEvents');
    const { projectId, patientId, data } = payload;
    yield call(() =>
      PatientApi.postPatientMedEvents(projectId, patientId, data)
    );
    const response = yield call(() =>
      PatientApi.getPatientMedEvents(projectId, patientId)
    );
    yield put(
      getPatientMedEventsSuccess({
        patientId,
        medEvents: response.data,
      })
    );
  } catch (error) {
    yield put(getPatientMedEventsError(error));
  }
}

export function* watchPostPatientMedEvents() {
  yield takeEvery(POST_PATIENT_MED_EVENTS, postPatientMedEvents);
}

function* deletePatientMedEvent({ payload }) {
  try {
    const { projectId, patientId, id } = payload;
    yield call(() =>
      PatientApi.deletePatientMedEvent(projectId, patientId, id)
    );
    const response = yield call(() =>
      PatientApi.getPatientMedEvents(projectId, patientId)
    );
    yield put(
      getPatientMedEventsSuccess({
        patientId,
        medEvents: response.data,
      })
    );
  } catch (error) {
    yield put(getPatientMedEventsError(error));
  }
}

export function* watchDeletePatientMedEvent() {
  yield takeEvery(DELETE_PATIENT_MED_EVENT, deletePatientMedEvent);
}

function* updatePatientMedEvent({ payload }) {
  try {
    const { projectId, patientId, id, data } = payload;
    yield call(() =>
      PatientApi.updatePatientMedEvent(projectId, patientId, id, data)
    );
    const response = yield call(() =>
      PatientApi.getPatientMedEvents(projectId, patientId)
    );
    yield put(
      getPatientMedEventsSuccess({
        patientId,
        medEvents: response.data,
      })
    );
  } catch (error) {
    yield put(getPatientMedEventsError(error));
  }
}

export function* watchUpdatePatientMedEvent() {
  yield takeEvery(UPDATE_PATIENT_MED_EVENT, updatePatientMedEvent);
}

function* getPatientSMS({ payload }) {
  try {
    const { projectId, patientId } = payload;
    const response = yield call(() =>
      PatientApi.getPatientSMS(projectId, patientId)
    );
    yield put(
      getPatientSMSSuccess({
        patientId,
        sms: response.data,
      })
    );
  } catch (error) {
    yield put(getPatientSMSError(error));
  }
}

export function* watchGetPatientSMS() {
  yield takeEvery(GET_PATIENT_SMS, getPatientSMS);
}

function* postPatientSMS({ payload }) {
  try {
    const { projectId, patientId, data } = payload;
    yield call(() => PatientApi.postSMS(projectId, patientId, data));
    const response = yield call(() =>
      PatientApi.getPatientSMS(projectId, patientId)
    );
    yield put(
      getPatientSMSSuccess({
        patientId,
        sms: response.data,
      })
    );
  } catch (error) {
    yield put(postPatientSMSError(error));
  }
}

export function* watchPostPatientSMS() {
  yield takeEvery(POST_PATIENT_SMS, postPatientSMS);
}

function* postPatientPathology({ payload }) {
  try {
    const { projectId, patientId, pathologyId } = payload;
    yield call(() =>
      PatientApi.postPatientPathology(projectId, patientId, pathologyId)
    );
    const response = yield call(() =>
      PatientApi.getPatientPathologies(projectId, patientId)
    );
    yield put(
      getPatientPathologySuccess({
        patientId,
        pathologies: response.data,
      })
    );
  } catch (error) {
    yield put(postPatientPathologyError(error));
  }
}

export function* watchPostPatientPathology() {
  yield takeEvery(POST_PATIENT_PATHOLOGIES, postPatientPathology);
}

function* getPatientPathology({ payload }) {
  try {
    const { projectId, patientId } = payload;
    const response = yield call(() =>
      PatientApi.getPatientPathologies(projectId, patientId)
    );
    yield put(
      getPatientPathologySuccess({
        patientId,
        pathologies: response.data,
      })
    );
  } catch (error) {
    yield put(getPatientPathologyError(error));
  }
}

export function* watchGetPatientPathology() {
  yield takeEvery(GET_PATIENT_PATHOLOGIES, getPatientPathology);
}

function* deletePatientPathology({ payload }) {
  try {
    const { projectId, patientId, pathologyId } = payload;
    yield call(() =>
      PatientApi.deletePatientPathology(projectId, patientId, pathologyId)
    );
    const response = yield call(() =>
      PatientApi.getPatientPathologies(projectId, patientId)
    );
    yield put(
      getPatientPathologySuccess({
        patientId,
        pathologies: response.data,
      })
    );
  } catch (error) {
    yield put(getPatientMedEventsError(error));
  }
}

export function* watchDeletePatientPathology() {
  yield takeEvery(DELETE_PATIENT_PATHOLOGY, deletePatientPathology);
}

export default function* rootSaga() {
  yield all([
    fork(watchGetProjects),
    fork(watchGetReviews),
    fork(watchGetPatients),
    fork(watchGetPatient),
    fork(watchCreatePatient),
    fork(watchUpdatePatient),
    fork(watchGetProjectCustomVariables),
    fork(watchGetPatientCustomVariables),
    fork(watchUpdatePatientCustomVariables),
    fork(watchGetPatientMedEvents),
    fork(watchGetPatientSMS),
    fork(watchPostPatientSMS),
    fork(watchGetProjectMedAlerts),
    fork(watchGetProjectMedEvents),
    fork(watchPostPatientMedEvents),
    fork(watchUpdatePatientMedEvent),
    fork(watchPostPatientPathology),
    fork(watchDeletePatientPathology),
    fork(watchGetPatientPathology),
    fork(watchGetPatientMedAlerts),
    fork(watchDeletePatientMedEvent),
  ]);
}
