/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-param-reassign */
import axios from 'axios';

const http = axios.create({});

http.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('access_token');
    if (token != null) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  (error) => Promise.reject(error)
);

function isTokenExpiredError(errorResponse) {
  return errorResponse.status === 401;
}

http.interceptors.response.use(
  // If the request succeeds, we don't have to do anything and just return the response
  (response) => {
    // // Scheduled refresh access-token
    // if (!response.data.value && response.data.data.message === 'token invalid') {
    //   // refresh token
    //   store.dispatch('refresh').then(response => {
    //     sessionStorage.setItem('access_token', response.data)
    //   }).catch(error => {
    //     throw new Error('token refresh' + error)
    //   })
    // }
    return response;
  },
  (error) => {
    const errorResponse = error.response;
    if (error.message.indexOf('401') > 0) {
      // unahtoirzed error
      // localStorage.clear();
      // window.location.reload();
      return Promise.reject(error);
    }

    if (error.message === 'Network Error') {
      return Promise.reject({
        ...error,
        ...{
          response: {
            data: {
              error_description: error.message,
            },
          },
        },
      });
    }
    if (isTokenExpiredError(errorResponse)) {
      localStorage.clear();
      // window.location.reload();
      // if you want to refresh the token from the server,see the following guide:
      // https://www.techynovice.com/setting-up-JWT-token-refresh-mechanism-with-axios/
    }
    // If the error is due to other reasons, we just throw it back to axios
    return Promise.reject(error);
  }
);
export default http;
