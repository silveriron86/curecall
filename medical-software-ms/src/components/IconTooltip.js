import React, { useState } from 'react';
import { Tooltip } from 'reactstrap';

const IconTooltip = ({ iconName, children, style }) => {
  const [tooltipOpen, setTooltipOpen] = useState(false);

  const toggleTooltip = () => {
    setTooltipOpen(!tooltipOpen);
  };

  return (
    <div id="helper-local-selector" style={style}>
      <i
        style={{
          fontSize: '1.2rem',
          marginTop: '0.5rem',
          display: 'flex',
        }}
        className={iconName}
        id="helper-local-selector-icon"
      />
      <Tooltip
        placement="bottom"
        isOpen={tooltipOpen}
        target="helper-local-selector-icon"
        toggle={toggleTooltip}
      >
        {children}
      </Tooltip>
    </div>
  );
};

export default IconTooltip;
