import React from 'react';
import { Card, CardBody } from 'reactstrap';

import botImg from '../../assets/img/bot.png';

const iconStyle = { fontSize: 26, marginTop: -10, marginRight: 5 };

const MessageCard = ({ item }) => {
  const icons = [
    <div key="sms" className="iconsminds-consulting" style={iconStyle} />,
    <div key="male" className="iconsminds-male-2" style={iconStyle} />,
    <div key="female" className="iconsminds-female-2" style={iconStyle} />,
  ];
  return (
    <>
      <Card
        className={`d-inline-block mb-3 float-${
          item.inbound === true ? 'left' : 'right'
        }`}
      >
        <div className="position-absolute  pt-1 pr-2 r-0">
          <span className="text-extra-small text-muted">{item.time}</span>
        </div>
        <CardBody style={{ minWidth: 120 }}>
          <div className="d-flex align-items-start">
            {item.inbound === true ? (
              <>
                {item.gender === 'MALE' && icons[1]}
                {item.gender === 'FEMALE' && icons[2]}
              </>
            ) : (
              <>
                {item.source === 'COPILOT' && (
                  <img
                    src={botImg}
                    alt=""
                    width="40"
                    height="40"
                    style={{ marginTop: -15, marginRight: 5 }}
                  />
                )}
                {item.source === 'USER' && icons[0]}
              </>
            )}

            <div>
              <div className="d-flex flex-row pb-1">
                {/* <img
              alt={sender.name}
              src={sender.thumb}
              className="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall"
            /> */}
                <div className=" d-flex flex-grow-1 min-width-zero">
                  <div className="pl-0 ml-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                    <div className="min-width-zero">
                      <p className="mb-0 truncate list-item-heading">
                        {item.inbound === false && item.senderId && (
                          <>{item.senderId}</>
                        )}
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div className="chat-text-left">
                <p className="mb-0 text-semi-muted">{item.text}</p>
              </div>
            </div>
          </div>
        </CardBody>
      </Card>
      <div className="clearfix" />
    </>
  );
};

export default React.memo(MessageCard);
