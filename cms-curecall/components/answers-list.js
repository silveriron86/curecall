import { useState, useEffect } from "react";
import { answers } from "../constants/answers";
import { Button } from "antd";
import { Answer } from "../pages/_styles";

function AnswersList(props) {
  const [selectedAnswer, selectAnswer] = useState(-1);
  const { isListenOpened, defaultValue, category, index, data } = props;

  const onSelect = (answerIndex) => {
    selectAnswer(answerIndex);
    props.onSelect(answerIndex);
  };

  useEffect(() => {
    selectAnswer(-1);
    if (defaultValue >= 0) {
      selectAnswer(defaultValue);
    }
  }, [index, defaultValue]);

  // console.log("selected naswer = ", selectedAnswer);

  const answersList = data ? data : answers[category][index];
  return (
    <div className={`answers ${isListenOpened ? "large" : ""}`}>
      {answersList.map((answer, i) => {
        return (
          <Answer
            key={`answer-${i}`}
            className={`${answer.length < 35 ? "one" : ""} ${
              isListenOpened ? "large" : ""
            }`}
          >
            <Button
              onClick={() => onSelect(i)}
              className={selectedAnswer === i ? "selected" : ""}
            >
              {String.fromCharCode(65 + i)}
            </Button>
            <div className="text">{answer}</div>
          </Answer>
        );
      })}
    </div>
  );
}

export default AnswersList;
