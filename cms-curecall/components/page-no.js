import { PageNoWrapper } from "./_styles";
import LineIcon from "../assets/images/Line 2@2x.png";

function PageNo(props) {
  const { value, total } = props;
  return (
    <PageNoWrapper>
      <div className="relative">
        <span className="value">{value}</span>
        <img src={LineIcon} />
        <span className="total">{total}</span>
      </div>
    </PageNoWrapper>
  );
}

export default PageNo;
