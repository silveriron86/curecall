import { HeaderWrapper } from "./_styles";
import securityIcon from "../assets/images/Security@2x.png";

function Header() {
  return (
    <HeaderWrapper>
      <img src={securityIcon} width="50" height="56" />
      <span>Connexion à CureCall</span>
    </HeaderWrapper>
  );
}

export default Header;
