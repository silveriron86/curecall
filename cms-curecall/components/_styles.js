import styled from "@emotion/styled";
import { Button, Input } from "antd";

export const HeaderWrapper = styled("div")({
  display: "flex",
  "&.center": {
    alignItems: "center",
  },
  "&.full-center": {
    width: "100%",
    height: "100vh",
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    span: {
      width: 302,
      textAlign: "center",
    },
  },
  img: {
    marginRight: 17.4,
  },
  span: {
    width: 189,
    fontFamily: "Nexa Heavy",
    fontWeight: 900,
    fontSize: 25,
    lineHeight: "30px",
    textAlign: "left",
    color: "#fff",
    "&.full": {
      width: "fit-content",
    },
  },
  ".block": {
    display: "block",
  },
});

export const ThemeButton = styled(Button)({
  height: 56,
  background: "#00fffd",
  outline: "none",
  border: "none",
  boxShadow: "none",
  padding: "0 32px",
  span: {
    fontFamily: "Nexa Heavy",
    fontWeight: 900,
    fontSize: 25,
    lineHeight: "25px",
    textAlign: "center",
    color: "#0800ff",
  },
  "&.error": {
    backgroundColor: "#FDFF00",
    span: {
      color: "#0800FF",
    },
  },
  "&.ant-btn-round": {
    borderRadius: 28,
  },
  "&:hover, &:focus": {
    background: "#00fffd !important",
  },
});

export const ThemeInput = styled(Input)({
  height: 66,
  borderRadius: 5,
  fontFamily: "Nexa Heavy",
  fontWeight: 900,
  fontSize: 18,
  lineHeight: "25px",
  color: "#0800ff",
  outline: "none",
  border: "none",
  "&::placeholder": {
    color: "#0800FF30",
  },
});

export const ThemeIconButton = styled(Button)({
  padding: 0,
  backgroundColor: "transparent",
  boxShadow: "none",
  outline: "none",
  border: "none",
  display: "flex",
  alignItems: "center",
  height: "fit-content",
  "&::after": {
    display: "none !important",
  },
  span: {
    fontFamily: "Nexa BoldItalic",
    fontWeight: "bold",
    fontStyle: "italic",
    fontSize: 16,
    lineHeight: "22px",
    textAlign: "left",
    color: "#00fffd",
    marginLeft: 6.6,
  },
  "&.laugh": {
    width: 126,
    display: "block",
    margin: "0 auto",
    marginTop: 46,
    backgroundColor: "transparent !important",
    p: {
      marginTop: "9px !important",
      display: "block",
      whiteSpace: "normal",
      lineHeight: "18px !important",
    },
  },
  "&.close-btn": {
    position: "absolute",
    top: 68,
    left: 40,
  },
  "&.listen-btn": {
    position: "absolute",
    top: 70,
    right: 37,
  },
});

export const PageNoWrapper = styled("div")({
  width: 59,
  height: 72,
  position: "absolute",
  top: 39,
  right: 37,
  ".relative": {
    position: "relative",
    img: {
      position: "absolute",
      marginTop: 10.5,
      right: 12.5,
    },
    span: {
      color: "white",
      fontFamily: "Nexa Heavy",
      fontWeight: 900,
      fontSize: 35,
      position: "absolute",
      "&.value": {},
      "&.total": {
        marginTop: 27,
        right: 0,
      },
    },
  },
});
