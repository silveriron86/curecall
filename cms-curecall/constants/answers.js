export const answers = [
  [
    // 8 answers
    [
      "Très bien",
      "C’est flou mais je distingue les lettres",
      "Je vois des taches noires mais sans pouvoir correctement distinguer les lettres",
      "Je ne vois rien de particulier",
    ],
    [
      "Très bien",
      "C’est flou mais je distingue les lettres",
      "Je vois des taches noires mais sans pouvoir correctement distinguer les lettres",
      "Je ne vois rien de particulier",
    ],
    [
      "Très bien",
      "C’est flou mais je distingue les lettres",
      "Je vois des taches noires mais sans pouvoir correctement distinguer les lettres",
      "Je ne vois rien de particulier",
    ],
    [
      "Très bien, je vois  C N H Z O K",
      "C’est flou mais je distingue les lettres",
      "Je vois des taches noires mais sans pouvoir correctement distinguer les lettres",
      "Je ne vois rien de particulier",
    ],
    [
      "Très bien, je vois N O D V H R",
      "C’est flou mais je distingue les lettres",
      "Je vois des taches noires mais sans pouvoir correctement distinguer les lettres",
      "Je ne vois rien de particulier",
    ],
    [
      "Très bien, je vois C D N Z S V",
      "C’est flou mais je distingue les lettres",
      "Je vois des taches noires mais sans pouvoir correctement distinguer les lettres",
      "Je ne vois rien de particulier",
    ],
    [
      "Très bien, je vois K C H O D K",
      "C’est flou mais je distingue les lettres",
      "Je vois des taches noires mais sans pouvoir correctement distinguer les lettres",
      "Je ne vois rien de particulier",
    ],
    [
      "Oui j’arrive à voir R S Z H V R",
      "C’est flou mais je distingue les lettres",
      "Je vois des taches noires mais sans pouvoir correctement distinguer les lettres",
      "Je ne vois rien de particulier",
    ],
  ],
  [
    // A2: 2 answers
    [
      "Normale, je ressens aucune gêne",
      "J’ai besoin de lunettes de soleil",
      "Mes yeux sont un peu douloureux",
      "Cette situation est vraiment inconfortable et douloureuse",
    ],
    [
      "Normale, je ressens aucune gêne",
      "J’ai besoin de plisser les yeux",
      "Mes yeux sont un peu douloureux, je pourrais avoir une migraine",
      "Cette situation m’est vraiment inconfortable et douloureuse",
    ],
  ],
  [
    // A3.1
    [
      "Non, je ne ressens aucune gène.",
      "Oui un petit peu, mais je vois très bien juste après.",
      "Tout à fait, mes yeux mettent beaucoup plus de temps à s’adapter à la pénombre.",
      "Même avec un peu de temps, il m’est très difficile de me déplacer dans l’obscurité.",
    ],
  ],
  [
    // A4.1
    [
      "Non, tout va bien",
      "Je ne peux pas regarder un écran toute la journée mais sinon un film ou une émission, pas de problème!",
      "Je ressens souvent une fatigue des yeux mais c’est supportable",
      "J'ai beaucoup de difficultés à regarder l’ordinateur ou la télé, cela me fatigue très vite les yeux.",
    ],
  ],
  [
    // A5.1
    [
      "Non jamais",
      "Oui de temps en temps",
      "Quasiment tous les jours",
      "Oui, toute la journée",
    ],
  ],
  [
    // A6.3
    [
      "Aucune difficultés",
      "De légères difficultés",
      "Des Difficultés",
      "Impossible de lire ce texte",
    ],
  ],
  [
    // A6.6
    [
      "Jamais",
      "Oui un peu",
      "Assez souvent",
      "Très souvent, cela m’est très difficile",
    ],
    [
      "Aucune difficulté",
      "De légères difficultés",
      "beaucoup de difficultés",
      "C’est impossible pour moi",
    ],
    [
      "Sans aucune difficulté",
      "Avec de légères difficultés",
      "Avec beaucoup de difficultés",
      "J’ai besoin d’aide",
    ],
    [
      "Aucune difficulté",
      "De légères difficultés",
      "beaucoup de difficultés",
      "C’est impossible pour moi",
    ],
  ],
  [
    // 7.1: 3 questions
    ["OUI", "NON"],
    [
      "Non jamais",
      "Ca m’est arrivé mais globalement ça va",
      "Pas tous les jours mais assez souvent",
      "Oui constamment",
    ],
    [],
  ],
];
