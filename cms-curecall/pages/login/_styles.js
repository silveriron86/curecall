import styled from "@emotion/styled";

export const CodeInputer = styled("div")({
  marginTop: 43,
  marginBottom: 45,
  ".label": {
    margin: 0,
    padding: 0,
    height: 23,
    fontFamily: "Nexa Heavy",
    fontWeight: 900,
    fontSize: 18,
    color: "#fff",
    marginBottom: 9,
  },
  ".react-code-input": {
    input: {
      marginLeft: "5px !important",
      marginRight: "5.9px !important",
      width: "49.52px !important",
      height: "66.86px !important",
      borderRadius: "5px !important",
      boxShadow: "none !important",
      outline: "none",
      border: "none !important",
      fontFamily: "Nexa Heavy !important",
      fontWeight: 900,
      fontSize: "25px !important",
      lineHeight: "25px",
      textAlign: "center",
      color: "#0800ff !important",
      padding: "0 !important",
    },
  },
  "&.error": {
    ".react-code-input": {
      input: {
        backgroundColor: "#FDFF00 !important",
      },
    },
  },
});

export const questionStyle = {
  fontFamily: "Nexa Book",
  fontWeight: "normal",
  fontSize: 16,
  lineHeight: "18px",
  textAlign: "left",
  color: "#fff",
};

export const loginBtnStyle = {
  marginTop: 103,
};
