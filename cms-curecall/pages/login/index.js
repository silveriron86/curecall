import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import ReactCodeInput from "react-code-input";

import { PageContainer, Flex, FlexCenter } from "../_styles";
import Header from "../../components/Header";
import { ThemeIconButton, ThemeButton } from "../../components/_styles";
import { CodeInputer, questionStyle, loginBtnStyle } from "./_styles";

import logo from "../../assets/images/LogoBlanc.png";
import backIcon from "../../assets/images/Come@2x.png";

function Connect() {
  const router = useRouter();
  const [isSplash, setSplash] = useState(true);
  const [error, setError] = useState(false);
  const [code, setCode] = useState("");

  useEffect(() => {
    setTimeout(() => {
      setSplash(false);
    }, 2000);
  });

  const onConnect = () => {
    if (code !== "" && error === false) {
      // success
      localStorage.setItem("PAGE_INDEX", "0");
      router.push("/evaluation");
    }
  };

  const onChange = (value) => {
    console.log(value);
    if (value.length === 5) {
      if (value === "12345") {
        setError(true);
        setCode("");
      } else {
        setError(false);
        setCode(value);
      }
    }
  };

  return (
    <PageContainer>
      {isSplash ? (
        <FlexCenter>
          <img src={logo} width="124" height="113" />
        </FlexCenter>
      ) : (
        <div className="wrapper">
          <Header></Header>
          <div className="content">
            <p>Vous avez reçu un code PIN par SMS au +33 6 ** ** ** 14</p>
            <CodeInputer className={error ? "error" : ""}>
              <div className="label">Saisissez votre code</div>
              <ReactCodeInput type="number" fields={5} onChange={onChange} />
            </CodeInputer>
            <p style={questionStyle}>Vous ne vous l’avez pas reçu?</p>
            <ThemeIconButton style={{ marginTop: 7 }}>
              <img src={backIcon} />
              Renvoyer
            </ThemeIconButton>
            <Flex className="h-center">
              {error ? (
                <ThemeButton
                  className="error"
                  shape="round"
                  style={loginBtnStyle}
                >
                  Error
                </ThemeButton>
              ) : (
                <ThemeButton
                  shape="round"
                  style={loginBtnStyle}
                  onClick={onConnect}
                >
                  Connexion
                </ThemeButton>
              )}
            </Flex>
          </div>
        </div>
      )}
    </PageContainer>
  );
}

export default Connect;
