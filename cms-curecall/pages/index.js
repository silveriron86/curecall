import React from "react";
import { NotionRenderer, BlockMapType } from "react-notion";
import Head from "next/head";
import fetch from "node-fetch";

export async function getStaticProps() {
  const NOTION_PAGE_ID =
    "Contenu-th-rapeutique-e1901a5c1ee141f7a6411b12ffeb78d7";
  const data = await fetch(
    `https://notion-api.splitbee.io/v1/page/${NOTION_PAGE_ID}`
  ).then((res) => res.json());

  return {
    props: {
      blockMap: data,
    },
  };
}

const App = ({ blockMap }) => (
  <div style={{width: 1520, margin: '0 auto'}}>
    <Head>
      <title>react-notion example</title>
    </Head>
    <NotionRenderer blockMap={blockMap} />
  </div>
);

export default App;
