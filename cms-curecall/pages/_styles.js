import styled from "@emotion/styled";

export const PageContainer = styled("div")({
  width: "100%",
  minHeight: "100vh",
  background: "linear-gradient(#0800ff 0%, #040080 100%)",
  ".wrapper": {
    position: "relative",
    width: 375,
    margin: "0 auto",
    padding: "70px 28px 27.5px",
    // minHeight: "100vh",
    "&.more": {
      padding: "70px 36px 27.5px",
      ".main": {
        ".content": {
          paddingLeft: 0,
        },
      },
    },

    ".page-no": {
      position: "asbolute",
      width: "100%",
      height: "100%",
    },
    ".content": {
      paddingLeft: 8,
      marginTop: 21,
      p: {
        fontFamily: "Nexa Heavy",
        fontWeight: 900,
        fontSize: 16,
        color: "white",
        margin: 0,
        padding: 0,
      },
      ".img-wrapper": {
        marginTop: 16,
        height: 77,
        display: "flex",
        alignItems: "center",
        backgroundColor: "white",
        "&.auto": {
          height: "auto",
          backgroundColor: "transparent",
          marginTop: 28,
          height: 210,
        },
      },
      ".answers": {
        paddingTop: 8.4,
        minHeight: 227,
        "&.large": {
          display: "flex",
          flexWrap: "wrap",
        },
      },
      "&.p0": {
        padding: 0,
      },
    },
    ".blue-italic": {
      fontFamily: "Nexa BoldItalic",
      fontWeight: "bold",
      fontStyle: "italic",
      fontSize: 16,
      lineHeight: "22px",
      textAlign: "left",
      color: "#00fffd",
      marginLeft: 6.6,
    },
  },
  ".footer": {
    padding: "0 36px",
    width: 375,
    left: "calc(50% - 187.5px)",
    position: "fixed",
    bottom: 25.5,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    "&.relative": {
      position: "relative",
      width: "100% !important",
      marginTop: 62,
      bottom: "auto",
    },
    "&.right": {
      justifyContent: "flex-end",
    },
  },
  ".ant-slider": {
    margin: 0,
    padding: 0,
    ".ant-slider-rail": {
      marginLeft: -10,
      marginRight: -10,
      width: "calc(100% + 20px)",
      height: 15,
      borderRadius: 7.5,
      background:
        "linear-gradient(to right, #fff 0%, #fff 8.87%, #fdff00 50.25%, #f00 100%)",
    },
    ".ant-slider-track": {
      backgroundColor: "transparent",
    },
    ".ant-slider-step": {
      height: 30,
    },
    ".ant-slider-handle": {
      width: 0,
      height: 0,
      borderTop: "none",
      borderLeft: "15px solid transparent !important",
      borderRight: "15px solid transparent !important",
      borderBottom: "25px solid white !important",
      backgroundColor: "transparent",
      borderRadius: 0,
      marginTop: 19,
    },
  },
});

export const FlexCenter = styled("div")({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
  minHeight: "100vh",
  "&.absolute": {
    position: "absolute",
    left: 0,
    top: 0,
  },
});

export const Flex = styled("div")({
  display: "flex",
  "&.h-center": {
    justifyContent: "center",
  },
  "&.v-center": {
    alignItems: "center",
  },
  "&.space-between": {
    justifyContent: "space-between",
    alignItems: "center",
  },
});

export const PageMark = styled("div")({
  width: 50,
  height: 56,
  position: "relative",
  span: {
    position: "absolute",
    width: "100%",
    height: "100%",
    display: "block",
    fontFamily: "Nexa Heavy",
    fontWeight: 900,
    fontSize: 33,
    textAlign: "center",
    color: "#fff",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 5,
  },
});

export const Answer = styled("div")({
  marginTop: 11.6,
  display: "flex",
  ".ant-btn": {
    width: 41,
    height: 41,
    border: "2px solid white",
    borderRadius: 10,
    boxShadow: "none",
    outline: "none",
    backgroundColor: "transparent",
    padding: 0,
    span: {
      fontFamily: "Nexa Heavy",
      fontSize: 25,
      lineHeight: "30px",
      color: "white",
    },
    "&.selected": {
      backgroundColor: "white !important",
      span: {
        color: "#0800FF",
      },
    },
  },
  ".text": {
    marginLeft: 11,
    marginTop: -3,
    width: 250,
    fontFamily: "Nexa Heavy",
    fontSize: 16,
    lineHeight: "25px",
    color: "white",
  },
  "&.one": {
    alignItems: "center",
    ".text": {
      marginTop: 0,
    },
  },
  "&.large": {
    marginRight: 21,
    marginBottom: 8.4,
    "&:nth-of-type(2)": {
      marginRight: 100,
    },
    ".ant-btn": {
      width: 80,
      height: 80,
      span: {
        fontSize: 45,
        lineHeight: "58px",
      },
    },
    ".text": {
      display: "none",
    },
  },
});
