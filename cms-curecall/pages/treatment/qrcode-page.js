import React, { useEffect } from "react";
import QrScanner from "qr-scanner";
QrScanner.WORKER_PATH = "/qr-scanner-worker.min.js";

function QrCodePage(props) {
  useEffect(() => {
    const video = document.getElementById("qr-video");
    const scanner = new QrScanner(
      video,
      (result) => {
        props.onSuccess(scanner.$canvas.toDataURL());
      },
      (error) => {
        // console.log(error);
      }
    );
    scanner.start().then(() => {
      console.log("Start qr scanner");
    });
  });

  const height = document.body.clientHeight - 74;
  return <video id="qr-video" style={{ height }}></video>;
}

export default QrCodePage;
