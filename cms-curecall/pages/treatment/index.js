import { useState, useEffect } from "react";
import {
  HeaderWrapper,
  ThemeIconButton,
  ThemeInput,
  ThemeButton,
} from "../../components/_styles";
import { PageContainer, Flex, FlexCenter } from "../_styles";
import AnswersList from "../../components/answers-list";
import QrCodePage from "./qrcode-page";

import listenCloseIcon from "../../assets/images/Listen close@2x.png";
import closeIcon from "../../assets/images/Close@2x.png";
import nextIcon from "../../assets/images/Come@2x.png";
import logoIcon from "../../assets/images/LogoBlanc.png";
import scanIcon from "../../assets/images/Scan icon@2x.png";
import subtractionIcon from "../../assets/images/Subtraction 3@2x.png";

function TreatmentPage(props) {
  const [pageIndex, setPage] = useState(0);
  const [scannedImg, setScannedImage] = useState("");
  const [scannedNumber, setNumber] = useState("");
  const [selectedAnswer, selectAnswer] = useState(-1);

  useEffect(() => {
    const index = localStorage.getItem("TREATMENT_INDEX");
    if (index !== null) {
      setPage(parseInt(index, 10));
    }
    const answer = localStorage.getItem("TREATMENT_FIRST_ANSWER");
    if (answer !== null) {
      selectAnswer(parseInt(answer, 10));
    }
  });

  const goNext = () => {
    const index = pageIndex + 1;
    setPage(pageIndex + 1);
    localStorage.setItem("TREATMENT_INDEX", index.toString());
  };

  const goPrev = () => {
    const index = pageIndex - 1;
    setPage(index);
    localStorage.setItem("TREATMENT_INDEX", index.toString());
  };

  const onSelectAnswer = (index) => {
    selectAnswer(index);
    localStorage.setItem("TREATMENT_FIRST_ANSWER", index.toString());
  };

  console.log("pageIndex = ", pageIndex);

  if (pageIndex === 1 && selectedAnswer === 1) {
    return (
      <PageContainer>
        <div className="wrapper more">
          <div className="main">
            <Flex className="v-center h-center">
              <ThemeIconButton style={{ marginTop: -15 }} onClick={goPrev}>
                <img src={logoIcon} width={82} height={75} />
              </ThemeIconButton>
            </Flex>
            <HeaderWrapper
              className="full-center"
              style={{ height: "calc(100vh - 260px)" }}
            >
              <span style={{ textAlign: "left" }}>
                Très bien merci pour cette information, vous pouvez à tout
                moment ajouter un traitement en envoyant
                <br />
                <span className="bold-italic blue">“Traitement”</span>
                <br />
                par SMS à Curecall.
              </span>
            </HeaderWrapper>
            <ThemeIconButton className="listen-btn">
              <img src={listenCloseIcon} />
            </ThemeIconButton>
          </div>
        </div>
      </PageContainer>
    );
  }

  if (pageIndex === 2) {
    return (
      <PageContainer>
        {scannedImg ? (
          <div className="wrapper">
            <div className="main">
              <HeaderWrapper>
                <span>Votre Code est bien enregistrement</span>
              </HeaderWrapper>
            </div>
            <FlexCenter className="absolute">
              <FlexCenter style={{ position: "relative" }}>
                <img src={subtractionIcon} style={{ position: "absolute" }} />
                <img src={scannedImg} width="192" height="192" />
              </FlexCenter>
            </FlexCenter>
            <Flex className="h-center">
              <ThemeButton
                shape="round"
                style={{ position: "fixed", bottom: 57 }}
                // onClick={goNext}
              >
                Commencer
              </ThemeButton>
            </Flex>
          </div>
        ) : (
          <>
            <QrCodePage onSuccess={setScannedImage} />
            <ThemeIconButton className="close-btn" onClick={goPrev}>
              <img src={closeIcon} />
            </ThemeIconButton>
            <div className="qrcode-exp bold-italic f14">
              Veuillez mettre votre code QR à l’intérieur de l’écran et faire la
              mise au point
            </div>
          </>
        )}
        <ThemeIconButton className="listen-btn">
          <img src={listenCloseIcon} />
        </ThemeIconButton>
      </PageContainer>
    );
  }

  return (
    <PageContainer>
      <div className="wrapper more">
        <HeaderWrapper>
          <span style={{ width: 235 }}>
            {pageIndex === 0
              ? "Votre ophtalmologue vous a t-il prescrit un traitement de fond ?"
              : pageIndex === 1
              ? "Votre traitement médical"
              : ""}
          </span>
        </HeaderWrapper>
        <ThemeIconButton className="listen-btn">
          <img src={listenCloseIcon} />
        </ThemeIconButton>
        {pageIndex === 0 && (
          <div style={{ marginTop: 14 }}>
            <AnswersList
              data={["OUI", "NON"]}
              defaultValue={selectedAnswer}
              index={0}
              onSelect={onSelectAnswer}
            />
          </div>
        )}
        {pageIndex === 1 && (
          <>
            <div className="content p0" style={{ marginTop: 18 }}>
              <p>
                <span className="f18">
                  Nous allons vous poser des questions sur votre traitement
                  médical en cours. Il s’agit uniquement du traitement que vous
                  a prescrit votre ophtalmologue.
                </span>
              </p>
            </div>
            <ThemeInput
              placeholder="Nom de votre médicament"
              style={{ marginTop: 8 }}
              onChange={(e) => setNumber(e.target.value)}
            />
            <div style={{ marginTop: 98 }}>
              {scannedNumber !== "" ? (
                <Flex className="h-center">
                  <ThemeButton shape="round" onClick={goNext}>
                    Commencer
                  </ThemeButton>
                </Flex>
              ) : (
                <>
                  <Flex className="h-center">
                    <ThemeIconButton>
                      <img src={scanIcon} />
                    </ThemeIconButton>
                  </Flex>
                  <p
                    className="blue-italic f14 text-center"
                    style={{ width: 166, margin: "10px auto 0" }}
                  >
                    Scanner le QRcode de votre médicament pour gagner du temps
                  </p>
                </>
              )}
            </div>
          </>
        )}
      </div>
      {pageIndex === 0 && selectedAnswer >= 0 && (
        <div className="footer right">
          <ThemeIconButton onClick={goNext}>
            <img src={nextIcon} />
          </ThemeIconButton>
        </div>
      )}
    </PageContainer>
  );
}

export default TreatmentPage;
