import { useState, useEffect } from "react";
import { Flex, PageMark, PageContainer, FlexCenter } from "../_styles";
import {
  HeaderWrapper,
  ThemeButton,
  ThemeIconButton,
} from "../../components/_styles";
import ImagePage from "./image-page";

import welcomeIcon from "../../assets/images/Welcome@2x.png";
import attentionIcon from "../../assets/images/Attention@2x.png";
import listenCloseIcon from "../../assets/images/Listen close@2x.png";
import nextIcon from "../../assets/images/Come@2x.png";
import prevIcon from "../../assets/images/Back@2x.png";
import listImg from "../../assets/images/18@2x.png";
import bgImg from "../../assets/images/Path.png";
import logoIcon from "../../assets/images/LogoBlanc.png";
import CompletePage from "./complete-page";
import SixPage from "./six-page";
import SevenPage from "./seven-page";

function Evaluation() {
  const [pageIndex, setPage] = useState(0);

  useEffect(() => {
    const index = localStorage.getItem("PAGE_INDEX");
    if (index !== null) {
      setPage(parseInt(index, 10));
    }
  });

  const goNext = () => {
    const index = pageIndex + 1;
    setPage(pageIndex + 1);
    localStorage.setItem("PAGE_INDEX", index.toString());
  };

  const goPrev = () => {
    const index = pageIndex - 1;
    setPage(index);
    localStorage.setItem("PAGE_INDEX", index.toString());
  };

  const toggleListen = () => {};
  console.log("pageIndex = ", pageIndex);
  if (pageIndex >= 24 && pageIndex < 31) {
    return (
      <SixPage
        pageIndex={pageIndex}
        goPrev={goPrev}
        toggleListen={toggleListen}
        goNext={goNext}
      />
    );
  }

  if (pageIndex >= 32 && pageIndex < 37) {
    return (
      <SevenPage
        pageIndex={pageIndex}
        goPrev={goPrev}
        toggleListen={toggleListen}
        goNext={goNext}
      />
    );
  }

  return (
    <PageContainer>
      <div className="wrapper more">
        {pageIndex === 0 && (
          <div className="main">
            <HeaderWrapper>
              <span className="full">
                Evaluation de votre fonction visuelle
              </span>
            </HeaderWrapper>

            <HeaderWrapper style={{ marginTop: 56.5 }} className="center">
              <img src={welcomeIcon} width="50" height="56" />
              <span>M. Mohammed</span>
            </HeaderWrapper>

            <div className="content" style={{ marginTop: 30.7 }}>
              <p className="f18">
                Bienvenue sur votre formulaire d’évaluation de votre fonction
                visuelle, il vous prendra{" "}
                <span className="yellow">6 minutes</span> à remplir
              </p>
            </div>
          </div>
        )}

        {pageIndex === 1 && (
          <div className="main">
            <HeaderWrapper>
              <span className="full">Pour commencer</span>
            </HeaderWrapper>

            <HeaderWrapper
              style={{ marginTop: 31.5, display: "block" }}
              className="center"
            >
              <img src={attentionIcon} width="50" height="56" />
              <span className="block full" style={{ marginTop: 22.7 }}>
                Augmentez la luminosité de votre téléphone au maximum
              </span>
            </HeaderWrapper>

            <div className="content" style={{ marginTop: 21 }}>
              <p className="f18">
                <span className="yellow">
                  Astuce : Si vous avez un iPhone ou un téléphone Android,
                  demandez le à SIRI ou à Google
                </span>
              </p>
            </div>
          </div>
        )}

        {pageIndex === 2 && (
          <div className="main">
            <HeaderWrapper>
              <span className="full">La perception des contrastes</span>
            </HeaderWrapper>
            <div className="content" style={{ marginTop: 14.7 }}>
              <p className="f18">
                <span className="yellow">
                  SPECS A SUIVRE POUR EFFECTUER LES VISUELS CI-DESSOUS
                </span>
              </p>
            </div>
            <img src={listImg} style={{ marginTop: 14.3 }} />
            <p className="blue-italic" style={{ marginBottom: 25.8 }}>
              Lors d’une baisse de la perception des contrastes, les objets, les
              paysages ont moins de relief et de nuances. Les couleurs s’avèrent
              soit plus soit moins tranchées ou vives ; ce qui peut entraîner
              une modification des perspectives (diminution de la vision des
              reliefs).
            </p>
            <Flex className="space-between">
              <ThemeIconButton className="hidden">
                <img src={nextIcon} />
              </ThemeIconButton>
              <ThemeIconButton onClick={toggleListen}>
                <img src={listenCloseIcon} />
              </ThemeIconButton>
              <ThemeIconButton onClick={goNext}>
                <img src={nextIcon} />
              </ThemeIconButton>
            </Flex>
          </div>
        )}

        {pageIndex === 3 && (
          <div className="main">
            <HeaderWrapper style={{ display: "block" }} className="center">
              <PageMark>
                <span>1</span>
                <img src={bgImg} width="50" height="56" />
              </PageMark>
              <span className="block full f18" style={{ marginTop: 16.2 }}>
                Nous allons commencer par vous montrer des lignes de lettres
                plus ou moins contrastées, il vous suffit d’évaluer ce que vous
                voyez. Il y a 8 images.
              </span>
            </HeaderWrapper>

            <div className="content" style={{ marginTop: 20 }}>
              <p className="f18">
                <span className="yellow">
                  Pensez à positionner la luminosité de votre écran au maximum.
                </span>
              </p>

              <ThemeIconButton style={{ marginTop: 18.8 }}>
                <img src={listenCloseIcon} />
              </ThemeIconButton>
              <Flex className="h-center">
                <ThemeButton
                  shape="round"
                  style={{ marginTop: 147.5 }}
                  onClick={goNext}
                >
                  Commencer
                </ThemeButton>
              </Flex>
            </div>
          </div>
        )}

        {pageIndex >= 4 && pageIndex < 12 && (
          <ImagePage
            category={0}
            index={pageIndex - 4}
            total={8}
            goPrev={goPrev}
            goNext={goNext}
          />
        )}

        {pageIndex === 12 && (
          <div className="main">
            <Flex className="v-center h-center">
              <img src={logoIcon} width={82} height={75} />
            </Flex>
            <HeaderWrapper style={{ margin: "23px 0" }}>
              <span className="full">
                Très bien Mohammed, merci pour avoir jouer le jeu de la
                perception des contrastes, en vous posant régulièrement cette
                question.
              </span>
            </HeaderWrapper>
            <HeaderWrapper>
              <span className="full">
                Cela vous permet de vous auto-évaluer et de communiquer cette
                information à votre médecin.
              </span>
            </HeaderWrapper>
          </div>
        )}

        {(pageIndex === 13 ||
          pageIndex === 17 ||
          pageIndex === 19 ||
          pageIndex === 21 ||
          pageIndex === 23 ||
          pageIndex === 31) && <CompletePage index={pageIndex} />}

        {pageIndex === 14 && (
          <div className="main">
            <HeaderWrapper style={{ display: "block" }} className="center">
              <PageMark>
                <span>2</span>
                <img src={bgImg} width="50" height="56" />
              </PageMark>
              <span className="block full f18" style={{ marginTop: 16.2 }}>
                Nous allons vous aider à évaluer votre sensibilité à la lumière
                au regard de certaines situations.
              </span>
            </HeaderWrapper>

            <div className="content" style={{ marginTop: 20 }}>
              <p className="f18">
                <span className="yellow">
                  Pensez à positionner la luminosité de votre écran au maximum.
                </span>
              </p>

              <ThemeIconButton style={{ marginTop: 18.8 }}>
                <img src={listenCloseIcon} />
              </ThemeIconButton>
              <Flex className="h-center">
                <ThemeButton
                  shape="round"
                  style={{ marginTop: 147.5 }}
                  onClick={goNext}
                >
                  Commencer
                </ThemeButton>
              </Flex>
            </div>
          </div>
        )}

        {pageIndex >= 15 && pageIndex < 17 && (
          <ImagePage
            category={1}
            index={pageIndex - 15}
            total={2}
            goPrev={goPrev}
            goNext={goNext}
          />
        )}

        {pageIndex === 18 && (
          <ImagePage
            category={2}
            index={0}
            total={1}
            goPrev={goPrev}
            goNext={goNext}
          />
        )}

        {pageIndex === 20 && (
          <ImagePage
            category={3}
            index={0}
            total={1}
            goPrev={goPrev}
            goNext={goNext}
          />
        )}

        {pageIndex === 22 && (
          <ImagePage
            category={4}
            index={0}
            total={1}
            goPrev={goPrev}
            goNext={goNext}
          />
        )}
      </div>

      {(pageIndex < 2 ||
        pageIndex === 12 ||
        pageIndex === 13 ||
        pageIndex === 17 ||
        pageIndex === 19 ||
        pageIndex === 21 ||
        pageIndex === 23 ||
        pageIndex === 31) && (
        <div className="footer">
          <ThemeIconButton
            className={pageIndex < 2 || pageIndex === 4 ? "hidden" : ""}
            onClick={goPrev}
          >
            <img src={prevIcon} />
          </ThemeIconButton>
          <ThemeIconButton onClick={toggleListen}>
            <img src={listenCloseIcon} />
          </ThemeIconButton>
          <ThemeIconButton onClick={goNext}>
            <img src={nextIcon} />
          </ThemeIconButton>
        </div>
      )}
    </PageContainer>
  );
}

export default Evaluation;
