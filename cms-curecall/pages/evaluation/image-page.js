import { useState, useEffect } from "react";
import { Button } from "antd";
import { HeaderWrapper, ThemeIconButton } from "../../components/_styles";
import { Flex, Answer, PageMark } from "../_styles";
import PageNo from "../../components/page-no";
import { answers } from "../../constants/answers";

import listenCloseIcon from "../../assets/images/Listen close@2x.png";
import listenOpenIcon from "../../assets/images/Listen open@2x.png";
import nextIcon from "../../assets/images/Come@2x.png";
import prevIcon from "../../assets/images/Back@2x.png";
import bgImg from "../../assets/images/Path.png";

import imgA12 from "../../assets/images/A1.2.png";
import imgA13 from "../../assets/images/A1.3.png";
import imgA14 from "../../assets/images/A1.4.png";
import imgA15 from "../../assets/images/A1.5.png";
import imgA16 from "../../assets/images/A1.6.png";
import imgA17 from "../../assets/images/A1.7.png";
import imgA18 from "../../assets/images/A1.8.png";
import imgA19 from "../../assets/images/A1.9.png";

import imgA21 from "../../assets/images/A21.png";
import imgA22 from "../../assets/images/A22.png";
import AnswersList from "../../components/answers-list";

const images = [
  [imgA12, imgA13, imgA14, imgA15, imgA16, imgA17, imgA18, imgA19],
  [imgA21, imgA22],
];

function ImagePage(props) {
  const [isListenOpened, setListen] = useState(false);
  const { category, index, total } = props;
  console.log(category, index, total);

  const toggleListen = () => {
    setListen(!isListenOpened);
  };

  let pageTitle = "Comment percevez-vous les lettres de cette première image ?";
  if (category === 1) {
    pageTitle = [
      "En voiture ou dans la rue, au levé du jour ou à la tombée de la nuit le soleil est rasant et très intense comme le montre la photo ci-dessous.<br/>Cette situation est pour vous :",
      "Dans certaines situations tels qu’un centre commercial ou parfois chez soi, la lumière artificielle provenant du plafond est intense.<br/>Cette situation est pour vous :",
    ][index];
  }

  return (
    <>
      <div className="main">
        {category < 2 && (
          <>
            <HeaderWrapper style={{ width: 247 }}>
              <span
                className="full"
                dangerouslySetInnerHTML={{ __html: pageTitle }}
              ></span>
            </HeaderWrapper>
            <PageNo value={index + 1} total={total} />
          </>
        )}

        {(category === 2 || category === 3 || category === 4) && (
          <HeaderWrapper style={{ display: "block" }} className="center">
            <PageMark>
              <span>{category + 1}</span>
              <img src={bgImg} width="50" height="56" />
            </PageMark>
            <span className="block full f18" style={{ marginTop: 16.2 }}>
              {category === 2
                ? "Quand vous vous déplacez dans la pénombre ou l’obscurité, la distinction des objets peut vous sembler moindre qu’auparavant. De même quand vous éteignez la lumière, vous devez attendre plus longtemps avant de distinguer les éléments qui vous entourent ?"
                : category === 3
                ? "Avez-vous du mal à supporter de regarder plus d’une heure un écran de télévision  ou un ordinateur ?"
                : category === 4
                ? "Avez-vous la sensation d’avoir du sable dans les yeux et que vos yeux sont plus secs ?"
                : ""}
            </span>
          </HeaderWrapper>
        )}

        <div className="content">
          {category < 2 && (
            <div className={`img-wrapper ${category === 0 ? "" : "auto"}`}>
              <img src={images[category][index]} />
            </div>
          )}
          <AnswersList
            category={category}
            index={index}
            isListenOpened={isListenOpened}
          />
        </div>
      </div>

      <div
        className={`footer ${
          category === 0 ||
          category === 4 ||
          (category === 6 && index < 3) ||
          category === 7
            ? ""
            : "relative"
        }`}
        style={{
          padding: 0,
          width: "calc(375px - 72px)",
          left: "calc(50% - 151.5px)",
          marginTop: category === 6 && index == 3 ? 40 : 62,
        }}
      >
        <ThemeIconButton
          className={category === 0 && index === 0 ? "hidden" : ""}
          onClick={props.goPrev}
        >
          <img src={prevIcon} />
        </ThemeIconButton>
        <ThemeIconButton onClick={toggleListen}>
          <img src={isListenOpened ? listenOpenIcon : listenCloseIcon} />
        </ThemeIconButton>
        <ThemeIconButton onClick={props.goNext}>
          <img src={nextIcon} />
        </ThemeIconButton>
      </div>
    </>
  );
}

export default ImagePage;
