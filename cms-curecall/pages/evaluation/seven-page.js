// A6.2
import { useState } from "react";
import { Slider } from "antd";
import { PageMark, PageContainer } from "../_styles";
import { ThemeIconButton, HeaderWrapper } from "../../components/_styles";
import listenCloseIcon from "../../assets/images/Listen close@2x.png";
import closeIcon from "../../assets/images/Close@2x.png";
import prevIcon from "../../assets/images/Back@2x.png";
import bgImg from "../../assets/images/Path.png";
import ImagePage from "./image-page";
import PageNo from "../../components/page-no";

import icon1 from "../../assets/images/Component 50 – 1@2x.png";
import icon2 from "../../assets/images/Component 51 – 1@2x.png";
import icon3 from "../../assets/images/Component 52 – 1@2x.png";
import icon4 from "../../assets/images/Component 53 – 1@2x.png";
import icon5 from "../../assets/images/Component 54 – 1@2x.png";
import icon6 from "../../assets/images/Component 55 – 1@2x.png";

const icons = [icon1, icon2, icon3, icon4, icon5, icon6];
const texts = [
  "Aucune Douleur",
  "Légère",
  "Modérée",
  "Forte",
  "Très forte",
  "La pire couleur qui soit",
];

const pageTitles = [
  "Sentez-vous une douleur à l’intérieur de votre œil ?",
  "Est- ce que cette douleur est souvent associée à des vomissements, des nausées et des sueurs ?",
  "Si vous deviez évaluer votre douleur sur une échelle de <span class='yellow'>0 (Pas de douleur)</span> à <span class='yellow'>10 (insupportable)</span> que diriez-vous ?",
];

function SevenPage(props) {
  const [isListenOpened, setListen] = useState(false);
  const [level, setLevel] = useState(0);
  const { pageIndex } = props;
  console.log("seven page = ", pageIndex);

  const toggleListen = () => {
    setListen(!isListenOpened);
  };

  if (pageIndex >= 32 && pageIndex < 35) {
    return (
      <PageContainer className="seven-page">
        <div className="wrapper more">
          <HeaderWrapper style={{ width: 247 }}>
            <PageMark>
              <span>{7}</span>
              <img src={bgImg} width="50" height="56" />
            </PageMark>
          </HeaderWrapper>
          <HeaderWrapper style={{ marginTop: 16, width: 290 }}>
            <span
              className="full"
              dangerouslySetInnerHTML={{ __html: pageTitles[pageIndex - 32] }}
            ></span>
          </HeaderWrapper>

          <PageNo value={pageIndex - 31} total={3} />
          {pageIndex === 34 && (
            <div className="content p0">
              <p
                className="f16 bold-italic"
                style={{ marginTop: 20, marginBottom: 27 }}
              >
                <span className="blue">Echelle de douleur :</span>
              </p>
              <div style={{ padding: "0 10px" }}>
                <Slider
                  defaultValue={0}
                  min={0}
                  max={5}
                  tooltipVisible={false}
                  onChange={setLevel}
                />
              </div>
              <ThemeIconButton className="laugh">
                <img src={icons[level]} />
                <p className="f16">{texts[level]}</p>
              </ThemeIconButton>
            </div>
          )}

          <div style={{ marginTop: -21 }}>
            <ImagePage
              category={7}
              index={pageIndex - 32}
              total={3}
              goPrev={props.goPrev}
              goNext={props.goNext}
            />
          </div>
        </div>
      </PageContainer>
    );
  }

  if (pageIndex === 35) {
    return (
      <PageContainer>
        <div className="wrapper">
          <HeaderWrapper
            className="full-center"
            style={{ height: "calc(100vh - 130px)" }}
          >
            <span>
              Merci à vous!
              <br />
              Vous avez fini toute la formulaire de “Evaluation de votre
              fonction visuelle”,
              <br />à bientôt !
            </span>
          </HeaderWrapper>
          <div className="footer">
            <ThemeIconButton onClick={props.goPrev}>
              <img src={prevIcon} />
            </ThemeIconButton>
            <ThemeIconButton style={{ visibility: "hidden" }}>
              <img src={listenCloseIcon} />
            </ThemeIconButton>
          </div>
          <ThemeIconButton className="close-btn">
            <img src={closeIcon} />
          </ThemeIconButton>
          <ThemeIconButton className="listen-btn">
            <img src={listenCloseIcon} />
          </ThemeIconButton>
        </div>
      </PageContainer>
    );
  }
}

export default SevenPage;
