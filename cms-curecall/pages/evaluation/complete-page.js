import { useState, useEffect } from "react";
import { HeaderWrapper } from "../../components/_styles";

function CompletePage(props) {
  const { index } = props;
  const description =
    index === 13
      ? "La sensibilité à la lumière, aussi nommée photophobie, correspond à une gêne douloureuse d’origine oculaire provoquée par l’exposition à la lumière. En contrôlant régulièrement vos sensations, nous donnons une  information importante à votre médecin lui permettant d’adapter votre traitement thérapeutique ou votre correction visuelle."
      : index === 17
      ? "Nos capacités en vision nocturne ou sous une lumière faible peuvent diminuées. La cécité nocturne peut causer des problèmes pour conduire la nuit ou se déplacer dans des endroits sombres. Cela peut être un temps d’adaptation plus long lorsque l’on passe de la lumière à l’obscurité ou une cécité évidente lorsqu’il n’y a plus de lumière."
      : index === 19
      ? "La fatigue oculaire est bien souvent le résultat d’une utilisation excessive de nos yeux. Il s’agit d’une altération temporaire du fonctionnement de l’œil, souvent associée à un effort trop important de celui-ci."
      : index === 21
      ? "La sécheresse oculaire est une insuffisance de larmes. Elle se manifeste des démangeaisons, picotements, clignement et sensation de brûlure ou de corps étranger dans les yeux."
      : index === 23
      ? "Souvent rapide, elle s’accompagne d’une diminution de la capacité à percevoir les détails. Un éclairage plus puissant devient nécessaire pour lire ou accomplir des tâches qui demandent de la minutie.<br/><br/>Votre vision de loin, notamment de près et sur les côtés est-ce-qu’elle est devenue moins précise ?"
      : index === 31
      ? "La douleur oculaire peut être sévère et sembler aiguë ou lancinante, ou la personne peut ressentir une légère irritation de la surface de l’œil ou avoir l’impression d’avoir un corps étranger dans l’œil (sensation de corps étranger)"
      : "";

  return (
    <div className="main">
      <HeaderWrapper>
        <span className="full">
          {index === 13
            ? "Sensibilité à la lumière"
            : index === 17
            ? "Cécité nocturne ou crépusculaire"
            : index === 19
            ? "Fatigue visuelle"
            : index === 21
            ? "Sécheresse oculaire"
            : index === 23
            ? "Baisse de l’acuité visuelle (vision déformée)"
            : index === 31
            ? "Sensibilité douloureuse au niveau des yeux"
            : ""}
        </span>
      </HeaderWrapper>
      <div className="content" style={{ marginTop: 16 }}>
        <p className="f16 bold-italic">
          <span
            className="blue"
            dangerouslySetInnerHTML={{ __html: description }}
          ></span>
        </p>
      </div>
    </div>
  );
}

export default CompletePage;
