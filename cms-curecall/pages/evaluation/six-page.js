// A6.2
import { useState, useEffect } from "react";
import { Flex, PageMark, PageContainer, FlexCenter } from "../_styles";
import { ThemeIconButton, HeaderWrapper } from "../../components/_styles";
import listenCloseIcon from "../../assets/images/Listen close@2x.png";
import listenOpenIcon from "../../assets/images/Listen open@2x.png";
import attentionIcon from "../../assets/images/Attention@2x.png";
import distanceImg from "../../assets/images/distance@2x.png";
import glassesImg from "../../assets/images/glasses@2x.png";
import nextIcon from "../../assets/images/Come@2x.png";
import prevIcon from "../../assets/images/Back@2x.png";
import bgImg from "../../assets/images/Path.png";
import eyeIcon from "../../assets/images/Union 16@2x.png";
import handIcon from "../../assets/images/Group 38@2x.png";
import AnswersList from "../../components/answers-list";
import ImagePage from "./image-page";
import PageNo from "../../components/page-no";

const pageTitles = [
  "Vous arrive-t-il de plisser les yeux pour bien lire les panneaux ?",
  "Semblez-vous voir flou de loin, par exemple éprouvez-vous des difficultés à reconnaître les visages des personnes familières ?",
  "Vous appréciez les distances dans certaines circonstances comme descendre les escaliers ou garer une voiture ?",
  "Pour voir sur les côtés, comme lorsqu’une voiture sort d’une pente de garage d’une rue latérale, ou lorsque quelqu’un sort d’un bâtiment vous ressentez ?",
];

function SixPage(props) {
  const [isListenOpened, setListen] = useState(false);
  const { pageIndex } = props;
  console.log("six page = ", pageIndex);

  const toggleListen = () => {
    setListen(!isListenOpened);
  };

  if (pageIndex >= 27 && pageIndex < 31) {
    return (
      <PageContainer>
        <div className="wrapper more">
          <HeaderWrapper style={{ width: 247 }}>
            <PageMark>
              <span>{6}</span>
              <img src={bgImg} width="50" height="56" />
            </PageMark>
          </HeaderWrapper>
          <HeaderWrapper style={{ marginTop: 16, width: 290 }}>
            <span
              className="full"
              dangerouslySetInnerHTML={{ __html: pageTitles[pageIndex - 27] }}
            ></span>
          </HeaderWrapper>

          <PageNo value={pageIndex - 26} total={4} />
          <div style={{ marginTop: -21 }}>
            <ImagePage
              category={6}
              index={pageIndex - 27}
              total={4}
              goPrev={props.goPrev}
              goNext={props.goNext}
            />
          </div>
        </div>
      </PageContainer>
    );
  }

  return (
    <PageContainer>
      <div className="wrapper more">
        {pageIndex === 24 && (
          <div className="main">
            <HeaderWrapper style={{ display: "block" }} className="center">
              <img src={attentionIcon} width="50" height="56" />
              <span className="block full" style={{ marginTop: 16 }}>
                La mesure de l’acuité visuelle est réalisée séparément pour
                chaque oeil, et à deux distances d’observation de près et de
                loin.
              </span>
            </HeaderWrapper>
            <div className="content">
              <p className="f18">Pour tester votre vision de près, </p>
              <Flex className="h-center" style={{ margin: "28px 0 25px" }}>
                <img src={distanceImg} width="234" />
              </Flex>
              <p
                className="f18 center"
                style={{ width: 250, margin: "0 auto" }}
              >
                Tenez votre portable à environ 35 cm de vos yeux{" "}
              </p>
              <Flex className="h-center" style={{ margin: "63px 0 21px" }}>
                <img src={glassesImg} width="172" />
              </Flex>
              <p className="f18 center">Retirez vos lunettes</p>
              <Flex className="h-center" style={{ margin: "76px 0 16px" }}>
                <ThemeIconButton>
                  <img src={eyeIcon} />
                </ThemeIconButton>
                <ThemeIconButton style={{ marginLeft: 30 }}>
                  <img src={handIcon} />
                </ThemeIconButton>
              </Flex>
              <p
                className="f18 center"
                style={{ width: 250, margin: "0 auto" }}
              >
                Avec votre main, masquez alternativement un oeil, puis l’autre
              </p>
            </div>
          </div>
        )}

        {pageIndex === 25 && (
          <div className="main">
            <HeaderWrapper style={{ display: "block" }} className="center">
              <img src={attentionIcon} width="50" height="56" />
              <span className="block full" style={{ marginTop: 16 }}>
                Eprouvez vous des difficultés à lire ce texte intégralement que
                ça soit de l’oeil droit ou l’oeil gauche?
              </span>
            </HeaderWrapper>
            <div className="content">
              <p className="f50 center">
                Tester son acuité visuelle est un exercice difficile sans votre
                médecin.
              </p>
              <p className="f30 center">
                C’est pourquoi nous allons évaluer avec vous des situations de
                vie quotidienne.
              </p>
              <p className="f18 center">
                Développer l’auto-évaluation vous permettra d’observer les
                changements
              </p>
              <p className="f10" style={{ width: 142, margin: "12px auto 0" }}>
                et donnera des informations importantes à votre médecin
              </p>
              <AnswersList
                category={5}
                index={0}
                isListenOpened={isListenOpened}
              />
            </div>
          </div>
        )}

        {pageIndex === 26 && (
          <div className="main">
            <HeaderWrapper style={{ display: "block" }} className="center">
              <img src={attentionIcon} width="50" height="56" />
              <span className="block full" style={{ marginTop: 16 }}>
                Pour tester votre vision de loin, voici quelques situations de
                la vie quotidienne.
              </span>
            </HeaderWrapper>
            <div className="content"></div>
          </div>
        )}

        <div
          className={`footer ${
            pageIndex === 24 || pageIndex === 25 ? "relative" : ""
          }`}
          style={{
            padding: pageIndex === 24 || pageIndex === 25 ? 0 : "0 72px 0 0",
            marginLeft: 36,
          }}
        >
          <ThemeIconButton onClick={props.goPrev}>
            <img src={prevIcon} />
          </ThemeIconButton>
          <ThemeIconButton onClick={toggleListen}>
            <img src={isListenOpened ? listenOpenIcon : listenCloseIcon} />
          </ThemeIconButton>
          <ThemeIconButton onClick={props.goNext}>
            <img src={nextIcon} />
          </ThemeIconButton>
        </div>
      </div>
    </PageContainer>
  );
}

export default SixPage;
