const withImages = require("next-images");
module.exports = withImages({
  webpack(config, options) {
    return config;
  },
  devIndicators() {
    return { autoPrerender: false };
  },

  useFileSystemPublicRoutes() {
    return true;
  },

  publicPath() {
    return "/_next/";
  },
});
