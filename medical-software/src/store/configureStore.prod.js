import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import history from "./history";
import { routerMiddleware } from "react-router-redux";
import rootReducer from "../reducers";

const router = routerMiddleware(history);

/**
 * Creates a preconfigured store.
 */
export default function configureStore(initialState) {
  return createStore(rootReducer, initialState, applyMiddleware(thunk, router));
}
