module.exports = {
  BLUE: "#3333CC",
  GREY: "#A5A5A5",
  WHITE: "#f7f7f7",
  LIGHT_GREY: "#f0eff0",
};
