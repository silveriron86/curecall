/* eslint-disable no-undef */
/* eslint-disable no-duplicate-case */
import { PatientConstants } from "../constants";
import { cloneDeep } from "lodash";

const initialState = {
  patients: [],
  patient: null,
  error: null,
  nextConsultationDate: null,
  treatments: [],
  graphs: {
    SYMPTOM: null,
    DIARY: null,
  },
  profileQuality: null,
  forms: {
    AMSLER_GRID: null,
    CONTRAST_PERCEPTION: null,
    VISUAL_ACUITY: null,
    EVERYDAY_LIFE: null,
    THERAPEUTIC_CARE: null,
  },
};

function PatientReducer(state = initialState, action) {
  switch (action.type) {
    case PatientConstants.GET_PATIENTS_SUCCESS:
      return Object.assign({}, state, {
        patients: action.response,
        error: null,
      });
    case PatientConstants.GET_PATIENTS_ERROR:
      return Object.assign({}, state, {
        patients: [],
        error: action.error,
      });

    case PatientConstants.GET_PATIENT_SUCCESS:
      return Object.assign({}, state, {
        patient: action.response,
        error: null,
      });

    case PatientConstants.ADD_PATIENT_SUCCESS:
      return Object.assign({}, state, {
        patient: action.response.patient,
        // Init when select new patient
        profileQuality: null,
        forms: {
          AMSLER_GRID: null,
          CONTRAST_PERCEPTION: null,
          VISUAL_ACUITY: null,
          EVERYDAY_LIFE: null,
          THERAPEUTIC_CARE: null,
        },
        error: null,
      });

    case PatientConstants.GET_PATIENT_ERROR:
    case PatientConstants.ADD_PATIENT_ERROR:
    case PatientConstants.UPDATE_PATIENT_ERROR:
      return Object.assign({}, state, {
        patient: null,
        error: action.error,
      });

    case PatientConstants.UPDATE_PATIENT_SUCCESS:
      return Object.assign({}, state, {
        // patient: action.response.result,
        error: null,
      });

    case PatientConstants.SET_VARIABLE_SUCCESS:
    case PatientConstants.GET_VARIABLE_SUCCESS:
      let data = {
        error: null,
      };
      data[action.response.variableName] = action.response.result
        ? action.response.result.absoluteValue
        : null;
      return Object.assign({}, state, data);

    case PatientConstants.SET_VARIABLE_ERROR:
    case PatientConstants.GET_VARIABLE_ERROR:
      return Object.assign({}, state, {
        error: action.error,
      });

    case PatientConstants.GET_TREATMENTS_SUCCESS:
      return Object.assign({}, state, {
        treatments: action.response.patientTreatments,
        error: null,
      });
    case PatientConstants.GET_TREATMENTS_ERROR:
      return Object.assign({}, state, {
        treatments: [],
        error: action.error,
      });

    case PatientConstants.UPDATE_TREATMENT_SUCCESS:
    case PatientConstants.ADD_TREATMENT_SUCCESS:
      return Object.assign({}, state, {
        // treatments: action.response.result,
        error: null,
      });

    case PatientConstants.UPDATE_PATIENT_ERROR:
    case PatientConstants.ADD_PATIENT_ERROR:
      return Object.assign({}, state, {
        treatments: null,
        error: action.error,
      });

    case PatientConstants.GET_GRAPH_SUCCESS:
      const graphs = { ...state.graphs };
      let config = action.response.data;
      if (action.response.type === "SYMPTOM") {
        let hasData = false;
        if (config && config.series.length > 0) {
          config.series.forEach((item, index) => {
            if (item.data.length > 0) {
              hasData = true;
            }
          });
        }
        if (hasData) {
          config.yAxis.min = 0;
          config.yAxis.tickInterval = 1;
          config.yAxis.max = 10;
        } else {
          config = null;
        }
      }
      graphs[action.response.type] = config;
      return Object.assign({}, state, {
        graphs,
        error: null,
      });

    case PatientConstants.GET_PROFILE_QUALITY_SUCCESS:
      return Object.assign({}, state, {
        profileQuality: action.response,
        error: null,
      });
    case PatientConstants.GET_PROFILE_QUALITY_ERROR:
      return Object.assign({}, state, {
        profileQuality: null,
        error: action.error,
      });

    case PatientConstants.GET_FORM_VARIABLES_SUCCESS:
      const { result, groupName } = action.response;
      let forms = cloneDeep(state.forms);
      forms[groupName] = result;
      return Object.assign({}, state, {
        forms,
        error: null,
      });
    case PatientConstants.GET_FORM_VARIABLES_ERROR:
      return Object.assign({}, state, {
        error: action.error,
      });

    default:
      return state;
  }
}

export default PatientReducer;
