import { PathologyConstants } from "../constants";

const initialState = {
  pathologies: [],
  error: null,
};

function PathologyReducer(state = initialState, action) {
  switch (action.type) {
    case PathologyConstants.GET_PATHOLOGIES_SUCCESS:
      return Object.assign({}, state, {
        pathologies: action.response.data,
        error: null,
      });
    case PathologyConstants.GET_PATHOLOGIES_ERROR:
      return Object.assign({}, state, {
        pathologies: [],
        error: action.error,
      });

    default:
      return state;
  }
}

export default PathologyReducer;
