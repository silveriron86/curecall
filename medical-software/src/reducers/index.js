import { combineReducers } from "redux";
import { routerReducer as routing } from "react-router-redux";
import AccountReducer from "./AccountReducer";
import PatientReducer from "./PatientReducer";
import PathologyReducer from "./PathologyReducer";

const rootReducer = combineReducers({
  AccountReducer,
  PatientReducer,
  PathologyReducer,
  routing,
});

export default rootReducer;
