import { AccountConstants } from "../constants";

const initialState = {
  userData: {},
  error: null,
};

function AccountReducer(state = initialState, action) {
  switch (action.type) {
    case AccountConstants.LGOIN:
      return Object.assign({}, state, {
        userData: {},
        error: null,
      });
    case AccountConstants.LOGIN_SUCCESS:
      localStorage.setItem("USER_DATA", JSON.stringify(action.response));
      return Object.assign({}, state, {
        userData: action.response,
        error: null,
      });
    case AccountConstants.LOGIN_ERROR:
      localStorage.removeItem("USER_DATA");
      return Object.assign({}, state, {
        userData: {},
        error: action.error,
      });

    case AccountConstants.REGISTER_SUCCESS:
      localStorage.setItem("USER_DATA", JSON.stringify(action.response));
      return Object.assign({}, state, {
        userData: action.response,
        error: null,
      });
    case AccountConstants.REGISTER_ERROR:
      localStorage.removeItem("USER_DATA");
      return Object.assign({}, state, {
        userData: {},
        error: action.error,
      });

    default:
      return state;
  }
}

export default AccountReducer;
