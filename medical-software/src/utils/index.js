import moment from "moment";
import jQuery from "jquery";

let Utils = {
  AuthHelper: {
    isLoggedIn: () => {
      console.log(localStorage.getItem("TOKEN"));
      return localStorage.getItem("TOKEN");
    },
  },

  capitalize: (s) => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.toLowerCase().slice(1);
  },

  getUserPhone: (user) => {
    let phone = "";
    if (user && user.phone) {
      phone = user.phone.replace("+33", "0");
      const tmp = phone.match(/.{1,2}/g);
      phone = tmp.join(" ");
    }
    return phone;
  },

  getAge: (birthdate) => {
    return birthdate ? moment().diff(moment(birthdate), "years") : 0;
  },

  getFullName: (record) => {
    return `${record.firstname ? record.firstname : ""} ${
      record.lastname ? record.lastname : ""
    }`.trim();
  },

  getHeader: () => {
    return {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("TOKEN")}`,
    };
  },

  splineGraphTooltipFormatter: function (v) {
    var toolTipTxt = "<b>" + moment.unix(this.x / 1000).format("LLL") + "</b>";
    jQuery.each(this.points, function (i, point) {
      toolTipTxt +=
        '<br/><span style="font-size: 16px; color:' +
        point.series.color +
        '">  ' +
        point.series.name +
        ": " +
        point.y +
        "</span>";
    });
    return toolTipTxt;
  },

  splineGraphLabesFormatter: (v) => {
    return Utils.capitalize(moment(v.value).format("MMM"));
  },

  formatDate: (v) => {
    return moment(v).format("DD/MM/YYYY");
  },

  splineGraphPlotOptions: (color) => {
    return {
      spline: {
        color: color,
        marker: {
          radius: 5,
          fillColor: color,
          lineColor: "#00000016",
          lineWidth: 1,
        },
      },
    };
  },

  getScore: (config) => {
    if (
      config === null ||
      config.series.length === 0 ||
      config.series[0].data.length === 0
    ) {
      return null;
    }

    const score = config.series[0].data[config.series[0].data.length - 1][1];
    return score;
  },

  getGraphBG: (config) => {
    const score = Utils.getScore(config);
    let background = "transparent";
    if (!score) {
      return background;
    }

    if (score >= 5 && score < 7) {
      // yellow
      background = "#FFE7BA";
    }
    if (score >= 7 && score <= 10) {
      // red
      background = "#F7B8B829";
    }
    return background;
  },

  getGraphColor: (score, defalutColor) => {
    let color = defalutColor ? defalutColor : "#A5A5A5";

    if (score >= 5 && score < 7) {
      // yellow
      color = "orange";
    }
    if (score >= 7 && score <= 10) {
      // red
      color = "#E31F1F";
    }
    return color;
  },

  getMatrixValue: (history, field, matrixId) => {
    const found = history.find((hist) => hist.name === field);
    if (found) {
      const matrix = found.values.find((v) => v.patientMatrixId === matrixId);
      return matrix ? matrix.value : "";
    }
    return "";
  },

  getMatrix: (history, field, matrixId) => {
    const found = history.find((hist) => hist.name === field);
    if (found) {
      const matrix = found.values.find((v) => v.patientMatrixId === matrixId);
      return matrix;
    }
    return null;
  },

  groupByDate: (data) => {
    const grouped = data.reduce((acc, value) => {
      const date = moment(value.date).format("YYYY-MM-DD");
      // Group initialization
      if (!acc[date]) {
        acc[date] = [];
      }

      // Grouping
      acc[date] = value;
      return acc;
    }, {});
    return Object.values(grouped);
  },
};

export default Utils;
