import React from "react";
import { Router, Route, Redirect } from "react-router";
import LoginPage from "./containers/Auth/LoginPage";
import MainPage from "./containers/Main";
// import ErrorPage from "./containers/404/ErrorPage";
import SignupPage from "./containers/Auth/SignupPage";
import ForgotPage from "./containers/Auth/ForgotPage";
import Utils from "./utils";
import history from "./store/history";

export default (
  <Router history={history}>
    {/* <Route exact path="/" component={LoginPage} /> */}
    <Route
      exact
      path="/"
      render={() => (
        <Redirect to={`/${Utils.AuthHelper.isLoggedIn() ? "main" : "login"}`} />
      )}
    />
    <Route
      exact
      path="/main"
      render={() =>
        Utils.AuthHelper.isLoggedIn() ? <MainPage /> : <Redirect to="/login" />
      }
    />
    <Route
      exact
      path="/main/:patientId"
      render={(props) =>
        Utils.AuthHelper.isLoggedIn() ? (
          <MainPage match={props.match} />
        ) : (
          <Redirect to="/login" />
        )
      }
    />
    <Route path="/login" component={LoginPage} />
    <Route path="/signup" component={SignupPage} />
    <Route path="/forgot" component={ForgotPage} />
    {/* <Route path="*" component={ErrorPage} /> */}
  </Router>
);
