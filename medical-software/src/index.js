import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Root from "./containers/Root";
import history from "./store/history";
import { syncHistoryWithStore } from "react-router-redux";
import configureStore from "./store/configureStore";

import * as serviceWorker from "./serviceWorker";
const store = configureStore();
const syncHistory = syncHistoryWithStore(history, store);

ReactDOM.render(
  <Root store={store} history={syncHistory} />,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
