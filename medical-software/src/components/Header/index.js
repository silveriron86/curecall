import React from "react";
import { connect } from "react-redux";
import { UserName, SearchBox } from "../../containers/Main/_styles";

const searchIcon = require("../../assets/svgs/Search.svg");

class Header extends React.Component {
  render() {
    // const { user } = this.props;
    const data = localStorage.getItem("USER_DATA");
    const user = data ? JSON.parse(data) : null;
    // console.log(user);
    return (
      <>
        <SearchBox
          prefix={<img src={searchIcon} alt="" width="37" />}
          placeholder="| Rechercher"
        />
        <UserName>
          {user ? `${user.title} ${user.firstname} ${user.lastname}` : ""}
        </UserName>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.AccountReducer.userData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
