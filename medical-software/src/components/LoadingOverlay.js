import React from "react";
import { LoadingWrapper, BlurWrapper } from "./_styles";

const loadingIcon = require("../assets/images/loading.gif");

export default class LoadingOverlay extends React.Component {
  render() {
    const { loading, block } = this.props;
    if (block === true) {
      return loading === true ? (
        <BlurWrapper>
          <img src={loadingIcon} alt="" />
        </BlurWrapper>
      ) : null;
    }
    return (
      <LoadingWrapper
        title={null}
        centered
        maskClosable={false}
        visible={loading}
        footer={false}
      >
        <img src={loadingIcon} alt="" />
      </LoadingWrapper>
    );
  }
}
