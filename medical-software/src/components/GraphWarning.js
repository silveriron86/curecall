import React from "react";

const warningIcon = require("../assets/svgs/Warning.svg");
const notifyIcon = require("../assets/svgs/Notify.svg");

export default class GraphWarning extends React.Component {
  render() {
    const { score, style } = this.props;
    if (!score) {
      return null;
    }
    /*
      Score >= 5 AND Score <7  : ORANGE
      Score >=7 AND Score <=10 : RED
    */
    let icon = null;
    if (score >= 5 && score < 7) {
      icon = notifyIcon;
    }
    if (score >= 7 && score <= 10) {
      icon = warningIcon;
    }
    return icon ? (
      <img src={icon} className="warning-icon" alt="" style={style} />
    ) : null;
  }
}
