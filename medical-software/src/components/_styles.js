import styled from "@emotion/styled";
import Modal from "antd/lib/modal/Modal";
import { Button } from "antd";
import { BLUE } from "../constants/ColorConstants";

export const LoadingWrapper = styled(Modal)({
  ".ant-modal-content": {
    backgroundColor: "transparent",
    boxShadow: "none",
    padding: 0,
    ".ant-modal-body": {
      padding: 0,
      img: {
        margin: "0 auto",
        display: "block",
      },
    },
  },
  ".ant-modal-close": {
    display: "none",
  },
});

export const DetailsButton = styled(Button)({
  marginRight: 21,
  marginTop: 15,
  padding: "0 3px",
  span: {
    fontFamily: "Nexa BoldItalic",
    textDecoration: "underline",
    fontSize: 15,
    lineHeight: "18px",
    textAlign: "left",
    color: BLUE,
  },
});

export const BlurWrapper = styled("div")({
  width: "100%",
  height: "100%",
  position: "absolute",
  zIndex: "1000",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  background: " rgba(255,255,255,0.3)",
  backdropFilter: "blur(1px)",
});
