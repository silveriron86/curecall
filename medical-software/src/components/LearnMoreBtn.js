import React from "react";
import { Button } from "antd";
import { LearnMore } from "../containers/Main/PatientRecord/_styles";
import { ImageButton } from "../containers/Main/_styles";

const closeIcon = require("../assets/svgs/Close litte.svg");

export default class LearnMoreBtn extends React.Component {
  render() {
    const { onOpen, onClose } = this.props;
    return (
      <LearnMore>
        <Button type="text" className="open-btn" onClick={onOpen}>
          En savoir plus sur ces données
        </Button>
        <ImageButton className="close-btn" onClick={onClose}>
          <img src={closeIcon} alt="" width="12" />
        </ImageButton>
      </LearnMore>
    );
  }
}
