import styled from "@emotion/styled";
import { Col } from "antd";
import { LIGHT_GREY, GREY, WHITE, BLUE } from "../../constants/ColorConstants";

const loginBG = require("../../assets/images/login_bg.png");

export const fullCenter = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
};

export const LoingBackground = styled(Col)({
  background: `url(${loginBG}) center no-repeat`,
  backgroundSize: "100% 100%",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  p: {
    position: "absolute",
    bottom: 35,
    color: "white",
    fontFamily: "Nexa Heavy",
    fontSize: 25,
    lineHeight: "38px'",
  },
});

export const MainWrapper = styled(Col)({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  background: LIGHT_GREY,
  boxShadow: "-3px 0px 20px rgba(126, 126, 126, 0.29)",
  ".sub-title": {
    fontFamily: "Nexa Regular",
    fontSize: 15,
    lineHeight: "18px",
    marginTop: 23,
    marginBottom: 19,
    color: GREY,
    maxWidth: 598,
    textAlign: "center",
  },
  ".back-link": {
    height: "fit-content",
    fontFamily: "Nexa Heavy",
    fontSize: 25,
    lineHeight: "38px",
    color: BLUE,
    padding: 0,
    marginTop: 47,
    marginBottom: 14,
    float: "left",
  },
  "&.forgot-page": {
    ".ant-form-item-explain": {
      visibility: "hidden !important",
      height: "20px !important",
    },
  },
  ".ant-form": {
    width: 492,
    maxWidth: "90%",
    ".ant-form-item-has-error": {
      ".ant-input, .ant-input-password, .ant-picker, .ant-select": {
        filter: "drop-shadow(1px 1px 10px rgba(227, 31, 31, 0.5))",
      },
      ".ant-form-item-explain": {
        visibility: "hidden",
      },
    },
    ".ant-input": {
      "&:focus": {
        borderWidth: 0,
        outline: "none",
        boxShadow: "3px 3px 20px rgba(211, 211, 211, 0.6)",
      },
    },
    input: {
      height: 65,
      borderRadius: 40,
      backgroundColor: WHITE,
      color: BLUE,
      fontFamily: "Nexa Bold",
      fontSize: 25,
      lineHeight: "33px",
      padding: "5px 40px 0",
      borderWidth: "0 !important",
      "&.first-name": {
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
        boxShadow: "none",
      },
      "&.last-name": {
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        boxShadow: "none",
      },
      "&::placeholder": {
        color: `${BLUE}40`,
        textAlign: "left",
      },
    },
    ".ant-input-password": {
      "&.ant-input-affix-wrapper": {
        "&:focus, &:hover, &-focused": {
          border: "none !important",
        },
      },
      height: 65,
      borderRadius: 40,
      paddingRight: 25,
      input: {
        marginTop: -4,
        marginLeft: -10,
        borderRadius: 0,
        backgroundColor: "transparent",
        boxShadow: "none !important",
      },

      ".ant-input-suffix": {
        width: 56,
        marginLeft: "0 !important",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      },
    },
    ".remember-row": {
      ".ant-checkbox-wrapper": {
        span: {
          color: GREY,
          fontFamily: "Nexa Light",
          fontSize: 15,
          lineHeight: "20px",
        },
        ".label": {
          float: "left",
          width: 120,
          marginLeft: 10,
        },
        ".ant-checkbox": {
          float: "left",
          marginTop: 3,
          ".ant-checkbox-inner": {
            width: 17,
            height: 17,
            borderRadius: 8.5,
            background: WHITE,
            boxShadow: "3px 3px 20px rgba(211, 211, 211, 0.6)",
            borderWidth: "0 !important",
            outline: "none !important",
          },
          "&.ant-checkbox-checked": {
            "&::after": {
              borderWidth: 0,
            },
            ".ant-checkbox-inner": {
              background:
                "linear-gradient(#33c 0%, rgba(240, 239, 240, 0) 100%)",
              "&::after": {
                borderWidth: 0,
                content: '""',
                transform: "none",
                transition: "none",
              },
            },
          },
        },
      },
    },
    ".names": {
      marginBottom: 24,
      borderRadius: 40,
      boxShadow: "3px 3px 20px rgba(211,211,211,0.6)",
      ".ant-form-item": {
        marginBottom: 0,
        ".ant-form-item-explain": {
          display: "none",
        },
      },
    },
  },
  "a.forgot-pwd": {
    float: "right",
    marginRight: -12,
    background: "transparent !important",

    span: {
      color: BLUE,
      fontFamily: "Nexa Light",
      fontSize: 15,
      lineHeight: "20px",
      paddingRight: 11,
    },
  },
  "&.treatment": {
    background: "transparent",
    boxShadow: "none",
    display: "block",
    ".ant-form": {
      width: "100%",
      maxWidth: "100%",
      "input.ant-input": {
        padding: "0 15px",
      },
      ".ant-picker-input": {
        input: {
          height: "auto",
        },
      },
      ".ant-form-item-has-error .ant-form-item-explain": {
        display: "none",
      },
    },
  },
});

export const ThemePhoneInput = styled("div")({
  ".react-tel-input": {
    input: {
      width: "100%",
      border: 0,
      borderRadius: 40,
      boxShadow: "3px 3px 20px rgba(211,211,211,0.6)",
      paddingLeft: 60,
    },
    ".flag-dropdown": {
      border: 0,
      background: "transparent",
      marginLeft: 15,
      ".selected-flag": {
        background: "transparent",
      },
    },
  },
  "&.invalid": {
    input: {
      filter: "drop-shadow(1px 1px 10px rgba(227,31,31,0.5))",
    },
  },
  marginBottom: 24,
});
