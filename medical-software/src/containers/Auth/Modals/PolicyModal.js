import React from "react";
import { ThemeModal, ImageButton } from "../../Main/_styles";

const closeIcon = require("../../../assets/svgs/Close.svg");

export default class PolicyModal extends React.Component {
  handleOk = () => {};

  handleCancel = () => {};

  render() {
    const { visible, onClose } = this.props;

    return (
      <ThemeModal
        className="learn-more privacy-modal"
        visible={visible}
        footer={null}
        closable={false}
        width={1200}
        centered
      >
        <div className="modal-wrapper">
          <h1>
            CONDITIONS GENERALES D’UTILISATION DU LOGICIEL WEB DE CURECALL
          </h1>
          <h2>1. Préambule</h2>
          <p>
            Les présentes Conditions Générales d’Utilisation (« CGU ») régissent
            l’utilisation par les médecins prescripteurs du logiciel web de la
            société Curecall et de tout contenu et fonctionnalité accessibles
            via le logiciel web éditée par la société Curecall (ci-après le «
            Service Curecall »).
          </p>
          <h2>2. Mentions légales</h2>
          <p>
            Le Service Curecall est édité par la société Curecall, Société par
            actions simplifiée, dont le siège social est situé :
          </p>
          <p>
            15 rue de montmorency 75003 Paris
            <br />
            <a href="mailto:hello@curecall.fr">hello@curecall.fr</a>
          </p>
          <p>
            Tél : 06.52.38.50.67
            <br />
            RCS de Paris n° 883 461 832– N° TVA : FR 70 815 293
            <br />
            Directeur de publication : Mohammed El Bojaddaini
          </p>
          <p>(ci-après « Curecall ») </p>
          <p>
            Le Service Curecall est hébergé par la société Corey, hébergeur
            agréé de données de santé.
          </p>

          <h2>3. Modification des CGU</h2>
          <p>
            Curecall peut modifier à tout moment les clauses non substantielles
            des CGU (clauses qui ne fixent pas les obligations principales du
            médecin ou celles de Curecall aux termes desdites CGU). Curecall en
            informera le médecin au préalable et la poursuite de l’utilisation
            du Service Curecall par le médecin après toute modification
            emportera son acceptation des CGU telles que modifiées.
          </p>
          <p>
            En cas de modifications substantielles des CGU (clauses modifiant
            les conditions financières, la durée, …), Curecall portera ces
            modifications à la connaissance du médecin avant leur entrée en
            vigueur. Le médecin devra alors accepter les nouvelles CGU avant de
            continuer à utiliser le Service Curecall.
          </p>
          <h2>4. Configuration technique</h2>
          <p>
            La configuration minimale requise pour pouvoir utiliser le Service
            Curecall est la suivante :
            <br />
            <ul>
              <li>Un Mac </li>
              <li>Un PC </li>
            </ul>
            Le matériel (ordinateur, box internet…) et les logiciels
            (navigateur, …) permettant l’accès et/ou l’utilisation du Service
            Curecall sont à la charge exclusive du médecin, de même que les
            frais de télécommunication induits par son utilisation.
          </p>
          <h2>5. Présentation générale du logiciel</h2>
          <p>
            Le Service Curecall a été conçu pour être un outil de communication
            pratique avec les patients afin de permettre au médecin de suivre
            plus aisément leurs patients qui peuvent, eux, gérer leur prise en
            charge et suivi médical par SMS.{" "}
          </p>
          <p>
            Cet outil conçu par Curecall assure la continuité des soins entre
            les consultations et optimise la communication entre le médecin et
            le patient. Le Service Curecall permet au médecin de gagner un temps
            significatif dans la gestion et le suivi des dossiers de chaque
            patient tout en améliorant la relation au patient en lui apportant
            un support et une aide dans sa prise en charge.
          </p>
          <p>
            Le Service Curecall permet ainsi au médecin de :
            <br />
            <ul>
              <li>
                Visualiser les dossiers de chaque patient qui contient nom,
                prénom, numéro de téléphone ;
              </li>
              <li>
                Suivre les événements médicaux en lien avec le suivi du patient
                ;
              </li>
              <li>
                Suivre les données de vie réelle du patient comprenant les
                résultats des questionnaires de qualité de vie, des test de la
                fonction visuelle ainsi que le suivi de son observance ;
              </li>
              <li>
                Avoir accès au système d’alerte qui remonte les anomalies en
                lien avec le parcours du patient.
              </li>
            </ul>
          </p>

          <h2>6. Création du compte utilisateur du médecin</h2>
          <p>
            L’utilisation du Service Curecall est réservé aux seules personnes
            ayant créé un compte utilisateur après avoir renseigné le formulaire
            de téléchargement et téléchargé le logiciel Curecall.
            <br />
            En s’inscrivant le médecin certifie et garantit toutes les
            informations qu’il a fourni au moment de l’inscription sont exactes,
            correctes et à jour et assume par les présentes la responsabilité de
            l’exactitude et de la véracité desdites informations fournies. Le
            médecin s’engage également à mettre à jour l’ensemble des
            informations relatives à son compte utilisateur en cas de changement
            de ces informations.
          </p>
          <p>
            Toute fausse déclaration, notamment en vue d’usurper l’identité d’un
            tiers, est de nature à entraîner la suppression définitive de
            l’inscription du médecin sans aucune contrepartie de quelque nature
            que ce soit et sans préjudice des autres droits de Curecall.
          </p>
          <p>
            Lors de la création de son compte utilisateur il sera demandé au
            médecin de renseigner son email et un mot de passe (« Identifiants
            ») qui serviront à son identification. Le mot de passe est
            confidentiel. Dans tous les cas, le médecin convient être
            responsable de l’utilisation qui pourra être faite de ses
            Identifiants et le médecin se porte garant de leur caractère
            confidentiel, et par suite, de toute utilisation qui pourra être
            faite de son compte utilisateur. En cas de soupçon d’utilisation non
            autorisée des Identifiants du médecin par un tiers, le médecin est
            invité à modifier son mot de passe. En tout état de cause, le
            médecin s’engage à informer Curecall sans délai de toute utilisation
            non autorisée de son compte utilisateur et/ou de toute atteinte à la
            confidentialité et/ou à la sécurité de ses Identifiants.
          </p>
          <p>
            S’il est fait une utilisation non autorisée des Identifiants du
            médecin, le médecin sera responsable de tout acte résultant de
            l’utilisation détournée du Service Curecall. Curecall ne pourra en
            aucun cas être tenu responsable de toute perte ou de tout dommage
            résultant d’un non-respect des obligations du médecin stipulées au
            présent article.
          </p>

          <h2>7. Accès aux fonctionnalités du Service Curecall</h2>
          <p>
            Le médecin peut, après avoir créé son compte utilisateur, accéder
            librement à toutes les fonctionnalités du Service Curecall.
          </p>

          <h2>8. Utilisation du Service Curecall</h2>
          <p>
            Le médecin s’engage à n’utiliser le Service Curecall que pour son
            propre usage professionnel et à ne pas, de manière générale, se
            livrer à des actes, de quelque nature que ce soit, qui auraient pour
            effet de porter atteinte à l’ordre public, aux droits de Curecall ou
            aux droits de tiers ou plus généralement, d’être contraires aux
            dispositions légales, réglementaires ou aux usages en vigueur.
          </p>
          <p>
            En particulier, le médecin s’engage notamment à respecter
            scrupuleusement les règles suivantes :
            <br />
            <ul>
              <li>
                Ne pas publier d’informations inexactes ou trompeuses lors de la
                création d’un compte utilisateur, notamment pour tromper autrui
                ;
              </li>
              <li>
                Utiliser le Service Curecall de manière loyale et conformément à
                sa finalité et aux CGU ;
              </li>
              <li>
                Ne pas utiliser tout ou partie du Service Curecall aux fins de
                publicité en faveur du site web personnel du médecin ou à toutes
                autres fins commerciales ;
              </li>
            </ul>
          </p>
          <p>
            En outre, le médecin s’engage également à ne pas porter atteinte ou
            tenter de porter atteinte à l’intégrité ou à la sécurité de
            l’infrastructure informatique à la base du fonctionnement de tout ou
            partie du Service Curecall. A ce titre, le médecin s’engage,
            notamment à respecter scrupuleusement la règle suivante :
            <br />
            <ul>
              <li>
                Ne pas extraire de données figurant dans le Service Curecall et
                ce afin d’en faire une réutilisation non autorisée ;
              </li>
            </ul>
          </p>
          <p>
            En cas de manquement par le médecin à l’une ou plusieurs de ces
            règles, l’accès et/ou utilisation du Service Curecall pourra être
            suspendu temporairement ou définitivement et ce, sans mise en
            demeure.
            <br />
            En outre, le compte utilisateur du médecin pourra être bloqué
            temporairement ou définitivement par Curecall. Dans ce cas, Curecall
            en informera le médecin au préalable (sauf en cas d’urgence).
          </p>
          <h2>9. Propriété intellectuelle</h2>
          <p style={{ marginTop: 15 }}>
            <ul>
              <li>
                <b>Services et contenu du Service Curecall.</b>
                <br />
                Les services ainsi que le contenu produit et mis à disposition
                par Curecall via le Service Curecall, sont la propriété de
                Curecall ou font l’objet d’une licence de la part des tiers au
                profit de Curecall. Curecall (ou ses partenaires) est (sont)
                le(s) titulaire(s) de tous les droits de propriété
                intellectuelle tant sur la structure que sur le contenu des
                services ou a (ont) acquis régulièrement les droits permettant
                l'exploitation de la structure et du contenu. <br />
                Toute reproduction (y compris par téléchargement, impression,
                etc.), représentation, adaptation, modification, traduction,
                transformation, diffusion, intégration dans un autre site,
                exploitation commerciale ou non, et/ou réutilisation de quelque
                manière que ce soit de tout ou partie des éléments reproduits
                sur le Service Curecall est strictement interdite. Toute
                utilisation non autorisée d'un des éléments reproduits sur le
                Service Curecall pourra, sans préjudice de la suppression du
                compte utilisateur, donner lieu à des poursuites judiciaires
                civiles et/ou pénales et au paiement de dommages et intérêts.
                Par souci de clarté, il est précisé que Curecall n’acquiert
                aucun droit sur les données fournies et échangées par le médecin
                et par les patientes.
              </li>
              <li style={{ marginTop: 10 }}>
                <b>Licence d’utilisation du Service Curecall.</b>
                <br />
                Le Service Curecall peut être utilisé par le médecin
                conformément aux dispositions des présentes CGU.
                <br />
                Curecall accorde au médecin une licence non-exclusive,
                personnelle et non transférable d’utilisation du Service
                Curecall et ce conformément aux présentes CGU. Cette licence est
                révocable à tout moment.
                <br />
                Cette licence donne au médecin les droits, strictement énumérés,
                d’accéder au Service Curecall et d’utiliser le contenu mis
                exclusivement à la disposition du médecin dans le cadre d’une
                utilisation personnelle et ne saurait en aucun cas être utilisé
                à d’autres fins. Le médecin reconnait que certains contenus ne
                peuvent être utilisés que sur le Service Curecall et dans le
                strict respect des CGU. L’utilisation de tout instrument
                automatisé ou informatisé non autorisé par Curecall pour
                accéder, obtenir ou télécharger des contenus ou pour utiliser le
                Service Curecall est strictement interdite. Toute forme de
                commercialisation du Service Curecall (contenu et/ou
                fonctionnalités) par le médecin, gratuitement ou à titre
                onéreux, est strictement interdite.
              </li>
            </ul>
          </p>
          <h2>10. Responsabilités</h2>
          <p>
            <ul>
              <li>
                <b>Notre Responsabilité</b>
                <br />
                <p>
                  Du fait de la nature particulière du réseau Internet, Curecall
                  et son sous-traitant n’acceptent aucune responsabilité pour
                  l’indisponibilité du Service Curecall ou toute difficulté ou
                  incapacité à accéder au contenu ou toute défaillance du
                  système de communication ou de stockage qui pourrait rendre le
                  Service Curecall indisponible du fait d’évènements qui sont
                  étrangers à Curecall.
                </p>
                <p>
                  Curecall n’est également pas responsable des vitesses d’accès
                  au Service Curecall ni des dysfonctionnements du matériel
                  utilisé par le médecin.
                </p>
                <p>
                  La responsabilité de Curecall ne saurait non plus être
                  recherchée en cas d’interruption d’accès au Service Curecall
                  du fait d’opérations de maintenance, de mises à jour ou de
                  modifications de tout ou partie du Service Curecall et en cas
                  de force majeure. Si Curecall devait être tenue responsable
                  d’un dommage non prévu au présent article, la responsabilité
                  de Curecall sera limitée aux dommages certains, réels et
                  directs.
                </p>
                <p>
                  En aucun cas Curecall, ses dirigeants, ses employés et de
                  manière générale, ses représentants et partenaires, ne
                  sauraient être tenus responsables des dommages indirects
                  résultant de l’utilisation du Service Curecall, ainsi que des
                  contenus qui y sont publiés, et notamment des dommages
                  découlant de la perte de données causée par l’impossibilité
                  d’utiliser le Service Curecall.
                </p>
              </li>
              <li style={{ marginTop: 25 }}>
                <b>La Responsabilité du médecin</b>
                <br />
                <p>
                  Le médecin est entièrement responsable de la fiabilité des
                  informations transmises au patient via le Service Curecall.
                </p>
                <p>
                  Le médecin est également entièrement responsable de la
                  préservation de la sécurité et de l’intégrité de ses données,
                  de son matériel et de ses logiciels lorsqu’il accède au
                  Service Curecall ou que le médecin utilise le Service
                  Curecall.{" "}
                </p>
                <p>
                  En cas d’interruption de l’accès au Service Curecall, le
                  médecin est invité à prendre contact avec le patient afin
                  d’assurer la pérennité et le bon suivi de son traitement.
                </p>
              </li>
            </ul>
          </p>

          <h2>
            11. Désinscription du Service Curecall et Résiliation du compte
            utilisateur
          </h2>
          <p>
            <ol>
              <li>
                <b>La désinscription du Service Curecall</b>
                <p>
                  Le médecin peut se désinscrire à tout moment en envoyant sa
                  demande{" "}
                  <a href="mailto:hello@curecall.fr">hello@curecall.fr</a>. A
                  compter de la réception d’un email de confirmation de la prise
                  en compte de la désinscription du médecin par Curecall, le
                  compte utilisateur du médecin sera désactivé et le médecin
                  n’aura plus accès aux services et contenus du Service
                  Curecall.
                </p>
                <p>
                  En cas de non-respect des CGU et/ou d’utilisation frauduleuse
                  de son compte utilisateur par le médecin ou par un tiers,
                  Curecall pourra désactiver le compte utilisateur du médecin et
                  le médecin n’aura plus accès aux services et contenus du
                  Service Curecall. Dans ce cas, Curecall en informera le
                  médecin au préalable.
                </p>
              </li>
              <li style={{ marginTop: 25 }}>
                <b>La résiliation du compte utilisateur</b>
                <p>
                  Le médecin peut résilier son compte utilisateur à tout moment
                  en formulant une demande à{" "}
                  <a href="mailto:hello@curecall.fr">hello@curecall.fr</a>.
                </p>
                <p>
                  Après cette résiliation, le médecin ne pourra plus accéder à
                  ses Identifiants ou accéder à son compte ou au contenu du
                  Service Curecall.
                </p>
                <p>
                  Curecall peut résilier ce contrat à tout moment par email
                  envoyé à l’adresse email que le médecin a renseignée lors de
                  la création de son compte utilisateur, en respectant un
                  préavis raisonnable notamment en cas d’inactivité sur le
                  Service Curecall c’est-à-dire si aucune des patientes du
                  médecin n’utilisent l’application qui est dédiée aux patientes
                  et qui est mis en œuvre par Curecall.
                </p>
                <p>
                  Curecall se réserve le droit d’interrompre temporairement ou
                  définitivement l’accès au Service Curecall, notamment en cas
                  de cessation de l’activité liée à la mise à disposition du
                  Service Curecall, ou en cas de procédure collective. Curecall
                  se réserve également le droit de résilier le compte
                  utilisateur si Curecall a été informée d’un non-respect par le
                  médecin des présentes CGU.
                </p>
              </li>
              <li style={{ marginTop: 25 }}>
                <b>Conséquences de la désinscription/résiliation</b>
                <p>
                  En cas de désinscription du Service Curecall ou en cas de
                  résiliation du compte utilisateur :<br />
                  <ul>
                    <li>
                      Curecall s’engage à assurer une réversibilité des données
                      du Service Curecall c’est-à-dire des dossiers des patients
                      du médecin stockés dans l’espace de stockage du Service
                      Curecall ;
                    </li>
                    <li>
                      les dossiers des patientes qui été stockés dans l’espace
                      de stockage du Service Curecall seront supprimés dans un
                      délai de quatre-vingt-dix (90) jours à compter de la
                      réception d’un email de confirmation de la prise en compte
                      de la désinscription/résiliation par Curecall.
                    </li>
                  </ul>
                </p>
              </li>
            </ol>
          </p>

          <h2>12. Protection des données personnelles</h2>
          <p>
            <ol>
              <li>
                <b>Protection des données personnelles des patientes</b>
                <p>
                  Conformément à la loi Informatique et Libertés, Curecall agit
                  en tant que soustraitant du médecin. A ce titre Curecall
                  n’agit que sur instruction du médecin et s’engage à prendre
                  toutes précautions utiles afin de préserver la sécurité des
                  informations et notamment d’empêcher que les données ne soient
                  déformées, endommagées ou communiquées à des personnes non
                  autorisées.
                </p>
                <p>
                  Le médecin, en tant que responsable de traitement, s’engage à
                  se conformer aux exigences de la loi Informatiques et Libertés
                  lui incombant et notamment à effectuer les déclarations
                  nécessaires auprès de la CNIL, recueillir le consentement des
                  patients pour la collecte et le traitement de leurs données de
                  santé, informer les patientes du traitement de leurs données.
                  <br />
                  Le médecin est informé et accepte que Curecall utilise les
                  données anonymisées des patients à des fins de statistiques et
                  d’études
                </p>
              </li>
              <li style={{ marginTop: 25 }}>
                <b>Protection des données personnelles du médecin</b>
                <p>
                  Curecall procède à la collecte et au traitement des données
                  personnelles du médecin, dans le cadre de la mise à
                  disposition du Service Curecall et de ses fonctionnalités
                  associées.
                  <br />
                  <ul>
                    <li style={{ marginTop: 25 }}>
                      <b>
                        Finalités pour lesquelles Curecall traite les données
                        personnelles du médecin
                      </b>
                      <p>
                        Curecall traite les données personnelles du médecin pour
                        les finalités suivantes :
                      </p>
                    </li>
                    <li style={{ marginTop: 25 }}>
                      permettre au médecin de s’inscrire sur le Service
                      Curecall, en créant un compte utilisateur et de bénéficier
                      des fonctionnalités du Service Curecall (communication
                      avec les patients ; transmission de leurs résultats et
                      traitements personnalisés) ;
                    </li>
                    <li style={{ marginTop: 25 }}>
                      administrer le Service Curecall, comprendre les tendances
                      d’utilisation et améliorer son fonctionnement.
                    </li>
                    <li style={{ marginTop: 25 }}>
                      <b>Destinataires des données personnelles du médecin</b>
                      <p>
                        L’accès aux données personnelles du médecin pour les
                        finalités décrites cidessus est réservé à Curecall et
                        celles-ci ne seront en aucun cas transmises à des tiers.
                      </p>
                    </li>
                    <li style={{ marginTop: 25 }}>
                      <b>
                        Durée de conservation des données personnelles du
                        médecin
                      </b>
                      <p>
                        Les données personnelles du médecin sont conservées
                        jusqu’à la suppression du compte utilisateur.
                      </p>
                    </li>
                    <li style={{ marginTop: 25 }}>
                      <b>
                        Les droits d’accès, de rectification, de suppression,
                        d’opposition et de portabilité du médecin
                      </b>
                      <p>
                        Conformément à la législation de protection des données
                        personnelles applicable, le médecin dispose d'un droit
                        d'accès, de rectification, de suppression des
                        informations personnelles le concernant, ainsi que de la
                        possibilité de s’opposer, pour des motifs légitimes, au
                        traitement de ses données, que le médecin peut exercer à
                        tout moment en adressant un courrier à l'adresse e-mail
                        suivante :{" "}
                        <a href="mailto:hello@curecall.fr">hello@curecall.fr</a>
                        .
                      </p>
                      <p>
                        Le médecin peut demander à recevoir les données
                        personnelles le concernant qu’il a fournies à Curecall
                        dans un format structuré, couramment utilisé et lisible
                        par machine, et le médecin a le droit de transmettre ses
                        données à un autre responsable de traitement.
                      </p>
                    </li>
                  </ul>
                </p>
              </li>
            </ol>
          </p>

          <h2>13. Contact</h2>
          <p>
            Pour toute information, le médecin peut contacter Curecall par
            courrier électronique à l’adresse suivante :{" "}
            <a href="mailto:contact@Curecall.fr">contact@Curecall.fr</a>
          </p>

          <h2>14. Loi applicable et attribution de juridiction</h2>
          <p>Les présentes CGU sont soumises à la loi française.</p>

          <h2>15. Généralités</h2>
          <p>
            Dans l’hypothèse où l’une des dispositions des présentes CGU serait
            déclarée invalide ou inopposable pour quelque cause que ce soit, les
            autres dispositions demeureront applicables sans changement.
          </p>
          <p>
            Votre acceptation des présentes CGU constitue l'accord global entre
            les parties relativement aux sujets concernés et prévaut sur tout
            autre accord précédent conclu entre les parties relativement à ces
            mêmes sujets et, sauf spécification contraire dans le présent
            document, il ne peut être modifié sans le consentement écrit signé
            par un représentant habilité des deux parties.{" "}
          </p>
        </div>
        <ImageButton className="close-btn" onClick={onClose}>
          <img src={closeIcon} alt="" width="30" />
        </ImageButton>
      </ThemeModal>
    );
  }
}
