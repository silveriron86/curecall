import React from "react";
import { connect } from "react-redux";
import { Row, Form, Checkbox, Button, Col, Select, notification } from "antd";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import LoadingOverlay from "../../components/LoadingOverlay";

import {
  LoingBackground,
  MainWrapper,
  fullCenter,
  ThemePhoneInput,
} from "./_styles";
import {
  ThemeSelect,
  ThemeInput,
  ThemePassword,
  GreyButton,
  FlexWrap,
} from "../Main/_styles";
import { AccountActions } from "../../actions";
import PolicyModal from "./Modals/PolicyModal";
import { BLUE } from "../../constants/ColorConstants";
import Utils from "../../utils";

const curecall = require("../../assets/svgs/Logo Font.svg");
const logo = require("../../assets/svgs/Logo white.svg");
const eyeSelectedIcon = require("../../assets/svgs/Eyes light_selected.svg");
const eyeIcon = require("../../assets/svgs/Eyes.svg");
const leftArrowIcon = require("../../assets/svgs/left_arrow.svg");

class SignupPage extends React.Component {
  formRef = React.createRef();

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      phone: "",
      loading: false,
      remember: false,
      visiblePolicyModal: false,
      user: {},
    };
  }

  onTogglePolicyModal = () => {
    this.setState({
      visiblePolicyModal: !this.state.visiblePolicyModal,
    });
  };

  showPhoneAlert = () => {
    notification.error({
      message: "Merci de vérifier votre numéro de portable.",
      description:
        "Vous devez saisir un numéro de portable pour que nous puissions vous envoyer votre code par SMS.",
    });
  };

  onFinish = (values) => {
    console.log(values);
    const { index, phone, user, remember } = this.state;
    if (index > 0 && !phone) {
      return;
    }

    if (index === 2) {
      const { email, token } = this.props.userData;
      const { code } = values;
      this.setState(
        {
          loading: true,
        },
        () => {
          this.props.verifySMS({
            email,
            code,
            cb: (res) => {
              this.setState(
                {
                  loading: false,
                },
                () => {
                  console.log(res);
                  if (res.success) {
                    localStorage.setItem("COMPLETED_SIGNUP", "true");
                    localStorage.setItem("TOKEN", token);
                    this.props.history.push("/main");
                  } else {
                    notification.error({
                      message: "Un problème est survenu.",
                      description:
                        "Nous ne parvenons pas à vous créer un compte utilisateur, veuillez contacter Curecall à l'adresse hello@curecall.com.",
                    });
                  }
                }
              );
            },
          });
        }
      );
    } else {
      if (index === 1) {
        if (phone.length < 11) {
          this.showPhoneAlert();
          return;
        }

        const { email, password } = values;
        let data = JSON.parse(JSON.stringify(user));
        delete data.number;
        data.phone = phone;
        data.email = email;
        data.password = password;
        console.log(data);
        this.setState(
          {
            loading: true,
          },
          () => {
            this.props.register({
              data,
              cb: (res) => {
                console.log(res);
                if (res.success) {
                  if (
                    res.response.projects &&
                    res.response.projects.length > 0
                  ) {
                    localStorage.setItem(
                      "PROJECT_ID",
                      res.response.projects[0].id.toString()
                    );
                  }
                  this.setState({
                    index: index + 1,
                    loading: false,
                  });
                } else {
                  this.setState(
                    {
                      loading: false,
                    },
                    () => {
                      const { msg } = res.response.data.errors;
                      if (msg && msg.indexOf("already taken") > 0) {
                        // when phone or email already exist
                        notification.error({
                          message: "Un compte existe déjà.",
                          description:
                            "Vous pouvez faire un rappel de mot de passe directement depuis la page de connexion ou envoyer un email à hello@curecall.com.",
                        });
                      } else {
                        notification.error({
                          message: "Un problème est survenu.",
                          description:
                            "Nous ne parvenons pas à vous créer un compte utilisateur, veuillez contacter Curecall à l'adresse hello@curecall.com.",
                        });
                      }
                    }
                  );
                }
              },
            });
          }
        );
      } else {
        if (!remember) {
          notification.error({
            message: "Veuillez accepter les conditions générales de Curecall",
            description:
              "Pour créer un compte vous devez accepter les conditions générales.",
          });
          return;
        }
        if (typeof user.firstname !== "undefined") {
          // already pre-filled by RPPS
          this.setState({
            user: {
              ...user,
              ...values,
            },
            index: index + 1,
          });
        } else {
          this.setState({
            user: JSON.parse(JSON.stringify(values)),
            index: index + 1,
          });
        }
      }
    }
  };

  onFinishFailed = (errorInfo) => {
    const { index, phone } = this.state;

    if (errorInfo.errorFields[0].name[0] === "email") {
      if (
        errorInfo.errorFields[0].errors[0].indexOf("is not a valid email") > 0
      ) {
        notification.error({
          message: "Merci de bien vouloir vérifier votre adresse email.",
          description: "Prenez le temps de vérifier la syntaxe de votre email.",
        });
        return;
      }
    }

    console.log(index, phone);
    if (index === 1 && phone === "") {
      this.showPhoneAlert();
    } else {
      notification.error({
        message: "Des informations sont manquantes.",
        description:
          "Veillez à bien remplir tous les champs pour que nous puissions vous créer un compte.",
      });
    }
  };

  onChangeRpps = (e) => {
    const value = e.target.value;
    if (!value || value.trim() === "") {
      return;
    }

    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.getRpps({
          rppsId: value,
          cb: (res) => {
            console.log(res);
            if (res.success) {
              let user = res.response.user;
              user.street = `${user.streetNumber} ${user.street}`.trim();
              delete user.streetNumber;
              user.nationalRegisterID = value
              let {
                firstname,
                lastname,
                civility,
                companyEmail,
                companyPhone,
              } = user;

              if (civility) {
                civility = `${Utils.capitalize(civility)}.`;
                user.civility = civility;
              }

              if (companyPhone && companyPhone.length > 0) {
                const phone = companyPhone.substr(1);
                console.log("companyPhone.substr(1) = ", phone);
                const formattedPhone = `+33${phone}`;
                this.setState({
                  phone: formattedPhone,
                });
                this.formRef.current.setFieldsValue({
                  phone: formattedPhone,
                });
                user.companyPhone = formattedPhone;
              }

              this.setState(
                {
                  user,
                  loading: false,
                },
                () => {
                  this.formRef.current.setFieldsValue({
                    firstname,
                    lastname,
                    title: civility,
                    email: companyEmail,
                  });
                }
              );
            } else {
              this.setState({
                user: {},
                loading: false,
              });
            }
          },
        });
      }
    );
  };

  handeOnChange = (value) => {
    this.setState({
      phone: value,
    });
  };

  onRemember = (e) => {
    this.setState({
      remember: e.target.checked,
    });
  };

  render() {
    const subTitles = [
      "Renseigner votre numéro de RPPS et gagner du temps",
      "Utilisez votre numéro de mobile pour vous inscrire",
      "Veuillez saisir le code reçu par SMS au",
    ];
    const { index, loading, phone, remember, visiblePolicyModal } = this.state;
    const { userData } = this.props;
    const layout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };
    console.log("userData = ", userData, remember);

    return (
      <Row style={{ minHeight: "125vh" }}>
        <LoingBackground md={11}>
          <img src={logo} alt="" width="209" />
          <p>Beyond the drop</p>
        </LoingBackground>
        <MainWrapper md={13}>
          <img src={curecall} alt="" width="241" />
          <div className="sub-title">{subTitles[index]}</div>
          <Form
            {...layout}
            name="basic"
            ref={this.formRef}
            initialValues={{ number: "10100" }}
            onFinish={this.onFinish}
            onFinishFailed={this.onFinishFailed}
          >
            <div style={{ height: 453 }}>
              {index === 0 && (
                <>
                  <Form.Item
                    name="number"
                    rules={[
                      {
                        required: true,
                        message: "Please input your RPPS number!",
                      },
                    ]}
                  >
                    <ThemeInput
                      placeholder="| N° RPPS"
                      // className="no-icon"
                      // visibilityToggle={false}
                      // iconRender={(visible) =>
                      //   visible ? (
                      //     <img src={eyeSelectedIcon} alt="" width="56" />
                      //   ) : (
                      //     <img src={eyeIcon} alt="" width="43" />
                      //   )
                      // }
                      maxLength={11}
                      onBlur={this.onChangeRpps}
                    />
                  </Form.Item>

                  <Form.Item
                    name="title"
                    rules={[
                      { required: true, message: "Please input your title!" },
                    ]}
                  >
                    <ThemeSelect placeholder="| Titre: Dr / Pr">
                      <Select.Option value="Dr.">
                        <div className="select-label">Dr</div>
                      </Select.Option>
                      <Select.Option value="Pr.">
                        <div className="select-label">Pr</div>
                      </Select.Option>
                    </ThemeSelect>
                  </Form.Item>

                  <Row className="names">
                    <Col md={14}>
                      <Form.Item
                        name="firstname"
                        rules={[
                          {
                            required: true,
                            message: "Please input your first name!",
                          },
                        ]}
                      >
                        <ThemeInput
                          placeholder="| Prénom"
                          className="first-name"
                        />
                      </Form.Item>
                    </Col>
                    <Col md={10}>
                      <Form.Item
                        name="lastname"
                        rules={[
                          {
                            required: true,
                            message: "Please input your last name!",
                          },
                        ]}
                      >
                        <ThemeInput placeholder="| Nom" className="last-name" />
                      </Form.Item>
                    </Col>
                  </Row>
                </>
              )}

              {index === 1 && (
                <>
                  {/* <ThemePhoneInput>
                    <Form.Item
                      name="phone"
                      rules={[
                        {
                          required: true,
                          message: "Please input your phone number!",
                        },
                      ]}
                    >
                      <PhoneInput
                        country={"fr"}
                        inputProps={{
                          name: "phone",
                          required: true,
                          autoFocus: true,
                        }}
                        value={phone}
                      />
                    </Form.Item>
                  </ThemePhoneInput> */}
                  <ThemePhoneInput
                    className={
                      phone !== "" && phone.length === 11 ? "" : "invalid"
                    }
                  >
                    <PhoneInput
                      country={"fr"}
                      inputProps={{
                        name: "phone",
                        required: true,
                        autoFocus: true,
                      }}
                      value={phone}
                      onChange={this.handeOnChange}
                    />
                  </ThemePhoneInput>

                  <Form.Item
                    name="email"
                    rules={[
                      {
                        required: true,
                        type: "email",
                        // message: "Please input your email!",
                      },
                    ]}
                  >
                    <ThemeInput placeholder="| Email" />
                  </Form.Item>

                  <Form.Item
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Please input your password!",
                      },
                    ]}
                  >
                    <ThemePassword
                      placeholder="| Mot de passe"
                      iconRender={(visible) =>
                        visible ? (
                          <img src={eyeSelectedIcon} alt="" />
                        ) : (
                          <img src={eyeIcon} alt="" width="43" />
                        )
                      }
                    />
                  </Form.Item>

                  <Form.Item
                    name="confirm"
                    dependencies={["password"]}
                    rules={[
                      {
                        required: true,
                        message: "Please confirm your password!",
                      },
                      ({ getFieldValue }) => ({
                        validator(rule, value) {
                          if (!value || getFieldValue("password") === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            "The two passwords that you entered do not match!"
                          );
                        },
                      }),
                    ]}
                  >
                    <ThemePassword
                      placeholder="| Confirmez le mot de passe"
                      iconRender={(visible) =>
                        visible ? (
                          <img src={eyeSelectedIcon} alt="" />
                        ) : (
                          <img src={eyeIcon} alt="" width="43" />
                        )
                      }
                    />
                  </Form.Item>
                </>
              )}

              {index === 2 && (
                <>
                  <div style={{ height: 90 }} />
                  {/* <ThemePhoneInput>
                    <Form.Item
                      name="phone"
                      rules={[
                        {
                          required: true,
                          message: "Please input your phone number!",
                        },
                      ]}
                    >
                      <PhoneInput
                        country={"fr"}
                        inputProps={{
                          name: "phone",
                          required: true,
                          autoFocus: true,
                        }}
                        value={phone}
                      />
                    </Form.Item>
                  </ThemePhoneInput> */}

                  <ThemePhoneInput className={phone !== "" ? "" : "invalid"}>
                    <PhoneInput
                      country={"fr"}
                      inputProps={{
                        name: "phone",
                        required: true,
                        autoFocus: true,
                      }}
                      value={phone}
                      onChange={this.handeOnChange}
                    />
                  </ThemePhoneInput>

                  <Form.Item
                    name="code"
                    rules={[
                      { required: true, message: "Please input the code!" },
                    ]}
                  >
                    <ThemeInput placeholder="Le code" />
                  </Form.Item>
                </>
              )}
              {index === 0 && (
                <FlexWrap className="remember-row">
                  <Col>
                    {/* <Form.Item
                      name="remember"
                      valuePropName="checked"
                      rules={[
                        {
                          required: true,
                        },
                      ]}
                    > */}
                    <Checkbox checked={remember} onChange={this.onRemember}>
                      <span
                        className="label"
                        style={{ width: "auto", marginTop: 3 }}
                      >
                        J’accepte
                      </span>
                    </Checkbox>
                    <span
                      style={{
                        color: BLUE,
                        textDecoration: "underline",
                        marginLeft: -10,
                        cursor: "pointer",
                      }}
                      onClick={this.onTogglePolicyModal}
                    >
                      les conditions d’utilisation du service et les conditions
                      <span
                        style={{ width: 29, display: "inline-block" }}
                      ></span>
                      générales Curecall
                    </span>
                    {/* </Form.Item> */}
                  </Col>
                </FlexWrap>
              )}
            </div>
            <PolicyModal
              visible={visiblePolicyModal}
              onClose={this.onTogglePolicyModal}
            />

            <div style={fullCenter}>
              <GreyButton htmlType="submit">S’inscrire</GreyButton>
            </div>
            {index === 0 && (
              <div style={fullCenter}>
                <Button
                  type="link"
                  href="/login"
                  className="forgot-pwd"
                  style={{ marginTop: 9 }}
                >
                  <img src={leftArrowIcon} alt="" width="46" />
                  Vous avez déjà un compte? Se connecter
                </Button>
              </div>
            )}
          </Form>
          <LoadingOverlay loading={loading} />
        </MainWrapper>
      </Row>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    error: state.AccountReducer.error,
    userData: state.AccountReducer.userData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getRpps: (req) => {
      dispatch(AccountActions.getRpps(req.rppsId, req.cb));
    },
    register: (req) => {
      dispatch(AccountActions.register(req.data, req.cb));
    },
    verifySMS: (req) => {
      dispatch(AccountActions.verifySMS(req.email, req.code, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignupPage);
