import React from "react";
import { connect } from "react-redux";
import { Row, Form, Button, notification } from "antd";
import { LoingBackground, MainWrapper, fullCenter } from "./_styles";
import { ThemeInput, ThemePassword, GreyButton } from "../Main/_styles";
import { AccountActions } from "../../actions";

const curecall = require("../../assets/svgs/Logo Font.svg");
const logo = require("../../assets/svgs/Logo white.svg");
const leftArrowIcon = require("../../assets/svgs/left_arrow.svg");
const eyeSelectedIcon = require("../../assets/svgs/Eyes light_selected.svg");
const eyeIcon = require("../../assets/svgs/Eyes.svg");

class ForgotPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      phone: "",
    };
  }

  onFinish = (values) => {
    const { index } = this.state;
    if (index === 0) {
      // Request SMS login code (GET /login/sms/:phone)
      const { phone } = values;
      this.setState(
        {
          phone,
        },
        () => {
          this.props.getLoginSMS({
            phone,
            cb: (res) => {
              if (res.status === 200) {
                this.setState({ index: index + 1 });
              } else {
                if (res.status === 403) {
                  notification.error({
                    message: "Vérifier votre numéro de mobile",
                    description:
                      "Le numéro de mobile saisi ne correspond pas à un compte Curecall.",
                  });
                } else {
                  notification.error({
                    message: "Erreur",
                    description: res.data.errors.msg,
                  });
                }
              }
            },
          });
        }
      );
    } else if (index === 1) {
      const { phone } = this.state;
      const { code } = values;
      this.props.postLoginSMS({
        data: {
          phone,
          code,
        },
        cb: (res) => {
          if (res.status === 200) {
            this.setState({ index: index + 1 });
            localStorage.setItem("USER_ID", res.data.id);
            localStorage.setItem("TOKEN", res.data.token);
          } else {
            notification.error({
              message: "Erreur",
              description: res.data.errors.msg,
            });
          }
        },
      });
    } else {
      const { password } = values;
      const user_id = localStorage.getItem("USER_ID");
      this.props.updateUser({
        user_id,
        data: {
          password,
        },
        cb: (res) => {
          if (res.status === 200) {
            window.location.href = "/main";
          } else {
            notification.error({
              message: "Erreur",
              description: res.data.errors.msg,
            });
          }
        },
      });
    }
  };

  goBack = () => {
    const { index } = this.state;
    if (index === 0) {
      this.props.history.goBack();
    } else {
      this.setState({ index: index - 1 });
    }
  };

  onFieldsChange = (changedFields, allFields) => {
    const found = allFields.find((field) => {
      return field.errors.length > 0;
    });
    if (found) {
      // const type = found.errors[0];
      // this._setError(type);
    }
  };

  onFinishFailed = (errorInfo) => {
    const type = errorInfo.errorFields[0].errors[0];
    this._setError(type);
  };

  _setError = (errorType) => {
    if (errorType === "DISMATCH_PASSWORD") {
      notification.error({
        message: "Recommencez",
        description:
          "Les mots de passe ne sont pas identiques, utilisez l'oeil pour les afficher.",
      });
    } else if (errorType === "INVALID_PASSWORD") {
      notification.error({
        message: "Mot de passe trop faible",
        description:
          "Votre mot de passe doit faire 8 caractères et doit contenir des lettres, chiffres, au moins une majuscule et un caractère spécial.",
      });
    }
  };

  isValidPassword = (rule, value, callback) => {
    if (
      !value.match(
        "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,15}$"
      )
    ) {
      return false;
    }
    return true;
  };

  render() {
    const subTitles = [
      "Vous allez recevoir un code par SMS.",
      "Un code vous a été envoyé par SMS, veuillez le saisir ci-dessous.",
      "Veuillez saisir un nouveau mot de passe.<br/>Il doit contenir des lettres, chiffres, au moins une majuscule et un caractère spécial. ",
    ];
    const { index } = this.state;
    const layout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };

    return (
      <Row style={{ minHeight: "125vh" }}>
        <LoingBackground md={11}>
          <img src={logo} alt="" width="209" />
          <p>Beyond the drop</p>
        </LoingBackground>
        <MainWrapper md={13} className="forgot-page">
          <img src={curecall} alt="" width="241" />
          <div className="ant-form">
            <Button type="text" className="back-link" onClick={this.goBack}>
              <img
                src={leftArrowIcon}
                alt=""
                style={{ marginRight: 16.2 }}
                width="46"
              />
              {index === 2 ? "Le nouveau mot de passe" : "Mot de passe"}
            </Button>
          </div>

          <div
            className="sub-title"
            style={{ marginTop: index === 2 ? 0 : 23 }}
            dangerouslySetInnerHTML={{ __html: subTitles[index] }}
          ></div>
          <Form
            {...layout}
            name="basic"
            onFinish={this.onFinish}
            onFieldsChange={this.onFieldsChange}
            onFinishFailed={this.onFinishFailed}
          >
            {index === 0 ? (
              <Form.Item
                name="phone"
                rules={[
                  {
                    required: true,
                    message: "Please input your email ou buméro de mobile!",
                  },
                ]}
              >
                <ThemeInput placeholder="| Email ou Numéro de mobile" />
              </Form.Item>
            ) : index === 1 ? (
              <Form.Item
                name="code"
                rules={[{ required: true, message: "Please input the code!" }]}
              >
                <ThemeInput placeholder="| Votre code" />
              </Form.Item>
            ) : (
              <>
                <Form.Item
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: "<b>errror</b>Please input your password!123",
                    },
                    ({ getFieldValue }) => ({
                      validator(rule, value) {
                        const isValid =
                          value &&
                          value.match(
                            "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{1,100}$"
                          );
                        if (isValid) {
                          return Promise.resolve();
                        }
                        return Promise.reject("INVALID_PASSWORD");
                      },
                    }),
                  ]}
                >
                  <ThemePassword
                    placeholder="| Mot de passe"
                    iconRender={(visible) =>
                      visible ? (
                        <img src={eyeSelectedIcon} alt="" width="56" />
                      ) : (
                        <img src={eyeIcon} alt="" width="43" />
                      )
                    }
                  />
                </Form.Item>

                <Form.Item
                  name="confirm"
                  dependencies={["password"]}
                  rules={[
                    {
                      required: true,
                      message: "Please confirm your password!",
                    },
                    ({ getFieldValue }) => ({
                      validator(rule, value) {
                        if (!value || getFieldValue("password") === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject("DISMATCH_PASSWORD");
                      },
                    }),
                  ]}
                >
                  <ThemePassword
                    placeholder="| Confirmez le mot de passe"
                    iconRender={(visible) =>
                      visible ? (
                        <img src={eyeSelectedIcon} alt="" />
                      ) : (
                        <img src={eyeIcon} alt="" width="43" />
                      )
                    }
                  />
                </Form.Item>
              </>
            )}

            <div style={{ ...fullCenter, ...{ marginTop: 124 } }}>
              <GreyButton htmlType="submit">
                {index === 0 ? "Recevoir" : "Se connecter"}
              </GreyButton>
            </div>
          </Form>
        </MainWrapper>
      </Row>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    getLoginSMS: (req) => {
      dispatch(AccountActions.getLoginSMS(req.phone, req.cb));
    },
    postLoginSMS: (req) => {
      dispatch(AccountActions.postLoginSMS(req.data, req.cb));
    },
    updateUser: (req) => {
      dispatch(AccountActions.updateUser(req.user_id, req.data, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPage);
