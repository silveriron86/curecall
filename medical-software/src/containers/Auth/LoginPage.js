import React from "react";
import { connect } from "react-redux";
import { Row, Form, Checkbox, Button, Col, notification } from "antd";
import { LoingBackground, MainWrapper } from "./_styles";
import {
  ThemeInput,
  ThemePassword,
  BlueButton,
  GreyButton,
  FlexWrap,
} from "../Main/_styles";
import { AccountActions } from "../../actions";
import LoadingOveraly from "../../components/LoadingOverlay";

const curecall = require("../../assets/svgs/Logo Font.svg");
const logo = require("../../assets/svgs/Logo white.svg");
const eyeSelectedIcon = require("../../assets/svgs/Eyes light_selected.svg");
const eyeIcon = require("../../assets/svgs/Eyes.svg");
const rigthArrowIcon = require("../../assets/svgs/Come.svg");

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  onFinish = (values) => {
    this.setState(
      {
        loading: true,
      },
      () => {
        const { email, password } = values;
        this.props.login({
          data: { email, password },
          cb: (res) => {
            this.setState(
              {
                loading: false,
              },
              () => {
                if (res.status) {
                  notification.error({
                    message: "Connexion impossible",
                    description:
                      "Mot de passe ou login incorrect, utilisez l'icône de l'oeil à droite pour vérifier votre mot de passe.",
                  });
                } else {
                  if (res.projects === null || res.projects.length === 0) {
                    notification.error({
                      message: "Erreur",
                      description: (
                        <>
                          Vous n'avez accès à aucun projet pour le moment.
                          Besoin d'aide ?{" "}
                          <a href="mailto:hello@curecall.com">
                            hello@curecall.com
                          </a>
                        </>
                      ),
                      duration: 5,
                    });
                  } else {
                    localStorage.setItem(
                      "PROJECT_ID",
                      res.projects[0].id.toString()
                    );
                    localStorage.setItem("TOKEN", res.token);
                    this.props.history.push("/main");
                  }
                }
              }
            );
          },
        });
      }
    );
  };

  onFinishFailed = (errorInfo) => {
    // console.log("Failed:", errorInfo);
  };

  onSignup = () => {
    this.props.history.push("/signup");
  };

  render() {
    const { loading } = this.state;
    const layout = {
      labelCol: { span: 0 },
      wrapperCol: { span: 24 },
    };

    return (
      <Row style={{ minHeight: "125vh" }}>
        <LoingBackground md={11}>
          <img src={logo} alt="" width="209" />
          <p>Beyond the drop</p>
        </LoingBackground>
        <MainWrapper md={13}>
          <img src={curecall} alt="" width="241" />
          <div className="sub-title">Bienvenue!</div>
          <Form
            {...layout}
            name="basic"
            onFinish={this.onFinish}
            onFinishFailed={this.onFinishFailed}
          >
            <Form.Item
              name="email"
              rules={[{ required: true, message: "Please input your email!" }]}
            >
              <ThemeInput placeholder="| Email ou Numéro de mobile" />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <ThemePassword
                placeholder="| Mot de passe"
                iconRender={(visible) =>
                  visible ? (
                    <img src={eyeSelectedIcon} alt="" width="56" />
                  ) : (
                    <img src={eyeIcon} alt="" width="43" />
                  )
                }
              />
            </Form.Item>

            <FlexWrap className="remember-row">
              <Col md={9}>
                <Form.Item name="remember" valuePropName="checked">
                  <Checkbox>
                    <span className="label">
                      Rester connecté pendant 15 jours
                    </span>
                  </Checkbox>
                </Form.Item>
              </Col>
              <Col md={15}>
                <Button type="link" className="forgot-pwd" href="/forgot">
                  J’ai oublié mon mot de passe
                  <img src={rigthArrowIcon} alt="" width="46" />
                </Button>
              </Col>
            </FlexWrap>

            <FlexWrap className="between" style={{ marginTop: 40 }}>
              <BlueButton htmlType="submit">Se connecter</BlueButton>
              <GreyButton onClick={this.onSignup}>S’inscrire</GreyButton>
            </FlexWrap>
          </Form>
          <LoadingOveraly loading={loading} />
        </MainWrapper>
      </Row>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.AccountReducer.userData,
    error: state.AccountReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (req) => {
      dispatch(AccountActions.login(req.data, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
