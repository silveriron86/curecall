import "./_errorPage.css";

import React from "react";

class ErrorPage extends React.Component {
  render() {
    if (!this.props) {
      return null;
    }

    return (
      <div className="page-wrapper">
        <h1>Page not found: 404 error</h1>
      </div>
    );
  }
}

export default ErrorPage;
