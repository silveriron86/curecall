import styled from "@emotion/styled";
import { BLUE, LIGHT_GREY } from "../../../constants/ColorConstants";

export const TagBox = styled("div")({
  height: 53,
  borderRadius: 26.5,
  borderWidth: 0,
  background: LIGHT_GREY,
  opacity: 0.6,
  boxShadow: "3px 3px 3px #d3d3d3, -3px -3px 3px #fff",
  fontFamily: "Nexa Regular",
  fontSize: 20,
  lineHeight: "30px",
  color: BLUE,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "fit-content",
  padding: "0 35px",
  margin: 6,
  cursor: "pointer",
  position: "relative",
  overflow: "hidden",
  "&.selected": {
    boxShadow: "none",
    backgroundColor: "white",
    fontFamily: "Nexa Heavy",
  },
  ".selected-grad": {
    position: "absolute",
    top: 0,
    width: "100%",
    height: 35,
    borderRadius: 65,
    background: "transparent",
    border: "12px solid rgba(51, 51, 204, 0.4)",
    filter: "blur(9px)",
    borderBottom: 0,
  },
  ".selected-white-grad": {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 52.56,
    background: "transparent",
    border: "6px solid #fff",
    filter: "blur(7.555910110473633px)",
  },
});

export const EventCol = styled("div")({
  fontFamily: "Nexa Bold",
  fontSize: 18,
  lineHeight: "24px",
  margin: "0 11px 1px 0",
  "&.non": {
    color: BLUE,
  },
});

export const FirstScreen = styled("div")({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
  minHeight: "125vh",
  margin: "-27px -12px",
  h3: {
    margin: 0,
    padding: 0,
    fontFamily: "Nexa Heavy",
    fontSize: 25,
    lineHeight: "38px",
    textAlign: "center",
    color: BLUE,
  },
  p: {
    marginTop: 50,
    marginBottom: 50,
    fontFamily: "Nexa Regular",
    fontWeight: "normal",
    fontSize: 20,
    lineHeight: "30px",
    textAlign: "center",
    color: BLUE,
  },
});
