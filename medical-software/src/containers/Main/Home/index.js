import React from "react";
import { connect } from "react-redux";
import { GreyButton } from "../_styles";
import { FirstScreen } from "./_styles";
import TableContainer from "./TableContainer";
import Header from "../../../components/Header";
import { PathologyActions, PatientActions } from "../../../actions";
import LoadingOveraly from "../../../components/LoadingOverlay";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.props.getPatients({
      cb: (res) => {
        this.props.getPathologies({
          cb: () => {
            this.setState({
              loading: false,
            });
          },
        });
      },
    });
  }

  onSelectPatient = (patient) => {
    this.props.setPatient({
      patient,
      cb: () => {
        this.props.onSelectPatient(patient);
      },
    });
  };

  render() {
    const { isFirstVisit, onStart, patients, pathologies } = this.props;
    const { loading } = this.state;
    if (isFirstVisit) {
      return (
        <FirstScreen>
          <h3>Bienvenue sur CureCall</h3>
          <p>L’agent conversationnel dédié au suivi de vos patients.</p>
          <p style={{ maxWidth: 640, marginTop: 0, marginBottom: 0 }}>
            Etape par étape on vous explique comment utiliser Curecall pour une
            expérience fluide, simple et efficace pour vous et vos patients.
          </p>
          <p>Suivez vos patients autrement !</p>
          <GreyButton onClick={onStart}>C’est parti !</GreyButton>
        </FirstScreen>
      );
    }

    return (
      <>
        <Header />
        <TableContainer
          patients={patients}
          pathologies={pathologies}
          selectPatient={this.onSelectPatient}
          onNewPatient={this.props.onNewPatient}
        />
        <LoadingOveraly loading={loading} />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pathologies: state.PathologyReducer.pathologies,
    patients: state.PatientReducer.patients,
    error: state.PatientReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setPatient: (req) => {
      dispatch(PatientActions.setPatient(req.patient, req.cb));
    },
    getPatients: (req) => {
      dispatch(PatientActions.getPatients(req.cb));
    },
    getPathologies: (req) => {
      dispatch(PathologyActions.getPathologies(req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
