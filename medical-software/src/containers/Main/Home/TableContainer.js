import React from "react";
import { ThemeTable, FlexWrap, ImageButton } from "../_styles";
import { EventCol } from "./_styles";
import Utils from "../../../utils";
import plusIcon from "../../../assets/svgs/Nouveau patient.svg";

export default class TableContainer extends React.Component {
  getPathologyName = (PathologyId) => {
    const { pathologies } = this.props;
    const found = pathologies
      ? pathologies.find((p) => p.id === PathologyId)
      : null;
    return found ? found.name : "";
  };

  render() {
    const { patients } = this.props;

    const columns = [
      {
        title: "Prénom / Nom",
        dataIndex: "firstname",
        width: "25%",
        sorter: (a, b) => {
          return Utils.getFullName(a).localeCompare(Utils.getFullName(b));
        },
        showSorterTooltip: true,
        render: (firstname, record) => {
          return Utils.getFullName(record);
        },
      },
      {
        title: "Age",
        dataIndex: "birthdate",
        width: "15%",
        sorter: {
          compare: (a, b) =>
            Utils.getAge(a.birthdate) - Utils.getAge(b.birthdate),
        },
        showSorterTooltip: true,
        render: (birthdate) => {
          return Utils.getAge(birthdate);
        },
      },
      {
        title: "Pathologie",
        dataIndex: "PathologyId",
        width: "25%",
        sorter: {
          compare: (a, b) =>
            this.getPathologyName(a.PathologyId).localeCompare(
              this.getPathologyName(b.PathologyId)
            ),
        },
        render: (PathologyId) => this.getPathologyName(PathologyId),
      },
      {
        title: "Événements",
        dataIndex: "status",
        width: "35%",
        key: "events",
        render: (status) => {
          return (
            <FlexWrap>
              {status.map((value, index) => {
                return (
                  <EventCol
                    key={`event-${index}`}
                    className={index === 0 ? "non" : null}
                  >
                    {value}
                  </EventCol>
                );
              })}
            </FlexWrap>
          );
        },
      },
    ];

    const onRow = (record, index) => {
      return {
        onClick: (_event) => {
          _event.preventDefault();
          this.props.selectPatient(record);
        },
        onMouseEnter: (_event) => {
          _event.preventDefault();
        },
        onMouseLeave: (_event) => {
          _event.preventDefault();
        },
      };
    };

    return (
      <ThemeTable
        className="patients"
        locale={{
          emptyText: (
            <div style={{ paddingTop: 20 }}>
              <ImageButton onClick={this.props.onNewPatient}>
                <img src={plusIcon} alt="" width="30" />
              </ImageButton>
              <p style={{ marginTop: 20 }}>
                Ajoutez dès maintenant votre premier patient
              </p>
            </div>
          ),
        }}
        style={{ marginTop: 25 }}
        dataSource={patients}
        columns={columns}
        pagination={{ hideOnSinglePage: true, pageSize: 10 }}
        rowKey="id"
        onRow={onRow}
      />
    );
  }
}
