import styled from "@emotion/styled";
import { Button } from "antd";
import InputMask from "react-input-mask";
import { BLUE, WHITE, LIGHT_GREY } from "../../../constants/ColorConstants";

export const Block = styled("div")({
  marginTop: 88,
  "p, label": {
    fontFamily: "Nexa Bold",
    fontSize: 23,
    lineHeight: "28px",
    color: "#A5A5A5",
  },
  label: {
    display: "block",
    marginTop: 12,
  },
});

export const LeftSide = styled("div")({
  flex: 1,
  padding: "39px 49px",
});

export const BlueText = styled("div")({
  fontFamily: "Nexa Heavy",
  fontSize: 20,
  lineHeight: "30px",
  color: BLUE,
});

export const RightSide = styled("div")({
  position: "relative",
  // width: "54.38%",
  padding: "39px 49px",
  // height: 230,
  borderRadius: "0px 0px 40px 40px",
  background: LIGHT_GREY,
  boxShadow: "3px 3px 20px rgba(211, 211, 211, 0.6)",
  ".ant-btn-text": {
    position: "absolute",
    bottom: 39,
    marginLeft: 15,
  },
  ".description": {
    fontFamily: "Nexa BoldItalic",
    fontSize: 15,
    lineHeight: "19px",
    textAlign: "center",
    marginTop: 65,
    marginBottom: 37,
  },
  input: {
    fontFamily: "Nexa Bold !important",
    fontSize: 19,
    lineHeight: "23px",
    color: BLUE,
    backgroundColor: "white",
    "&::placeholder": {
      fontFamily: "Nexa Bold !important",
      fontSize: "19px !important",
      color: `${BLUE}40`,
    },
  },
  ".ant-select": {
    backgroundColor: "white !important",
  },
});

export const CopyButton = styled(Button)({
  position: "relative",
  marginLeft: 17,
  height: 32.23,
  width: 66,
  borderRadius: "16.11px !important",
  background: LIGHT_GREY,
  boxShadow: "-3px -3px 3px #fff, 3px 3px 3px #d3d3d360",
  borderWidth: 0,
  outline: "none",
  overflow: "hidden",
  padding: 0,
  span: {
    fontFamily: "Nexa Heavy",
    fontSize: 15,
    lineHeight: "22px",
    color: BLUE,
  },
  ".white": {
    display: "none",
    position: "absolute",
    width: "100%",
    top: -8.5,
    height: 38,
    background: "transparent",
    // border: "6px solid #fff",
    borderWidth: 1,
    filter: "blur(8px)",
  },
  ".focused": {
    display: "none",
    position: "absolute",
    width: "100%",
    top: 1.88,
    height: 30,
    background: "transparent",
    border: "6px solid #fff",
    filter: "blur(7.555910110473633px)",
  },
  ".grad": {
    display: "none",
    position: "absolute",
    top: 0,
    marginLeft: -13.5,
    width: 93,
    height: 8.43,
    background: "transparent",
    border: "10px solid #3333CC47",
    filter: "blur(8.838459968566895px)",
  },
  "&:hover, &:focus, &-focused, &.selected": {
    opacity: 1,
    boxShadow: "none",
    ".focused, .grad, .white": {
      display: "block",
    },
  },
  "&.full": {
    borderRadius: "40px !important",
    height: 53,
    width: "fit-content",
    padding: "0 25px",
    ".grad": {
      width: "100%",
    },
    span: {
      fontFamily: "Nexa Regular",
      fontSize: 20,
      lineHeight: "30px",
      color: BLUE,
    },
  },
  "&.large": {
    borderRadius: "65px !important",
    height: 39,
    width: 142,
    marginLeft: 19,
    marginTop: 13,
    span: {
      color: "#707070",
    },
    ".focused": {
      marginLeft: -6.5,
      width: 155,
      top: 8,
      borderWidth: 6,
    },
    ".grad": {
      width: 169,
      borderWidth: 6,
    },
    "&.red": {
      span: {
        color: "#E31F1F",
      },
    },
    "&.yellow": {
      span: {
        color: "orange",
      },
    },
    "&.black": {
      span: {
        color: "black",
      },
    },
  },
});

export const PhoneInput = styled(InputMask)({
  width: 269,
  height: 54,
  borderWidth: 0,
  outline: "0 !important",
  borderRadius: 27,
  background: WHITE,
  boxShadow: "3px 3px 20px rgba(211, 211, 211, 0.6)",
  color: BLUE,
  fontFamily: "Nexa Bold",
  fontSize: 25,
  lineHeight: "38px",
  padding: "0 20px",
  "&.in-modal": {
    width: "100%",
    borderRadius: 10,
    fontSize: 20,
    lineHeight: "27px",
    padding: "0 15px",
    "&.disabled": {
      boxShadow: "none",
      cursor: "unset",
      color: "#A5A5A5",
      "&::placeholder": {
        color: "#A5A5A5 !important",
      },
    },
  },
  "&:focus": {
    boxShadow: "3px 3px 20px rgba(51,51,204,0.6)",
  },
});

export const Questions = styled("div")({
  paddingLeft: 44,
  fontFamily: "Nexa Regular",
  fontSize: 20,
  lineHeight: "30px",
  color: "#707070",
  ul: {
    marginBottom: 0,
  },
  ".bold": {
    margin: "13px 0",
    fontFamily: "Nexa Bold",
    fontSize: 20,
    lineHeight: "24px",
  },
});
