import React from "react";
import { connect } from "react-redux";
import { Button, notification, Row, Col, Form, Select } from "antd";
import Header from "../../../components/Header";
import {
  BlockHeader,
  FlexWrap,
  ConfirmModal,
  verticalCenter,
  ThemeInput,
} from "../_styles";
import {
  Block,
  RightSide,
  Questions,
  PhoneInput,
  BlueText,
  CopyButton,
} from "./_styles";
import { PatientActions } from "../../../actions";
import LoadingOveraly from "../../../components/LoadingOverlay";
import { NewSelect, Flex1 } from "../PatientRecord/Results/_styles";

const layout = {
  labelCol: { span: 0 },
  wrapperCol: { span: 24 },
};

class NewPatient extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      submitted: false,
      phone: "",
      values: null,
    };
  }

  setModalVisible = (visible) => {
    this.setState({
      modalVisible: visible,
    });
  };

  onOk = () => {
    this.setModalVisible(true);
  };

  onChangePhone = (e) => {
    const { value } = e.target;
    let phone = "";

    if (value.indexOf("_") < 0) {
      phone = value.replace(/ /g, "");
    }
    this.setState({
      phone,
    });
  };

  handleValid = () => {
    const { phone, values } = this.state;
    this.setState(
      {
        adding: true,
      },
      () => {
        this.setModalVisible(false);
        this.props.addPatient({
          data: {
            phone,
            ...values,
          },
          cb: (res) => {
            if (res.errors) {
              this.setState({
                adding: false,
              });
              notification.error({
                message: "Erreur",
                description: res.errors.msg,
              });
            } else {
              this.props.getPatients({
                cb: () => {},
              });
              this.props.onSelectPatient(res.patient);
              this.setState({
                adding: false,
              });
            }
          },
        });
      }
    );
  };

  onFinish = (values) => {
    const { phone } = this.state;
    const invalidPhone = phone === "";
    this.setState(
      {
        values,
        submitted: invalidPhone,
      },
      () => {
        if (!invalidPhone) {
          this.onOk();
        }
      }
    );
  };

  onFinishFailed = (values) => {};

  render() {
    const { phone, adding, submitted } = this.state;
    const { pathologies } = this.props;
    console.log("phone = ", phone);
    return (
      <>
        <Header />
        <Block>
          <Row>
            <Col span={12}>
              <BlockHeader className="old-grad">
                Ajouter un nouveau Patient
              </BlockHeader>
              <RightSide style={{ width: "100%" }}>
                <p>
                  Pour ajouter un suivi par SMS à l’un de vos patients, veuillez
                  remplir le formulaire ci-dessous
                </p>
                <Form
                  {...layout}
                  name="basic"
                  onFinish={this.onFinish}
                  onFinishFailed={this.onFinishFailed}
                >
                  <Row>
                    <Col span={12}>
                      <label>Prénom du Patient</label>
                    </Col>
                    <Col span={12}>
                      <FlexWrap>
                        <Flex1>
                          <Form.Item
                            name="firstname"
                            rules={[
                              {
                                required: true,
                                message: "Obligatoire",
                              },
                            ]}
                          >
                            <ThemeInput
                              placeholder="| Prénom"
                              className="no-round"
                            />
                          </Form.Item>
                        </Flex1>
                        <div style={{ width: 12 }}></div>
                        <Flex1>
                          <Form.Item
                            name="lastname"
                            rules={[
                              {
                                required: true,
                                message: "Obligatoire",
                              },
                            ]}
                          >
                            <ThemeInput
                              placeholder="| Nom"
                              className="no-round"
                            />
                          </Form.Item>
                        </Flex1>
                      </FlexWrap>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={12}>
                      <label>Pathologie du Patient</label>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name="pathologyId"
                        rules={[
                          {
                            required: true,
                            message: "Obligatoire",
                          },
                        ]}
                      >
                        <NewSelect>
                          {pathologies.map((pathology, index) => {
                            if (pathology.name === "Cataracte") {
                              return null;
                            }
                            return (
                              <Select.Option
                                key={`pathology-${index}`}
                                value={pathology.id}
                              >
                                {pathology.name}
                              </Select.Option>
                            );
                          })}
                        </NewSelect>
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={12}>
                      <label>Numéro de téléphone du patient</label>
                    </Col>
                    <Col span={12}>
                      <PhoneInput
                        // eslint-disable-next-line no-octal-escape
                        mask="+3\3 9 99 99 99 99"
                        maskplaceholder="+33|0 00 00 00 00"
                        alwaysShowMask={true}
                        onChange={this.onChangePhone}
                      />
                      {submitted && phone === "" && (
                        <div className="ant-form-item-explain">
                          <div style={{ color: "#ff4d4f" }}>Obligatoire</div>
                        </div>
                      )}
                    </Col>
                  </Row>
                  <div className="description">
                    En cliquant sur “Démarrer le suivi du Patient”, vous
                    certifiez que le patient a été informé du fait qu’il va
                    bénéficier d’un suivi par sms automatiques et qu’il a donné
                    son accord
                  </div>
                  <FlexWrap style={{ justifyContent: "center" }}>
                    <CopyButton
                      className="full"
                      htmlType="submit"
                      onClick={() => {
                        this.setState({ submitted: true });
                      }}
                    >
                      Démarrer le suivi du Patient
                      <div className="white"></div>
                      <div className="focused"></div>
                      <div className="grad"></div>
                    </CopyButton>
                  </FlexWrap>
                </Form>
              </RightSide>
            </Col>
            <Col span={12}>
              <Questions>
                <BlueText>
                  Comment le suivi par sms fonctionne pour vos patients ?
                </BlueText>
                <div className="bold">
                  Et accompagner d’un schéma expliquant le fonctionnement du
                  suivi par SMS des patients
                </div>
                Voici le déroulement du programme de suivi par SMS :
                <ul>
                  <li>
                    Inscription du Patient dans le logiciel avec un agent
                    conversationnel par SMS
                  </li>
                  <li>
                    Explication de sa pathologie le 1er, 3ème, 4ème et 5ème jour
                  </li>
                  <li>
                    Invitation du client à renseigner ses traitements en cours
                  </li>
                  <li>Test de l’acuité visuelle 2 fois par mois</li>
                  <li>Test de la Perception des contrastes 2 fois par mois</li>
                  <li>
                    Pour les patients atteints de DMLA, Maculopathie et
                    Rétinopathie, Test de la grille d'Amsler 2 fois par mois.{" "}
                  </li>
                  <li>
                    Questionnaire sur la qualité de vie du patient tous les deux
                    mois
                  </li>
                </ul>
                <BlueText style={{ marginTop: 25, marginBottom: 21 }}>
                  Vous pourrez retrouver l’ensemble de ces informations sur la
                  fiche du patient.
                </BlueText>
                Curecall met à disposition du patient un agent conversationnel
                lui permettant :
                <ul>
                  <li>Signaler un effet secondaire sur son traitement</li>
                  <li>Modifier / Ajouter un traitement</li>
                  <li>
                    Tester son acuité visuelle, sa perception des contrastes
                  </li>
                  <li>Signaler un changement sur sa qualité de vie</li>
                </ul>
              </Questions>
            </Col>
          </Row>
        </Block>

        <ConfirmModal
          title={null}
          closable={false}
          maskClosable={false}
          footer={null}
          centered
          visible={this.state.modalVisible}
        >
          <p>Êtes-vous sûr d’ajouter ce patient?</p>
          <FlexWrap style={verticalCenter}>
            <Button
              type="text"
              className="cancel-btn"
              onClick={() => this.setModalVisible(false)}
            >
              Annuler
            </Button>
            <div style={{ width: 120 }} />
            <Button type="text" className="ok-btn" onClick={this.handleValid}>
              Valider
            </Button>
          </FlexWrap>
        </ConfirmModal>
        <LoadingOveraly loading={adding} />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pathologies: state.PathologyReducer.pathologies,
    error: state.PatientReducer.error,
    patient: state.PatientReducer.patient,
    user: state.AccountReducer.userData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addPatient: (req) => {
      dispatch(PatientActions.addPatient(req.data, req.cb));
    },
    getPatients: (req) => {
      dispatch(PatientActions.getPatients(req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewPatient);
