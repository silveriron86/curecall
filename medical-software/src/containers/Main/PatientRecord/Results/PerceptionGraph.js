import React from "react";
import ReactHighcharts from "react-highcharts";
import moment from "moment";
import { ExpandedTitle, BlockPlaceholder } from "./_styles";
import { FlexWrap } from "../../_styles";
import { cloneDeep } from "lodash";
import Utils from "../../../../utils";
import LangConstants from "../../.././../constants/LangConstants";

export default class PerceptionGraph extends React.Component {
  render() {
    const { data } = this.props;
    const values = ["100", "80", "40", "25", "15", "10", "8", "4", "2"];
    const colors = ["#EA78FD", "#E6C122", "#72A8FF"];
    const hasValues = data && data.patientMatrixHistory.length > 0;
    if (!hasValues) {
      return (
        <div>
          <ExpandedTitle>Perception des contrastes</ExpandedTitle>
          <FlexWrap style={{ height: 200 }}>
            <BlockPlaceholder>
              Le patient n’a pas encore
              <br />
              réalisé de test
            </BlockPlaceholder>
          </FlexWrap>
        </div>
      );
    }

    let rData = [];
    let lData = [];
    const patientMatrixHistory = cloneDeep(data.patientMatrixHistory);
    const grouped = Utils.groupByDate(patientMatrixHistory);
    grouped.forEach((hist, _index) => {
      const matrixId = hist.patientMatrixId;
      const rMatrix = Utils.getMatrix(
        data.history,
        "contrastPerceptionR",
        matrixId
      );
      const when = moment(hist.date).format("YYYY-MM-DDT00:00:00.000Z");
      let foundIndex = values.indexOf(
        rMatrix && rMatrix.value !== 0 ? rMatrix.value.toString() : "100"
      );
      if (foundIndex >= 0) {
        rData.push([new Date(when).getTime(), foundIndex * 10]);
      }

      const lMatrix = Utils.getMatrix(
        data.history,
        "contrastPerceptionL",
        matrixId
      );

      foundIndex = values.indexOf(
        lMatrix && lMatrix.value !== 0 ? lMatrix.value.toString() : "100"
      );
      if (foundIndex >= 0) {
        lData.push([new Date(when).getTime(), foundIndex * 10]);
      }
    });

    const config = {
      chart: {
        type: "spline",
        backgroundColor: "transparent",
      },
      lang: {
        months: LangConstants.months,
        shortMonths: LangConstants.months,
        weekdays: LangConstants.weekdays,
      },
      credits: {
        enabled: false,
      },
      title: {
        text: "",
      },
      subtitle: {
        text: "",
      },
      rangeSelector: {
        selected: 1,
      },
      xAxis: {
        type: "datetime",
        tickWidth: 0,
        gridLineWidth: 1,
        gridLineColor: "#70707020",
        lineColor: "#70707020",
        tickmarkPlacement: "on",
        minTickInterval: 1000 * 3600 * 24,
        dateTimeLabelFormats: {
          millisecond: "%e. %b",
          second: "%e. %b",
          minute: "%e. %b",
          hour: "%e. %b",
          day: "%e. %b",
          week: "%e. %b",
          month: "%b '%y",
          year: "%Y",
        },
        labels: {
          style: {
            color: "#707070",
            fontFamily: "Nexa Light",
            fontSize: "20px",
            lineHeight: "24px",
          },
          // formatter: function () {
          //   if (this.isFirst || this.isLast)
          //     return ReactHighcharts.Highcharts.dateFormat(
          //       this.dateTimeLabelFormat,
          //       this.value
          //     );
          //   else return "";
          // },
        },
      },
      yAxis: {
        min: 0,
        max: 80,
        tickInterval: 10,
        gridLineWidth: 1,
        gridLineColor: "#70707020",
        title: {
          text: "",
        },
        labels: {
          style: {
            color: "#A5A5A5",
            fontFamily: "Nexa Bold",
            fontSize: "15px",
            lineHeight: "18px",
          },
          formatter: function () {
            return values[this.value / 10] + "%";
          },
        },
      },
      tooltip: {
        crosshairs: true,
        shared: true,
        formatter: function () {
          let result = Utils.capitalize(
            moment(this.points[0].x).format("DD/MM")
          );
          this.points.forEach(function (point) {
            let label = values[point.y / 10] + "%";
            if (point.y === 0) {
              label = "Le patient n'a pas perçu les contrastes dès 80%.";
            }
            result +=
              "<br/><div style='color: " +
              colors[point.series.index] +
              "'>" +
              point.series.name +
              ": " +
              label;
          }, "<b>" + this.x + "</b></div>");
          return result;
        },
      },
      plotOptions: {
        spline: {
          marker: {
            radius: 4,
            lineWidth: 0,
          },
          pointPlacement: "on",
        },
      },
      series: [
        {
          name: "Œil droit",
          color: colors[0],
          marker: {
            symbol: "circle",
          },
          data: rData,
        },
        {
          name: "Œil gauche",
          color: colors[1],
          marker: {
            symbol: "circle",
          },
          data: lData,
        },
      ],
    };

    return (
      <div style={{ paddingBottom: 20 }}>
        <ExpandedTitle>Perception des contrastes</ExpandedTitle>
        <div style={{ marginTop: 30, zoom: "125%" }}>
          <ReactHighcharts
            config={config}
            ref="chart"
            style={{ position: "absolute" }}
          ></ReactHighcharts>
        </div>
      </div>
    );
  }
}
