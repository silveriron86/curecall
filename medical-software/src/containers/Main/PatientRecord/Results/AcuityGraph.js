import React from "react";
import { Select } from "antd";
import ReactHighcharts from "react-highcharts";
import moment from "moment";
import { ExpandedTitle, NewSelect, BlockPlaceholder } from "./_styles";
import { FlexWrap } from "../../_styles";
import Utils from "../../../../utils";
import { cloneDeep } from "lodash";
import AcuityUnits from "../../../../constants/AcuityUnits";
import LangConstants from "../../../../constants/LangConstants";

const { Option } = Select;
export default class AcuityGraph extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      unit: "Snellen",
    };
  }

  render() {
    const values = AcuityUnits["Snellen"];

    const colors = ["#EA78FD", "#E6C122", "#72A8FF"];
    const { data } = this.props;
    const { unit } = this.state;
    const hasValues = data && data.patientMatrixHistory.length > 0;
    if (!hasValues) {
      return (
        <div>
          <ExpandedTitle>Acuité visuelle</ExpandedTitle>
          <FlexWrap style={{ height: 200 }}>
            <BlockPlaceholder>
              Le patient n’a pas encore
              <br />
              réalisé de test
            </BlockPlaceholder>
          </FlexWrap>
        </div>
      );
    }

    let acuityRData = [];
    let acuityLData = [];
    let acuityData = [];
    const patientMatrixHistory = cloneDeep(data.patientMatrixHistory);
    const grouped = Utils.groupByDate(patientMatrixHistory);
    grouped.forEach((hist, _index) => {
      const matrixId = hist.patientMatrixId;
      const rMatrix = Utils.getMatrix(data.history, "visualAcuityR", matrixId);
      const when = moment(hist.date).format("YYYY-MM-DDT00:00:00.000Z");
      let foundIndex = values.indexOf(
        rMatrix && rMatrix.value !== "0" ? rMatrix.value : " "
      );
      if (foundIndex >= 0) {
        acuityRData.push([new Date(when).getTime(), foundIndex * 100]);
      }

      const lMatrix = Utils.getMatrix(data.history, "visualAcuityL", matrixId);
      foundIndex = values.indexOf(
        lMatrix && lMatrix.value !== "0" ? lMatrix.value : " "
      );
      if (foundIndex >= 0) {
        acuityLData.push([new Date(when).getTime(), foundIndex * 100]);
      }

      const matrix = Utils.getMatrix(data.history, "visualAcuity", matrixId);
      foundIndex = values.indexOf(
        matrix && matrix.value !== "0" ? matrix.value : " "
      );
      if (foundIndex >= 0) {
        acuityData.push([new Date(when).getTime(), foundIndex * 100]);
      }
    });

    const config = {
      chart: {
        type: "spline",
        backgroundColor: "transparent",
      },
      lang: {
        months: LangConstants.months,
        shortMonths: LangConstants.months,
        weekdays: LangConstants.weekdays,
      },
      credits: {
        enabled: false,
      },
      title: {
        text: "",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        type: "datetime",
        tickWidth: 0,
        gridLineWidth: 1,
        gridLineColor: "#70707020",
        lineColor: "#70707020",
        tickmarkPlacement: "on",
        minTickInterval: 1000 * 3600 * 24,
        labels: {
          style: {
            color: "#707070",
            fontFamily: "Nexa Light",
            fontSize: "20px",
            lineHeight: "24px",
          },
          // formatter: function () {
          //   if (this.isFirst || this.isLast)
          //     return ReactHighcharts.Highcharts.dateFormat(
          //       this.dateTimeLabelFormat,
          //       this.value
          //     );
          //   else return "";
          // },
        },
        dateTimeLabelFormats: {
          millisecond: "%e. %b",
          second: "%e. %b",
          minute: "%e. %b",
          hour: "%e. %b",
          day: "%e. %b",
          week: "%e. %b",
          month: "%b '%y",
          year: "%Y",
        },
      },
      yAxis: {
        min: 0,
        max: 700,
        tickInterval: 100,
        gridLineWidth: 1,
        gridLineColor: "#70707020",
        title: {
          text: "",
        },
        labels: {
          style: {
            color: "#A5A5A5",
            fontFamily: "Nexa Bold",
            fontSize: "15px",
            lineHeight: "18px",
          },
          formatter: function () {
            return AcuityUnits[unit][this.value / 100];
          },
        },
      },
      tooltip: {
        crosshairs: true,
        shared: true,
        formatter: function () {
          // return this.points.reduce(function (s, point) {
          //   return (
          //     Utils.capitalize(moment(point.x).format("DD/MM")) +
          //     "<br/><div style='color: " +
          //     colors[point.series.index] +
          //     "'>" +
          //     point.series.name +
          //     ": " +
          //     AcuityUnits[unit][point.y / 100]
          //   );
          // }, "<b>" + this.x + "</b></div>");

          let result = Utils.capitalize(
            moment(this.points[0].x).format("DD/MM")
          );
          this.points.forEach(function (point) {
            let label = AcuityUnits[unit][point.y / 100];
            if (point.y === 0) {
              label = "Test échoué.";
            }
            result +=
              "<br/><div style='color: " +
              colors[point.series.index] +
              "'>" +
              point.series.name +
              ": " +
              label;
          }, "<b>" + this.x + "</b></div>");
          return result;
        },
      },
      plotOptions: {
        spline: {
          marker: {
            radius: 4,
            lineWidth: 0,
          },
          pointPlacement: "on",
        },
      },
      series: [
        {
          name: "Œil droit",
          color: colors[0],
          marker: {
            symbol: "circle",
          },
          data: acuityRData,
        },
        {
          name: "Œil gauche",
          color: colors[1],
          marker: {
            symbol: "circle",
          },
          data: acuityLData,
        },
        {
          name: "Vision binoculaire",
          color: colors[2],
          marker: {
            symbol: "circle",
          },
          data: acuityData,
        },
      ],
    };

    return (
      <div
        style={{
          paddingBottom: 50,
          borderBottom: "1px solid #70707020",
          marginBottom: 70,
        }}
      >
        <FlexWrap>
          <ExpandedTitle>Acuité visuelle</ExpandedTitle>
          {hasValues && (
            <NewSelect
              dropdownClassName="blue"
              className="sm"
              style={{ marginLeft: 32, width: 175 }}
              value={unit}
              onChange={(unit) => this.setState({ unit })}
            >
              <Option value="Snellen">Snellen</Option>
              <Option value="LogMar">LogMar</Option>
              <Option value="Monoyer">Monoyer</Option>
            </NewSelect>
          )}
        </FlexWrap>
        <div style={{ marginTop: 30, zoom: "125%" }}>
          <ReactHighcharts
            config={config}
            ref="chart"
            style={{ position: "absolute" }}
          ></ReactHighcharts>
        </div>
      </div>
    );
  }
}
