import React from "react";
import moment from "moment";
import ResultOverview from "./Overview";
import { ChartBlock } from "../_styles";
import {
  ExpandedTitle,
  Flex1,
  BlockTitle,
  AmslerResultsTable,
  BlockPlaceholder,
} from "./_styles";
import { FlexWrap } from "../../_styles";
import AcuityGraph from "./AcuityGraph";
import PerceptionGraph from "./PerceptionGraph";
import Utils from "../../../../utils";
import { cloneDeep } from "lodash";
const gridImage = require("../../../../assets/svgs/quality/grid.svg");

export default class Results extends React.Component {
  getAnswerBtn = (val) => {
    if (val === null) {
      return <div className="col"></div>;
    }
    const label = val ? "oui" : "non";
    return <div className={`col ${label}`}>{label.toUpperCase()}</div>;
  };

  render() {
    const { isExpanded, forms } = this.props;
    const { AMSLER_GRID, CONTRAST_PERCEPTION, VISUAL_ACUITY } = forms;

    if (!isExpanded) {
      return <ResultOverview {...this.props} />;
    }
    const { history } = AMSLER_GRID;
    const patientMatrixHistory = cloneDeep(
      AMSLER_GRID.patientMatrixHistory
    ).reverse();
    const matrixLength = patientMatrixHistory.length;
    const data = [];
    if (matrixLength > 0) {
      Utils.groupByDate(patientMatrixHistory).forEach((h, _index) => {
        const matrixId = h.patientMatrixId;
        let rValues = [
          Utils.getMatrixValue(history, "amslerGridStraightLinesR", matrixId),
          Utils.getMatrixValue(history, "amslerGridSameSizeSquareR", matrixId),
          Utils.getMatrixValue(
            history,
            "amslerGridDistortedObscuredR",
            matrixId
          ),
        ];
        let lValues = [
          Utils.getMatrixValue(history, "amslerGridStraightLinesL", matrixId),
          Utils.getMatrixValue(history, "amslerGridSameSizeSquareL", matrixId),
          Utils.getMatrixValue(
            history,
            "amslerGridDistortedObscuredL",
            matrixId
          ),
        ];
        // history.index
        data.push({
          date: moment(h.date).format("DD/MM/YYYY"),
          rValues,
          lValues,
        });
      });
    }

    const titleCols = (
      <>
        <div className="col question">
          <span>Les lignes Sont-elles droites et régulières ?</span>
        </div>
        <div className="col question">
          <span>Tous les carrés sont-ils de la même taille ?</span>
        </div>
        <div className="col question">
          <span>Y a-t-il des zones où l’image est déformée ou obscurcie ?</span>
        </div>
      </>
    );

    return (
      <>
        <ChartBlock
          style={{
            flex: 1,
            padding: "0 53px 70px 33px",
            borderBottom: "1px solid #70707020",
            marginBottom: 70,
          }}
          className="expanded"
        >
          {AMSLER_GRID && AMSLER_GRID.patientMatrixHistory.length > 0 ? (
            <>
              <FlexWrap>
                <div>
                  <ExpandedTitle>Grille d’Amsler</ExpandedTitle>
                  <ExpandedTitle className="sm" style={{ marginTop: 9 }}>
                    Œil droit
                  </ExpandedTitle>
                  <img src={gridImage} alt="" style={{ marginTop: 11 }} />
                </div>
                <div style={{ width: 54.5 }}></div>
                <Flex1>
                  <AmslerResultsTable>
                    <div className="row">
                      <div className="col">
                        <BlockTitle className="grey">
                          TEST
                          <br />
                          Œil droit
                        </BlockTitle>
                      </div>
                      {titleCols}
                    </div>
                    {data.map((q, index) => {
                      return (
                        <div className="row values" key={`amsler-row-${index}`}>
                          <div className="col date">{q.date}</div>
                          {this.getAnswerBtn(q.rValues[0])}
                          {this.getAnswerBtn(q.rValues[1])}
                          {this.getAnswerBtn(q.rValues[2])}
                        </div>
                      );
                    })}
                  </AmslerResultsTable>
                </Flex1>
              </FlexWrap>

              <FlexWrap style={{ marginTop: 50 }}>
                <div>
                  <ExpandedTitle className="sm">Œil gauche</ExpandedTitle>
                  <img src={gridImage} alt="" style={{ marginTop: 11 }} />
                </div>
                <div style={{ width: 54.5 }}></div>
                <Flex1>
                  <AmslerResultsTable>
                    <div className="row">
                      <div className="col">
                        <BlockTitle className="grey">
                          TEST
                          <br />
                          Œil gauche
                        </BlockTitle>
                      </div>
                      {titleCols}
                    </div>
                    {data.map((q, index) => {
                      return (
                        <div className="row values" key={`amsler-row-${index}`}>
                          <div className="col date">{q.date}</div>
                          {this.getAnswerBtn(q.lValues[0])}
                          {this.getAnswerBtn(q.lValues[1])}
                          {this.getAnswerBtn(q.lValues[2])}
                        </div>
                      );
                    })}
                  </AmslerResultsTable>
                </Flex1>
              </FlexWrap>
            </>
          ) : (
            <>
              <ExpandedTitle style={{ marginLeft: -33 }}>
                Grille d’Amsler
              </ExpandedTitle>
              <FlexWrap style={{ height: 200 }}>
                <BlockPlaceholder>
                  Le patient n’a pas encore réalisé de test
                </BlockPlaceholder>
              </FlexWrap>
            </>
          )}
        </ChartBlock>
        <AcuityGraph data={VISUAL_ACUITY} />
        <PerceptionGraph data={CONTRAST_PERCEPTION} />
      </>
    );
  }
}
