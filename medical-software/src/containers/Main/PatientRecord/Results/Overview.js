import React from "react";
import { Select } from "antd";
import moment from "moment";
import { FlexWrap } from "../../_styles";
import {
  BlockTitle,
  BlockWrapper,
  BlockPlaceholder,
  Flex1,
  AmslerGrids,
  NewSelect,
} from "./_styles";
import { ChartBlock } from "../_styles";
import AcuityUnits from "../../../../constants/AcuityUnits";

const { Option } = Select;
const gridImage = require("../../../../assets/svgs/quality/grid.svg");
export default class ResultOverview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      unit: "Snellen",
    };
  }

  getAmslerValue = (lastAmser, field) => {
    const found = lastAmser.history.find((h) => h.name === field);
    const value = found ? found.value.value : null;
    if (value === null) {
      return "";
    }
    return value ? "OUI" : "NON";
  };

  getLastTest = (formData) => {
    if (!formData) {
      return null;
    }

    let lastOne = null;
    const { history, patientMatrixHistory } = formData;
    // const history = cloneDeep(formData.history);
    const matrixLength = patientMatrixHistory.length;
    if (matrixLength > 0) {
      const lastHistory = patientMatrixHistory[matrixLength - 1];
      const matrixId = lastHistory.patientMatrixId;
      history.forEach((h, index) => {
        const found = h.values
          ? h.values.find((v) => v.patientMatrixId === matrixId)
          : null;
        history[index].value = found;
      });
      lastOne = {
        date: moment(lastHistory.date).format("DD/MM/YYYY"),
        history,
      };
    }
    return lastOne;
  };

  getAcuityValue = (lastOne, field) => {
    const { unit } = this.state;
    const found = lastOne.history.find((h) => h.name === field);
    console.log("found= ", found);
    if (
      typeof found === "undefined" ||
      typeof found.value === "undefined" ||
      found.value === "undefined" ||
      found.value.value === "0"
    ) {
      return "Test échoué.";
    }

    const value = found ? found.value.value : null;
    if (!value) {
      return "Test échoué.";
    }

    const index = AcuityUnits["Snellen"].indexOf(value);
    return AcuityUnits[unit][index];
  };

  getPerceptionValue = (lastOne, field) => {
    const found = lastOne.history.find((h) => h.name === field);
    const value = found && found.value ? found.value.value : null;
    return value ? `${value}%` : "Test échoué.";
  };

  render() {
    const { forms } = this.props;
    const { unit } = this.state;
    const { AMSLER_GRID, CONTRAST_PERCEPTION, VISUAL_ACUITY } = forms;

    let lastAmsler = this.getLastTest(AMSLER_GRID);
    let lastAcuity = this.getLastTest(VISUAL_ACUITY);
    let lastPerception = this.getLastTest(CONTRAST_PERCEPTION);

    return (
      <ChartBlock className="time-table results">
        <FlexWrap className="full-height">
          <Flex1>
            <BlockWrapper>
              <BlockTitle>Grille d’Amsler</BlockTitle>
              {lastAmsler ? (
                <FlexWrap>
                  <img src={gridImage} alt="" width="149" height="149" />
                  <Flex1 style={{ padding: "0 30.5px 0 20.5px" }}>
                    <AmslerGrids>
                      <div className="row">
                        <div className="col">
                          Date du test: {lastAmsler.date}
                        </div>
                        <div className="col bool">OD</div>
                        <div className="col bool">OG</div>
                      </div>
                      <div className="row">
                        <div className="col">Lignes droite et régulières</div>
                        <div className="col bool">
                          {this.getAmslerValue(
                            lastAmsler,
                            "amslerGridStraightLinesR"
                          )}
                        </div>
                        <div className="col bool">
                          {this.getAmslerValue(
                            lastAmsler,
                            "amslerGridStraightLinesL"
                          )}
                        </div>
                      </div>
                      <div className="row">
                        <div className="col">Carrées de même taille</div>
                        <div className="col bool">
                          {this.getAmslerValue(
                            lastAmsler,
                            "amslerGridSameSizeSquareR"
                          )}
                        </div>
                        <div className="col bool">
                          {this.getAmslerValue(
                            lastAmsler,
                            "amslerGridSameSizeSquareL"
                          )}
                        </div>
                      </div>
                      <div className="row">
                        <div className="col">Zones déformées</div>
                        <div className="col bool">
                          {this.getAmslerValue(
                            lastAmsler,
                            "amslerGridDistortedObscuredR"
                          )}
                        </div>
                        <div className="col bool">
                          {this.getAmslerValue(
                            lastAmsler,
                            "amslerGridDistortedObscuredL"
                          )}
                        </div>
                      </div>
                    </AmslerGrids>
                  </Flex1>
                </FlexWrap>
              ) : (
                <BlockPlaceholder>
                  Le patient n’a pas encore réalisé de test
                </BlockPlaceholder>
              )}
            </BlockWrapper>
          </Flex1>
          <Flex1>
            <Flex1>
              <BlockWrapper className="sm">
                <div className="l-border"></div>
                <BlockTitle>Acuité visuelle</BlockTitle>
                {lastAcuity ? (
                  <AmslerGrids className="no-border">
                    <div className="row">
                      <div className="col">Date du test: {lastAcuity.date}</div>
                    </div>
                    <NewSelect
                      dropdownClassName="blue"
                      className="sm"
                      style={{ margin: "1px 0 17px", width: 175 }}
                      value={unit}
                      onChange={(unit) => this.setState({ unit })}
                    >
                      <Option value="Snellen">Snellen</Option>
                      <Option value="LogMar">LogMar</Option>
                      <Option value="Monoyer">Monoyer</Option>
                    </NewSelect>
                    <div className="row">
                      <div className="col">Œil droit</div>
                      <div className="col bool">
                        {this.getAcuityValue(lastAcuity, "visualAcuityR")}
                      </div>
                    </div>
                    <div className="row">
                      <div className="col">Œil gauche</div>
                      <div className="col bool">
                        {this.getAcuityValue(lastAcuity, "visualAcuityL")}
                      </div>
                    </div>
                    <div className="row">
                      <div className="col">Deux yeux</div>
                      <div className="col bool">
                        {this.getAcuityValue(lastAcuity, "visualAcuity")}
                      </div>
                    </div>
                  </AmslerGrids>
                ) : (
                  <BlockPlaceholder>
                    Le patient n’a pas encore
                    <br />
                    réalisé de test
                  </BlockPlaceholder>
                )}
              </BlockWrapper>
            </Flex1>
            <Flex1>
              <BlockWrapper className="sm">
                <div className="l-border"></div>
                <BlockTitle>Perception des contrastes</BlockTitle>
                {lastPerception ? (
                  <AmslerGrids className="no-border">
                    <div className="row">
                      <div className="col">
                        Date du test: {lastPerception.date}
                      </div>
                    </div>
                    <NewSelect
                      dropdownClassName="blue"
                      className="sm"
                      style={{
                        margin: "1px 0 17px",
                        width: 175,
                        visibility: "hidden",
                      }}
                    ></NewSelect>
                    <div className="row">
                      <div className="col">Œil droit</div>
                      <div className="col bool">
                        {this.getPerceptionValue(
                          lastPerception,
                          "contrastPerceptionR"
                        )}
                      </div>
                    </div>
                    <div className="row">
                      <div className="col">Œil gauche</div>
                      <div className="col bool">
                        {this.getPerceptionValue(
                          lastPerception,
                          "contrastPerceptionL"
                        )}
                      </div>
                    </div>
                  </AmslerGrids>
                ) : (
                  <BlockPlaceholder>
                    Le patient n’a pas encore
                    <br />
                    réalisé de test
                  </BlockPlaceholder>
                )}
              </BlockWrapper>
            </Flex1>
          </Flex1>
        </FlexWrap>
      </ChartBlock>
    );
  }
}
