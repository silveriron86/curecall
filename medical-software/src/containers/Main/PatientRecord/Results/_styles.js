import styled from "@emotion/styled";
import { BLUE, GREY } from "../../../../constants/ColorConstants";
import downIcon from "../../../../assets/svgs/quality/down.svg";
import { Select } from "antd";

export const Flex1 = styled("div")({
  display: "flex",
  flexWrap: "wrap",
  flex: 1,
  // border: "1px solid red",
  "&.r-border": {
    width: 1,
    borderRight: "1px solid #70707020",
    height: "100%",
  },
});

export const BlockWrapper = styled("div")({
  position: "relative",
  width: "100%",
  height: "100%",
  display: "flex",
  flexDirection: "column",
  "&.sm": {
    padding: "0 25.5px",
  },
  ".l-border": {
    position: "absolute",
    top: 13,
    left: 0,
    width: 1,
    borderLeft: "1px solid #707070",
    height: "calc(100% - 26px)",
    opacity: 0.2,
  },
});

export const BlockTitle = styled("div")({
  fontFamily: "Nexa Heavy",
  fontSize: 20,
  lineHeight: "25px",
  color: BLUE,
  marginBottom: 14,
  "&.grey": {
    color: "#707070",
    marginBottom: 0,
    textAlign: "center",
    width: "100%",
  },
});

export const BlockPlaceholder = styled("div")({
  fontFamily: "Nexa Bold",
  fontSize: 15,
  lineHeight: "18px",
  color: GREY,
  flex: 1,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  textAlign: "center",
});

export const AmslerGrids = styled("div")({
  width: "100%",
  ".row": {
    width: "100%",
    display: "flex",
    borderBottom: `1px solid ${GREY}`,
    padding: "2px 0 0 0 !important",
    height: 42,
    alignItems: "center",
    ".col": {
      flex: 1,
      fontFamily: "Nexa Bold",
      fontSize: 20,
      LineHeight: "24px",
      color: "#707070",
      "&.bool": {
        width: 60,
        flex: "unset",
        textAlign: "center",
        color: "black",
      },
    },
    "&:nth-of-type(1)": {
      height: "auto",
      paddingBottom: "6.5px !important",
      ".col": {
        fontFamily: "Nexa Book",
        fontSize: 15,
        LineHeight: "22px",
        color: "black",
      },
    },
  },
  "&.no-border": {
    ".row": {
      borderBottom: 0,
      height: "auto",
      paddingBottom: "11px !important",
      ".col": {
        "&.bool": {
          width: 120,
        },
      },
    },
  },
});

export const NewSelect = styled(Select)({
  "&.ant-select": {
    outline: "none !important",
    height: 58,
    width: "100%",
    backgroundColor: "#f7f7f7",
    borderRadius: 10,
    boxShadow: "3px 3px 20px rgba(211, 211, 211, 0.6)",
    "&.sm": {
      height: 45,
    },
    ".ant-select-selection-item": {
      color: BLUE,
      fontFamily: "Nexa Bold",
      fontWeight: "normal",
      fontSize: 20,
      lineHeight: "24px",
      marginTop: 3,
      // opacity: 0.4,
    },
    ".ant-select-selector": {
      height: "100%",
      backgroundColor: "transparent",
      border: 0,
      display: "flex",
      alignItems: "center",
      paddingLeft: 24,
      outline: "none !important",
      boxShadow: "none !important",
    },
    ".ant-select-arrow": {
      background: `url(${downIcon})`,
      width: 26,
      height: 16,
      marginTop: -8,
      right: 17,
      ".anticon": {
        display: "none",
      },
    },
    "&-open": {
      boxShadow: "3px 3px 20px rgba(51,51,204,0.6) !important",
      ".ant-select-arrow": {
        transform: `rotate(180deg)`,
      },
      ".ant-select-selection-item": {
        opacity: 1,
      },
    },
    "&-disabled": {
      boxShadow: "none !important",
      ".ant-select-selection-item": {
        color: "#A5A5A5",
      },
      ".ant-select-arrow": {
        display: "none !important",
      },
    },
  },
});

export const ExpandedTitle = styled("div")({
  fontFamily: "Nexa Heavy",
  fontSize: 30,
  lineHeight: "35px",
  color: BLUE,
  "&.sm": {
    fontFamily: "Nexa Bold",
    fontSize: 20,
    lineHeight: "24px",
  },
});

export const AmslerResultsTable = styled("div")({
  ".row": {
    display: "flex",
    height: 62,
    marginBottom: 5,
    "&.values:hover": {
      ".col": {
        boxShadow: "0px 1px 20px rgba(0, 0, 0, 0.3)",
        "&.non": {
          backgroundColor: "#E31F1F",
        },
        "&.oui": {
          backgroundColor: "#8BCC33",
        },
        "&.date": {
          color: "black",
        },
        fontFamily: "Nexa Bold",
        fontSize: 25,
        lineHeight: "30px",
        color: "white",
      },
    },
    "&:nth-of-type(1)": {
      height: 77,
    },
    ".col": {
      display: "flex",
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 10,
      backgroundColor: "#F7F7F7",
      height: "100%",
      borderRight: 10,
      marginLeft: 5,
      paddingTop: 3,
      "&.question": {
        fontFamily: "Nexa Bold",
        fontSize: 15,
        lineHeight: "18px",
        color: "#707070",
        justifyContent: "flex-start",
        span: {
          padding: "14px 19px",
        },
      },
      "&.date": {
        fontFamily: "Nexa Book",
        fontSize: 15,
        lineHeight: "22px",
        color: "black",
      },
      "&.non": {
        color: "#E31F1F",
        backgroundColor: "#E31F1F20",
      },
      "&.oui": {
        backgroundColor: "#8BCC3320",
        color: "#8BCC33",
      },
      "&.oui, &.non": {
        fontFamily: "Nexa Bold",
        fontSize: 20,
        lineHeight: "24px",
      },
    },
  },
});
