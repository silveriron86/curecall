import React from "react";
import { connect } from "react-redux";
import Header from "../../../components/Header";
import {
  FlexWrap,
  BlockHeader,
  BlockReturnHeader,
  ImageButton,
} from "../_styles";
import PatientIdentify from "./PatientIdentify";
import OngoingTreatment from "./OngoingTreatment";
import { PatientActions } from "../../../actions";
import LoadingOverlay from "../../../components/LoadingOverlay";
import { notification } from "antd";
import Results from "./Results";
import ImpactOnQuality from "./Quality";
import { DetailsButton } from "../../../components/_styles";
import { cloneDeep } from "lodash";

const arrowIcon = require("../../../assets/svgs/left_arrow.svg");
const backIcon = require("../../../assets/svgs/Back light.svg");

class PatientRecord extends React.Component {
  state = {
    loading: false,
    chartIndex: 0,
    visibleLearnBtn: false,
    visibleLearnMorePage: false,
    key: new Date().getTime(),
    isExpanded: false,
    matrixId1: null,
    matrixId2: null,
  };

  componentWillReceiveProps(nextProps) {
    if (
      JSON.stringify(nextProps.patient) !== JSON.stringify(this.props.patient)
    ) {
      this.setState({ key: new Date().getTime() });
      this._loadVariable(nextProps);
    }

    if (nextProps.nextConsultationDate !== this.props.nextConsultationDate) {
      this.setState({ key: new Date().getTime() });
    }

    if (
      nextProps.forms.EVERYDAY_LIFE &&
      nextProps.forms.EVERYDAY_LIFE.patientMatrixHistory.length > 0 &&
      (!this.props.forms.EVERYDAY_LIFE ||
        cloneDeep(this.props.forms.EVERYDAY_LIFE.patientMatrixHistory) !==
          cloneDeep(nextProps.forms.EVERYDAY_LIFE.patientMatrixHistory))
    ) {
      const matrixHistory = cloneDeep(
        nextProps.forms.EVERYDAY_LIFE.patientMatrixHistory
      ).reverse();
      const matrixId = matrixHistory[0].patientMatrixId;
      this.setState({
        matrixId1: matrixId,
      });
    }

    if (
      nextProps.forms.THERAPEUTIC_CARE &&
      nextProps.forms.THERAPEUTIC_CARE.patientMatrixHistory.length > 0 &&
      (!this.props.forms.THERAPEUTIC_CARE ||
        cloneDeep(this.props.forms.THERAPEUTIC_CARE.patientMatrixHistory) !==
          cloneDeep(nextProps.forms.THERAPEUTIC_CARE.patientMatrixHistory))
    ) {
      const matrixHistory = cloneDeep(
        nextProps.forms.THERAPEUTIC_CARE.patientMatrixHistory
      ).reverse();
      const matrixId = matrixHistory[0].patientMatrixId;
      this.setState({
        matrixId2: matrixId,
      });
    }
  }

  componentDidMount() {
    this._loadVariable(this.props);
  }

  _loadVariable = (props) => {
    const { patient } = props;
    if (patient) {
      this.props.getVariable({
        patientId: patient.id,
        variableName: "nextConsultationDate",
        cb: () => {
          // console.log(res);
        },
      });

      this.props.getFormVariables({
        patientId: patient.id,
        groupName: "EVERYDAY_LIFE",
        cb: (res) => {
          // console.log(res);
        },
      });

      this.props.getFormVariables({
        patientId: patient.id,
        groupName: "THERAPEUTIC_CARE",
        cb: (res) => {
          // console.log(res);
        },
      });

      this.props.getFormVariables({
        patientId: patient.id,
        groupName: "AMSLER_GRID",
        cb: (res) => {
          // console.log(res);
        },
      });

      this.props.getFormVariables({
        patientId: patient.id,
        groupName: "CONTRAST_PERCEPTION",
        cb: (res) => {
          // console.log(res);
        },
      });

      this.props.getFormVariables({
        patientId: patient.id,
        groupName: "VISUAL_ACUITY",
        cb: (res) => {
          // console.log(res);
        },
      });
    }
  };

  onSelectChart = (index) => {
    this.setState({
      chartIndex: index,
      visibleLearnBtn: index > 0 ? true : false,
    });
  };

  handleReturn = () => {
    this.setState({
      chartIndex: 0,
    });
  };

  onCloseLearnBtn = () => {
    this.setState({
      visibleLearnBtn: false,
    });
  };

  onOpenLearnMore = () => {
    this.setState({
      visibleLearnMorePage: true,
    });
  };

  onUpdatePatient = (data) => {
    const { patient } = this.props;
    this.setState(
      {
        loading: true,
      },
      () => {
        const { nextConsultationDate } = data;
        delete data.nextConsultationDate;
        this.props.updatePatient({
          data,
          cb: (res) => {
            if (res.errors) {
              this.handleError();
            } else {
              this.props.saveVariable({
                patientId: patient.id,
                variableName: "nextConsultationDate",
                data: {
                  valueString: nextConsultationDate,
                },
                cb: (res) => {
                  if (res.errors) {
                    this.handleError();
                  } else {
                    this.props.getPatients({
                      cb: () => {
                        this.setState({
                          loading: false,
                        });
                      },
                    });
                    this.props.getPatient({
                      patientId: patient.id,
                      cb: () => {
                        this.setState({
                          loading: false,
                        });
                      },
                    });
                  }
                },
              });
            }
          },
        });
      }
    );
  };

  handleError = () => {
    this.setState(
      {
        loading: false,
      },
      () => {
        notification.error({
          message: "Erreur lors de la mise à jour",
          description: "Veuillez ressayer ou envoyer votre demander à Curecall",
        });
      }
    );
  };

  onToggleExpand = () => {
    this.setState({
      isExpanded: !this.state.isExpanded,
    });
  };

  render() {
    const { loading, key, isExpanded, matrixId1, matrixId2 } = this.state;
    const { patient, pathologies, nextConsultationDate, forms } = this.props;
    if (!patient) {
      return null;
    }

    return (
      <>
        <Header />
        <FlexWrap style={{ marginTop: 4 }} key={key}>
          <PatientIdentify
            patient={patient}
            pathologies={pathologies}
            nextConsultationDate={nextConsultationDate}
            onUpdate={this.onUpdatePatient}
          />
          <div style={{ width: 61 }} />
          <OngoingTreatment patient={patient} />
        </FlexWrap>

        {isExpanded ? (
          <BlockReturnHeader className="new">
            <ImageButton onClick={this.onToggleExpand}>
              <img src={backIcon} alt="" width="46" />
            </ImageButton>
            Résultats des derniers tests de suivi
          </BlockReturnHeader>
        ) : (
          <BlockHeader className="fit">
            Résultats des derniers tests de suivi
            <div style={{ position: "absolute", right: 77 }}>
              <DetailsButton
                type="link"
                onClick={this.onToggleExpand}
                style={{ marginTop: 32 }}
              >
                <img src={arrowIcon} alt="" style={{ marginRight: 8.6 }} />+ De
                détails
              </DetailsButton>
            </div>
          </BlockHeader>
        )}

        <Results forms={forms} isExpanded={isExpanded} />
        {!isExpanded && (
          <>
            <BlockHeader className="fit">
              Impact sur la qualité de vie
            </BlockHeader>
            <ImpactOnQuality
              forms={forms}
              matrixId1={matrixId1}
              matrixId2={matrixId2}
              onChangeMatrixId1={(v) => this.setState({ matrixId1: v })}
              onChangeMatrixId2={(v) => this.setState({ matrixId2: v })}
            />
          </>
        )}

        <LoadingOverlay loading={loading} />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    patient: state.PatientReducer.patient,
    nextConsultationDate: state.PatientReducer.nextConsultationDate,
    pathologies: state.PathologyReducer.pathologies,
    profileQuality: state.PatientReducer.profileQuality,
    error: state.PatientReducer.error,
    graphs: state.PatientReducer.graphs,
    forms: state.PatientReducer.forms,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addPatient: (req) => {
      dispatch(PatientActions.addPatient(req.data, req.cb));
    },
    updatePatient: (req) => {
      dispatch(PatientActions.updatePatient(req.data, req.cb));
    },
    saveVariable: (req) => {
      dispatch(
        PatientActions.saveVariableByName(
          req.patientId,
          req.variableName,
          req.data,
          req.cb
        )
      );
    },
    getVariable: (req) => {
      dispatch(
        PatientActions.getVariableByName(
          req.patientId,
          req.variableName,
          req.cb
        )
      );
    },
    getPatients: (req) => {
      dispatch(PatientActions.getPatients(req.cb));
    },
    getPatient: (req) => {
      dispatch(PatientActions.getPatient(req.patientId, req.cb));
    },
    getFormVariables: (req) => {
      dispatch(
        PatientActions.getFormVariables(req.patientId, req.groupName, req.cb)
      );
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PatientRecord);
