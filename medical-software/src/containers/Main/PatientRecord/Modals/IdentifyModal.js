import React from "react";
import {
  TabModal,
  ImageButton,
  FlexWrap,
  ThemeDatePicker,
  ThemeInput,
} from "../../_styles";
import { BlockWithTab } from "../_styles";
import {
  Input,
  Row,
  Col,
  Select,
  Form,
  Switch,
  Button,
  notification,
} from "antd";
import moment from "moment";
import { PhoneInput } from "../../NewPatient/_styles";
const { Option } = Select;

const shrinkIcon = require("../../../../assets/svgs/Shrink.svg");
const editIcon = require("../../../../assets/svgs/Pencil.svg");
const editLightIcon = require("../../../../assets/svgs/Pencil light.svg");
const calendarIcon = require("../../../../assets/svgs/Calendar.svg");
const calendarPinkIcon = require("../../../../assets/svgs/CalendarPink.svg");

export default class IdentifyModal extends React.Component {
  formRef = React.createRef();
  constructor(props) {
    super(props);
    this.state = {
      editing: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.visible !== this.props.visible &&
      nextProps.visible === false
    ) {
      this.setState(
        {
          editing: false,
        },
        () => {
          this.formRef.current.resetFields();
        }
      );
    }
  }

  handleOk = () => {};

  handleCancel = () => {};

  toggleEdit = () => {
    const { editing } = this.state;
    this.setState(
      {
        editing: !editing,
      },
      () => {
        // if (editing) {
        //   this.props.onClose();
        //   setTimeout(() => {
        //     this.onUpdate();
        //   }, 30);
        // }
      }
    );
  };

  onUpdate = () => {
    const { name, phone, birthdate } = this.state;
    if (name || phone || birthdate) {
      let data = {
        id: this.props.patient.id,
      };
      if (name) {
        const tmp = name.split(" ");
        data.firstname = tmp[0];
        if (tmp.length > 1) {
          data.lastname = tmp[1];
        }
      }
      if (phone) {
        data.phone = phone;
      }
      if (birthdate) {
        data.birthdate = moment(birthdate).format("YYYY-MM-DD");
      }
      this.props.onUpdate(data);
    }
  };

  onValuesChange = (changedValues, allValues) => {
    if (typeof changedValues.birthdate !== "undefined") {
      const age = moment().diff(changedValues.birthdate, "years");
      if (age < 8) {
        notification.warning({
          message: "Notification",
          description:
            "Le service de suivi par SMS suppose que le patient puisse lire et écrire ainsi qu'interpréter ses sensations visuelles et sensorielles. Ainsi nous considérons le service non-adapté aux enfants de moins de 8ans.",
        });
      } else if (age < 18) {
        notification.warning({
          message: "Notification",
          description:
            "Le patient étant mineur, nous vous invitons à saisir le numéro de mobile que vous indiquera son responsable légal.",
        });
      }
    }
  };

  onFinish = (values) => {
    const {
      PathologyId,
      birthdate,
      firstname,
      lastname,
      phone,
      zipcode,
      hasCaregiver,
      diagnosticDate,
      nextConsultationDate,
    } = values;
    const data = {
      id: this.props.patient.id,
      PathologyId,
      birthdate: birthdate ? birthdate.format("YYYY-MM-DD") : null,
      firstname,
      lastname,
      phone: phone.replace(/\s|_/g, ""),
      zipcode,
      hasCaregiver,
      diagnosticDate: diagnosticDate
        ? diagnosticDate.format("YYYY-MM-DD")
        : null,
      nextConsultationDate: nextConsultationDate
        ? nextConsultationDate.format("YYYY-MM-DD")
        : null,
    };

    this.props.onClose();
    setTimeout(() => {
      this.setState(
        {
          editing: false,
        },
        () => {
          this.props.onUpdate(data);
        }
      );
    }, 50);
  };

  render() {
    const {
      patient,
      pathologies,
      visible,
      onClose,
      nextConsultationDate,
    } = this.props;
    const { editing } = this.state;
    const {
      firstname,
      lastname,
      birthdate,
      phone,
      zipcode,
      diagnosticDate,
      PathologyId,
      hasCaregiver,
    } = patient;
    let hasCaregiverText = hasCaregiver ? "Oui" : "Non";
    if (this.formRef.current) {
      hasCaregiverText = this.formRef.current.getFieldValue("hasCaregiver")
        ? "Oui"
        : "Non";
    }

    return (
      <TabModal
        visible={visible}
        footer={null}
        closable={false}
        centered
        width={1000}
      >
        <BlockWithTab className="modal-content">
          <div className="title">Identité du patient</div>
          <Form
            ref={this.formRef}
            name="control-ref"
            onFinish={this.onFinish}
            onValuesChange={this.onValuesChange}
            initialValues={{
              firstname,
              lastname,
              birthdate: birthdate
                ? moment(birthdate)
                : moment().subtract(50, "years"),
              phone,
              zipcode,
              diagnosticDate: diagnosticDate ? moment(diagnosticDate) : null,
              PathologyId,
              hasCaregiver,
              nextConsultationDate: nextConsultationDate
                ? moment(nextConsultationDate)
                : null,
            }}
          >
            <div className="content">
              <Row gutter={[16, 31]}>
                <Col md={5}>
                  <Form.Item name="firstname">
                    <Input
                      placeholder="|Prénom"
                      maxLength={12}
                      disabled={!editing}
                    />
                  </Form.Item>
                </Col>
                <Col md={5}>
                  <Form.Item name="lastname">
                    <Input
                      placeholder="|Nom"
                      maxLength={12}
                      disabled={!editing}
                    />
                  </Form.Item>
                </Col>
                <Col md={4}>
                  {editing ? (
                    <Form.Item name="birthdate">
                      <ThemeDatePicker
                        format="DD/MM/YYYY"
                        placeholder="JJ/DD/AAAA"
                        allowClear={false}
                        disabled={!editing}
                        suffixIcon={<img src={calendarIcon} alt="" />}
                      />
                    </Form.Item>
                  ) : (
                    <ThemeInput
                      placeholder="Age"
                      value={
                        this.formRef.current
                          ? `${moment().diff(
                              this.formRef.current.getFieldValue("birthdate"),
                              "years"
                            )} ans`
                          : birthdate
                          ? `${moment().diff(birthdate, "years")} ans`
                          : ""
                      }
                      disabled={true}
                    />
                  )}
                </Col>
                <Col md={6}>
                  <Form.Item name="phone">
                    <PhoneInput
                      // eslint-disable-next-line no-octal-escape
                      mask="+3\3 9 99 99 99 99"
                      maskplaceholder="Numéro"
                      alwaysShowMask={true}
                      className={`in-modal ${editing ? "" : "disabled"}`}
                      disabled={!editing}
                    />
                  </Form.Item>
                </Col>
                <Col md={4}>
                  <Form.Item name="zipcode">
                    <Input placeholder="|Postal" disabled={!editing} />
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={[37, 37]}>
                <Col md={8}>
                  <Form.Item
                    name="PathologyId"
                    label="Pathologie principale suivie"
                    style={{ display: "block" }}
                  >
                    <Select disabled={!editing}>
                      {typeof pathologies !== "undefined"
                        ? pathologies.map((pathology, index) => {
                            return (
                              <Option
                                key={`pathology-${index}`}
                                value={pathology.id}
                              >
                                {pathology.name}
                              </Option>
                            );
                          })
                        : null}
                    </Select>
                  </Form.Item>
                </Col>
                <Col md={7}>
                  <Form.Item name="diagnosticDate" label="Année du diagnostic">
                    <ThemeDatePicker
                      format="YYYY"
                      picker="year"
                      placeholder=""
                      allowClear={false}
                      style={{ width: 150 }}
                      disabled={!editing}
                      suffixIcon={<img src={calendarIcon} alt="" />}
                    />
                  </Form.Item>
                </Col>
                <Col md={9} className="pink">
                  <Form.Item
                    name="nextConsultationDate"
                    label="Date de la prochaine consultation"
                  >
                    <ThemeDatePicker
                      format="DD/MM/YYYY"
                      placeholder=""
                      allowClear={false}
                      style={{ width: 221 }}
                      disabled={!editing}
                      suffixIcon={<img src={calendarPinkIcon} alt="" />}
                    />
                  </Form.Item>
                </Col>
                <Col md={12}>
                  <Form.Item
                    name="hasCaregiver"
                    label="La patient a un aidant à ses côtés"
                    valuePropName="checked"
                    style={{ display: "block" }}
                  >
                    {editing ? (
                      <Switch style={{ marginLeft: 46, zIndex: 1 }} />
                    ) : (
                      <span className="disabled">{hasCaregiverText}</span>
                    )}
                  </Form.Item>
                  <div>
                    {editing && (
                      <FlexWrap className="select">
                        <span>Non</span>
                        <div style={{ width: 20 }}></div>
                        <span className="oui">Oui</span>
                      </FlexWrap>
                    )}
                  </div>
                </Col>
              </Row>
              <div className="valider">
                <Button
                  htmlType="submit"
                  style={{ visibility: editing ? "visible" : "hidden" }}
                >
                  Valider
                </Button>
              </div>
              <FlexWrap className="buttons">
                <ImageButton
                  onClick={this.toggleEdit}
                  className={editing ? "selected" : ""}
                >
                  <img
                    src={editing ? editLightIcon : editIcon}
                    alt=""
                    width="37"
                  />
                </ImageButton>
                <ImageButton style={{ marginLeft: 5 }} onClick={onClose}>
                  <img src={shrinkIcon} alt="" width="44" />
                </ImageButton>
              </FlexWrap>
              <div className="clearfix"></div>
            </div>
          </Form>
        </BlockWithTab>
      </TabModal>
    );
  }
}
