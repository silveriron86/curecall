import React from "react";
import moment from "moment";
import {
  TabModal,
  ImageButton,
  FlexWrap,
  verticalCenter,
  ConfirmModal,
  ThemeInput,
  ThemeDatePicker,
} from "../../_styles";
import { BlockWithTab, RedArea } from "../_styles";
import { Form, Button, Select, Table } from "antd";
import GraphWarning from "../../../../components/GraphWarning";
import Utils from "../../../../utils";
import LoadingOverlay from "../../../../components/LoadingOverlay";
import { MainWrapper } from "../../../Auth/_styles";

const shrinkIcon = require("../../../../assets/svgs/Shrink.svg");
const editIcon = require("../../../../assets/svgs/Pencil.svg");
const editLightIcon = require("../../../../assets/svgs/Pencil light.svg");
const deleteIcon = require("../../../../assets/svgs/delete.svg");
const addIcon = require("../../../../assets/svgs/Ajouter.svg");
const calendarPinkIcon = require("../../../../assets/svgs/CalendarPink.svg");

const options1 = [
  "Goutte(s) de collyre",
  "Comprimé(s) / Gélule(s)",
  "Unité(s) / autre",
];

export default class TreatmentModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editing: false,
      confirmModalVisible: false,
      id: -1,
      forms: [],
      initialValues: {},
      updated: 0,
    };
  }

  handleOk = () => {
    this.setState(
      {
        editing: false,
      },
      () => {
        this.props.onClose();
      }
    );
  };

  toggleEdit = () => {
    const { editing } = this.state;
    if (editing === false) {
      const { treatments } = this.props;
      if (treatments && treatments.length > 0) {
        let initialValues = {};
        treatments.forEach((row, i) => {
          initialValues[`drugName${row.id}`] = row.drugName;
          initialValues[`drugIntake${row.id}`] = row.drugIntake;
          initialValues[`drugForm${row.id}`] = row.drugForm;
          initialValues[`frequencyIntake${row.id}`] = row.frequencyIntake
            ? parseInt(row.frequencyIntake, 10)
            : row.frequencyIntake;
          initialValues[`startOfTreatment${row.id}`] = moment(
            row.startOfTreatment
          );
          initialValues[`endOfTreatment${row.id}`] = row.endOfTreatment
            ? moment(row.endOfTreatment)
            : null;
        });
        this.setState({
          initialValues,
          editing: !editing,
          forms: JSON.parse(JSON.stringify(treatments)),
        });
        return;
      }
    }

    this.setState({
      editing: !editing,
    });
  };

  toggleConfirmModalVisible = () => {
    this.setState({
      confirmModalVisible: !this.state.confirmModalVisible,
    });
  };

  onClickDelete = (id) => {
    console.log("id= ", id);
    this.setState({
      id,
      confirmModalVisible: true,
    });
  };

  onAddEmpty = () => {
    const { initialValues } = this.state;
    let forms = JSON.parse(JSON.stringify(this.state.forms));
    const data = {
      drugName: null,
      drugIntake: null,
      drugForm: null,
      frequencyIntake: null,
      startOfTreatment: null,
      endOfTreatment: null,
    };
    forms.unshift(data);
    this.setState({ initialValues: { ...initialValues, ...data }, forms });
  };

  handleDelete = () => {
    const { id } = this.state;
    this.setState({ confirmModalVisible: false }, () => {
      this.props.onDelete(id);
    });
  };

  onFinish = (values) => {
    console.log(values);
    const { forms } = this.state;

    let modifiedData = [];
    forms.forEach((row, i) => {
      const {
        drugName,
        drugIntake,
        drugForm,
        frequencyIntake,
        startOfTreatment,
        endOfTreatment,
      } = row;
      let oldData = {
        drugName,
        drugIntake,
        drugForm,
        frequencyIntake,
        startOfTreatment: moment(startOfTreatment).format(
          "YYYY-MM-DD HH:mm:ss"
        ),
        endOfTreatment: endOfTreatment
          ? moment(endOfTreatment).format("YYYY-MM-DD HH:mm:ss")
          : null,
      };

      let data = {
        drugName: values[`drugName${row.id}`],
        drugIntake: values[`drugIntake${row.id}`],
        drugForm: values[`drugForm${row.id}`],
        frequencyIntake:
          typeof values[`frequencyIntake${row.id}`] !== "undefined" &&
          values[`frequencyIntake${row.id}`] !== null
            ? values[`frequencyIntake${row.id}`].toString()
            : null,
        startOfTreatment:
          typeof values[`startOfTreatment${row.id}`] !== "undefined" &&
          values[`startOfTreatment${row.id}`]
            ? values[`startOfTreatment${row.id}`].format("YYYY-MM-DD HH:mm:ss")
            : null,
        endOfTreatment:
          typeof values[`endOfTreatment${row.id}`] !== "undefined" &&
          values[`endOfTreatment${row.id}`]
            ? values[`endOfTreatment${row.id}`].format("YYYY-MM-DD HH:mm:ss")
            : null,
      };

      if (JSON.stringify(oldData) !== JSON.stringify(data)) {
        let t = JSON.parse(JSON.stringify(data));
        if (t.endOfTreatment === null) {
          delete t.endOfTreatment;
        }
        if (t.drugIntake === null) {
          delete t.drugIntake;
        }
        if (t.drugForm === null) {
          delete t.drugForm;
        }
        if (t.frequencyIntake === null) {
          delete t.frequencyIntake;
        }

        if (typeof row.id !== "undefined") {
          modifiedData.push({
            data: t,
            id: row.id,
          });
        }
      }
    });

    if (typeof values.drugName !== "undefined") {
      // create
      const {
        drugName,
        drugIntake,
        drugForm,
        frequencyIntake,
        startOfTreatment,
        endOfTreatment,
      } = values;

      let data = {
        drugName,
        startOfTreatment,
      };
      if (typeof drugIntake !== "undefined") {
        data.drugIntake = drugIntake;
      }
      if (typeof drugForm !== "undefined") {
        data.drugForm = drugForm;
      }
      if (typeof frequencyIntake !== "undefined") {
        data.frequencyIntake = frequencyIntake.toString();
      }
      if (typeof endOfTreatment !== "undefined") {
        data.endOfTreatment = endOfTreatment;
      }

      this.props.onAdd(data, () => {
        if (modifiedData.length === 0) {
          this.setState({
            editing: false,
          });
        }
      });
    }

    if (modifiedData.length > 0) {
      this.setState(
        {
          updated: 0,
        },
        () => {
          modifiedData.forEach((row, i) => {
            this.props.onUpdate(row.id, row.data, () => {
              this.setState(
                {
                  updated: this.state.updated + 1,
                },
                () => {
                  console.log(
                    "***** ",
                    this.state.updated,
                    modifiedData.length
                  );
                  if (this.state.updated === modifiedData.length) {
                    this.setState({
                      editing: false,
                    });
                  }
                }
              );
            });
          });
        }
      );
    }

    if (typeof values.drugName === "undefined" && modifiedData.length === 0) {
      this.setState({
        editing: false,
      });
    }
  };

  render() {
    const { loading, visible, treatments } = this.props;
    const { editing, confirmModalVisible, forms, initialValues } = this.state;
    const { sideEffects } = this.props.patient;
    let ocularSides = [];
    let systemicSides = [];
    if (sideEffects) {
      sideEffects.forEach((r, i) => {
        // if (sideEffects.ocularSideEffects) {
        if (1) {
          if (
            r.name !== "ocularSideEffects" &&
            r.name.indexOf("ocularSideEffects") === 0
          ) {
            ocularSides.push(r.display);
          }
        }
        // if (sideEffects.systemicSideEffects) {
        if (1) {
          if (
            r.name !== "systemicSideEffects" &&
            r.name.indexOf("systemicSideEffects") === 0
          ) {
            systemicSides.push(r.display);
          }
        }
      });
    }

    const columns = [
      {
        title: "drugName",
        dataIndex: "drugName",
      },
      {
        title: "drugIntake",
        dataIndex: "drugIntake",
      },
      {
        title: "drugForm",
        dataIndex: "drugForm",
      },
      {
        title: "frequencyIntake",
        dataIndex: "frequencyIntake",
        width: 30,
        render: (frequencyIntake) =>
          `${frequencyIntake ? frequencyIntake : 0}/Jour`,
      },
      {
        title: "startOfTreatment",
        dataIndex: "startOfTreatment",
        render: (startOfTreatment, record) => {
          return (
            <>
              <span className="blue">À partir du&nbsp;</span>
              {Utils.formatDate(startOfTreatment)}
              {record.endOfTreatment && (
                <>
                  <span className="blue">&nbsp;jusqu’au&nbsp;</span>
                  {Utils.formatDate(record.endOfTreatment)}
                </>
              )}
            </>
          );
        },
      },
      {
        title: "id",
        dataIndex: "id",
        width: 20,
        render: (id) => {
          return (
            <ImageButton onClick={() => this.onClickDelete(id)}>
              <img src={deleteIcon} alt="" />
            </ImageButton>
          );
        },
      },
    ];

    let redArea = null;
    if (ocularSides.length > 0 && systemicSides.length > 0) {
      redArea = (
        <RedArea>
          <div className="description">
            <GraphWarning score={8} style={{ marginRight: 14.4 }} />
            Votre patient a signalé un problème avec son traitement il y a moins
            de 4 semaines.
          </div>
          <div className="flex">
            <div className="col">
              <u>Effets secondaires liés à son traitement</u>
              <ul>
                {ocularSides.map((o, i) => {
                  return <li key={`o-${i}`}>{o}</li>;
                })}
              </ul>
            </div>
            <div className="col">
              <u>Effets secondaires systémiques ressentis par le patient</u>
              <ul>
                {systemicSides.map((s, i) => {
                  return <li key={`s-${i}`}>{s}</li>;
                })}
              </ul>
            </div>
          </div>
        </RedArea>
      );
    }

    return (
      <TabModal
        visible={visible}
        footer={null}
        closable={false}
        centered
        className="treatment-modal"
      >
        <LoadingOverlay loading={loading} block={true} />
        <BlockWithTab className="modal-content">
          <div className="title">Traitement en cours</div>
          <div className="content">
            <MainWrapper className="treatment">
              {editing && (
                <ImageButton
                  className="treatment-add"
                  onClick={this.onAddEmpty}
                >
                  <img src={addIcon} alt="" style={{ width: 80 }} />
                </ImageButton>
              )}
              <div
                style={
                  editing
                    ? {
                        minHeight: 300,
                        height: "100%",
                      }
                    : {
                        minHeight: 300,
                        height: "calc(100% - 50px)",
                        overflow: "auto",
                      }
                }
              >
                {editing ? (
                  <Form
                    initialValues={initialValues}
                    ref={this.formRef}
                    onFinish={this.onFinish}
                  >
                    {forms.map((row, index) => {
                      const rowId = typeof row.id !== "undefined" ? row.id : "";
                      return (
                        <div key={`form-${index}`}>
                          <FlexWrap className="treatment-row">
                            <Form.Item
                              name={`drugName${rowId}`}
                              className="col"
                              style={{ marginLeft: 0 }}
                              rules={[{ required: true }]}
                            >
                              <ThemeInput
                                placeholder="| Nom du médicament"
                                style={{ width: 290 }}
                              />
                            </Form.Item>
                            <Form.Item
                              name={`drugIntake${rowId}`}
                              className="col"
                            >
                              <ThemeInput
                                placeholder="| 0"
                                style={{ width: 55 }}
                              />
                            </Form.Item>
                            <Form.Item
                              name={`drugForm${rowId}`}
                              className="col"
                            >
                              <Select style={{ width: 230 }}>
                                {options1.map((opt, index) => {
                                  return (
                                    <Select.Option
                                      key={`drugForm-${index}`}
                                      value={opt}
                                    >
                                      {opt}
                                    </Select.Option>
                                  );
                                })}
                              </Select>
                            </Form.Item>
                            <Form.Item
                              name={`frequencyIntake${rowId}`}
                              className="col"
                              style={{ marginRight: 0 }}
                            >
                              <Select
                                placeholder="0/Jour"
                                style={{ width: 110 }}
                              >
                                {[1, 2, 3, 4, 5].map((opt, index) => {
                                  return (
                                    <Select.Option
                                      key={`frequencyIntake-${index}`}
                                      value={opt}
                                    >
                                      {opt}/Jour
                                    </Select.Option>
                                  );
                                })}
                              </Select>
                            </Form.Item>
                            <div className="col">
                              <span className="blue">À partir du</span>
                            </div>
                            <Form.Item
                              name={`startOfTreatment${rowId}`}
                              className="col"
                              rules={[{ required: true }]}
                            >
                              <ThemeDatePicker
                                format="DD/MM/YYYY"
                                placeholder="JJ/DD/AAAA"
                                allowClear={false}
                                suffixIcon={
                                  <img src={calendarPinkIcon} alt="" />
                                }
                                style={{ width: 147 }}
                              />
                            </Form.Item>
                            <div className="col">
                              <span className="blue">au</span>
                            </div>
                            <Form.Item
                              name={`endOfTreatment${rowId}`}
                              className="col"
                            >
                              <ThemeDatePicker
                                format="DD/MM/YYYY"
                                placeholder="JJ/DD/AAAA"
                                allowClear={false}
                                suffixIcon={
                                  <img src={calendarPinkIcon} alt="" />
                                }
                              />
                            </Form.Item>
                          </FlexWrap>
                          <hr
                            style={{
                              height: 3,
                              backgroundColor: "#D3D3D3",
                              border: 0,
                              margin: "4px 15px 12px",
                            }}
                          />
                        </div>
                      );
                    })}

                    {forms.length > 0 && (
                      <div
                        className="valider"
                        style={{
                          position: "absolute",
                          width: "100%",
                          paddingTop: 5,
                          bottom: 5,
                        }}
                      >
                        <Button
                          htmlType="submit"
                          style={{ visibility: editing ? "visible" : "hidden" }}
                        >
                          Valider
                        </Button>
                      </div>
                    )}

                    {redArea}
                  </Form>
                ) : (
                  <>
                    <Table
                      className="patient-treatments"
                      showHeader={false}
                      dataSource={treatments}
                      columns={columns}
                      pagination={false}
                    />
                    {redArea}
                  </>
                )}
              </div>

              <ConfirmModal
                title={null}
                closable={false}
                maskClosable={false}
                footer={null}
                centered
                visible={confirmModalVisible}
              >
                <p>Souhaitez-vous supprimer ce traitement ?</p>
                <FlexWrap style={verticalCenter}>
                  <Button
                    type="text"
                    className="cancel-btn"
                    onClick={this.toggleConfirmModalVisible}
                  >
                    Annuler
                  </Button>
                  <div style={{ width: 120 }} />
                  <Button
                    type="text"
                    className="ok-btn"
                    onClick={this.handleDelete}
                  >
                    Supprimer
                  </Button>
                </FlexWrap>
              </ConfirmModal>

              <div style={{ height: 50 }}>
                <FlexWrap className="buttons" style={{ bottom: 0, right: 0 }}>
                  <ImageButton
                    onClick={this.toggleEdit}
                    className={editing ? "selected" : ""}
                  >
                    <img
                      src={editing ? editLightIcon : editIcon}
                      alt=""
                      width="37"
                    />
                  </ImageButton>
                  <ImageButton
                    style={{ marginLeft: 5 }}
                    onClick={this.handleOk}
                  >
                    <img src={shrinkIcon} alt="" width="44" />
                  </ImageButton>
                </FlexWrap>
              </div>
            </MainWrapper>
          </div>
        </BlockWithTab>
      </TabModal>
    );
  }
}
