import React from "react";
import { ImageButton, ThemeModal } from "../../_styles";

const closeIcon = require("../../../../assets/svgs/Close.svg");

export default class LearnMoreModal extends React.Component {
  handleOk = () => {};

  handleCancel = () => {};

  render() {
    const { chartIndex, visible, onClose } = this.props;

    return (
      <ThemeModal
        className="learn-more"
        visible={visible}
        footer={null}
        closable={false}
        width={1038}
        centered
      >
        {chartIndex === 9 ? (
          <>
            <h1>
              Suivi de la qualité de vie : Impact perçu par le patient sur sa
              qualité de vie
            </h1>
            <h2>Qu'est ce que le suivi de la qualité de vie dans Curecall ?</h2>
            <p>
              Pour vos patients ayant un traitement long court et de fond,
              Curecall s'occupe de collecter automatiquement leurs ressentis,
              expérience vécu et l'évolution déclarée de leur qualité de vie.
            </p>
            <p>
              Le suivi de qualité de vie permet de suivre les données de vie
              réelle des patients en lien avec sa qualité de vie ressenti.{" "}
            </p>
            <h2>Comment les données sont récoltées ?</h2>
            <p>
              Le patient reçoit une fois par mois le formulaire de suivi de sa
              qualité de vie (et plus si le patient en ressent le besoin), dans
              le but d’identifier les problèmes auxquels été confronté suite à
              la progression de la maladie.{" "}
            </p>
            <p>
              A partir des réponses du patient, un score bon, moyen ou mauvais
              sera attribué, évaluant l'impact du traitement et de la pathologie
              sur sa qualité de vie.
            </p>
            <h2>Comment lire le graphique ?</h2>
            <p>
              Ce schéma reprend les 7 dimensions étudiées dans le cadre de
              d’évaluation de la qualité de vie : 
              <br />
              <ul>
                <li>Contraintes</li>
                <li>Image de soi</li>
                <li>Anxiété</li>
                <li>Psychisme</li>
                <li>Prise en charge</li>
                <li>Vie quotidienne</li>
                <li>Conduite</li>
              </ul>
              Ce graphe représente les scores qualité de vie par dimension. Ces
              scores sont présentés sous forme de « radars ». A partir des
              réponses du patient, un score bon, moyen ou mauvais sera attribué,
              évaluant la gêne ressentie par le patient pour chaque dimension
              testée. Les scores par dimension sont obtenus par sommation simple
              des questions de la dimension.
              <br />
              Les bars qui se trouvent sur la droite des “radars” correspondent
              à l’évolution de chaque dimension dans le temps, avec un score
              attribué de niveau normal à inquiétant, un code couleur sera
              attribué selon la gravité du score du bleu au rouge. 
              <br />
              Les scores qualité sont présentés sous forme de « radars ». A
              partir des réponses du patient, un score bon, moyen ou mauvais
              sera attribué, évaluant la gêne ressentie par le patient pour
              chaque dimension testée. Les scores par dimension sont obtenus par
              sommation simple des questions de la dimension.
            </p>
          </>
        ) : (
          <>
            <h1>Suivi des symptômes : Impact perçu par le patient</h1>
            <h2>Qu'est ce que le suivi des symptômes dans Curecall ?</h2>
            <p>
              Pour vos patients ayant un traitement long court et de fond,
              Curecall s'occupe de collecter automatiquement leurs ressentis,
              expérience vécu et l'évolution des symptômes déclarés.
            </p>
            <p>
              Le suivi de symptômes permet de suivre les données de vie réelle
              des patients en lien avec l'impact du traitement ressenti par le
              patient sur ses symptômes.
            </p>
            <h2>Comment les données sont récoltées ?</h2>
            <p>
              Le patient reçoit une fois par mois le formulaire de la fonction
              visuelle (et plus si le patient en ressent le besoin), dans le but
              d’identifier les problèmes auxquels été confronté suite à la
              progression de la maladie.
            </p>
            <p>
              Le suivi de ces symptômes vous permet de suivre l’évolution de la
              pathologie et son impact sur la fonction visuelle
              <br />
              A partir des réponses du patient, un score bon, moyen ou mauvais
              sera attribué, évaluant la perception des symptômes par le
              patient.
              <br />
              Pour faciliter la lecture des scores, on ne fait pas apparaître le
              score brut, mais le score exprimé en pourcentage. Par souci de
              simplification, dans la représentation synthétique en pourcentage,
              le score maximum possible est de 10.
            </p>
            <h2>Comment lire le graphique ?</h2>
            <p>
              Ce graphique représente une vue globale de l’évolution de la
              vision du patient dans le cadre du suivi médicale oculaire. Chaque
              courbe correspond au suivi d’un symptôme particulier mesurer dans
              le cadre de l’évaluation visuelle au fil du temps. Les symptômes
              suivis dans ce schéma sont les suivants : 
              <br />
              <ul>
                <li>Perception des contrastes</li>
                <li>Sensibilité douloureuse</li>
                <li>Éblouissement face à la lumière</li>
                <li>Acuité visuelle</li>
                <li>Fatigue visuelle</li>
                <li>Sécheresse oculaire</li>
                <li>Cécité nocturne</li>
              </ul>
              <br />
              Vous pouvez voir le détail de chaque symptôme suivi et l’intensité
              du gêne ressentie par le patient interprété par un triangle de
              signalisation.
            </p>
          </>
        )}
        <ImageButton className="close-btn" onClick={onClose}>
          <img src={closeIcon} alt="" width="30" />
        </ImageButton>
      </ThemeModal>
    );
  }
}
