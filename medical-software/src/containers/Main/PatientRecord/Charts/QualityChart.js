import React from "react";
import ReactHighcharts from "react-highcharts";
import HighchartsMore from "highcharts-more";
import { Button } from "antd";
import moment from "moment";
import { ChartBlock } from "../_styles";
import { CopyButton } from "../../NewPatient/_styles";
import { DetailsButton } from "../../../../components/_styles";
import { FlexWrap, GraphPlaceholder } from "../../_styles";
import { connect } from "react-redux";
import { PatientActions } from "../../../../actions";
import LoadingOverlay from "../../../../components/LoadingOverlay";
import GraphWarning from "../../../../components/GraphWarning";
import Utils from "../../../../utils";

HighchartsMore(ReactHighcharts.Highcharts);

class QualityChart extends React.Component {
  state = {
    isSelected: false,
    visibleSelf: false,
    type: -1,
    loading: true,
    hasData: false,
    childrenIndex: -1,
  };

  onSelect = () => {
    const { isSelected } = this.state;
    this.props.onSelect(isSelected ? 9 : 0);
    this.setState({
      isSelected: !isSelected,
    });
  };

  onTouch = (type, childrenIndex) => {
    this.setState({
      type,
      childrenIndex,
    });
  };

  onSelectType = (type) => {
    this.setState({
      type,
      childrenIndex: -1,
      visibleSelf: true,
    });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.profileQuality !== null) {
      if (
        this.props.profileQuality === null ||
        JSON.stringify(nextProps.profileQuality) !==
          JSON.stringify(this.props.profileQuality)
      ) {
        const { isExpanded } = nextProps;
        this.renderGraph(isExpanded, nextProps.profileQuality);
      }
    }
  }

  componentDidMount() {
    const { isExpanded, patient, profileQuality } = this.props;
    this.renderGraph(isExpanded, profileQuality);
    this.props.getProfileQuality({
      patientId: patient.id,
      cb: () => {
        this.setState({
          loading: false,
        });
      },
    });
  }

  renderGraph = (isExpanded, profileQuality) => {
    if (
      typeof profileQuality === "undefined" ||
      profileQuality === null ||
      profileQuality.history.length === 0
    ) {
      this.setState({
        hasData: false,
      });
    } else {
      this.setState(
        {
          hasData: true,
        },
        () => {
          var d3 = window.d3;
          d3.select(window.self.frameElement).style("height", "300px");
          // var d = [
          //   [
          //     { axis: "Vie quotidienne", value: 0.75 },
          //     { axis: "Prise en charge", value: 0.45 },
          //     { axis: "Contraintes", value: 0.2 },
          //     { axis: "Image de soi", value: 0.7 },
          //     { axis: "Anxiété", value: 0.1 },
          //     { axis: "Conduite", value: 0.4 },
          //     { axis: "Vie quotidienne", value: 0.2 },
          //   ],
          //   [
          //     { axis: "Vie quotidienne", value: 0.3 },
          //     { axis: "Prise en charge", value: 0.5 },
          //     { axis: "Contraintes", value: 0.7 },
          //     { axis: "Image de soi", value: 0.2 },
          //     { axis: "Anxiété", value: 0.4 },
          //     { axis: "Conduite", value: 0.2 },
          //     { axis: "Vie quotidienne", value: 0.1 },
          //   ],
          //   [
          //     { axis: "Vie quotidienne", value: 0.2 },
          //     { axis: "Prise en charge", value: 0.3 },
          //     { axis: "Contraintes", value: 0.4 },
          //     { axis: "Image de soi", value: 0.5 },
          //     { axis: "Anxiété", value: 0.4 },
          //     { axis: "Conduite", value: 0.3 },
          //     { axis: "Vie quotidienne", value: 0.2 },
          //   ],
          // ];
          // var d = JSON.parse(JSON.stringify(graphs.QUALITYOFLIFE));
          // if (d.length > 0) {
          //   d = d.slice(d.length - 1);
          //   d[0].map((v, i) => {
          //     v.value = v.value / 10;
          //     return v;
          //   });
          // }
          var d = [];
          profileQuality.history.forEach((hist, i) => {
            d.push({
              axis: hist.label,
              value: hist.values[hist.values.length - 1].value / 10,
            });
          });
          d = [d];

          //Options for the Radar chart, other than default
          var config = {
            radius: 2,
            w: isExpanded ? 320 : 250,
            h: isExpanded ? 320 : 250,
            factor: 1,
            factorLegend: 1,
            levels: 10,
            // maxValue: 0,
            radians: 2 * Math.PI,
            opacityArea: 0.5,
            ToRight: 5,
            TranslateX: 100,
            TranslateY: 30,
            ExtraWidthX: 200,
            ExtraWidthY: 100,
            color: d3.scale.ordinal().range(["white"]),
            // color: d3.scale.ordinal().range(["#A2D2F0", "#AAEFE4", "#F2CFF8"]),
          };

          window.RadarChart.draw(".radarChart", d, config);
        }
      );
    }
  };

  render() {
    const {
      isSelected,
      visibleSelf,
      type,
      loading,
      hasData,
      childrenIndex,
    } = this.state;
    const { isExpanded, profileQuality } = this.props;
    const history = profileQuality ? profileQuality.history : [];
    // const { history } = profileQuality;

    let lastValues = [];
    if (type >= 0) {
      lastValues = history[type].values.slice(
        Math.max(history[type].values.length - 4, 0)
      );
    } else if (history.length > 0) {
      lastValues = history[0].values.slice(
        Math.max(history[0].values.length - 4, 0)
      );
    }

    return (
      <ChartBlock
        style={{ overflow: "hidden" }}
        className={
          isSelected ? "with-grad" : isExpanded ? "expanded" : "collapsed"
        }
      >
        <LoadingOverlay loading={loading} block={true} />
        <div className={isSelected ? "" : "hidden"}>
          <div className="gradient"></div>
          <div className="white-grad"></div>
        </div>
        {!isExpanded && (
          <FlexWrap className="between">
            <Button
              type="text"
              className="title"
              // onClick={this.onSelect}
              onMouseDown={this.onSelect}
              onMouseUp={this.onSelect}
            >
              <div className="graph-title">
                Impact de la pathologie vs Qualité de vie
              </div>
            </Button>
            <div style={{ position: "absolute", right: 0 }}>
              <DetailsButton
                type="link"
                onMouseDown={this.onSelect}
                onMouseUp={this.onSelect}
              >
                + De détails
              </DetailsButton>
            </div>
          </FlexWrap>
        )}

        {isExpanded && hasData && (
          <div style={{ display: "flex", alignItems: "flex-start" }}>
            <div>
              <div
                style={{
                  width: 520,
                  marginRight: 100,
                  marginTop: 15,
                  position: "relative",
                }}
              >
                <div className="radar-bg red"></div>
                <div className="radar-bg yellow"></div>
                <div className="radar-bg green"></div>
                <div
                  className="radarChart"
                  style={{
                    width: 520,
                    marginRight: 100,
                    marginTop: 15,
                    position: "relative",
                    zIndex: 4,
                  }}
                ></div>
              </div>
              <div
                className="label-btns"
                style={{ marginTop: hasData ? -30 : 40 }}
              >
                {history.map((hist, index) => {
                  const { label } = hist;
                  return (
                    <CopyButton
                      key={`label-${index}`}
                      type="text"
                      className={`large ${type === index ? "selected" : ""}`}
                      onClick={() => this.onSelectType(index)}
                    >
                      {label}
                      <div className="white"></div>
                      <div className="focused"></div>
                      <div className="grad"></div>
                    </CopyButton>
                  );
                })}
              </div>
            </div>
            <div style={{ width: 30 }}></div>
            <div>
              {type >= 0 ? (
                <div className="self-image">
                  <div
                    style={{
                      color: Utils.getGraphColor(
                        history[type].values[history[type].values.length - 1]
                          .value
                      ),
                    }}
                  >
                    {history[type].label}
                  </div>
                  <GraphWarning
                    score={
                      history[type].values[history[type].values.length - 1]
                        .value
                    }
                  />
                </div>
              ) : (
                <div className="self-image"></div>
              )}

              <div className="bar-charts">
                <div>
                  <div className="impact">Inquiétant</div>
                  <div className="impact second">Normal</div>
                </div>
                {lastValues.map((v, i) => {
                  return (
                    <div className="bar" key={`bar-${i}`}>
                      <div className="wrapper">
                        <div className="left"></div>
                        <div className="right"></div>
                        {type >= 0 && (
                          <>
                            <div className="value">
                              {visibleSelf && (
                                <div
                                  className="filled"
                                  style={{ height: (355.78 * v.value) / 10 }}
                                ></div>
                              )}
                            </div>
                            {visibleSelf && (
                              <div className="buttons">
                                <Button
                                  className="transparent"
                                  onClick={() => this.onTouch(type, i)}
                                ></Button>
                              </div>
                            )}
                          </>
                        )}
                      </div>
                      <p>{moment(v.date).format("DD/MM")}</p>
                    </div>
                  );
                })}

                <div className="bar-description">
                  {type >= 0 && childrenIndex === -1 ? (
                    "Cliquez pour voir le détail des questions posées à votre patient"
                  ) : type >= 0 && childrenIndex >= 0 ? (
                    <div
                      style={{
                        color: Utils.getGraphColor(
                          lastValues[childrenIndex].value,
                          "#8BCC33"
                        ),
                      }}
                    >
                      <div className="label-value">
                        {history[type].label}{" "}
                        {Number.isInteger(lastValues[childrenIndex].value)
                          ? lastValues[childrenIndex].value
                          : lastValues[childrenIndex].value.toFixed(1)}
                        /10
                      </div>
                      <div className="children">
                        {lastValues[childrenIndex].children.map((v, i) => {
                          return <div key={`c-${i}`}>{v.label}</div>;
                        })}
                      </div>
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        )}

        {hasData ? (
          <>
            {!isExpanded && (
              <div
                className="small"
                style={{
                  width: 420,
                  margin: "30px auto 0",
                  position: "relative",
                }}
              >
                <div className="radar-bg red"></div>
                <div className="radar-bg yellow"></div>
                <div className="radar-bg green"></div>
                <div
                  className="radarChart"
                  style={{
                    width: 420,
                    position: "relative",
                    zIndex: 4,
                  }}
                ></div>
              </div>
            )}
          </>
        ) : (
          <GraphPlaceholder>
            Données vie réelles et Traitement en cours.
          </GraphPlaceholder>
        )}
      </ChartBlock>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileQuality: state.PatientReducer.profileQuality,
    patient: state.PatientReducer.patient,
    graphs: state.PatientReducer.graphs,
    error: state.PatientReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getGraph: (req) => {
      dispatch(
        PatientActions.getGraph(req.patientId, req.variableGroup, req.cb)
      );
    },
    getProfileQuality: (req) => {
      dispatch(PatientActions.getProfileQuality(req.patientId, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(QualityChart);
