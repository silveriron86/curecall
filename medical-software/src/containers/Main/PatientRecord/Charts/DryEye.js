import React from "react";
import { ChartBlock } from "../_styles";
import ReactHighcharts from "react-highcharts";
import { Button } from "antd";
import moment from "moment";
import "moment/locale/fr";
import Utils from "../../../../utils";
import { DetailsButton } from "../../../../components/_styles";
import { connect } from "react-redux";
import { FlexWrap, GraphPlaceholder } from "../../_styles";
import GraphWarning from "../../../../components/GraphWarning";
moment.locale("fr");

class DryEyeChart extends React.Component {
  state = {
    isSelected: false,
  };

  onSelect = () => {
    const { isSelected } = this.state;
    this.props.onSelect(isSelected ? 7 : 0);
    this.setState({
      isSelected: !isSelected,
    });
  };

  render() {
    const { isSelected } = this.state;
    const { isExpanded, graphs } = this.props;
    let config = null;
    if (graphs && typeof graphs !== "undefined") {
      config = graphs.SYMPTOM
        ? JSON.parse(JSON.stringify(graphs.SYMPTOM))
        : null;
      if (config) {
        config.series.map((item, index) => {
          item.data.sort((a, b) => a[0] - b[0]);
          return item;
        });
        config.series = config.series.filter((item) => {
          return item.name === "Sécheresse oculaire";
        });

        config.tooltip.padding = 15;
        config.tooltip.style = {
          lineHeight: "30px",
        };
        config.tooltip["formatter"] = Utils.splineGraphTooltipFormatter;
        config.xAxis.labels.formatter = Utils.splineGraphLabesFormatter;
        config.chart.height = isExpanded ? 400 : 140;
        config.xAxis.labels.enabled = true;
        config.plotOptions = Utils.splineGraphPlotOptions("#F4C4BB");
      }
    }

    return (
      <ChartBlock
        style={{
          flex: 1,
          backgroundColor: isExpanded
            ? "transparent"
            : Utils.getGraphBG(config),
        }}
        className={
          isSelected ? "with-grad" : isExpanded ? "expanded" : "collapsed"
        }
      >
        <div className={isSelected ? "" : "hidden"}>
          <div className="gradient"></div>
          <div className="white-grad"></div>
        </div>
        {!isExpanded && (
          <FlexWrap className="between">
            <Button
              type="text"
              className="title"
              onMouseDown={this.onSelect}
              onMouseUp={this.onSelect}
            >
              <div className="graph-title">Sécheresse oculaire</div>
              <div className="sub-title">Valeur initiale : Moyen</div>
            </Button>
            <div style={{ position: "absolute", right: 0 }}>
              <GraphWarning score={Utils.getScore(config)} />
              <DetailsButton
                type="link"
                onMouseDown={this.onSelect}
                onMouseUp={this.onSelect}
              >
                + De détails
              </DetailsButton>
            </div>
          </FlexWrap>
        )}
        {isExpanded && (
          <div className="expanded sub-title">
            Données vie réelles et Traitement en cours
          </div>
        )}
        <div style={{ position: "relative", paddingLeft: isExpanded ? 80 : 0 }}>
          {isExpanded && (
            <>
              <div className="impact">
                Impact
                <br />
                important
              </div>
              <div className="impact second">
                Impact
                <br />
                faible
              </div>
            </>
          )}
          {config !== null && (
            <div className="chart-wrapper">
              <ReactHighcharts
                config={config}
                ref="chart"
                style={{ position: "absolute" }}
              ></ReactHighcharts>
            </div>
          )}
        </div>
        {(config === null ||
          config.series.length === 0 ||
          (config.series.length > 0 && config.series[0].data.length === 0)) && (
          <GraphPlaceholder>
            Données vie réelles et Traitement en cours.
          </GraphPlaceholder>
        )}
      </ChartBlock>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    graphs: state.PatientReducer.graphs,
    error: state.PatientReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(DryEyeChart);
