import React from "react";
import ReactHighcharts from "react-highcharts";
import { Button } from "antd";
import { ChartBlock } from "../_styles";
import moment from "moment";
import "moment/locale/fr";
import jQuery from "jquery";
import Utils from "../../../../utils";
import { DetailsButton } from "../../../../components/_styles";
import { GraphPlaceholder, FlexWrap } from "../../_styles";
import { connect } from "react-redux";
import { PatientActions } from "../../../../actions";
import LoadingOverlay from "../../../../components/LoadingOverlay";
moment.locale("fr");

const bottomArrowIcon = require("../../../../assets/svgs/Bottom.svg");
const rightArrowIcon = require("../../../../assets/svgs/Come.svg");

const graphsTitles = [
  "Perception des contrastes",
  "Eblouissement face à la lumière",
  "Cécité nocturne",
  "Fatigue visuelle",
  "Sécheresse oculaire",
  "Acuité visuelle",
  "Sensibilité douloureuse",
];

class SymptomChart extends React.Component {
  state = {
    isSelected: false,
    loading: false,
  };

  componentDidMount() {
    // const { patient } = this.props;
    // this.props.getGraph({
    //   patientId: patient.id,
    //   variableGroup: "SYMPTOM",
    //   graphType: "SYMPTOM",
    //   cb: (res) => {
    //     this.setState({
    //       loading: false,
    //     });
    //   },
    // });
  }

  onSelect = () => {
    const { isSelected } = this.state;
    this.props.onSelect(isSelected ? 1 : 0);
    this.setState({
      isSelected: !isSelected,
    });
  };

  onNext = () => {
    const { isSelected } = this.state;
    this.props.onSelect(10);
    this.setState({
      isSelected: !isSelected,
    });
  };

  render() {
    const { isSelected, loading } = this.state;
    const { isExpanded, graphs } = this.props;
    let config = null;
    if (graphs && typeof graphs !== "undefined") {
      config = graphs.SYMPTOM
        ? JSON.parse(JSON.stringify(graphs.SYMPTOM))
        : null;
      if (config && config.series.length > 0) {
        config.series.map((item, index) => {
          item.data.sort((a, b) => a[0] - b[0]);
          return item;
        });
        console.log("*** series = ", config.series);
        config.series = config.series.filter((item) => {
          return graphsTitles.indexOf(item.name) >= 0;
        });

        config.tooltip.padding = 15;
        config.tooltip.style = {
          lineHeight: "30px",
        };
        config.tooltip["formatter"] = function () {
          var toolTipTxt =
            "<div style='font-size: 13px'>" +
            moment.unix(this.x / 1000).format("LLL") +
            "</div>";
          jQuery.each(this.points, function (i, point) {
            let label = point.y;
            if (point.y < 5) {
              label = "Impact faible";
            } else if (point.y >= 5 && point.y < 7) {
              label = "Impact moyen";
            } else if (point.y >= 7 && point.y <= 10) {
              label = "Impact Important";
            }
            toolTipTxt +=
              '<br/><div style="color:' +
              point.series.color +
              '; font-size: 16px;">  ' +
              point.series.name +
              ": " +
              label +
              "</div>";
          });
          return toolTipTxt;
        };

        config.xAxis.labels.formatter = (v) => {
          return Utils.capitalize(moment(v.value).format("MMM"));
        };

        if (isExpanded) {
          config.chart.height = 400;
          config.legend.enabled = true;
          config.legend.itemStyle = {
            fontSize: "13px",
          };
          config.xAxis.labels.enabled = true;
        }
      }
    }

    return (
      <ChartBlock
        className={
          isSelected ? "with-grad" : isExpanded ? "expanded" : "collapsed"
        }
      >
        <LoadingOverlay loading={loading} block={true} />
        <div className={isSelected ? "" : "hidden"}>
          <div className="gradient"></div>
          <div className="white-grad"></div>
        </div>
        {!isExpanded && (
          <FlexWrap className="between">
            <Button
              type="text"
              className="title"
              onMouseDown={this.onSelect}
              onMouseUp={this.onSelect}
            >
              <div className="graph-title">
                Suivi des symptômes : Impact perçu par le patient
              </div>
            </Button>
            <div style={{ position: "absolute", right: 0 }}>
              <DetailsButton
                type="link"
                onMouseDown={this.onSelect}
                onMouseUp={this.onSelect}
              >
                + De détails
              </DetailsButton>
            </div>
          </FlexWrap>
        )}
        <div
          className="symptom-chart"
          style={{ paddingLeft: isExpanded ? 80 : 0 }}
        >
          {isExpanded && (
            <>
              <div className="impact">
                Impact
                <br />
                important
              </div>
              <div className="impact second">
                Impact
                <br />
                faible
              </div>
            </>
          )}
          {config ? (
            <div className="chart-wrapper">
              <ReactHighcharts config={config} ref="chart"></ReactHighcharts>
            </div>
          ) : (
            <GraphPlaceholder style={{ marginTop: isExpanded ? -160 : 0 }}>
              Votre patient sera régulièrement interrogé sur ses symptômes.
              C’est une façon pour vous de pouvoir visualiser les données de vie
              réelle de votre patient. Son appréciation est un élément important
              permettant d’évaluer en vie réelle l'évolution de sa pathologie.
            </GraphPlaceholder>
          )}
        </div>
        {isExpanded && (
          <div className="symptom-footer">
            <img
              src={bottomArrowIcon}
              alt=""
              style={{ marginTop: 17, marginBottom: 17 }}
              width="30"
            />
            <div className="back-desc">
              Votre patient communiquera dans cet espace les questions qu’il
              souhaite aborder avec vous lors de la prochaine consultation.
            </div>
            <Button type="text" className="next-link" onClick={this.onNext}>
              <img src={rightArrowIcon} alt="" width="46" />
              Carnet de suivi du patient
            </Button>
          </div>
        )}
      </ChartBlock>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    patient: state.PatientReducer.patient,
    graphs: state.PatientReducer.graphs,
    error: state.PatientReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getGraph: (req) => {
      dispatch(
        PatientActions.getGraph(req.patientId, req.variableGroup, req.cb)
      );
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SymptomChart);
