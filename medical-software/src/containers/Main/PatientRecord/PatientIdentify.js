import React from "react";
import { ImageButton } from "../_styles";
import { BlockWithTab } from "./_styles";
import { Row, Col } from "antd";
import moment from "moment";
import IdentifyModal from "./Modals/IdentifyModal";

const expandIcon1 = require("../../../assets/svgs/Enlarge.svg");

export default class PatientIdentify extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }

  setModalVisible = (visible) => {
    this.setState({
      modalVisible: visible,
    });
  };

  render() {
    const { patient, pathologies, nextConsultationDate } = this.props;
    const { firstname, lastname, birthdate, phone } = patient;
    const { modalVisible } = this.state;

    let pathologyName = "";
    if (patient.PathologyId && pathologies.length > 0) {
      const found = pathologies.find((p) => p.id === patient.PathologyId);
      if (found) {
        pathologyName = found.name;
      }
    }
    return (
      <>
        <div style={{ width: 515 }}>
          {!modalVisible && (
            <BlockWithTab>
              <div className="title">Identité du patient</div>
              <div className="content">
                <Row>
                  <Col md={12}>
                    {firstname || lastname
                      ? `${firstname} ${lastname}`
                      : "Prénom/Nom"}
                  </Col>
                  <Col md={4}>
                    {birthdate
                      ? `${moment().diff(birthdate, "years")} ans`
                      : "Age"}
                  </Col>
                  <Col md={8}>{phone ? phone : "Numéro"}</Col>
                </Row>
                <p>{pathologyName}</p>
                <ImageButton onClick={() => this.setModalVisible(true)}>
                  <img src={expandIcon1} alt="" width="44" />
                </ImageButton>
              </div>
            </BlockWithTab>
          )}
        </div>
        <IdentifyModal
          patient={patient}
          pathologies={pathologies}
          nextConsultationDate={nextConsultationDate}
          visible={modalVisible}
          onUpdate={this.props.onUpdate}
          onClose={() => this.setModalVisible(false)}
        />
      </>
    );
  }
}
