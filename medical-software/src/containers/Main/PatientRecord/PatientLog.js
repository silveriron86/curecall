import React from "react";
import { Button } from "antd";
import { FlexWrap, GraphPlaceholder } from "../_styles";
import { ChartBlock } from "./_styles";
import { DetailsButton } from "../../../components/_styles";

export default class PatientLog extends React.Component {
  state = {
    isSelected: false,
  };

  onSelect = () => {
    const { isSelected } = this.state;
    this.props.onSelect(isSelected ? 10 : 0);
    this.setState({
      isSelected: !isSelected,
    });
  };

  render() {
    const { isSelected } = this.state;
    const { isExpanded } = this.props;
    const data = []; // testing for now

    return (
      <ChartBlock
        className={`time-table ${
          isSelected ? "with-grad" : isExpanded ? "expanded" : "collapsed"
        }`}
        style={{ minHeight: 500 }}
      >
        <div className={isSelected ? "" : "hidden"}>
          <div className="gradient"></div>
          <div className="white-grad"></div>
        </div>
        {!isExpanded && (
          <FlexWrap className="between">
            <Button
              type="text"
              className="title"
              onMouseDown={this.onSelect}
              onMouseUp={this.onSelect}
              style={{ marginBottom: 60 }}
            >
              <div className="graph-title">Carnet de suivi du patient</div>
            </Button>
            <div style={{ position: "absolute", right: 0 }}>
              <DetailsButton
                type="link"
                onMouseDown={this.onSelect}
                onMouseUp={this.onSelect}
              >
                + De détails
              </DetailsButton>
            </div>
          </FlexWrap>
        )}
        {/* <div className="title">Carnet de suivi du patient</div> */}
        {data.length > 0 ? (
          <>
            <div className="grad-border"></div>
            <FlexWrap className="row">
              <div className="date">
                17/07
                <br />
                2020
              </div>
              <div className="description">
                Votre patient communiquera dans cet espace les questions qu’il
                souhaite aborder avec vous lors de la prochaine consultation.
              </div>
            </FlexWrap>
            <FlexWrap className="row">
              <div className="date">
                28/01
                <br />
                2020
              </div>
              <div className="description">
                Votre patient sera régulièrement interrogé sur ses symptômes.
                C’est une façon pour vous de pouvoir visualiser les données de
                vie réelle de votre patient. Son appréciation est un élément
                important permettant d’évaluer
              </div>
            </FlexWrap>
            <FlexWrap className="row">
              <div className="date">
                20/03
                <br />
                2020
              </div>
              <div className="description">
                Votre patient communiquera dans cet espace les questions qu’il
                souhaite aborder avec vous lors de la prochaine consultation.
              </div>
            </FlexWrap>
            <FlexWrap className="row">
              <div className="date">
                13/02
                <br />
                2020
              </div>
              <div className="description">
                Votre patient sera régulièrement interrogé sur ses symptômes.
                C’est une façon pour vous de pouvoir visualiser les données de
                vie réelle de votre patient. Son appréciation est un élément
                important permettant d’évaluer
              </div>
            </FlexWrap>
            <FlexWrap className="row">
              <div className="date">
                09/12
                <br />
                2020
              </div>
              <div className="description">
                Votre patient sera régulièrement interrogé sur ses symptômes.
                C’est une façon pour vous de pouvoir visualiser les données de
                vie réelle de votre patient. Son appréciation est un élément
                important permettant d’évaluer. Votre patient communiquera dans
                cet espace les questions qu’il souhaite aborder avec vous lors
                de la prochaine consultation. Votre patient communiquera dans
                cet espace les questions qu’il souhaite aborder avec vous lors
                de la prochaine consultation.
              </div>
            </FlexWrap>
          </>
        ) : (
          <GraphPlaceholder>
            Votre patient communiquera dans cet espace les questions qu’il
            souhaite aborder avec vous lors de la prochaine consultation.
          </GraphPlaceholder>
        )}
      </ChartBlock>
    );
  }
}
