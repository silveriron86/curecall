import styled from "@emotion/styled";

export const ImpactTables = styled("div")({
  width: "100%",
  marginTop: 35,
  ".row": {
    width: "100%",
    display: "flex",
    padding: "19px 0 !important",
    alignItems: "center",
    ".col": {
      display: "flex",
      alignItems: "center",
      "&:nth-of-type(1)": {
        width: 65,
        justifyContent: "center",
      },
      "&:nth-of-type(2)": {
        flex: 1,
        flexDirection: "column",
        alignItems: "flex-start",
        padding: "0 32px",
        fontFamily: "Nexa Bold",
        fontWeight: "normal",
        fontSize: 20,
        lineHeight: "24px",
        color: "#707070",
        ".description": {
          fontFamily: "Nexa Book",
          fontSize: 15,
          lineHeight: "19px",
          paddingLeft: 0,
        },
      },
      "&:nth-of-type(3)": {
        width: 129,
      },
    },
  },
});
