import React from "react";
import { FlexWrap } from "../../_styles";
import { BlockWrapper, BlockTitle, Flex1, NewSelect } from "../Results/_styles";
import { ChartBlock } from "../_styles";
import { Select } from "antd";
import { ReactComponent as BesoinSvg } from "../../../../assets/svgs/quality/i_besoin.svg";
import { ReactComponent as BesoinSvg1 } from "../../../../assets/svgs/quality/i_besoin1.svg";
import { ReactComponent as CloseSvg } from "../../../../assets/svgs/quality/i_close.svg";
// import { ReactComponent as TickSvg } from "../../../../assets/svgs/quality/i_tick.svg";
import { ImpactTables } from "./_styles";
import Empty from "./Empty";
import moment from "moment";
import { cloneDeep } from "lodash";
import Utils from "../../../../utils";

const { Option } = Select;
const tickImg = require("../../../../assets/svgs/quality/i_tick.svg");

export default class ImpactOnQuality extends React.Component {
  render() {
    const { forms, matrixId1, matrixId2 } = this.props;
    const { EVERYDAY_LIFE, THERAPEUTIC_CARE } = forms;
    const hasData = EVERYDAY_LIFE && EVERYDAY_LIFE.history.length > 0;

    if (!hasData || !matrixId1 || !matrixId2) {
      return <Empty />;
    }

    const { patientMatrixHistory } = EVERYDAY_LIFE;
    const history1 = EVERYDAY_LIFE.history;
    const history2 = THERAPEUTIC_CARE.history;
    const matrixHistory = cloneDeep(
      Utils.groupByDate(patientMatrixHistory)
    ).reverse();
    const icons = [<img src={tickImg} alt="" />, <CloseSvg />, <BesoinSvg />];
    const icons1 = [<CloseSvg />, <img src={tickImg} alt="" />, <BesoinSvg1 />];

    const lData = [
      {
        field: "difficultyReadAndWrite",
        icon: require("../../../../assets/svgs/quality/l1.svg"),
        label: "Lire et écrire",
      },
      {
        field: "difficultySeeingKnownPeople",
        icon: require("../../../../assets/svgs/quality/l2.svg"),
        label: "Reconnaissance des visages à 1m",
      },
      {
        field: "difficultyDoingDailyActivities",
        icon: require("../../../../assets/svgs/quality/l3.svg"),
        label: "Gestes de la vie quotidienne",
        description: "(Repas, conduite…)",
      },
      {
        field: "difficultyLookingScreens",
        icon: require("../../../../assets/svgs/quality/l4.svg"),
        label: "Utilisation téléphone Et outils de communication",
      },
      {
        field: "difficultyGesturalSkill",
        icon: require("../../../../assets/svgs/quality/l5.svg"),
        label: "Adresse gestuelle",
        description: "(Tendance à casser, renverser, se cogner…)",
      },
      {
        field: "difficultyMoveInside",
        icon: require("../../../../assets/svgs/quality/l6.svg"),
        label: "Déplacements intérieurs",
      },
      {
        field: "difficultyMoveOutside",
        icon: require("../../../../assets/svgs/quality/l7.svg"),
        label: "Déplacements extérieurs",
      },
    ];

    const rData = [
      {
        field: "difficultyTakeTreatmentEveryDay",
        icon: require("../../../../assets/svgs/quality/r1.svg"),
        label: "Prise traitement",
      },
      {
        field: "difficultyUsingTreatment",
        icon: require("../../../../assets/svgs/quality/r2.svg"),
        label: "Utilisation du traitement",
      },
      {
        field: "difficultyAdaptingTreatmentChange",
        icon: require("../../../../assets/svgs/quality/r3.svg"),
        label: "Changement de traitement",
      },
      {
        field: "effectiveTreatment",
        icon: require("../../../../assets/svgs/quality/r4.svg"),
        label: "A la sentiment que son traitement est efficace",
      },
      {
        field: "difficultyToBeUnderstoodByOthers",
        icon: require("../../../../assets/svgs/quality/r5.svg"),
        label: "Incompréhension des difficultés visuelles par les tiers",
      },
      {
        field: "feelDiscouragedFragile",
        icon: require("../../../../assets/svgs/quality/r6.svg"),
        label: "Découragé, déprime et se sent fragile à cause de sa pathologie",
      },
      {
        field: "careSatisfaction",
        icon: require("../../../../assets/svgs/quality/r7.svg"),
        label: "Satisfait de sa prise en charge",
      },
    ];

    return (
      <ChartBlock
        className="time-table impact-quality"
        style={{ height: "auto" }}
      >
        <FlexWrap className="full-height">
          <Flex1 className="r-border" style={{ padding: "0 52.5px" }}>
            <BlockWrapper>
              <FlexWrap className="between">
                <BlockTitle>
                  Le patient a des difficultés
                  <br />
                  au quotidien à / dans
                </BlockTitle>
                <NewSelect
                  dropdownClassName="blue"
                  style={{ width: 235 }}
                  value={matrixId1}
                  onChange={this.props.onChangeMatrixId1}
                >
                  {matrixHistory.map((h, i) => {
                    return (
                      <Option key={`md2-${i}`} value={h.patientMatrixId}>
                        {moment(h.date).format("DD/MM/YYYY")}
                      </Option>
                    );
                  })}
                </NewSelect>
              </FlexWrap>
              <ImpactTables>
                {lData.map((row, index) => {
                  const value = Utils.getMatrixValue(
                    history1,
                    row.field,
                    matrixId1
                  );
                  return (
                    <div key={`l-${index}`} className="row">
                      <div className="col">
                        <img src={row.icon} alt="" />
                      </div>
                      <div className="col">
                        {row.label}
                        <div className="description">{row.description}</div>
                      </div>
                      <div className="col" style={{ width: 129 }}>
                        {value !== "" ? icons[value] : ""}
                      </div>
                    </div>
                  );
                })}
              </ImpactTables>
            </BlockWrapper>
          </Flex1>
          <Flex1 style={{ padding: "0 52.5px" }}>
            <BlockWrapper>
              <FlexWrap className="between">
                <BlockTitle>
                  Le patient a des difficultés
                  <br />
                  dans sa prise en charge
                </BlockTitle>
                <NewSelect
                  dropdownClassName="blue"
                  style={{ width: 235 }}
                  value={matrixId2}
                  onChange={this.props.onChangeMatrixId2}
                >
                  {matrixHistory.map((h, i) => (
                    <Option key={`md2-${i}`} value={h.patientMatrixId}>
                      {moment(h.date).format("DD/MM/YYYY")}
                    </Option>
                  ))}
                </NewSelect>
              </FlexWrap>
              <ImpactTables>
                {rData.map((row, index) => {
                  let value = Utils.getMatrixValue(
                    history2,
                    row.field,
                    matrixId2
                  );
                  if (typeof value === "boolean") {
                    value = value === true ? 1 : 0;
                  }

                  return (
                    <div key={`l-${index}`} className="row">
                      <div className="col">
                        <img src={row.icon} alt="" />
                      </div>
                      <div className="col">
                        {row.label}
                        <div className="description">{row.description}</div>
                      </div>
                      <div className="col" style={{ width: 129 }}>
                        {value !== ""
                          ? row.field === "effectiveTreatment" ||
                            row.field === "careSatisfaction"
                            ? icons1[value]
                            : icons[value]
                          : ""}
                      </div>
                    </div>
                  );
                })}
              </ImpactTables>
            </BlockWrapper>
          </Flex1>
        </FlexWrap>
      </ChartBlock>
    );
  }
}
