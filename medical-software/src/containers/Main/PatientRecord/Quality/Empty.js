import React from "react";
import { FlexWrap } from "../../_styles";
import {
  BlockWrapper,
  BlockTitle,
  BlockPlaceholder,
  Flex1,
} from "../Results/_styles";
import { ChartBlock } from "../_styles";

export default class Empty extends React.Component {
  render() {
    return (
      <ChartBlock className="time-table impact-quality" style={{ height: 750 }}>
        <FlexWrap className="full-height">
          <Flex1 className="r-border" style={{ padding: "0 52.5px" }}>
            <BlockWrapper>
              <FlexWrap className="between">
                <BlockTitle>
                  Le patient a des difficultés
                  <br />
                  au quotidien à / dans
                </BlockTitle>
              </FlexWrap>

              <BlockPlaceholder>
                Le patient n’a pas encore évalué
                <br />
                ses contraintes de vie
              </BlockPlaceholder>
            </BlockWrapper>
          </Flex1>
          <Flex1 style={{ padding: "0 52.5px" }}>
            <BlockWrapper>
              <FlexWrap className="between">
                <BlockTitle>
                  Le patient a des difficultés
                  <br />
                  dans sa prise en charge
                </BlockTitle>
              </FlexWrap>

              <BlockPlaceholder>
                Le patient n’a pas encore renseigné les difficultés
                <br />
                rencontrées dans sa prise en charge
              </BlockPlaceholder>
            </BlockWrapper>
          </Flex1>
        </FlexWrap>
      </ChartBlock>
    );
  }
}
