import React from "react";
import { ImageButton } from "../_styles";
import { BlockWithTab, TreatmentsList } from "./_styles";
import TreatmentModal from "./Modals/TreatmentModal";
import { connect } from "react-redux";
import { PatientActions } from "../../../actions";

const expandIcon = require("../../../assets/svgs/Enlarge.svg");
// const notifyIcon = require("../../../assets/svgs/Notify.svg");

class OngoingTreatment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      overlayVisible: false,
      loading: false,
    };
  }

  componentDidMount() {
    const { patient } = this.props;
    this.props.getTreatments({
      patientId: patient.id,
      cb: (res) => {
        // console.log("Treatments = ", res);
      },
    });
  }

  setModalVisible = (visible) => {
    this.setState({
      modalVisible: visible,
    });
  };

  setOverlayVisible = () => {
    this.setState({
      overlayVisible: !this.state.overlayVisible,
    });
  };

  onUpdate = (id, data, callback) => {
    console.log(id);
    console.log(data);
    const { patient } = this.props;
    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.updateTreatment({
          patientId: patient.id,
          id,
          data,
          cb: (res) => {
            console.log("* updated treatment: ", res);
            this.props.getTreatments({
              patientId: patient.id,
              cb: (result) => {
                this.setState({
                  loading: false,
                });
                callback();
              },
            });
          },
        });
      }
    );
  };

  onDelete = (id) => {
    const { patient } = this.props;
    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.deleteTreatment({
          patientId: patient.id,
          id,
          cb: (res) => {
            console.log("* deleted treatment: ", res);
            this.props.getTreatments({
              patientId: patient.id,
              cb: (result) => {
                this.setState({
                  loading: false,
                });
              },
            });
          },
        });
      }
    );
  };

  onAdd = (data, callback) => {
    const { patient } = this.props;
    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.addTreatment({
          patientId: patient.id,
          data,
          cb: (res) => {
            console.log("* added treatment: ", res);
            this.props.getTreatments({
              patientId: patient.id,
              cb: (result) => {
                this.setState({
                  loading: false,
                });
                callback();
              },
            });
          },
        });
      }
    );
  };

  render() {
    const { patient, treatments } = this.props;
    const { modalVisible, overlayVisible, loading } = this.state;

    let patientTreatment = null;
    if (typeof treatments !== "undefined" && treatments.length > 0) {
      patientTreatment = treatments[0];
    }

    return (
      <>
        <div style={{ width: 515 }}>
          {!modalVisible && (
            <BlockWithTab>
              <div className="title">Traitement en cours</div>
              <div className="content">
                <div
                  className="relative"
                  style={{ height: "100%", maxWidth: "100%" }}
                >
                  {/* <p>COMBIGAN, 1 goutte 2 fois par jour</p>
                  <ImageButton
                    className="notify-btn"
                    onClick={this.setOverlayVisible}
                  >
                    <img src={notifyIcon} alt="" />
                  </ImageButton> */}
                  {patientTreatment ? (
                    <TreatmentsList>
                      <div>
                        {`${patientTreatment.drugName} ${patientTreatment.drugIntake} ${patientTreatment.drugForm} ${patientTreatment.frequencyIntake} par jour`}
                      </div>
                      <div style={{ marginTop: 10 }}>
                        <span className="blue">Agrandissez pour voir plus</span>
                      </div>
                    </TreatmentsList>
                  ) : (
                    <p>Non communiqué</p>
                  )}
                  {overlayVisible && (
                    <div className="overlay">
                      Cette information a été renseignée par votre patient,
                      cliquez pour mettre à jour.
                    </div>
                  )}
                </div>
                <ImageButton onClick={() => this.setModalVisible(true)}>
                  <img src={expandIcon} alt="" width="44" />
                </ImageButton>
              </div>
            </BlockWithTab>
          )}
        </div>
        <TreatmentModal
          patient={patient}
          treatments={treatments}
          visible={modalVisible}
          loading={loading}
          onDelete={this.onDelete}
          onUpdate={this.onUpdate}
          onAdd={this.onAdd}
          onClose={() => this.setModalVisible(false)}
        />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    treatments: state.PatientReducer.treatments,
    error: state.PatientReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPatient: (req) => {
      dispatch(PatientActions.getPatient(req.patientId, req.cb));
    },
    addTreatment: (req) => {
      dispatch(PatientActions.addTreatment(req.patientId, req.data, req.cb));
    },
    updateTreatment: (req) => {
      dispatch(
        PatientActions.updateTreatment(req.patientId, req.id, req.data, req.cb)
      );
    },
    deleteTreatment: (req) => {
      dispatch(PatientActions.deleteTreatment(req.patientId, req.id, req.cb));
    },
    getTreatments: (req) => {
      dispatch(PatientActions.getTreatments(req.patientId, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OngoingTreatment);
