import React from "react";
import { connect } from "react-redux";
import { Tabs, Button } from "antd";
import { ImageButton, ThemeTabs } from "./_styles";
import Home from "./Home";
import PatientRecord from "./PatientRecord";
import NewPatient from "./NewPatient";
import LoadingOveraly from "../../components/LoadingOverlay";
import { PatientActions } from "../../actions";
import { Slide } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";

const homeIcon = require("../../assets/svgs/Home.svg");
const homeLightIcon = require("../../assets/svgs/Home light.svg");
const newIcon = require("../../assets/svgs/Nouveau patient.svg");
const newLightIcon = require("../../assets/svgs/Nouveau patient light.svg");
const recordIcon = require("../../assets/svgs/Fiche Patient.svg");
const recordLightIcon = require("../../assets/svgs/Fiche patient light.svg");
const logoIcon = require("../../assets/svgs/Logo color.svg");
const signoutIcon = require("../../assets/svgs/Sign out.svg");
const closeIcon = require("../../assets/svgs/Close-white.svg");

const { TabPane } = Tabs;
const tutoImages = [
  require("../../assets/svgs/tuto/tuto2.svg"),
  require("../../assets/svgs/tuto/tuto3.svg"),
  require("../../assets/svgs/tuto/tuto4.svg"),
  require("../../assets/svgs/tuto/tuto5.svg"),
  require("../../assets/svgs/tuto/tuto6.svg"),
  require("../../assets/svgs/tuto/tuto7.svg"),
  require("../../assets/svgs/tuto/tuto8.svg"),
];
class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFirstVisit: false,
      loading: false,
      activeKey: "1",
      height: 0,
      isVisitedTuto: true,
    };
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions = () => {
    console.log(window);
    this.setState({
      width: window.innerWidth,
      height: (window.innerHeight * 10) / 8,
    });
  };

  componentWillMount() {
    const isCompleted = localStorage.getItem("COMPLETED_SIGNUP");
    if (isCompleted === "true") {
      this.setState({
        isFirstVisit: true,
        activeKey: "0",
      });
    }
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
    const patientId = this.props.match ? this.props.match.params.patientId : "";
    if (patientId) {
      this.setState(
        {
          loading: true,
        },
        () => {
          this.props.getPatient({
            patientId,
            cb: (res) => {
              if (res) {
                this.setState({
                  loading: false,
                  activeKey: "3",
                });
              } else {
                // window.location.href = "/main";
              }
            },
          });
        }
      );
    }
  }

  onChange = (activeKey) => {
    const patientId = this.props.match ? this.props.match.params.patientId : "";
    this.setState({ activeKey });
    if (activeKey === "1" && patientId !== "") {
      window.location.href = "/main";
    }
  };

  signOut = () => {
    const { isFirstVisit } = this.state;
    if (isFirstVisit) {
      return;
    }

    localStorage.clear();
    window.location.href = "/login";
  };

  onStart = () => {
    localStorage.removeItem("COMPLETED_SIGNUP");
    this.setState({
      activeKey: "1",
      isFirstVisit: false,
      isVisitedTuto: false,
    });
  };

  onNewPatient = () => {
    this.setState({
      activeKey: "2",
    });
  };

  onSelectPatient = (patient) => {
    // this.setState({
    //   activeKey: "3",
    // });
    window.location.href = `/main/${patient.id}`;
  };

  onCloseTuto = () => {
    this.setState({
      isVisitedTuto: true,
    });
  };

  render() {
    const {
      activeKey,
      isFirstVisit,
      loading,
      height,
      isVisitedTuto,
    } = this.state;
    const { patient } = this.props;

    if (!isVisitedTuto) {
      return (
        <>
          <Slide
            canSwipe={true}
            arrows={false}
            indicators={true}
            autoplay={false}
          >
            {tutoImages.map((t, i) => {
              return (
                <div className="each-slide" key={`slide-${i}`}>
                  <div style={{ backgroundImage: `url(${t})`, height }}></div>
                </div>
              );
            })}
          </Slide>
          <ImageButton className="slideshow-close" onClick={this.onCloseTuto}>
            <img src={closeIcon} alt="" />
          </ImageButton>
        </>
      );
    }

    return (
      <>
        <LoadingOveraly loading={loading} />
        <ThemeTabs
          defaultActiveKey="1"
          activeKey={activeKey}
          tabPosition="left"
          onChange={this.onChange}
        >
          <TabPane
            tab={<img src={logoIcon} alt="Curecall" width="108" />}
            key="0"
            disabled
          >
            <Home isFirstVisit={isFirstVisit} onStart={this.onStart} />
          </TabPane>
          <TabPane
            tab={
              <Button className="action-tab">
                <img
                  src={activeKey === "1" ? homeLightIcon : homeIcon}
                  alt="Mes patients"
                  width="47"
                />
                <span>Mes patients</span>
              </Button>
            }
            key="1"
            disabled={isFirstVisit}
          >
            {isFirstVisit === false && (
              <Home
                isFirstVisit={isFirstVisit}
                onNewPatient={this.onNewPatient}
                onSelectPatient={this.onSelectPatient}
              />
            )}
          </TabPane>
          <TabPane
            tab={
              <Button className="action-tab">
                <img
                  src={activeKey === "2" ? newLightIcon : newIcon}
                  alt="Nouveau Patient"
                  width="30"
                />
                <span>Nouveau Patient</span>
              </Button>
            }
            key="2"
            disabled={isFirstVisit}
          >
            <NewPatient onSelectPatient={this.onSelectPatient} />
          </TabPane>
          <TabPane
            tab={
              <Button className="action-tab">
                <img
                  src={activeKey === "3" ? recordLightIcon : recordIcon}
                  alt="Fiche Patient"
                  width="36"
                />
                <span>Fiche Patient</span>
              </Button>
            }
            key="3"
            disabled={isFirstVisit || !patient}
          >
            <PatientRecord />
          </TabPane>
          <TabPane
            tab={
              <Button
                onClick={this.signOut}
                className={isFirstVisit ? "action-tab" : ""}
              >
                <img src={signoutIcon} alt="Déconnexion" width="41" />
                <span>Déconnexion</span>
              </Button>
            }
            key="4"
            disabled
          ></TabPane>
        </ThemeTabs>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    patient: state.PatientReducer.patient,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPatient: (req) => {
      dispatch(PatientActions.getPatient(req.patientId, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
