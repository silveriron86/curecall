import styled from "@emotion/styled";
import { Tabs, Input, Table, Button, DatePicker, Select } from "antd";
import { BLUE, GREY, WHITE, LIGHT_GREY } from "../../constants/ColorConstants";
import Modal from "antd/lib/modal/Modal";
import upIcon from "../../assets/svgs/Dropup.svg";

export const verticalCenter = {
  alignItems: "center",
  justifyContent: "center",
};

export const ThemeTabs = styled(Tabs)({
  "&": {
    background: "#EAEBEF",
    ".ant-tabs-nav": {
      backgroundColor: "#EAEBEF",
      minHeight: "125vh",
      ".ant-tabs-ink-bar": {
        background: "transparent",
      },
      ".ant-tabs-tab": {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        width: 140,
        height: 102,
        padding: 0,
        marginBottom: 21,
        ".ant-tabs-tab-btn": {
          width: "100%",
          height: "100%",
          button: {
            background: "transparent !important",
            borderWidth: 0,
            outline: "none",
            width: "100%",
            height: "100%",
            boxShadow: "none",
            span: {
              display: "block",
              color: BLUE,
              fontFamily: "Nexa Regular",
              fontSize: 15,
              lineHeight: "18px",
              marginTop: 7,
              whiteSpace: "normal",
            },
          },
        },
        "&.ant-tabs-tab-disabled": {
          cursor: "default",
          "&:first-of-type": {
            margin: "62px 0 94px",
            backgroundImage: "none",
            filter: "none",
          },
        },
        "&.ant-tabs-tab-active": {
          // width: 166,
          height: 102,
          borderRadius: "51px 0px 0px 51px",
          backgroundImage: `linear-gradient(to right, ${BLUE}, #33cccc)`,
          filter: "drop-shadow(3px 3px 3px #d3d3d3)",
          padding: 0,
          ".ant-tabs-tab-btn": {
            button: {
              span: {
                fontFamily: "Nexa Heavy",
                color: "white",
              },
            },
          },
        },

        "&:nth-last-of-type(2)": {
          marginTop: "105px !important",
        },
      },
    },
    ".ant-tabs-content": {
      paddingBottom: 30,
    },
    ".ant-tabs-content-holder": {
      background: LIGHT_GREY,
      borderLeft: 0,
      borderTopLeftRadius: 50,
      filter: "drop-shadow(-3px 0px 20px rgba(126, 126, 126, 0.29))",
      padding: "27px 53px 0",
    },
    ".ant-tabs-tab-disabled": {
      ".action-tab": {
        cursor: "default",
      },
    },
  },
});

export const UserName = styled("div")({
  position: "absolute",
  top: 44,
  right: 40,
  fontFamily: "Nexa Regular",
  fontWeight: "normal",
  fontSize: 30,
  textAlign: "left",
  color: "#a5a5a5",
});

export const FlexWrap = styled("div")({
  display: "flex",
  flexWrap: "wrap",
  "&.full-height": {
    height: "100%",
  },
  "&.between": {
    justifyContent: "space-between",
    zIndex: 1,
    position: "relative",
  },
  "&.select": {
    zIndex: 0,
    position: "absolute",
    width: 140,
    marginTop: -47,
    alignItems: "center",
    justifyContent: "space-between",
    span: {
      fontFamily: "Nexa Regular",
      fontSize: 20,
      lineHeight: "27px",
      color: BLUE,
      "&.oui": {
        color: `${BLUE}40`,
      },
    },
  },
  "&.treatment-row": {
    width: "100%",
    paddingLeft: 15,
    ".col": {
      margin: 6,
      ".blue": {
        display: "block",
        marginTop: 16,
        fontSize: 15,
        fontFamily: "Nexa Heavy",
      },
    },
  },
});

export const SearchBox = styled(Input)({
  width: 492,
  height: 69,
  borderRadius: 34.5,
  borderWidth: 0,
  background: WHITE,
  boxShadow: "3px 3px 20px rgba(211, 211, 211, 0.6)",
  marginBottom: 25,
  "&.ant-input-affix-wrapper": {
    "&:focus, &:hover, &-focused": {
      border: "none !important",
      boxShadow: `3px 3px 20px ${BLUE}60`,
    },
  },
  input: {
    paddingTop: "5px !important",
    background: WHITE,
    borderTopRightRadius: 34.5,
    borderBottomRightRadius: 34.5,
    color: BLUE,
    fontSize: 25,
    fontFamily: "Nexa Bold",
    lineHeight: "69px",
    "&:hover, &:focus, &-focused": {
      boxShadow: "none !important",
      border: "0 !important",
      backgroundColor: "transparent",
    },
  },
  ".ant-input-prefix": {
    marginLeft: 20,
    marginRight: 13,
  },
});

export const ThemeTable = styled(Table)({
  borderTopLeftRadius: 40,
  borderTopRightRadius: 40,
  overflow: "hidden",
  backgroundColor: WHITE,
  ".ant-table-thead": {
    tr: {
      height: 62,
      borderRadius: "40px 40px 0px 0px",
      background:
        "linear-gradient(to Left, #3cc 0%, #4040ef 29.46%, #3cc 73.46%, #33c 100%)",
      boxShadow: "3px 3px 20px #d3d3d3",
      borderTopLeftRadius: 40,
      borderTopRightRadius: 40,
      th: {
        backgroundColor: "transparent",
        fontFamily: "Nexa Heavy",
        fontSize: 25,
        color: "white",
        lineHeight: "38px",
        userSelect: "none",
        padding: 0,
        ".ant-table-column-sorters": {
          padding: "11px 16px",
        },
        "&:first-of-type": {
          paddingLeft: 55,
          ".ant-table-column-sorters": {
            paddingLeft: 0,
          },
        },
        "&:hover": {
          backgroundColor: "#00000030",
        },
        "&:last-child": {
          paddingLeft: 16,
        },
      },
    },
  },
  td: {
    backgroundColor: WHITE,
    fontFamily: "Nexa Bold",
    fontSize: 23,
    lineHeight: "31px",
    color: GREY,
    "&:first-of-type": {
      paddingLeft: 55,
    },
  },
  ".ant-table-column-sorter-up, .ant-table-column-sorter-down": {
    fontSize: 20,
    // color: "transparent",
    "&.active": {
      color: "white",
    },
  },
  ".ant-table-pagination.ant-pagination": {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    float: "none",
    ".ant-pagination-prev, .ant-pagination-next": {
      display: "none",
    },
    ".ant-pagination-item": {
      width: 13.58,
      height: 13.58,
      minWidth: 13.58,
      borderRadius: 13.58 / 2,
      background: WHITE,
      filter: "drop-shadow(1px 1px 4px rgba(51, 51, 204, 0.3))",
      margin: "0 10.5px",
      "&-active": {
        backgroundColor: BLUE,
      },
      a: {
        display: "none",
      },
    },
  },
  "&.patients": {
    ".ant-table-tbody": {
      tr: {
        cursor: "pointer",
      },
    },
  },
});

export const Block = styled("div")({
  margin: "88px 172px 0",
});

export const BlockHeader = styled("div")({
  height: 62,
  borderRadius: "40px 40px 0px 0px",
  background: "linear-gradient(to Left, #3cc 0%, #33c 100%)",
  boxShadow: "3px 3px 20px #d3d3d3",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  color: "white",
  fontFamily: "Nexa Heavy",
  fontSize: 25,
  lineHeight: "38px",
  "&.fit": {
    width: "fit-content",
    padding: "0 53px",
  },
  "&.old-grad": {
    background:
      "linear-gradient(to Left, #3cc 0%, #4040ef 29.46%, #3cc 73.46%, #33c 100%)",
  },
});

export const BlockReturnHeader = styled("div")({
  height: 62,
  borderRadius: "0px 40px 0px 0px",
  background: "linear-gradient(to Left, #3cc 0%, #33c 100%)",
  boxShadow: "3px 3px 20px #d3d3d3",
  display: "flex",
  alignItems: "center",
  width: "fit-content",
  padding: "0 53px",
  color: "white",
  fontFamily: "Nexa Heavy",
  fontSize: 25,
  lineHeight: "38px",
  paddingLeft: 0,
  marginLeft: -57,
  ".ant-btn": {
    padding: "16px 30px",
  },
  "&.warning": {
    background:
      "linear-gradient(to Left, rgba(227, 31, 31, 0) 0%, #e31f1f 100%)",
    boxShadow: "none",
  },
  "&.notify": {
    background:
      "linear-gradient(to left, rgba(255, 177, 0, 0) 0%, #ffb100 100%)",
    boxShadow: "none",
  },
  "&.new": {
    marginLeft: -77,
  },
});

const InputPwd = {
  height: 54,
  borderWidth: 0,
  outline: "0 !important",
  borderRadius: 27,
  background: WHITE,
  boxShadow: "3px 3px 20px rgba(211, 211, 211, 0.6)",
};

export const ThemeInput = styled(Input)({
  ...InputPwd,
  "&.no-round": {
    borderRadius: 10,
    "&:hover": {
      borderRight: "0 !important",
    },
    "&:focus": {
      boxShadow: "3px 3px 20px rgba(51,51,204,0.6) !important",
      borderRight: "0 !important",
    },
  },
});

export const ThemeSelect = styled(Select)({
  ".ant-select-selector": {
    height: "60px !important",
    borderWidth: "0 !important",
    borderColor: "transparent !important",
    outline: "0 !important",
    borderRadius: "30px !important",
    backgroundColor: "#f7f7f7 !important",
    boxShadow: "3px 3px 20px rgba(211, 211, 211, 0.6)",
    ".ant-select-selection-placeholder": {
      color: "#3333cc90 !important",
      fontFamily: "Nexa Light",
      fontSize: 25,
      boxShadow: "none !important",
      padding: "15px 30px",
    },
    ".ant-select-selection-item": {
      fontSize: 25,
      color: "#3333cc",
      backgroundColor: "transparent",
      fontFamily: "Nexa Bold",
      lineHeight: "33px",
      padding: "12px 30px",
    },
  },
  ".ant-select-arrow": {
    display: "none",
  },
});

export const ThemePassword = styled(Input.Password)({
  ...InputPwd,
  "&.no-icon": {
    ".ant-input-suffix": {
      display: "none",
    },
  },
});

export const TextButton = styled(Button)({
  height: "auto",
  span: {
    fontFamily: "Nexa Bold",
    fontSize: 40,
    lineHeight: "53px",
    color: BLUE,
  },
  "&:disabled": {
    span: {
      color: `${BLUE}40`,
    },
  },
});

export const BlueButton = styled(Button)({
  height: 53,
  backgroundColor: `${BLUE} !important`,
  padding: "0 42px",
  boxShadow: "3px 3px 3px #d3d3d3",
  borderWidth: 0,
  outline: 0,
  borderRadius: 40,
  span: {
    fontFamily: "Nexa Bold",
    fontSize: 20,
    lineHeight: "27px",
    color: "white",
  },
});

export const GreyButton = styled(Button)({
  height: 53,
  backgroundColor: `${LIGHT_GREY} !important`,
  padding: "0 42px",
  borderRadius: 40,
  borderWidth: 0,
  outline: 0,
  boxShadow: "3px 3px 3px #d3d3d3, -3px -3px 3px #fff",
  span: {
    fontFamily: "Nexa Bold",
    fontSize: 20,
    lineHeight: "27px",
    color: BLUE,
  },
});

export const ConfirmModal = styled(Modal)({
  ".ant-modal-content": {
    width: 492,
    // height: 210,
    background: WHITE,
    filter: "drop-shadow(3px 3px 30px rgba(0, 0, 0, 0.3))",
    borderRadius: 20,
    p: {
      marginTop: 22,
      fontFamily: "Nexa Bold",
      fontSize: 25,
      lineHeight: "33px",
      color: "#A5A5A5",
      marginBottom: 49,
      textAlign: "center",
    },
    button: {
      height: "auto",
      padding: 0,
      span: {
        fontSize: 25,
        lineHeight: "38px",
      },
      "&.cancel-btn": {
        span: {
          fontFamily: "Nexa Heavy",
          color: "#33CCCC",
        },
      },
      "&.ok-btn": {
        span: {
          fontFamily: "Nexa Heavy",
          color: BLUE,
        },
      },
    },
  },
});

export const ImageButton = styled(Button)({
  height: "fit-content",
  background: "transparent",
  border: 0,
  boxShadow: "none",
  "&.treatment-add": {
    float: "right",
    marginRight: 0,
    marginTop: -20,
    zIndex: 1,
    height: "auto",
    width: "auto !important",
    padding: "10px !important",
  },
  "&:hover, &:focus": {
    background: "transparent",
  },
  "&.slideshow-close": {
    position: "absolute",
    top: 48,
    right: 55,
    borderRadius: "50%",
    width: 50,
    height: 50,
    padding: 0,
  },
});

export const TabModal = styled(Modal)({
  paddingBottom: 0,
  backgroundColor: WHITE,
  borderRadius: 25,
  minWidth: 815,
  ".ant-modal-content": {
    borderRadius: 25,
    borderTopLeftRadius: 0,
    ".ant-modal-body": {
      position: "relative",
      padding: 0,
      ".modal-content": {
        marginBottom: 0,
        ".title": {
          background: WHITE,
          // filter: "drop-shadow(3px 3px 20px rgba(211, 211, 211, 0.6))",
        },
        ".content": {
          marginTop: "0 !important",
          width: "100%",
          height: "auto",
          ".ant-btn": {
            width: 56,
            height: 56,
            padding: 0,
            position: "relative",
            bottom: -1,
            right: -10,
            "&.selected": {
              borderRadius: 10,
              background: "linear-gradient(#3cc 0%, #33c 100%)",
            },
          },
          ".desc-title": {
            fontFamily: "Nexa Heavy",
            fontWeight: 900,
            fontSize: 15,
            textAlign: "left",
            color: BLUE,
          },
          ".ant-input.no-border": {
            fontFamily: "Nexa Book",
            fontWeight: "normal",
            fontSize: 15,
            textAlign: "left",
            color: BLUE,
          },
          ".ant-input, .ant-picker-input > input, .ant-select span": {
            fontFamily: "Nexa Bold",
            fontSize: 20,
            lineHeight: "27px",
            color: BLUE,
          },
          ".ant-picker": {
            width: "100%",
          },
          ".ant-picker-input > input": {
            "&:disabled": {
              color: "#A5A5A5",
            },
          },
          ".ant-input, .ant-picker, .ant-select": {
            border: 0,
            borderRadius: 10,
            background: "#f7f7f7",
            boxShadow: "3px 3px 20px rgba(211, 211, 211, 0.6)",
            height: 56,
            "&:focus, &-focused": {
              boxShadow: "3px 3px 20px rgba(51, 51, 204, 0.6)",
            },
            "&:disabled": {
              boxShadow: "none",
              cursor: "unset",
              color: "#A5A5A5",
              "&::placeholder": {
                color: "#A5A5A5 !important",
              },
            },
          },
          "span.disabled": {
            fontSize: 20,
            lineHeight: "27px",
            color: "#A5A5A5 !important",
          },
          ".ant-select": {
            width: "100%",
            span: {
              lineHeight: "56px",
            },
            ".ant-select-selector": {
              height: "100%",
              backgroundColor: "transparent",
              border: 0,
            },
            ".ant-select-arrow": {
              background: `url(${upIcon})`,
              width: 34,
              height: 24,
              marginTop: -14,
              transform: "rotate(180deg)",
              ".anticon": {
                display: "none",
              },
            },
            "&-open": {
              ".ant-select-arrow": {
                transform: `rotate(0)`,
              },
            },
            "&-disabled": {
              boxShadow: "none !important",
              ".ant-select-selection-item": {
                color: "#A5A5A5",
              },
              ".ant-select-arrow": {
                display: "none !important",
              },
            },
          },
          ".ant-form-item-label > label": {
            fontFamily: "Nexa Heavy",
            fontSize: 15,
            lineHeight: "18px",
            color: BLUE,
          },
          ".ant-switch": {
            outline: "none !important",
            border: "none !important",
            backgroundColor: `${BLUE}10`,
            width: 54.5,
            height: 28.5,
            ".ant-switch-handle": {
              width: 22,
              height: 22,
              top: 3,
              left: 4,
              "&:before": {
                borderRadius: 11,
              },
            },
          },
          ".ant-switch-checked": {
            backgroundColor: BLUE,
            ".ant-switch-handle": {
              left: "calc(100% - 18px - 8px)",
            },
          },
          ".pink": {
            ".ant-form-item-label > label": {
              color: "#9F01C4",
            },
          },
          ".valider": {
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            ".ant-btn": {
              backgroundColor: BLUE,
              color: "white",
              width: 100,
              height: 38,
              borderRadius: 19,
            },
          },
          ".col-1": {
            fontFamily: "Nexa Book",
            fontSize: 15,
            textAlign: "left",
            color: "#a5a5a5",
            lineHeight: "22px",
          },
          ".col-3": {
            fontFamily: "Nexa Light",
            fontWeight: "normal",
            fontSize: 13,
            lineHeight: "17px",
            textAlign: "left",
            padding: "14px 0",
            "&.yellow": {
              color: "#FFB100",
            },
          },
        },
        ".buttons": {
          position: "absolute",
          right: 32,
          bottom: 26,
        },
      },
    },
  },
  "&.treatment-modal": {
    height: "70%",
    ".ant-modal-content": {
      height: "100%",
      ".ant-modal-body, .modal-content": {
        height: "100%",
        ".content": {
          height: "100%",
          paddingLeft: 20,
          paddingRight: 20,
          ".treatment": {
            height: "100%",
            form: {
              height: "calc(100% - 100px)",
              overflowX: "auto",
            },
          },
          "input, .ant-select span, input::placeholder": {
            fontSize: "15px !important",
          },
          ".ant-picker input": {
            padding: "0 !important",
            textAlign: "center",
          },
        },
      },
    },
  },
});

export const ThemeModal = styled(Modal)({
  "&.learn-more": {
    borderRadius: 40,
    background: "#f7f7f7",
    boxShadow: "3px 3px 20px rgba(211, 211, 211, 0.6)",
    overflow: "hidden",
    paddingBottom: 0,
    ".ant-modal-body": {
      padding: "77px 73px",
    },
    ".close-btn": {
      position: "absolute",
      top: 33,
      right: 40,
    },
    h1: {
      fontFamily: "Nexa Regular",
      fontWeight: 900,
      fontSize: 25,
      textAlign: "left",
      color: "#33c",
    },
    h2: {
      fontFamily: "Nexa Bold",
      fontSize: 22,
      color: "#a5a5a5",
      marginTop: 35,
    },
    ul: {
      marginTop: 5,
      marginBottom: 5,
    },
    p: {
      fontFamily: "Nexa Regular",
      fontWeight: "normal",
      fontSize: 20,
      textAlign: "left",
      color: "#a5a5a5",
      marginTop: 25,
      marginBottom: 0,
    },
    "&.privacy-modal": {
      height: "99%",
      ".ant-modal-content": {
        height: "100%",
        ".ant-modal-body": {
          padding: "77px 45px 50px",
          height: "100%",
          ".modal-wrapper": {
            height: "100%",
            overflowY: "auto",
            padding: "0 20px",
          },
        },
      },
    },
  },
});

export const ThemeDatePicker = styled(DatePicker)({
  "&.ant-picker-disabled": {
    boxShadow: "none !important",
    cursor: "unset",
    ".ant-picker-suffix": {
      display: "none",
    },
  },
});

export const GraphPlaceholder = styled("div")({
  position: "absolute",
  top: 0,
  width: "100%",
  padding: "30px 15% 0",
  height: "100%",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  // border: "1px solid red",
  textAlign: "center",
  fontFamily: "Nexa Heavy",
  fontSize: 16,
  lineHeight: "22px",
  color: "#d3d3d3",
  zIndex: 0,
});

export const Full = styled("div")({
  width: "100%",
  height: "100%",
});
