import React, { Component } from "react";
import { Provider } from "react-redux";
import { Router } from "react-router";
import routes from "../routes";

import "antd/dist/antd.css";
import { ConfigProvider } from "antd";
import frFR from "antd/lib/locale/fr_FR";

export default class Root extends Component {
  render() {
    const { store, history } = this.props;
    if (!this.props) {
      return null;
    }

    return (
      <Provider store={store}>
        <Router history={history}>
          <ConfigProvider locale={frFR}>{routes}</ConfigProvider>
        </Router>
      </Provider>
    );
  }
}
