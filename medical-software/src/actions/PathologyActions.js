import axios from "axios";
import { ApiPathConstants, PathologyConstants } from "../constants";
import Utils from "../utils";

let PatientActions = {
  getPathologiesError: function (error) {
    return {
      error,
      type: PathologyConstants.GET_PATHOLOGIES_ERROR,
    };
  },

  getPathologiesSuccess: function (response) {
    return {
      response,
      type: PathologyConstants.GET_PATHOLOGIES_SUCCESS,
    };
  },

  getPathologies: function (cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/pathologies`,
        headers: Utils.getHeader(),
      })
        .then((response) => {
          dispatch(this.getPathologiesSuccess(response));
          cb(response);
        })
        .catch((error) => {
          dispatch(this.getPathologiesError(error.response));
          cb(error.response);
        });
    };
  },
};

export default PatientActions;
