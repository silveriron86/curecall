import axios from "axios";
import { ApiPathConstants, AccountConstants } from "../constants";
import Utils from "../utils";

let AccountActions = {
  loginError: function (error) {
    return {
      error,
      type: AccountConstants.LOGIN_ERROR,
    };
  },
  loginSuccess: function (response) {
    return {
      response,
      type: AccountConstants.LOGIN_SUCCESS,
    };
  },
  login: function (data, cb) {
    return (dispatch) => {
      axios({
        method: "POST",
        url: `${ApiPathConstants.getApiPath()}users/login`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        data,
      })
        .then((response) => {
          dispatch(this.loginSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.loginError(error.response));
          cb(error.response);
        });
    };
  },

  getLoginSMSError: function (error) {
    return {
      error,
      type: AccountConstants.GET_SMS_ERROR,
    };
  },
  getLoginSMSSuccess: function (response) {
    return {
      response,
      type: AccountConstants.GET_SMS_SUCCESS,
    };
  },
  getLoginSMS: function (phone, cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}users/login/sms/${phone}`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          dispatch(this.getLoginSMSSuccess(response));
          cb(response);
        })
        .catch((error) => {
          dispatch(this.getLoginSMSError(error.response));
          cb(error.response);
        });
    };
  },

  postLoginSMSError: function (error) {
    return {
      error,
      type: AccountConstants.POST_SMS_ERROR,
    };
  },
  postLoginSMSSuccess: function (response) {
    return {
      response,
      type: AccountConstants.POST_SMS_SUCCESS,
    };
  },
  postLoginSMS: function (data, cb) {
    return (dispatch) => {
      axios({
        method: "POST",
        url: `${ApiPathConstants.getApiPath()}users/login/sms`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        data,
      })
        .then((response) => {
          dispatch(this.postLoginSMSSuccess(response));
          cb(response);
        })
        .catch((error) => {
          dispatch(this.postLoginSMSError(error.response));
          cb(error.response);
        });
    };
  },

  updateUserError: function (error) {
    return {
      error,
      type: AccountConstants.UPDATE_USER_ERROR,
    };
  },
  updateUserSuccess: function (response) {
    return {
      response,
      type: AccountConstants.UPDATE_USER_SUCCESS,
    };
  },
  updateUser: function (user_id, data, cb) {
    return (dispatch) => {
      axios({
        method: "PATCH",
        url: `${ApiPathConstants.getApiPath()}users/${user_id}`,
        headers: Utils.getHeader(),
        data,
      })
        .then((response) => {
          dispatch(this.updateUserSuccess(response));
          cb(response);
        })
        .catch((error) => {
          dispatch(this.updateUserError(error.response));
          cb(error.response);
        });
    };
  },

  registerError: function (error) {
    return {
      error,
      type: AccountConstants.REGISTER_ERROR,
    };
  },
  registerSuccess: function (response) {
    return {
      response,
      type: AccountConstants.REGISTER_SUCCESS,
    };
  },
  register: function (data, cb) {
    return (dispatch) => {
      axios({
        method: "POST",
        url: `${ApiPathConstants.getApiPath()}users/register`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        data,
      })
        .then((response) => {
          dispatch(this.registerSuccess(response.data));
          cb({ success: true, response: response.data });
        })
        .catch((error) => {
          dispatch(this.registerError(error.response));
          cb({ success: false, response: error.response });
        });
    };
  },

  getRppsError: function (error) {
    return {
      error,
      type: AccountConstants.GET_RPPS_ERROR,
    };
  },
  getRppsSuccess: function (response) {
    return {
      response,
      type: AccountConstants.GET_RPPS_SUCCESS,
    };
  },
  getRpps: function (rpps_id, cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        // url: "https://rpps.cur.cl/?rpps=10100028389",
        url: `https://rpps.cur.cl/?rpps=${rpps_id}`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          dispatch(this.getRppsSuccess(response.data));
          cb({ success: true, response: response.data });
        })
        .catch((error) => {
          console.log(error);
          dispatch(this.getRppsError(error.response));
          cb({ success: false, response: error.response });
        });
    };
  },

  verifySMSError: function (error) {
    return {
      error,
      type: AccountConstants.VERIFY_SMS_ERROR,
    };
  },
  verifySMSSuccess: function (response) {
    return {
      response,
      type: AccountConstants.VERIFY_SMS_SUCCESS,
    };
  },
  verifySMS: function (email, code, cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}users/verify-sms?email=${email}&code=${code}`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          dispatch(this.verifySMSSuccess(response.data));
          cb({ success: true, response: response.data });
        })
        .catch((error) => {
          dispatch(this.verifySMSError(error.response));
          cb({ success: false, response: error.response });
        });
    };
  },
};

export default AccountActions;
