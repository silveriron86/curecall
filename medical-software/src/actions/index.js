import AccountActions from "./AccountActions";
import PatientActions from "./PatientActions";
import PathologyActions from "./PathologyActions";

export { AccountActions, PatientActions, PathologyActions };
