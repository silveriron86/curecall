import axios from "axios";
import { ApiPathConstants, PatientConstants } from "../constants";
import Utils from "../utils";

let PatientActions = {
  getPatientsError: function (error) {
    return {
      error,
      type: PatientConstants.GET_PATIENTS_ERROR,
    };
  },
  getPatientsSuccess: function (response) {
    return {
      response,
      type: PatientConstants.GET_PATIENTS_SUCCESS,
    };
  },
  getPatients: function (cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients`,
        headers: Utils.getHeader(),
      })
        .then((response) => {
          dispatch(this.getPatientsSuccess(response.data));
          cb(response);
        })
        .catch((error) => {
          dispatch(this.getPatientsError(error.response));
          cb(error.response);
        });
    };
  },

  getPatientError: function (error) {
    return {
      error,
      type: PatientConstants.GET_PATIENT_ERROR,
    };
  },
  getPatientSuccess: function (response) {
    return {
      response,
      type: PatientConstants.GET_PATIENT_SUCCESS,
    };
  },
  getPatient: function (patientId, cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${patientId}`,
        headers: Utils.getHeader(),
      })
        .then((response) => {
          dispatch(this.getPatientSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.getPatientError(error.response));
          cb(error.response);
        });
    };
  },

  setPatient: function (patient, cb) {
    return (dispatch) => {
      dispatch(
        this.addPatientSuccess({
          patient,
        })
      );
      cb();
    };
  },

  addPatientError: function (error) {
    return {
      error,
      type: PatientConstants.ADD_PATIENT_ERROR,
    };
  },
  addPatientSuccess: function (response) {
    return {
      response,
      type: PatientConstants.ADD_PATIENT_SUCCESS,
    };
  },
  addPatient: function (data, cb) {
    return (dispatch) => {
      axios({
        method: "POST",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients`,
        headers: Utils.getHeader(),
        data,
      })
        .then((response) => {
          dispatch(this.addPatientSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          console.log(error);
          dispatch(this.addPatientError(error.response));
          cb(error.response.data);
        });
    };
  },

  updatePatientError: function (error) {
    return {
      error,
      type: PatientConstants.UPDATE_PATIENT_ERROR,
    };
  },
  updatePatientSuccess: function (response) {
    return {
      response,
      type: PatientConstants.UPDATE_PATIENT_SUCCESS,
    };
  },
  updatePatient: function (data, cb) {
    return (dispatch) => {
      axios({
        method: "PATCH",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${data.id}`,
        headers: Utils.getHeader(),
        data,
      })
        .then((response) => {
          dispatch(this.updatePatientSuccess(response.data));
          cb(response);
        })
        .catch((error) => {
          dispatch(this.updatePatientError(error.response));
          cb(error.response);
        });
    };
  },

  saveVariableByNameError: function (error) {
    return {
      error,
      type: PatientConstants.SET_VARIABLE_ERROR,
    };
  },
  saveVariableByNameSuccess: function (response) {
    return {
      response,
      type: PatientConstants.SET_VARIABLE_SUCCESS,
    };
  },
  saveVariableByName: function (patientId, variableName, data, cb) {
    return (dispatch) => {
      axios({
        method: "POST",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${patientId}/variables/${variableName}`,
        headers: Utils.getHeader(),
        data,
      })
        .then((response) => {
          dispatch(
            this.saveVariableByNameSuccess({
              variableName,
              result:
                response.data.result.length > 0
                  ? response.data.result[0]
                  : null,
            })
          );
          cb(response);
        })
        .catch((error) => {
          dispatch(this.saveVariableByNameError(error.response));
          cb(error.response);
        });
    };
  },

  getVariableByNameError: function (error) {
    return {
      error,
      type: PatientConstants.GET_VARIABLE_ERROR,
    };
  },
  getVariableByNameSuccess: function (response) {
    return {
      response,
      type: PatientConstants.GET_VARIABLE_SUCCESS,
    };
  },
  getVariableByName: function (patientId, variableName, cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${patientId}/variables/${variableName}?offset=0&limit=1&order=desc`,
        headers: Utils.getHeader(),
      })
        .then((response) => {
          dispatch(
            this.getVariableByNameSuccess({
              variableName,
              result:
                response.data.length > 0 &&
                response.data[0].patientProfiles.length > 0
                  ? response.data[0].patientProfiles[0]
                  : null,
            })
          );
          cb(response);
        })
        .catch((error) => {
          dispatch(this.getVariableByNameError(error.response));
          cb(error.response);
        });
    };
  },

  getTreatmentsError: function (error) {
    return {
      error,
      type: PatientConstants.GET_TREATMENTS_ERROR,
    };
  },
  getTreatmentsSuccess: function (response) {
    return {
      response,
      type: PatientConstants.GET_TREATMENTS_SUCCESS,
    };
  },
  getTreatments: function (patientId, cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${patientId}/treatments`,
        headers: Utils.getHeader(),
      })
        .then((response) => {
          dispatch(this.getTreatmentsSuccess(response.data));
          cb(response);
        })
        .catch((error) => {
          dispatch(this.getTreatmentsError(error.response));
          cb(error.response);
        });
    };
  },

  addTreatmentError: function (error) {
    return {
      error,
      type: PatientConstants.ADD_TREATMENT_ERROR,
    };
  },
  addTreatmentSuccess: function (response) {
    return {
      response,
      type: PatientConstants.ADD_TREATMENT_SUCCESS,
    };
  },
  addTreatment: function (patientId, data, cb) {
    return (dispatch) => {
      axios({
        method: "POST",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${patientId}/treatments`,
        headers: Utils.getHeader(),
        data,
      })
        .then((response) => {
          dispatch(this.addTreatmentSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.addTreatmentError(error.response));
          cb(error.response);
        });
    };
  },

  updateTreatmentError: function (error) {
    return {
      error,
      type: PatientConstants.UPDATE_PATIENT_ERROR,
    };
  },
  updateTreatmentSuccess: function (response) {
    return {
      response,
      type: PatientConstants.UPDATE_PATIENT_SUCCESS,
    };
  },
  updateTreatment: function (patientId, id, data, cb) {
    return (dispatch) => {
      axios({
        method: "PATCH",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${patientId}/treatments/${id}`,
        headers: Utils.getHeader(),
        data,
      })
        .then((response) => {
          dispatch(this.updateTreatmentSuccess(response.data));
          cb(response);
        })
        .catch((error) => {
          dispatch(this.updateTreatmentError(error.response));
          cb(error.response);
        });
    };
  },

  deleteTreatmentError: function (error) {
    return {
      error,
      type: PatientConstants.DELETE_TREATMENT_ERROR,
    };
  },
  deleteTreatmentSuccess: function (response) {
    return {
      response,
      type: PatientConstants.DELETE_TREATMENT_SUCCESS,
    };
  },
  deleteTreatment: function (patientId, id, cb) {
    return (dispatch) => {
      axios({
        method: "DELETE",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${patientId}/treatments/${id}`,
        headers: Utils.getHeader(),
      })
        .then((response) => {
          dispatch(this.deleteTreatmentError(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.deleteTreatmentError(error.response));
          cb(error.response);
        });
    };
  },

  getGraphError: function (error) {
    return {
      error,
      type: PatientConstants.GET_GRAPH_ERROR,
    };
  },
  getGraphSuccess: function (response) {
    return {
      response,
      type: PatientConstants.GET_GRAPH_SUCCESS,
    };
  },
  getGraph: function (patientId, variableGroup, cb) {
    let type = variableGroup;
    if (type === "SYMPTOM") {
      type = "SYMPTOMS";
    }
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${patientId}/graphs/${type}`,
        headers: Utils.getHeader(),
      })
        .then((response) => {
          dispatch(
            this.getGraphSuccess({
              type: variableGroup,
              data: response.data,
            })
          );
          cb(response);
        })
        .catch((error) => {
          dispatch(this.getGraphError(error.response));
          cb(error.response);
        });
    };
  },

  getProfileQualityError: function (error) {
    return {
      error,
      type: PatientConstants.GET_PROFILE_QUALITY_ERROR,
    };
  },
  getProfileQualitySuccess: function (response) {
    return {
      response,
      type: PatientConstants.GET_PROFILE_QUALITY_SUCCESS,
    };
  },
  getProfileQuality: function (patientId, cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${patientId}/profile/QUALITY_OF_LIFE`,
        headers: Utils.getHeader(),
      })
        .then((response) => {
          dispatch(this.getProfileQualitySuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.getProfileQualityError(error.response));
          cb(error.response);
        });
    };
  },

  getFormVariablesError: function (error) {
    return {
      error,
      type: PatientConstants.GET_FORM_VARIABLES_ERROR,
    };
  },
  getFormVariablesSuccess: function (response, groupName) {
    return {
      response: { result: response, groupName },
      type: PatientConstants.GET_FORM_VARIABLES_SUCCESS,
    };
  },
  getFormVariables: function (patientId, groupName, cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}projects/${localStorage.getItem(
          "PROJECT_ID"
        )}/patients/${patientId}/profile/${groupName}`,
        headers: Utils.getHeader(),
      })
        .then((response) => {
          dispatch(this.getFormVariablesSuccess(response.data, groupName));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.getFormVariablesError(error.response));
          cb(error.response);
        });
    };
  },
};

export default PatientActions;
