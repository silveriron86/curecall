import React, { useEffect, useState } from "react";
import ReactCodeInput from "react-code-input";
import { PageContainer, Flex, FlexCenter } from "../_styles";
import Header from "../../components/header";
import { ThemeOutlineButton, ThemeButton } from "../../components/_styles";
import { CodeInputer, loginBtnStyle } from "./_styles";
import { EvaluationVariables, QualityVariables } from "../../constants";

import logo from "../../assets/svg/LogoCurecall.svg";
// import backIcon from "../../assets/svg/Come.svg";
import { connect } from "react-redux";
import { FormActions, AccountActions } from "../../actions";
import LoadingOverlay from "../../components/Loading-overlay";

function LoginPage(props) {
  const [isSplash, setSplash] = useState(true);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [code, setCode] = useState("");

  useEffect(() => {
    setTimeout(() => {
      setSplash(false);
    }, 2000);
  });

  const onConnect = () => {
    if (code !== "" && error === false) {
      // success
      localStorage.setItem("PAGE_INDEX", "0");
      setLoading(true);
      props.getForm({
        pinCode: code,
        cb: (res) => {
          if (typeof res.status !== "undefined") {
            setError(true);
            setLoading(false);
          } else {
            // success
            setError(false);
            const { form, variablesAlreadyCompleted } = res;
            console.log(form, variablesAlreadyCompleted);
            _clearLocalStorage();

            localStorage.setItem("PIN_CODE", code);
            // localStorage.setItem("FORM_TYPE", form);

            let lowerCaseVariables = [];
            if (variablesAlreadyCompleted.length > 0) {
              lowerCaseVariables = variablesAlreadyCompleted.map((v) => {
                return v.toLowerCase();
              });
            }

            let lastPageIndex = 0;
            if (form === "sideEffects") {
              // secondaries
              lastPageIndex = _setSideEffectsForm(lowerCaseVariables);
            } else if (form === "visualFunction") {
              // evaluation
              lastPageIndex = _setEvaluationForm(lowerCaseVariables);
            } else if (form === "treatment") {
              // treatment
              localStorage.removeItem("TREATMENT_ID");
              // no need because treatment form does not have any variable
            } else if (form === "qualityOfLife") {
              // quality
              lastPageIndex = _setQualityForm(lowerCaseVariables);
            } else if (form === "visualAcuity") {
              // visual Acuity
              lastPageIndex = _setVisualAcuityForm(lowerCaseVariables);
            } else if (form === "contrastPerception") {
              // contrast perception
              lastPageIndex = _setContrastPerceptionForm(lowerCaseVariables);
            } else if (form === "amslerGrid") {
              // amsler grid
              lastPageIndex = _setAmslerGridForm(lowerCaseVariables);
            } else if (form === "quality-life") {
              // new quality-life
              lastPageIndex = _setQualityLifeForm(lowerCaseVariables);
            }
            localStorage.setItem("LAST_PAGE_INDEX", lastPageIndex);

            if (form === "treatment") {
              props.tradePin({
                pin: code,
                cb: (res) => {
                  console.log("*** tradePin = ", res);
                  if (typeof res.status !== "undefined") {
                    setError(true);
                  } else {
                    localStorage.setItem("TOKEN", res.token);
                    setError(false);
                    goForm(form);
                  }
                  setLoading(false);
                },
              });
            } else {
              setLoading(false);
              goForm(form);
            }
          }
        },
      });
    }
  };

  const goForm = (form) => {
    const urls = [
      {
        key: "visualFunction",
        value: "visual-function",
      },
      {
        key: "qualityOfLife",
        value: "quality-of-life",
      },
      {
        key: "sideEffects",
        value: "side-effects",
      },
      {
        key: "visualAcuity",
        value: "visual-acuity",
      },
      {
        key: "contrastPerception",
        value: "contrast-perception",
      },
      {
        key: "amslerGrid",
        value: "amsler-grid",
      },
    ];
    const found = urls.find((u) => u.key === form);
    const route = found ? found.value : form;
    localStorage.setItem("FORM_TYPE", route);
    window.location.href = `/form/${route}`;
  };

  const _clearLocalStorage = () => {
    // remove all of the page indexes
    localStorage.removeItem("PAGE_INDEX");
    localStorage.removeItem("SECONDARIES_INDEX");
    localStorage.removeItem("TREATMENT_INDEX");
    localStorage.removeItem("QUALITY_INDEX");
    localStorage.removeItem("VISUAL_ACUITY_INDEX");
    localStorage.removeItem("CONTRAST_PERCEPTION_INDEX");
    localStorage.removeItem("LAST_PAGE_INDEX");
    localStorage.removeItem("AMSLER_GRID_INDEX");
    localStorage.removeItem("QUALITY_LIFE_INDEX");

    // remove all of the variables
    localStorage.removeItem("EVALUATION_INIT_VARIABLES");
    localStorage.removeItem("EVALUATION_VARIABLES");
    localStorage.removeItem("SECONDARIES_VARIABLES");
    localStorage.removeItem("QUALITY_VARIABLES");
    localStorage.removeItem("TREATMENT_VARIABLES");
    localStorage.removeItem("VISUAL_ACUITY_VARIABLES");
    localStorage.removeItem("CONTRAST_PERCEPTION_VARIABLES");
    localStorage.removeItem("AMSLER_GRID_VARIABLES");

    localStorage.removeItem("TOKEN");
  };

  const _setSideEffectsForm = (variablesAlreadyCompleted) => {
    let pageIndex = 0;
    if (variablesAlreadyCompleted.length === 17) {
      pageIndex = 4;
    }
    localStorage.setItem("SECONDARIES_INDEX", pageIndex);
    return pageIndex;
  };

  const _setEvaluationForm = (variablesAlreadyCompleted) => {
    let pageIndex = 0;
    [
      "contrastPerception80",
      "contrastPerception40",
      "contrastPerception25",
      "contrastPerception15",
      "contrastPerception10",
      "contrastPerception8",
      "contrastPerception4",
      "contrastPerception2",
    ].forEach((v, i) => {
      if (variablesAlreadyCompleted.indexOf(v.toLowerCase()) >= 0) {
        pageIndex = i + 5;
      }
    });
    if (pageIndex === 12) {
      pageIndex++;
    }

    EvaluationVariables.forEach((v, i) => {
      if (
        variablesAlreadyCompleted.indexOf(v.variableName.toLowerCase()) >= 0
      ) {
        pageIndex = i + 16;
      }
    });
    localStorage.setItem("PAGE_INDEX", pageIndex);
    return pageIndex;
  };

  const _setQualityForm = (variablesAlreadyCompleted) => {
    let pageIndex = 0;
    QualityVariables.forEach((v, i) => {
      if (variablesAlreadyCompleted.indexOf(v.name.toLowerCase()) >= 0) {
        pageIndex = i + 3;
      }
    });
    localStorage.setItem("QUALITY_INDEX", pageIndex);
    return pageIndex;
  };

  const _setQualityLifeForm = () => {
    let pageIndex = 0;
    localStorage.setItem("QUALITY_LIFE_INDEX", pageIndex);
    return pageIndex;
  };

  const _setVisualAcuityForm = () => {
    let pageIndex = 0;
    localStorage.setItem("VISUAL_ACUITY_INDEX", pageIndex);
    ["visualAcuityR", "visualAcuityL", "visualAcuity"].forEach(
      (variableName) => {
        console.log(`${variableName}_LAST_VALUE`, `${variableName}_RESULT`);
        localStorage.removeItem(`${variableName}_LAST_VALUE`);
        localStorage.removeItem(`${variableName}_RESULT`);
      }
    );
    return pageIndex;
  };

  const _setContrastPerceptionForm = () => {
    let pageIndex = 0;
    localStorage.setItem("CONTRAST_PERCEPTION_INDEX", pageIndex);
    ["contrastPerceptionR", "contrastPerceptionL"].forEach((variableName) => {
      localStorage.removeItem(`${variableName}_LAST_VALUE`);
    });
    return pageIndex;
  };

  const _setAmslerGridForm = () => {
    let pageIndex = 0;
    localStorage.setItem("AMSLER_GRID_INDEX", pageIndex);
    return pageIndex;
  };

  const onReConnect = () => {
    // reconnect
    // setCode("");
    setError(false);
  };

  const onChange = (value) => {
    setCode(value);
    if (value.length === 6) {
    } else {
      setError(false);
    }
  };

  const notFullCode = code.length < 6;

  return (
    <PageContainer className="login-page">
      {isSplash ? (
        <FlexCenter>
          <img src={logo} width="124" height="113" alt="" />
        </FlexCenter>
      ) : (
        <div className="wrapper">
          <Header></Header>
          <div className="content login-content">
            <p className="theme-blue">
              Vous avez reçu un code PIN dans votre SMS
            </p>
            <CodeInputer className={error ? "error" : ""}>
              <div className="label theme-blue">Saisissez votre code</div>
              <ReactCodeInput
                value={code}
                type="number"
                fields={6}
                onChange={onChange}
                className={`${code !== "" ? "focused" : ""} ${
                  error === true ? "error" : ""
                }`}
              />
            </CodeInputer>
            {error === true && (
              <p className="f18 red" style={{ marginTop: 15 }}>
                Votre code PIN est composé de 6 chiffres, il est dans le dernier
                SMS que vous avez reçu.
              </p>
            )}
            {/* <p style={questionStyle}>Vous ne vous l’avez pas reçu?</p>
            <ThemeIconButton style={{ marginTop: 7 }}>
              <img src={backIcon} alt="" />
              Renvoyer
            </ThemeIconButton> */}
            <Flex className="h-center footer-btns">
              {error || notFullCode ? (
                <ThemeOutlineButton
                  className="error"
                  shape="round"
                  // size="small"
                  style={loginBtnStyle}
                  onClick={onReConnect}
                  disabled={notFullCode}
                >
                  {notFullCode
                    ? "Connexion".toUpperCase()
                    : "Veuillez corriger"}
                </ThemeOutlineButton>
              ) : (
                <ThemeButton
                  shape="round blue"
                  style={loginBtnStyle}
                  onClick={onConnect}
                >
                  {"Connexion".toUpperCase()}
                </ThemeButton>
              )}
            </Flex>
          </div>
        </div>
      )}
      <LoadingOverlay loading={loading} />
    </PageContainer>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.AccountReducer.form,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getForm: (req) => {
      dispatch(FormActions.getForm(req.pinCode, req.cb));
    },
    tradePin: (req) => {
      dispatch(AccountActions.tradePin(req.pin, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
