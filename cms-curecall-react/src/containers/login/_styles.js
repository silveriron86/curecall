import styled from "@emotion/styled";

export const CodeInputer = styled("div")({
  marginTop: 43,
  marginBottom: 10,
  ".label": {
    margin: 0,
    padding: 0,
    fontFamily: "Luciole Bold",
    fontSize: 18,
    lineHeight: "26px",
    color: "#fff",
    marginBottom: 9,
  },
  ".react-code-input": {
    width: "100%",
    input: {
      marginLeft: "5px !important",
      marginRight: "5.9px !important",
      width: "13% !important",
      height: "66.86px !important",
      borderRadius: "5px !important",
      boxShadow: "1px 1px 4px rgba(0, 0, 0, 0.16) !important",
      outline: "none",
      border: "none !important",
      fontFamily: "Luciole Bold !important",
      fontSize: "25px !important",
      lineHeight: "33px",
      textAlign: "center",
      color: "#3333CC !important",
      padding: "0 !important",
    },
    "&.focused": {
      input: {
        boxShadow: "1px 1px 20px rgba(0, 0, 0, 0.16) !important",
      },
      "&.error": {
        input: {
          boxShadow: "3px 3px 15px rgba(255, 0, 0, 0.4) !important",
        },
      },
    },
  },
});

export const questionStyle = {
  fontFamily: "Nexa Book",
  fontWeight: "normal",
  fontSize: 16,
  lineHeight: "18px",
  textAlign: "left",
  color: "#fff",
};

export const loginBtnStyle = {
  marginTop: 60,
};
