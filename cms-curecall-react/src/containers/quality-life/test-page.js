import React from "react";
import { VisualAcuityWrapper, Title } from "../visualAcuity/_styles";
import QualityLifeVariables from "../../constants/QualityLifeVariables";
import { Flex } from "../_styles";
import { ThemeOutlineButton } from "../../components/_styles";

export default function QualityLifeTestPage(props) {
  const { variableIndex } = props;

  // current variable
  const v = QualityLifeVariables[variableIndex];

  const onSelect = (val) => {
    let data = {
      variableName: v.variableName,
      valueInt: val,
    };

    if (variableIndex >= QualityLifeVariables.length - 3) {
      data = {
        variableName: v.variableName,
        valueBool: val === 1 ? true : false,
      };
    }

    const completed = variableIndex === QualityLifeVariables.length - 1;
    props.onFinished([data], completed);
  };

  return (
    <VisualAcuityWrapper className="height-auto">
      <div
        className="main between quality-life-answers"
        style={{ paddingBottom: 20 }}
      >
        <Title>{v.question}</Title>
        <div style={{ width: "100%" }}>
          <Flex
            className="space-between mt33"
            style={{ width: "100%", marginTop: 33 }}
          >
            <div style={{ flex: 1 }}>
              <ThemeOutlineButton
                key={`answer-${variableIndex}-0`}
                onClick={() => onSelect(1)}
              >
                {v.answers[0]}
              </ThemeOutlineButton>
            </div>
            <div style={{ width: 10 }}></div>
            <div style={{ flex: 1 }}>
              <ThemeOutlineButton
                key={`answer-${variableIndex}-1`}
                onClick={() => onSelect(0)}
              >
                {v.answers[1]}
              </ThemeOutlineButton>
            </div>
          </Flex>
          {v.answers.length > 2 && (
            <div style={{ marginTop: 8.3, width: "100%" }}>
              <ThemeOutlineButton
                key={`answer-${variableIndex}-2`}
                className={`narrow ${
                  v.answers[2].length > 25 ? "two-lines" : ""
                }`}
                onClick={() => onSelect(2)}
              >
                {v.answers[2]}
              </ThemeOutlineButton>
            </div>
          )}
        </div>
      </div>
    </VisualAcuityWrapper>
  );
}
