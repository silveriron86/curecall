import React from "react";
import { VisualAcuityWrapper, Title } from "../visualAcuity/_styles";
import { ReactComponent as LogoIcon } from "../../assets/svg/LogoCurecall.svg";

export default function QualityLifeEndPage() {
  return (
    <VisualAcuityWrapper className="height-auto">
      <div className="main">
        <LogoIcon />
        <Title className="text-center" style={{ marginTop: 36, width: 250 }}>
          Merci pour vos réponses qui serviront à mieux personnaliser votre
          suivi
        </Title>
      </div>
    </VisualAcuityWrapper>
  );
}
