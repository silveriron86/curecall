import React, { useState, useEffect } from "react";
import LoadingOverlay from "../../components/Loading-overlay";
import { FormActions } from "../../actions";
import { connect } from "react-redux";
import Utils from "../../utils";
import QualityLifeStartPage from "./start-page";
import { PageContainer } from "../_styles";
import QualityLifeEndPage from "./end-page";
import QualityLifeTestPage from "./test-page";

// new quality-life form
function QualityLifePage(props) {
  const [loading, setLoading] = useState(false);
  const [pageIndex, setPage] = useState(0);
  const [variableIndex, setVariableIndex] = useState(0);

  useEffect(() => {
    const pin = localStorage.getItem("PIN_CODE");
    const type = localStorage.getItem("FORM_TYPE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }
    if (type !== "quality-life") {
      window.location.href = `/form/${type}`;
      return;
    }

    const index = localStorage.getItem("QUALITY_LIFE_INDEX");
    if (index !== null) {
      setPage(parseInt(index, 10));
    }
    Utils.setMobileViewPort();
  }, []);

  const _setPage = (index) => {
    setPage(index);
    localStorage.setItem("QUALITY_LIFE_INDEX", index.toString());
  };

  const goNext = () => {
    let index = pageIndex + 1;
    _setPage(index);
  };

  const onFinished = (data, completed) => {
    const pin = localStorage.getItem("PIN_CODE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }

    setLoading(true);
    props.postForm({
      data: {
        pin,
        completed,
        variables: data,
      },
      cb: (res) => {
        if (typeof res.status !== "undefined") {
          setLoading(false);
          Utils.showProblemAlert();
        } else {
          setLoading(false);
          if (completed) {
            localStorage.removeItem("PIN_CODE");
            localStorage.removeItem("QUALITY_LIFE_INDEX");
            goNext();
          } else {
            setVariableIndex(variableIndex + 1);
          }
        }
      },
    });
  };

  const goPrev = () => {
    let index = pageIndex - 1;
    if (index < 0) {
      return;
    }
    _setPage(index);
  };

  console.log(pageIndex);

  return (
    <>
      <PageContainer className="pg-white">
        {pageIndex < 2 ? (
          <QualityLifeStartPage
            {...props}
            pageIndex={pageIndex}
            goPrev={goPrev}
            goNext={goNext}
          />
        ) : pageIndex === 2 ? (
          <QualityLifeTestPage
            variableIndex={variableIndex}
            onFinished={onFinished}
          />
        ) : (
          <QualityLifeEndPage />
        )}
      </PageContainer>
      <LoadingOverlay loading={loading} />
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.FormReducer.form,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getForm: (req) => {
      dispatch(FormActions.getForm(req.pinCode, req.cb));
    },
    postForm: (req) => {
      dispatch(FormActions.postForm(req.data, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(QualityLifePage);
