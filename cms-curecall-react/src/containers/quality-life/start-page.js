import React from "react";
import { ThemeButton } from "../../components/_styles";
import { VisualAcuityWrapper, Title } from "../visualAcuity/_styles";
import hartIcon from "../../assets/svg/BlueHart.svg";

export default function QualityLifeStartPage(props) {
  const { pageIndex, goNext } = props;
  
  return (
    <VisualAcuityWrapper className="height-auto">
      {pageIndex === 0 ? (
        <div className="main">
          <img src={hartIcon} alt="" />
          <p style={{ width: 243 }} className="bold">
            Votre médecin souhaite vous poser quelques questions sur les
            difficultés que vous rencontrez au quotidien
          </p>
        </div>
      ) : (
        <div className="main between" style={{ paddingBottom: 20 }}>
          <Title>Instructions pour le test</Title>
          <div className="main">
            <p className="small black">
              Vos réponses sont précieuses pour votre médecin afin de
              personnaliser votre suivi.
              <br />
              <br />
              Choisissez la réponse qui correspond le mieux à votre vécu et
              ressenti, il n’y a pas de bonne ou mauvaise réponse.
            </p>
          </div>
        </div>
      )}

      <ThemeButton shape="round" className="blue" onClick={goNext}>
        {(pageIndex === 0 ? "Continuer" : "Commencer").toUpperCase()}
      </ThemeButton>
    </VisualAcuityWrapper>
  );
}
