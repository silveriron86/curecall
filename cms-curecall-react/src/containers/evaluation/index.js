import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import LoadingOverlay from "../../components/Loading-overlay";
import { Flex, PageMark, PageContainer } from "../_styles";
import {
  HeaderWrapper,
  ThemeButton,
  ThemeIconButton,
} from "../../components/_styles";
import ImagePage from "./image-page";

import welcomeIcon from "../../assets/svg/Welcome.svg";
import attentionIcon from "../../assets/svg/Attention.svg";
// import listenCloseIcon from "../../assets/svg/Listen close.svg";
import nextIcon from "../../assets/svg/Come.svg";
import prevIcon from "../../assets/svg/Back.svg";
import bgImg from "../../assets/svg/Path.svg";
import logoIcon from "../../assets/svg/LogoBlanc.svg";
import CompletePage from "./complete-page";
import SixPage from "./six-page";
import SevenPage from "./seven-page";
import FormActions from "../../actions/FormActions";
import { EvaluationVariables } from "../../constants";
import Utils from "../../utils";

function EvaluationPage(props) {
  const [loading, setLoading] = useState(false);
  // const [answer, setAnswer] = useState(-1);
  const [pageIndex, setPage] = useState(0);
  const [initVariables, setInitVariables] = useState([
    { variableName: "contrastPerception80", valueInt: -1 },
    { variableName: "contrastPerception40", valueInt: -1 },
    { variableName: "contrastPerception25", valueInt: -1 },
    { variableName: "contrastPerception15", valueInt: -1 },
    { variableName: "contrastPerception10", valueInt: -1 },
    { variableName: "contrastPerception8", valueInt: -1 },
    { variableName: "contrastPerception4", valueInt: -1 },
    { variableName: "contrastPerception2", valueInt: -1 },
  ]);

  const [variables, setVariables] = useState(EvaluationVariables);

  useEffect(() => {
    const pin = localStorage.getItem("PIN_CODE");
    const type = localStorage.getItem("FORM_TYPE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }
    if (type !== "visual-function") {
      window.location.href = `/form/${type}`;
      return;
    }

    const index = localStorage.getItem("PAGE_INDEX");
    if (index !== null) {
      setPage(parseInt(index, 10));
    }

    const vs = localStorage.getItem("EVALUATION_INIT_VARIABLES");
    if (vs) {
      setInitVariables(JSON.parse(vs));
    }

    const vs1 = localStorage.getItem("EVALUATION_VARIABLES");
    if (vs1) {
      setVariables(JSON.parse(vs1));
    }
  }, []);

  // useEffect(() => {
  //   setAnswer(-1);
  // }, [pageIndex]);

  const onSelectAnswer = (selected) => {
    // setAnswer(selected);
    if (pageIndex < 13) {
      const vs = JSON.parse(JSON.stringify(initVariables));
      vs[pageIndex - 4].valueInt = selected;
      localStorage.setItem("EVALUATION_INIT_VARIABLES", JSON.stringify(vs));
      setInitVariables(vs);
    } else {
      const vs = JSON.parse(JSON.stringify(variables));
      vs[pageIndex - 15].valueInt = selected;
      localStorage.setItem("EVALUATION_VARIABLES", JSON.stringify(vs));
      setVariables(vs);
    }
  };

  const _next = () => {
    let index = pageIndex + 1;
    if (pageIndex === 11) {
      index++;
    }
    setPage(index);
    localStorage.setItem("PAGE_INDEX", index.toString());
  };

  const _skipToLast = () => {
    setPage(35);
    localStorage.setItem("PAGE_INDEX", "35");
  };

  const goNext = () => {
    if (pageIndex >= 4 && pageIndex <= 11) {
      const data = initVariables[pageIndex - 4];
      onFinished([data], _next);
    } else if (pageIndex >= 15 && pageIndex <= 34) {
      const data = variables[pageIndex - 15];
      if (data.variableName !== "") {
        if (
          pageIndex === 32 &&
          data.variableName === "eyeSensitivity" &&
          data.valueInt === 0
        ) {
          // when eyeSensitivity is FALSE
          onFinished(
            [
              data,
              { variableName: "eyeSensitivityNausea", valueInt: 0 },
              { variableName: "eyePainScale", valueInt: 0 },
            ],
            _skipToLast
          );
        } else {
          onFinished([data], _next);
        }
      } else {
        _next();
      }
    } else {
      _next();
    }
  };

  const goPrev = () => {
    console.log("*** go prev *** ", pageIndex, variables);
    let index = pageIndex - 1;
    if (pageIndex === 13) {
      index--;
    }
    if (pageIndex === 35 && variables[17].valueInt === 0) {
      index = 32;
    }
    setPage(index);
    localStorage.setItem("PAGE_INDEX", index.toString());
  };

  const onFinished = (data, callback) => {
    const pin = localStorage.getItem("PIN_CODE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }
    setLoading(true);

    let completed = false;
    if (
      pageIndex === 34 ||
      (pageIndex === 32 && variables[17].valueInt === 0)
    ) {
      completed = true;
    }

    props.postForm({
      data: {
        pin,
        completed,
        variables: data,
      },
      cb: (res) => {
        console.log(res.status);
        if (typeof res.status !== "undefined") {
          setLoading(false);
          Utils.showProblemAlert();
        } else {
          setLoading(false);
          if (completed === true) {
            localStorage.removeItem("PAGE_INDEX");
            localStorage.removeItem("PIN_CODE");
            localStorage.removeItem("EVALUATION_INIT_VARIABLES");
            localStorage.removeItem("EVALUATION_VARIABLES");
          }
          callback();
        }
      },
    });
  };

  const toggleListen = () => {};
  console.log("pageIndex = ", pageIndex);
  console.log("*** Variables = ", variables);

  let vAnswer = 0;
  if (pageIndex >= 15 && pageIndex <= 34) {
    vAnswer = variables[pageIndex - 15].valueInt;
  }

  if (pageIndex >= 24 && pageIndex < 31) {
    return (
      <>
        <SixPage
          pageIndex={pageIndex}
          goPrev={goPrev}
          toggleListen={toggleListen}
          goNext={goNext}
          answer={vAnswer}
          onSelectAnswer={onSelectAnswer}
        />
        <LoadingOverlay loading={loading} />
      </>
    );
  }

  if (pageIndex >= 32 && pageIndex < 37) {
    return (
      <>
        <SevenPage
          pageIndex={pageIndex}
          goPrev={goPrev}
          toggleListen={toggleListen}
          goNext={goNext}
          answer={vAnswer}
          onSelectAnswer={onSelectAnswer}
        />
        <LoadingOverlay loading={loading} />
      </>
    );
  }

  return (
    <PageContainer>
      <div className="wrapper more">
        {pageIndex === 0 && (
          <div className="main">
            <HeaderWrapper>
              <span className="full">
                Evaluation de votre fonction visuelle
              </span>
            </HeaderWrapper>

            <HeaderWrapper style={{ marginTop: 56.5 }} className="center">
              <img src={welcomeIcon} width="50" height="56" alt="" />
              {/* <span>M. Mohammed</span> */}
            </HeaderWrapper>

            <div className="content" style={{ marginTop: 30.7 }}>
              <p className="f18">
                Ensemble, nous allons évaluer votre fonction visuelle. Temps
                estimé pour réaliser ce test:{" "}
                <span className="yellow">6 minutes</span>.
              </p>
            </div>
          </div>
        )}

        {pageIndex === 1 && (
          <div className="main">
            <HeaderWrapper>
              <span className="full">Pour commencer</span>
            </HeaderWrapper>

            <HeaderWrapper
              style={{ marginTop: 31.5, display: "block" }}
              className="center"
            >
              <img src={attentionIcon} width="50" height="56" alt="" />
              <span className="block full" style={{ marginTop: 22.7 }}>
                Augmentez la luminosité de votre téléphone au maximum
              </span>
            </HeaderWrapper>

            <div className="content" style={{ marginTop: 21 }}>
              <p className="f18">
                <span className="yellow">
                  Astuce : Si vous avez un iPhone ou un téléphone Android,
                  demandez le à SIRI ou à Google
                </span>
              </p>
            </div>
          </div>
        )}

        {pageIndex === 2 && (
          <div className="main">
            <HeaderWrapper>
              <span className="full">La perception des contrastes</span>
            </HeaderWrapper>
            {/* <div className="content" style={{ marginTop: 14.7 }}>
              <p className="f18">
                <span className="yellow">
                  SPECS A SUIVRE POUR EFFECTUER LES VISUELS CI-DESSOUS
                </span>
              </p>
            </div> */}
            <p
              className="blue-italic"
              style={{ marginTop: 14.7, marginBottom: 25.8 }}
            >
              La baisse de la perception des contrastes peut être un bon
              indicateur de l'évolution de votre vision. En contrôlant son
              évolution entre les consultations, votre Ophtalmologue pourra
              adapter votre prise en charge et améliorer son diagnostic.
            </p>
            {/* <Flex className="space-between">
              <ThemeIconButton className="hidden">
                <img src={nextIcon} alt="" />
              </ThemeIconButton>
              <ThemeIconButton onClick={toggleListen}>
                <img src={listenCloseIcon} alt="" />
              </ThemeIconButton>
              <ThemeIconButton onClick={goNext}>
                <img src={nextIcon} alt="" />
              </ThemeIconButton>
            </Flex> */}
          </div>
        )}

        {pageIndex === 3 && (
          <div className="main">
            <HeaderWrapper style={{ display: "block" }} className="center">
              <PageMark>
                <span>1</span>
                <img src={bgImg} width="50" height="56" alt="" />
              </PageMark>
              <span className="block full f18" style={{ marginTop: 16.2 }}>
                Nous allons commencer par vous montrer des lignes de lettres
                plus ou moins contrastées, il vous suffit d’évaluer ce que vous
                voyez. Il y a 8 images.
              </span>
            </HeaderWrapper>

            <div className="content" style={{ marginTop: 20 }}>
              <p className="f18">
                <span className="yellow">
                  Pensez à positionner la luminosité de votre écran au maximum.
                </span>
              </p>

              {/* <ThemeIconButton style={{ marginTop: 18.8 }}>
                <img src={listenCloseIcon} alt="" />
              </ThemeIconButton> */}
              <Flex className="h-center">
                <ThemeButton
                  shape="round"
                  style={{ marginTop: 147.5 }}
                  onClick={goNext}
                >
                  Commencer
                </ThemeButton>
              </Flex>
            </div>
          </div>
        )}

        {pageIndex >= 4 && pageIndex < 12 && (
          <ImagePage
            category={0}
            index={pageIndex - 4}
            pageIndex={pageIndex}
            total={8}
            goPrev={goPrev}
            goNext={goNext}
            answer={initVariables[pageIndex - 4].valueInt}
            onSelectAnswer={onSelectAnswer}
          />
        )}

        {pageIndex === 12 && (
          <div className="main">
            <Flex className="v-center h-center">
              <img src={logoIcon} width={82} height={75} alt="" />
            </Flex>
            <HeaderWrapper style={{ margin: "23px 0" }}>
              <span className="full">
                Merci pour vos réponses. Toues les données ont été transmises à
                votre Ophtalmologue. Ces données serviront aussi à personnaliser
                votre suivi et prise en charge.
              </span>
            </HeaderWrapper>
          </div>
        )}

        {(pageIndex === 13 ||
          pageIndex === 17 ||
          pageIndex === 19 ||
          pageIndex === 21 ||
          pageIndex === 23 ||
          pageIndex === 31) && <CompletePage index={pageIndex} />}

        {pageIndex === 14 && (
          <div className="main">
            <HeaderWrapper style={{ display: "block" }} className="center">
              <PageMark>
                <span>2</span>
                <img src={bgImg} width="50" height="56" alt="" />
              </PageMark>
              <span className="block full f18" style={{ marginTop: 16.2 }}>
                Nous allons vous aider à évaluer votre sensibilité à la lumière
                au regard de certaines situations.
              </span>
            </HeaderWrapper>

            <div className="content" style={{ marginTop: 20 }}>
              <p className="f18">
                <span className="yellow">
                  Pensez à positionner la luminosité de votre écran au maximum.
                </span>
              </p>

              {/* <ThemeIconButton style={{ marginTop: 18.8 }}>
                <img src={listenCloseIcon} alt="" />
              </ThemeIconButton> */}
              <Flex className="h-center">
                <ThemeButton
                  shape="round"
                  style={{ marginTop: 100 }}
                  onClick={goNext}
                >
                  Commencer
                </ThemeButton>
              </Flex>
            </div>
            <div style={{ position: "fixed", bottom: 25 }}>
              <ThemeIconButton
                className={pageIndex <= 2 || pageIndex === 4 ? "hidden" : ""}
                onClick={goPrev}
              >
                <img src={prevIcon} alt="" />
              </ThemeIconButton>
            </div>
          </div>
        )}

        {pageIndex >= 15 && pageIndex < 17 && (
          <ImagePage
            category={1}
            pageIndex={pageIndex}
            index={pageIndex - 15}
            total={2}
            goPrev={goPrev}
            goNext={goNext}
            answer={vAnswer}
            onSelectAnswer={onSelectAnswer}
          />
        )}

        {pageIndex === 18 && (
          <ImagePage
            category={2}
            index={0}
            pageIndex={pageIndex}
            total={1}
            goPrev={goPrev}
            goNext={goNext}
            answer={vAnswer}
            onSelectAnswer={onSelectAnswer}
          />
        )}

        {pageIndex === 20 && (
          <ImagePage
            category={3}
            index={0}
            pageIndex={pageIndex}
            total={1}
            goPrev={goPrev}
            goNext={goNext}
            answer={vAnswer}
            onSelectAnswer={onSelectAnswer}
          />
        )}

        {pageIndex === 22 && (
          <ImagePage
            category={4}
            index={0}
            pageIndex={pageIndex}
            total={1}
            goPrev={goPrev}
            goNext={goNext}
            answer={vAnswer}
            onSelectAnswer={onSelectAnswer}
          />
        )}
      </div>

      {(pageIndex <= 2 ||
        pageIndex === 12 ||
        pageIndex === 13 ||
        pageIndex === 17 ||
        pageIndex === 19 ||
        pageIndex === 21 ||
        pageIndex === 23 ||
        pageIndex === 31) && (
        <div className="footer">
          <ThemeIconButton
            className={pageIndex <= 2 || pageIndex === 4 ? "hidden" : ""}
            onClick={goPrev}
          >
            <img src={prevIcon} alt="" />
          </ThemeIconButton>
          {/* <ThemeIconButton onClick={toggleListen}>
            <img src={listenCloseIcon} alt="" />
          </ThemeIconButton> */}
          <div style={{ width: 45 }}></div>
          <ThemeIconButton onClick={goNext}>
            <img src={nextIcon} alt="" />
          </ThemeIconButton>
        </div>
      )}
      <LoadingOverlay loading={loading} />
    </PageContainer>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.FormReducer.form,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getForm: (req) => {
      dispatch(FormActions.getForm(req.pinCode, req.cb));
    },
    postForm: (req) => {
      dispatch(FormActions.postForm(req.data, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EvaluationPage);
