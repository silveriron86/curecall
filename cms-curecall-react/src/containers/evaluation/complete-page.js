import React from "react";
import { HeaderWrapper } from "../../components/_styles";

function CompletePage(props) {
  const { index } = props;
  const description =
    index === 13
      ? "La sensibilité à la lumière, aussi nommée photophobie, correspond à une gêne douloureuse d’origine oculaire provoquée par l’exposition à la lumière. En contrôlant régulièrement vos sensations, nous donnons une information importante à votre médecin lui permettant d’adapter votre traitement thérapeutique ou votre correction visuelle."
      : index === 17
      ? "Certaines atteintes visuelles se manifestent par l’apparition d’une cécité nocturne, nommée héméralopie. Elle se caractérise par une diminution progressive ou une perte de la vision de nuit. Evaluer régulièrement votre capacité à voir la nuit peut aider votre Ophtalmologue à adapter votre prise en charge et à orienter son diagnostic."
      : index === 19
      ? "Il est fréquent que les patients se plaignent d'une fatigue oculaire qui se manifeste suite à un effort intense des yeux. Auto évaluer cette fatigue régulièrement permettra à votre ophtalmologue de mieux suivre l'évolution de vos symptômes afin de vous apporter la meilleure prise en charge possible."
      : index === 21
      ? "La sécheresse oculaire est due à une insuffisance de la production de larmes. Certains traitements sont associés à de nombreuses modifications de la surface oculaire. Le suivi de la manifestation d'une sécheresse oculaire donnera des informations importantes à votre Ophtalmologue sur la toxicité locale de votre traitement afin d'adapter au mieux votre prise en charge."
      : index === 23
      ? "L'acuité visuelle est la capacité de discernement des informations apportées au cerveau par la vue et détermine la qualité de la vue. Le suivi de la qualité de votre vision est essentiel dans le cadre de votre suivi. Les auto-tests réalisés permettront à votre Ophtalmo d'adapter votre prise en charge et traitement éventuellement."
      : index === 31
      ? "L'apparition de douleur oculaire est une information clé dans votre prise en charge. Elle permettra à votre Ophtalmologue de surveiller à distance l'évolution de la santé de vos yeux. Evaluer celle-ci permettra d'adapter votre prise en charge via Curecall."
      : "";

  return (
    <div className="main">
      <HeaderWrapper>
        <span className="full">
          {index === 13
            ? "Sensibilité à la lumière"
            : index === 17
            ? "Cécité nocturne ou crépusculaire"
            : index === 19
            ? "Fatigue visuelle"
            : index === 21
            ? "Sécheresse oculaire"
            : index === 23
            ? "Baisse de l’acuité visuelle (vision déformée)"
            : index === 31
            ? "Sensibilité douloureuse au niveau des yeux"
            : ""}
        </span>
      </HeaderWrapper>
      <div className="content" style={{ marginTop: 16 }}>
        <p className="f16 bold-italic">
          <span
            className="blue"
            dangerouslySetInnerHTML={{ __html: description }}
          ></span>
        </p>
      </div>
    </div>
  );
}

export default CompletePage;
