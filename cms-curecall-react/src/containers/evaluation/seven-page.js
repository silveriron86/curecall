// A6.2
import React from "react";
import { Slider } from "antd";
import { PageMark, PageContainer } from "../_styles";
import { ThemeIconButton, HeaderWrapper } from "../../components/_styles";
// import listenCloseIcon from "../../assets/svg/Listen close.svg";
import prevIcon from "../../assets/svg/Back.svg";
import bgImg from "../../assets/svg/Path.svg";
import ImagePage from "./image-page";
import PageNo from "../../components/page-no";

import icon1 from "../../assets/svg/Component 50.svg";
import icon2 from "../../assets/svg/Component 51.svg";
import icon3 from "../../assets/svg/Component 52.svg";
import icon4 from "../../assets/svg/Component 53.svg";
import icon5 from "../../assets/svg/Component 54.svg";
import icon6 from "../../assets/svg/Component 55.svg";

const icons = [
  icon1,
  icon1,
  icon2,
  icon2,
  icon3,
  icon3,
  icon4,
  icon4,
  icon5,
  icon6,
  icon6,
];
const texts = [
  "Aucune Douleur",
  "Aucune Douleur",
  "Légère",
  "Légère",
  "Modérée",
  "Modérée",
  "Forte",
  "Forte",
  "Très forte",
  "La pire douleur qui soit",
  "La pire douleur qui soit",
];

const pageTitles = [
  "Sentez-vous une douleur à l’intérieur de votre œil ?",
  "Est- ce que cette douleur est souvent associée à des vomissements, des nausées et des sueurs ?",
  "Si vous deviez évaluer votre douleur sur une échelle de <span class='yellow'>0 (Pas de douleur)</span> à <span class='yellow'>10 (insupportable)</span> que diriez-vous ?",
];

function SevenPage(props) {
  // const [isListenOpened, setListen] = useState(false);
  // const [level, setLevel] = useState(0);
  const { pageIndex, answer, onSelectAnswer } = props;
  console.log("seven page = ", pageIndex);

  // const toggleListen = () => {
  //   setListen(!isListenOpened);
  // };

  if (pageIndex >= 32 && pageIndex < 35) {
    return (
      <PageContainer className="seven-page">
        <div className="wrapper more">
          <HeaderWrapper style={{ width: 247 }}>
            <PageMark>
              <span>{7}</span>
              <img src={bgImg} width="50" height="56" alt="" />
            </PageMark>
          </HeaderWrapper>
          <HeaderWrapper style={{ marginTop: 16, width: 290 }}>
            <span
              className="full"
              dangerouslySetInnerHTML={{ __html: pageTitles[pageIndex - 32] }}
            ></span>
          </HeaderWrapper>

          <PageNo value={pageIndex - 31} total={3} />
          {pageIndex === 34 && (
            <div className="content p0">
              <p
                className="f16 bold-italic"
                style={{ marginTop: 20, marginBottom: 27 }}
              >
                <span className="blue">Echelle de douleur :</span>
              </p>
              <div style={{ padding: "0 10px" }}>
                <Slider
                  className="gradient-slider"
                  value={answer}
                  min={0}
                  max={10}
                  tooltipVisible={false}
                  onChange={onSelectAnswer}
                />
              </div>
              <ThemeIconButton className="laugh">
                <img src={icons[answer]} alt="" />
                <p className="f16">{texts[answer]}</p>
              </ThemeIconButton>
            </div>
          )}

          <div style={{ marginTop: -21 }}>
            <ImagePage
              category={7}
              pageIndex={pageIndex}
              index={pageIndex - 32}
              total={3}
              goPrev={props.goPrev}
              goNext={props.goNext}
              answer={pageIndex >= 34 ? 0 : answer}
              onSelectAnswer={onSelectAnswer}
            />
          </div>
        </div>
      </PageContainer>
    );
  }

  if (pageIndex === 35) {
    const lastPL = localStorage.getItem("LAST_PAGE_INDEX");
    const lastPageIndex = lastPL ? parseInt(lastPL, 10) : 0;

    return (
      <PageContainer>
        <div className="wrapper">
          <HeaderWrapper
            className="full-center"
            style={{ height: "calc(100vh - 130px)" }}
          >
            <span>
              Merci pour vos réponses.
              <br />
              Toutes les données ont été transmises à votre Ophtalmologue. Ces
              données serviront aussi à personnaliser votre suivi et prise en
              charge.
            </span>
          </HeaderWrapper>
        </div>
        <div className="footer">
          {pageIndex > lastPageIndex ? (
            <ThemeIconButton onClick={props.goPrev}>
              <img src={prevIcon} alt="" />
            </ThemeIconButton>
          ) : (
            <div />
          )}
          <ThemeIconButton style={{ visibility: "hidden", width: 45 }}>
            {/* <img src={listenCloseIcon} alt="" /> */}
          </ThemeIconButton>
        </div>
        {/* <ThemeIconButton className="close-btn">
          <img src={closeIcon} alt="" />
        </ThemeIconButton> */}
        {/* <ThemeIconButton className="listen-btn" onClick={toggleListen}>
          <img src={listenCloseIcon} alt="" />
        </ThemeIconButton> */}
      </PageContainer>
    );
  }
}

export default SevenPage;
