// A6.2
import React from "react";
import { Flex, PageMark, PageContainer } from "../_styles";
import { ThemeIconButton, HeaderWrapper } from "../../components/_styles";
// import listenCloseIcon from "../../assets/svg/Listen close.svg";
// import listenOpenIcon from "../../assets/svg/Listen Open.svg";
import attentionIcon from "../../assets/svg/Attention.svg";
import distanceImg from "../../assets/svg/distance.svg";
import glassesImg from "../../assets/svg/glasses.svg";
import nextIcon from "../../assets/svg/Come.svg";
import prevIcon from "../../assets/svg/Back.svg";
import bgImg from "../../assets/svg/Path.svg";
import eyeIcon from "../../assets/svg/Union 16.svg";
import handIcon from "../../assets/svg/Group 38.svg";
import AnswersList from "../../components/answers-list";
import ImagePage from "./image-page";
import PageNo from "../../components/page-no";

const pageTitles = [
  "Vous arrive-t-il de plisser les yeux pour bien lire les panneaux ?",
  "Semblez-vous voir flou de loin, par exemple éprouvez-vous des difficultés à reconnaître les visages des personnes familières ?",
  "Vous appréciez les distances dans certaines circonstances comme descendre les escaliers ou garer une voiture ?",
  "Pour voir sur les côtés, comme lorsqu’une voiture sort d’une pente de garage d’une rue latérale, ou lorsque quelqu’un sort d’un bâtiment vous ressentez ?",
];

function SixPage(props) {
  // const [isListenOpened, setListen] = useState(false);
  const { pageIndex, answer, onSelectAnswer } = props;
  console.log("six page = ", pageIndex);

  // const toggleListen = () => {
  //   setListen(!isListenOpened);
  // };

  if (pageIndex >= 27 && pageIndex < 31) {
    return (
      <PageContainer>
        <div className="wrapper more">
          <HeaderWrapper style={{ width: 247 }}>
            <PageMark>
              <span>{6}</span>
              <img src={bgImg} width="50" height="56" alt="" />
            </PageMark>
          </HeaderWrapper>
          <HeaderWrapper style={{ marginTop: 16, width: 290 }}>
            <span
              className="full"
              dangerouslySetInnerHTML={{ __html: pageTitles[pageIndex - 27] }}
            ></span>
          </HeaderWrapper>

          <PageNo value={pageIndex - 26} total={4} />
          <div style={{ marginTop: -21 }}>
            <ImagePage
              category={6}
              pageIndex={pageIndex}
              index={pageIndex - 27}
              total={4}
              goPrev={props.goPrev}
              goNext={props.goNext}
              answer={answer}
              onSelectAnswer={onSelectAnswer}
            />
          </div>
        </div>
      </PageContainer>
    );
  }

  const lastPL = localStorage.getItem("LAST_PAGE_INDEX");
  const lastPageIndex = lastPL ? parseInt(lastPL, 10) : 0;

  return (
    <PageContainer>
      <div className="wrapper more">
        {pageIndex === 24 && (
          <div className="main">
            <HeaderWrapper style={{ display: "block" }} className="center">
              <img src={attentionIcon} width="50" height="56" alt="" />
              <span className="block full" style={{ marginTop: 16 }}>
                La mesure de l’acuité visuelle est réalisée séparément pour
                chaque oeil, et à deux distances d’observation de près et de
                loin.
              </span>
            </HeaderWrapper>
            <div className="content">
              <p className="f18">Pour tester votre vision de près, </p>
              <Flex className="h-center" style={{ margin: "28px 0 25px" }}>
                <img src={distanceImg} width="234" alt="" />
              </Flex>
              <p
                className="f18 center"
                style={{ width: 250, margin: "0 auto" }}
              >
                Tenez votre portable à environ 35 cm de vos yeux{" "}
              </p>
              <Flex className="h-center" style={{ margin: "63px 0 21px" }}>
                <img src={glassesImg} width="172" alt="" />
              </Flex>
              <p className="f18 center">Retirez vos lunettes</p>
              <Flex
                className="h-center v-center"
                style={{ margin: "76px 0 16px" }}
              >
                <ThemeIconButton>
                  <img src={eyeIcon} alt="" />
                </ThemeIconButton>
                <ThemeIconButton style={{ marginLeft: 30 }}>
                  <img src={handIcon} alt="" />
                </ThemeIconButton>
              </Flex>
              <p
                className="f18 center"
                style={{ width: 250, margin: "0 auto" }}
              >
                Avec votre main, masquez alternativement un oeil, puis l’autre
              </p>
            </div>
          </div>
        )}

        {pageIndex === 25 && (
          <div className="main">
            <HeaderWrapper style={{ display: "block" }} className="center">
              {/* <img src={attentionIcon} width="50" height="56" alt="" /> */}
              <HeaderWrapper style={{ width: 247 }}>
                <PageMark>
                  <span>{6}</span>
                  <img src={bgImg} width="50" height="56" alt="" />
                </PageMark>
              </HeaderWrapper>
              <span className="block full" style={{ marginTop: 16 }}>
                Eprouvez vous des difficultés à lire ce texte intégralement que
                ça soit de l’oeil droit ou l’oeil gauche?
              </span>
            </HeaderWrapper>
            <div className="content">
              <p className="f50 center">
                Tester son acuité visuelle est un exercice difficile sans votre
                médecin.
              </p>
              <p className="f30 center">
                C’est pourquoi nous allons évaluer avec vous des situations de
                vie quotidienne.
              </p>
              <p className="f18 center">
                Développer l’auto-évaluation vous permettra d’observer les
                changements
              </p>
              <p className="f10" style={{ width: 142, margin: "12px auto 0" }}>
                et donnera des informations importantes à votre médecin
              </p>
              <AnswersList
                category={5}
                index={0}
                // isListenOpened={isListenOpened}
                multiple={false}
                answer={answer}
                onSelect={onSelectAnswer}
              />
            </div>
          </div>
        )}

        {pageIndex === 26 && (
          <div className="main">
            <HeaderWrapper style={{ display: "block" }} className="center">
              <img src={attentionIcon} width="50" height="56" alt="" />
              <span className="block full" style={{ marginTop: 16 }}>
                Pour tester votre vision de loin, voici quelques situations de
                la vie quotidienne.
              </span>
            </HeaderWrapper>
            <div className="content"></div>
          </div>
        )}
      </div>
      <div
        className={`footer ${
          pageIndex === 24 || pageIndex === 25 ? "relative pdb-25" : ""
        }`}
      >
        {pageIndex > lastPageIndex ? (
          <ThemeIconButton onClick={props.goPrev}>
            <img src={prevIcon} alt="" />
          </ThemeIconButton>
        ) : (
          <div />
        )}

        {/* <ThemeIconButton onClick={toggleListen}>
          <img src={isListenOpened ? listenOpenIcon : listenCloseIcon} alt="" />
        </ThemeIconButton> */}
        <div style={{ width: 45 }}></div>
        {pageIndex !== 25 || answer >= 0 ? (
          <ThemeIconButton onClick={props.goNext}>
            <img src={nextIcon} alt="" />
          </ThemeIconButton>
        ) : (
          <div style={{ width: 45 }}></div>
        )}
      </div>
    </PageContainer>
  );
}

export default SixPage;
