import React from "react";
import { HeaderWrapper, ThemeIconButton } from "../../components/_styles";
import { PageMark } from "../_styles";
import PageNo from "../../components/page-no";

// import listenCloseIcon from "../../assets/svg/Listen close.svg";
// import listenOpenIcon from "../../assets/svg/Listen Open.svg";
import nextIcon from "../../assets/svg/Come.svg";
import prevIcon from "../../assets/svg/Back.svg";
import bgImg from "../../assets/svg/Path.svg";

import imgA12 from "../../assets/svg/A1.2.svg";
import imgA13 from "../../assets/svg/A1.3.svg";
import imgA14 from "../../assets/svg/A1.4.svg";
import imgA15 from "../../assets/svg/A1.5.svg";
import imgA16 from "../../assets/svg/A1.6.svg";
import imgA17 from "../../assets/svg/A1.7.svg";
import imgA18 from "../../assets/svg/A1.8.svg";
import imgA19 from "../../assets/svg/A1.9.svg";

import imgA21 from "../../assets/svg/A21.svg";
import imgA22 from "../../assets/svg/A22.svg";
import AnswersList from "../../components/answers-list";

const images = [
  [imgA12, imgA13, imgA14, imgA15, imgA16, imgA17, imgA18, imgA19],
  [imgA21, imgA22],
];

function ImagePage(props) {
  // const [isListenOpened, setListen] = useState(false);
  const { category, index, total, answer, onSelectAnswer, pageIndex } = props;
  console.log(category, index, total);

  // const toggleListen = () => {
  //   setListen(!isListenOpened);
  // };

  let pageTitle = "Comment percevez-vous les lettres de cette image ?";
  if (category === 1) {
    pageTitle = [
      "En voiture ou dans la rue, au lever du jour ou à la tombée de la nuit, le soleil est rasant et très intense comme le montre la photo ci-dessous. Cette situation est pour vous :",
      "Dans certaines situation telles qu'un centre commercial ou parfois chez soi, la lumière artificielle provenant du plafond est intense. Cette situation est pour vous :",
    ][index];
  }

  const lastPL = localStorage.getItem("LAST_PAGE_INDEX");
  const lastPageIndex = lastPL ? parseInt(lastPL, 10) : 0;

  return (
    <>
      <div className="main">
        {category < 2 && (
          <>
            <HeaderWrapper style={{ width: 247 }}>
              <span
                className="full"
                dangerouslySetInnerHTML={{ __html: pageTitle }}
              ></span>
            </HeaderWrapper>
            <PageNo value={index + 1} total={total} />
          </>
        )}

        {(category === 2 || category === 3 || category === 4) && (
          <HeaderWrapper style={{ display: "block" }} className="center">
            <PageMark>
              <span>{category + 1}</span>
              <img src={bgImg} width="50" height="56" alt="" />
            </PageMark>
            <span className="block full f18" style={{ marginTop: 16.2 }}>
              {category === 2
                ? "Quand vous vous déplacez dans la pénombre ou l’obscurité, la distinction des objets peut vous sembler moindre qu’auparavant. De même quand vous éteignez la lumière, vous devez attendre plus longtemps avant de distinguer les éléments qui vous entourent ?"
                : category === 3
                ? "Avez-vous du mal à supporter de regarder plus d’une heure un écran de télévision  ou un ordinateur ?"
                : category === 4
                ? "Avez-vous la sensation d’avoir du sable dans les yeux et que vos yeux sont plus secs ?"
                : ""}
            </span>
          </HeaderWrapper>
        )}

        <div className="content">
          {category < 2 && (
            <div className={`img-wrapper ${category === 0 ? "" : "auto"}`}>
              <img src={images[category][index]} alt="" />
            </div>
          )}
          <AnswersList
            category={category}
            index={index}
            // isListenOpened={isListenOpened}
            multiple={false}
            answer={answer}
            onSelect={onSelectAnswer}
          />
        </div>
      </div>

      <div
        className={`footer ${
          category === 0 ||
          category === 4 ||
          (category === 6 && index < 3) ||
          category === 7 ||
          category === 20
            ? ""
            : "relative"
        }`}
        style={{
          padding: 0,
          // width: "calc(375px - 72px)",
          left: 0,
          // left: "calc(50% - 151.5px)",
          marginTop: category === 6 && index === 3 ? 40 : 62,
        }}
      >
        {pageIndex > lastPageIndex ? (
          <ThemeIconButton
            className={category === 0 && index === 0 ? "hidden" : ""}
            onClick={props.goPrev}
          >
            <img src={prevIcon} alt="" />
          </ThemeIconButton>
        ) : (
          <div />
        )}
        {/* <ThemeIconButton onClick={toggleListen}>
          <img src={isListenOpened ? listenOpenIcon : listenCloseIcon} alt="" />
        </ThemeIconButton> */}
        <div style={{ width: 45 }}></div>
        {answer >= 0 ? (
          <ThemeIconButton onClick={props.goNext}>
            <img src={nextIcon} alt="" />
          </ThemeIconButton>
        ) : (
          <div style={{ width: 45 }}></div>
        )}
      </div>
    </>
  );
}

export default ImagePage;
