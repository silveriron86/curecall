import React, { useState, useEffect } from "react";
import { HeaderWrapper } from "../../components/_styles";
import Footer from "./footer";
import { PageContainer, PageMark } from "../_styles";
import EndPage from "./end-page";

import welcomeIcon from "../../assets/svg/Welcome.svg";
import attentionIcon from "../../assets/svg/Attention.svg";
import eyeIcon from "../../assets/svg/Eye.svg";
// import ListenBtn from "../../components/listen-btn";
import bgImg from "../../assets/svg/Path.svg";
import AnswersList from "../../components/answers-list";
import LoadingOverlay from "../../components/Loading-overlay";
import { FormActions } from "../../actions";
import { connect } from "react-redux";
import Utils from "../../utils";

function SecondariesPage(props) {
  // const [isListenOpened, setListen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [pageIndex, setPage] = useState(0);
  // const [answer, setAnswer] = useState([]);
  const [variables, setVariables] = useState([
    { variableName: "ocularSideEffectsIrritationEye", valueBool: false },
    { variableName: "ocularSideEffectsBlurredVision", valueBool: false },
    { variableName: "ocularSideEffectsEyeDiscomfort", valueBool: false },
    { variableName: "ocularSideEffectsConjunctivitis", valueBool: false },
    {
      variableName: "ocularSideEffectsImpairmentPeripheralVision",
      valueBool: false,
    },
    {
      variableName: "ocularSideEffectsImpairmentCentralVision",
      valueBool: false,
    },
    { variableName: "ocularSideEffectsLightPoints", valueBool: false },
    { variableName: "ocularSideEffectsLightFlash", valueBool: false },
    { variableName: "ocularSideEffectsVisionFlyingFlies", valueBool: false },
    { variableName: "ocularSideEffectsShadowBlackVeil", valueBool: false },
    { variableName: "ocularSideEffectsChangingEyeColor", valueBool: false },
    { variableName: "ocularSideEffectsEyeLashModification", valueBool: false },
    { variableName: "ocularSideEffectsDarkCircles", valueBool: false },
    { variableName: "ocularSideEffectsHavePuffyEyes", valueBool: false },
    { variableName: "ocularSideEffectsBurningSensation", valueBool: false },
    { variableName: "ocularSideEffects", valueBool: false },
    { variableName: "ocularSideEffectsOther", valueString: "" },
    { variableName: "systemicSideEffectsRhinitis", valueBool: false },
    { variableName: "systemicSideEffectsDyspnea", valueBool: false },
    { variableName: "systemicSideEffectsBronchitis", valueBool: false },
    { variableName: "systemicSideEffectsFatigue", valueBool: false },
    { variableName: "systemicSideEffectsAllergicReactions", valueBool: false },
    { variableName: "systemicSideEffectsPalpitations", valueBool: false },
    { variableName: "systemicSideEffectsArrhythmia", valueBool: false },
    { variableName: "systemicSideEffectsHeadaches", valueBool: false },
    { variableName: "systemicSideEffectsAsthenia", valueBool: false },
    { variableName: "systemicSideEffectsDepression", valueBool: false },
    { variableName: "systemicSideEffectsDryMouth", valueBool: false },
    { variableName: "systemicSideEffectsTasteModification", valueBool: false },
    { variableName: "systemicSideEffectsNausea", valueBool: false },
    { variableName: "systemicSideEffectsVertigo", valueBool: false },
    { variableName: "systemicSideEffectsSkinrashesFever", valueBool: false },
    { variableName: "systemicSideEffectsSleepApnea", valueBool: false },
    { variableName: "systemicSideEffects", valueBool: false },
    { variableName: "systemicSideEffectsOther", valueString: "" },
  ]);

  const setAnswer = (selected) => {
    const vs = JSON.parse(JSON.stringify(variables));
    let start = 0;
    let cnt = 17;
    let aucun = 15;
    if (pageIndex === 4) {
      start = 17;
      cnt = 18;
      aucun = 33;
    }

    for (let i = start; i < start + cnt; i++) {
      vs[i].valueBool =
        i !== aucun && selected.indexOf(i - start) >= 0 ? true : false;
    }
    if (selected.indexOf(aucun - start) >= 0) {
      // if P is selected, all should be false and empty string
      for (let i = start; i < start + cnt; i++) {
        vs[i].valueBool = false;
      }
      vs[aucun].valueBool = true;
      vs[aucun + 1].valueBool = false;
      vs[aucun + 1].valueString = "";
    } else {
      // if P is NOT selected, P should be false
      vs[aucun].valueBool = false;
    }
    localStorage.setItem("SECONDARIES_VARIABLES", JSON.stringify(vs));
    setVariables(vs);
  };

  const onChangeString = (e) => {
    const vs = JSON.parse(JSON.stringify(variables));
    vs[pageIndex === 3 ? 16 : 34].valueString = e.target.value;
    setVariables(vs);
  };

  const onFinished = (callback) => {
    const pin = localStorage.getItem("PIN_CODE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }

    const start = pageIndex === 3 ? 0 : 17;
    const vs = JSON.parse(JSON.stringify(variables));
    delete vs[pageIndex === 3 ? 16 : 34].valueBool;
    let data = vs.splice(start, pageIndex === 3 ? 17 : 18);

    if (pageIndex === 3) {
      data[15].valueBool = !data[15].valueBool;
    } else {
      data[16].valueBool = !data[16].valueBool;
    }
    setLoading(true);
    props.postForm({
      data: {
        pin,
        completed: pageIndex === 4 ? true : false,
        variables: data,
      },
      cb: (res) => {
        console.log(res.status);
        if (typeof res.status !== "undefined") {
          setLoading(false);
          Utils.showProblemAlert();
        } else {
          if (pageIndex === 4) {
            localStorage.removeItem("PIN_CODE");
            localStorage.removeItem("SECONDARIES_INDEX");
            localStorage.removeItem("SECONDARIES_VARIABLES");
          }
          setLoading(false);
          callback();
        }
      },
    });
  };

  // const onListen = (val) => {
  //   setListen(val);
  // };

  useEffect(() => {
    const pin = localStorage.getItem("PIN_CODE");
    const type = localStorage.getItem("FORM_TYPE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }
    if (type !== "side-effects") {
      window.location.href = `/form/${type}`;
      return;
    }

    const index = localStorage.getItem("SECONDARIES_INDEX");
    if (index !== null) {
      setPage(parseInt(index, 10));
    }
    const vs = localStorage.getItem("SECONDARIES_VARIABLES");
    if (vs) {
      setVariables(JSON.parse(vs));
    }
  }, []);

  console.log("pageIndex = ", pageIndex);
  if (pageIndex === 5) {
    // end page
    return <EndPage pageIndex={pageIndex} onPageChanged={setPage} />;
  }

  const pageTitles = [
    "Parmi ces effets secondaires oculaires, sélectionnez ceux que vous avez ressentis même si cela ne concerne qu’un seul œil :",
    "Parmi ces effets secondaires systémiques, sélectionnez ceux que vous avez ressentis au cours du dernier mois :",
  ];

  let answer = [];
  variables.forEach((v, i) => {
    if (v.valueBool === true) {
      const index = i - (pageIndex === 3 ? 0 : 17);
      answer.push(index);
    }
  });
  const strings = [variables[16].valueString, variables[34].valueString];

  return (
    <PageContainer>
      <div
        className={`wrapper more ${
          pageIndex === 2 ? "footer-has-commencer" : ""
        }`}
      >
        <div className="main">
          {pageIndex === 0 ? (
            <>
              <HeaderWrapper>
                <span className="full">Déclaration des effets secondaires</span>
              </HeaderWrapper>

              <HeaderWrapper style={{ marginTop: 56.5 }} className="center">
                <img src={welcomeIcon} width="50" height="56" alt="" />
              </HeaderWrapper>

              <div className="content" style={{ marginTop: 30.7 }}>
                <p className="f18">
                  Bienvenue sur votre formulaire de déclaration des effets
                  secondaires, il vous prendra{" "}
                  <span className="yellow">2 minutes</span> à remplir. Ces
                  réponses sont précieuses pour votre ophtalmologue afin de vous
                  aider à mieux gérer la gêne ressentie tout en vous proposant
                  une meilleure prise en charge thérapeutique favorisant une
                  bonne observance. Les réponses serviront également à
                  personnaliser votre suivi Curecall.
                </p>
              </div>
            </>
          ) : pageIndex === 1 ? (
            <>
              <HeaderWrapper>
                <span className="full">Pour commencer</span>
              </HeaderWrapper>
              <img
                src={attentionIcon}
                width="50"
                height="56"
                alt=""
                style={{ marginTop: 31.5, marginBottom: 22.7 }}
              />
              <HeaderWrapper style={{ width: 273 }}>
                <span className="full">
                  Augmentez la luminosité de votre téléphone au maximum
                </span>
              </HeaderWrapper>
              <div className="content">
                <p>
                  <span className="f18 yellow">
                    Astuce : Si vous avez un iPhone ou un téléphone Android,
                    demandez le à SIRI ou à Google
                  </span>
                </p>
              </div>
            </>
          ) : pageIndex === 2 ? (
            <>
              <img src={eyeIcon} width="50" height="56" alt="" />
              <div className="content">
                <p>
                  <span className="f18">
                    Il faut savoir que certains traitements peuvent entraîner
                    des effets indésirables soit au niveau local, comme une
                    sensation de brûlure ou au niveau général, sensation de
                    fatigue ou problèmes respiratoires… C’est pour cela que
                    renseigner les effets indésirables ressentis est un élément
                    essentiel dans le suivi de votre pathologie !
                  </span>
                </p>
                {/* <ListenBtn inContent={true} /> */}
              </div>
            </>
          ) : pageIndex === 3 || pageIndex === 4 ? (
            <>
              <HeaderWrapper style={{ display: "block" }} className="center">
                <PageMark>
                  <span>{pageIndex - 2}</span>
                  <img src={bgImg} width="50" height="56" alt="" />
                </PageMark>
                <span className="block full" style={{ marginTop: 16.2 }}>
                  {pageTitles[pageIndex - 3]}
                </span>
              </HeaderWrapper>

              <div className="content" style={{ marginTop: 10 }}>
                <AnswersList
                  category={8}
                  index={pageIndex - 3}
                  multiple={true}
                  // isListenOpened={isListenOpened}
                  answer={answer}
                  strings={strings}
                  onSelect={setAnswer}
                  onChangeString={onChangeString}
                />
              </div>
            </>
          ) : null}
        </div>
      </div>
      <Footer
        pageIndex={pageIndex}
        onPageChanged={setPage}
        canNext={pageIndex < 3 || answer.length > 0}
        onFinished={onFinished}
        // onListen={onListen}
      />
      <LoadingOverlay loading={loading} />
    </PageContainer>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.FormReducer.form,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getForm: (req) => {
      dispatch(FormActions.getForm(req.pinCode, req.cb));
    },
    postForm: (req) => {
      dispatch(FormActions.postForm(req.data, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SecondariesPage);
