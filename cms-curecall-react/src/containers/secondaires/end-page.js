import React from "react";
import { PageContainer } from "../_styles";
import { HeaderWrapper } from "../../components/_styles";
import Footer from "./footer";

function EndPage(props) {
  const { pageIndex, onPageChanged } = props;

  return (
    <PageContainer>
      <div className="wrapper">
        <HeaderWrapper
          className="full-center"
          style={{ height: "calc(100vh - 130px)" }}
        >
          <span>
            Merci
            <br />
            pour vos réponses qui aident votre ophtalmologue à mieux adapter
            votre traitement.
          </span>
        </HeaderWrapper>
      </div>
      <Footer pageIndex={pageIndex} onPageChanged={onPageChanged} />
    </PageContainer>
  );
}

export default EndPage;
