import React from "react";
import { ThemeButton, ThemeIconButton } from "../../components/_styles";
import nextIcon from "../../assets/svg/Come.svg";
import prevIcon from "../../assets/svg/Back.svg";
import { Flex } from "../_styles";
// import ListenBtn from "../../components/listen-btn";

function Footer(props) {
  const { pageIndex, onPageChanged, canNext, onFinished } = props;

  const goNext = () => {
    if (pageIndex === 3 || pageIndex === 4) {
      onFinished(_next);
    } else {
      _next();
    }
  };

  const _next = () => {
    const index = pageIndex + 1;
    onPageChanged(pageIndex + 1);
    localStorage.setItem("SECONDARIES_INDEX", index.toString());
  };

  const goPrev = () => {
    const index = pageIndex < 1 ? 0 : pageIndex - 1;
    onPageChanged(index);
    localStorage.setItem("SECONDARIES_INDEX", index.toString());
  };

  const lastPL = localStorage.getItem("LAST_PAGE_INDEX");
  const lastPageIndex = lastPL ? parseInt(lastPL, 10) : 0;

  return (
    <div
      className={`footer block ${
        pageIndex > 2 && pageIndex < 6 ? "relative secondaries" : ""
      }`}
    >
      {pageIndex === 2 && (
        <Flex className="h-center">
          <ThemeButton shape="round" onClick={goNext}>
            Commencer
          </ThemeButton>
        </Flex>
      )}

      <Flex
        className={`space-between ${
          pageIndex > 2 && pageIndex <= 6 ? "" : "pd-b-36"
        }`}
        style={{ width: "100%" }}
      >
        {pageIndex > lastPageIndex ? (
          <ThemeIconButton onClick={goPrev}>
            <img src={prevIcon} alt="" />
          </ThemeIconButton>
        ) : (
          <div />
        )}
        {/* {pageIndex !== 2 && (
          <ListenBtn inFooter={true} inContent={true} onListen={onListen} />
        )} */}
        {pageIndex < 5 && canNext && pageIndex !== 2 && (
          <ThemeIconButton onClick={goNext}>
            <img src={nextIcon} alt="" />
          </ThemeIconButton>
        )}

        {pageIndex === 5 && <div style={{ width: 46 }} />}
      </Flex>
    </div>
  );
}

export default Footer;
