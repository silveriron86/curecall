import React, { useEffect, useState } from "react";
import { NotionRenderer } from "react-notion";
import "react-notion/src/styles.css";
import "prismjs/themes/prism-tomorrow.css";
import { useParams } from "react-router";
import { NotionWrapper } from "./_styles";
import jQuery from "jquery";

import robotIcon1 from "../assets/svg/robot1.svg";
import robotIcon2 from "../assets/svg/robot2.svg";
import robotIcon4 from "../assets/svg/robot4.svg";
import logoBlancIcon from "../assets/svg/LogoBlanc.svg";
import LoadingOverlay from "../components/Loading-overlay";

function NotionPage(props) {
  const { pageId } = useParams();
  const [loading, setLoading] = useState(true);
  const [blockMap, setBlockMap] = useState(null);
  useEffect(() => {
    fetch(`https://notion-api.splitbee.io/v1/page/${pageId}`)
      .then((response) => response.json())
      .then((responseJson) => {
        setBlockMap(responseJson);

        const pageLinks = jQuery(".notion-page-link");
        pageLinks.map((index, link) => {
          let title = jQuery(link).find(".notion-page-text").text().trim();
          title = title.replace(/[&\\/\\#,+()$~%.'":*?<>{}]/g, "");
          title = title.replace(/ /g, "-");
          jQuery(link).attr(
            "href",
            title + "-" + jQuery(link).attr("href").substr(1)
          );
          return link;
        });

        const hasH1 = jQuery(".notion-wrapper .notion-h1").length > 0;
        const hasH2 = jQuery(".notion-wrapper .notion-h2").length > 0;
        const hasH3 = jQuery(".notion-wrapper .notion-h3").length > 0;

        if (hasH1) {
          jQuery(".robot1").removeClass("hide");
          jQuery(".logo-icon").removeClass("hide");
        } else if (hasH2) {
          jQuery(".robot2").removeClass("hide");
          // jQuery(".welcome-icon").removeClass("hide");
        } else if (hasH3) {
          jQuery(".robot4").removeClass("hide");
        }

        if (hasH2 && jQuery(".notion-asset-wrapper").length > 0) {
          jQuery(".notion-asset-wrapper").each((idx, obj) => {
            if (jQuery(obj).next().hasClass("notion-list")) {
              jQuery(obj).addClass("two-cols");
              jQuery(obj).next().addClass("two-cols");
            }
          });
          setTimeout(() => {
            jQuery(".notion-asset-wrapper").each((idx, obj) => {
              if (jQuery(obj).next().hasClass("notion-list")) {
                const height = jQuery(obj).next().height();
                jQuery(obj).css("height", height);
              }
            });
          }, 100);
        }

        // if (!hasH1) {
        const links = jQuery(".notion-page-link");
        if (links.length > 0) {
          if (links.length === 1) {
            jQuery(links[0]).addClass("left-arrow");
          } else {
            jQuery(links[links.length - 2]).addClass("left-arrow");
            jQuery(links[links.length - 1]).addClass("right-arrow");
          }
        }

        setTimeout(() => {
          setLoading(false);
        }, 1000);
      });
  }, [pageId]);

  return (
    <NotionWrapper className="notion-wrapper">
      <img src={robotIcon1} alt="" className="robot1 hide" />
      <img src={robotIcon2} alt="" className="robot2 hide" />
      <img src={robotIcon4} alt="" className="robot4 hide" />
      {blockMap && <NotionRenderer blockMap={blockMap} />}
      <div className="bottom">
        <div></div>
        <img src={logoBlancIcon} alt="" className="logo-icon hide" />
        {/* <img src={welcomeIcon} alt="" className="welcome-icon hide" /> */}
        <div></div>
      </div>
      <LoadingOverlay loading={loading} />
    </NotionWrapper>
  );
}

export default NotionPage;
