import styled from "@emotion/styled";
import { InputNumber, Select } from "antd";
import ColorConstants from "../constants/ColorConstants";

export const PageContainer = styled("div")({
  width: "100%",
  minHeight: "100vh",
  background: "linear-gradient(#0800ff 0%, #040080 100%)",
  "&.login-page": {
    background: "white",
    color: ColorConstants.BLUE,
    ".wrapper": {
      ".content": {
        p: {
          fontFamily: "Luciole Bold",
          fontSize: 18,
          lineHeight: "26px",
        },
      },
    },
  },
  "&.pg-white": {
    /* contrast perception */
    background: "white",
    padding: "50px 36px 35px",
    // height: "100vh",
    minHeight: "calc(var(--vh, 1vh) * 100)",
    height: "calc(var(--vh, 1vh) * 100)",
  },
  ".wrapper": {
    position: "relative",
    // maxWidth: 375,
    margin: "0 auto",
    padding: "70px 28px 27.5px",
    // minHeight: "100vh",
    "&.more": {
      padding: "70px 36px 27.5px",
      ".main": {
        ".content": {
          paddingLeft: 0,
        },
      },
    },

    ".page-no": {
      position: "asbolute",
      width: "100%",
      height: "100%",
    },
    ".content": {
      paddingLeft: 8,
      marginTop: 21,
      "&.no-left": {
        paddingLeft: 0,
      },
      p: {
        fontFamily: "Nexa Heavy",
        fontWeight: 900,
        fontSize: 16,
        color: "white",
        margin: 0,
        padding: 0,
      },
      ".img-wrapper": {
        marginTop: 16,
        height: 77,
        display: "flex",
        alignItems: "center",
        backgroundColor: "white",
        "&.auto": {
          // height: 210,
          height: "auto",
          backgroundColor: "transparent",
          marginTop: 28,
        },
        img: {
          margin: "0 auto",
        },
      },
      ".answers": {
        paddingTop: 8.4,
        minHeight: 227,
        "&.flex": {
          minHeight: "auto",
          ".text": {
            width: "auto",
          },
        },
        "&.large": {
          display: "flex",
          flexWrap: "wrap",
        },
      },
      "&.p0": {
        padding: 0,
      },
    },
    ".blue-italic": {
      fontFamily: "Nexa BoldItalic",
      fontWeight: "bold",
      fontStyle: "italic",
      fontSize: 16,
      lineHeight: "22px",
      textAlign: "left",
      color: "#00fffd",
      marginLeft: 6.6,
    },
  },
  ".footer": {
    // padding: "0 36px",
    width: "100%",
    // maxWidth: 375,
    // left: "calc(50% - 187.5px)",
    position: "fixed",
    bottom: 25.5,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    "&.block": {
      display: "block",
    },
    "&.relative": {
      position: "relative",
      width: "100% !important",
      marginTop: 62,
      bottom: "auto",
      "&.secondaries": {
        marginTop: 26,
        paddingBottom: 40.5,
      },
    },
    "&.right": {
      justifyContent: "flex-end",
    },
    ".percentage": {
      position: "relative",
      width: "100%",
      height: 3.5,
      backgroundColor: "#00FFFD30",
      borderRadius: 8,
      marginBottom: 7,
      ".filled": {
        position: "absolute",
        height: 3.5,
        borderRadius: 8,
        backgroundColor: "#00FFFD",
      },
    },
  },

  ".ant-slider": {
    margin: 0,
    padding: 0,
    ".ant-slider-rail": {
      marginLeft: -10,
      marginRight: -10,
      width: "calc(100% + 20px)",
      height: 15,
      borderRadius: 7.5,
      background:
        "linear-gradient(to right, #fff 0%, #fff 8.87%, #fdff00 50.25%, #f00 100%)",
    },
    ".ant-slider-track": {
      backgroundColor: "transparent",
    },
    ".ant-slider-step": {
      height: 30,
    },
    ".ant-slider-handle": {
      width: 0,
      height: 0,
      borderTop: "none",
      borderLeft: "15px solid transparent !important",
      borderRight: "15px solid transparent !important",
      borderBottom: "25px solid white !important",
      backgroundColor: "transparent",
      borderRadius: 0,
      marginTop: 19,
    },
    "&.yellow-slider": {
      marginTop: 10,
      marginLeft: -10,
      marginRight: -10,
      ".ant-slider-rail": {
        background:
          "linear-gradient(to right, #fff 0%, #fdff00 100%) !important",
        marginLeft: 0,
        marginRight: 0,
        width: "100%",
      },
      ".ant-slider-handle": {
        marginTop: 24,
      },
    },
  },
});

export const FlexCenter = styled("div")({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
  minHeight: "100vh",
  "&.absolute": {
    position: "absolute",
    left: 0,
    top: 0,
  },
  "&.scanned": {
    position: "relative",
    width: 224,
    height: 222,
    minHeight: 222,
  },
});

export const Flex = styled("div")({
  display: "flex",
  "&.h-center": {
    justifyContent: "center",
  },
  "&.v-center": {
    alignItems: "center",
  },
  "&.space-between": {
    justifyContent: "space-between",
    alignItems: "center",
  },
  "&.full": {
    width: "100%",
  },
});

export const PageMark = styled("div")({
  width: 50,
  height: 56,
  position: "relative",
  span: {
    position: "absolute",
    width: "100%",
    height: "100%",
    fontFamily: "Nexa Heavy",
    fontWeight: 900,
    fontSize: 33,
    textAlign: "center",
    color: "#fff",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 5,
  },
});

export const Answer = styled("div")({
  marginTop: 11.6,
  display: "flex",
  ".ant-btn": {
    width: 41,
    height: 41,
    border: "2px solid white",
    borderRadius: 10,
    boxShadow: "none",
    outline: "none",
    backgroundColor: "transparent",
    padding: 0,
    span: {
      fontFamily: "Nexa Heavy",
      fontSize: 25,
      lineHeight: "30px",
      color: "white",
    },
    "&.selected": {
      backgroundColor: "white !important",
      span: {
        color: "#0800FF",
      },
    },
  },
  ".text": {
    marginLeft: 11,
    marginTop: -3,
    width: 250,
    fontFamily: "Nexa Heavy",
    fontSize: 16,
    lineHeight: "25px",
    color: "white",
  },
  "&.one": {
    alignItems: "center",
    ".text": {
      marginTop: 0,
    },
  },
  "&.large": {
    marginRight: 21,
    marginBottom: 8.4,
    "&:nth-of-type(2)": {
      marginRight: 100,
    },
    ".ant-btn": {
      width: 80,
      height: 80,
      span: {
        fontSize: 45,
        lineHeight: "58px",
      },
    },
    ".text": {
      display: "none",
    },
  },
  "&.obsolete": {
    display: "none",
  },
});

export const CenterCropped = styled("div")({
  width: 204,
  height: 202,
  backgroundPosition: "center center",
  backgroundRepeat: "no-repeat",
});

export const NotionWrapper = styled("div")({
  // width: 375px;
  // background: "linear-gradient(to top, #3cc 0%, #0800ff 100%)",
  background: "#0800ff",
  minHeight: "100vh",
  color: "white",
  padding: "60px 37px",
  overflowX: "hidden",
  ".robot1": {
    marginLeft: -90,
    marginTop: -40,
  },
  ".robot2": {
    marginTop: -25,
  },
  ".robot4": {
    marginTop: 65,
    marginRight: -70,
    float: "right",
  },
  ".bubble2": {
    float: "right",
  },
  ".bubble3": {
    top: 0,
  },
  ".bottom": {
    // position: "fixed",
    // bottom: 42,
    marginTop: 42,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    ".logo-icon": {
      width: 82,
    },
    ".welcome-icon": {
      width: 50,
    },
  },
  ".two-cols": {
    float: "left",
    width: "50% !important",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    margin: 0,
    img: {
      width: "auto",
      height: "auto",
      "&.notion-image-inset": {
        position: "relative",
      },
    },
  },
  ".notion-asset-wrapper": {
    "&.two-cols": {
      clear: "both",
      "&>div": {
        paddingBottom: "0 !important",
      },
    },
  },
  ".notion-list": {
    "&:after": {
      content: "' '",
      display: "block",
      clear: "both",
    },
    "&.two-cols": {
      li: {
        background: "none",
        paddingLeft: 0,
        fontFamily: "Nexa Book",
        fontWeight: "normal",
        fontSize: 16,
        lineHeight: "18px",
      },
    },
  },
  // ".notion-asset-wrapper": {
  //   clear: "both",
  //   alignItems: "center",
  //   justifyContent: "center",
  // },
  // ".notion-asset-wrapper .notion-image-inset": {
  //   width: "auto",
  //   height: "auto",
  // },
  // ".notion-asset-wrapper, .notion-list-disc": {
  //   float: "left",
  //   width: "50% !important",
  // },
  // ".notion-list-disc": {
  //   listStyle: "none",
  //   paddingInlineStart: 0,
  // },
});

export const DottedBorder = styled("div")({
  position: "relative",
  width: "100%",
  maxWidth: 302,
  minHeight: 260,
  margin: "50px auto 13px",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  padding: 20,
  border: "1px solid white",
  borderRadius: 24,
  img: {
    "&.border": {
      position: "absolute",
      left: 0,
      top: 0,
    },
  },
  p: {
    // width: 244,
    textAlign: "center",
    // paddingTop: "12px !important",
    // paddingBottom: "37px !important",
  },
  ".f14": {
    fontFamily: "Nexa Light !important",
  },
});

export const ThemeHowManyInput = styled(InputNumber)({
  width: 75,
  height: 50,
  borderRadius: 5,
  outline: "none",
  border: "none",
  ".ant-input-number-input": {
    height: 50,
    fontFamily: "Nexa Heavy",
    fontWeight: 900,
    fontSize: 25,
    lineHeight: "25px",
    color: "#0800ff",
    textAlign: "center",
    "&::placeholder": {
      color: "#0800FF30",
    },
  },
});

export const TakeTimeSelect = styled(Select)({
  width: 75,
  height: 50,
  ".ant-select-selector": {
    height: "100% !important",
    padding: "0 !important",
    borderRadius: "5px !important",
  },
  ".ant-select-arrow": {
    display: "none !important",
  },
  ".ant-select-selection-item, .ant-select-selection-placeholder": {
    height: 50,
    fontFamily: "Nexa Heavy",
    fontWeight: 900,
    fontSize: 25,
    lineHeight: "25px",
    color: "#0800ff",
    textAlign: "center",
    paddingRight: "0 !important",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  ".ant-select-selection-placeholder": {
    color: "#0800FF30",
  },
});
