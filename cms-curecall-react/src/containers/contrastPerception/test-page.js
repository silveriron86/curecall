import React from "react";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonNext,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import {
  VisualAcuityWrapper,
  Title,
  LetterButton,
  SlideStyle,
} from "../visualAcuity/_styles";
import { Flex, FlexCenter, PageContainer } from "../_styles";
import jQuery from "jquery";
import ContrastPerceptionConstants from "../../constants/ContrastPerceptionConstants";

const opacities = [1, 0.4, 0.25, 0.15, 0.1, 0.08, 0.04, 0.02];
const values = [80, 40, 25, 15, 10, 8, 4, 2];

export default function CPTestPage(props) {
  const CASES = 8;

  const goNextSlide = () => {
    jQuery("#next-btn").trigger("click");
  };

  const onSelect = (val, index) => {
    const { pageIndex } = props;
    const variableNames = ["contrastPerceptionR", "contrastPerceptionL"];
    const variableName = variableNames[(pageIndex - 5) / 2];

    if (val === true) {
      localStorage.setItem(
        `${variableName}_LAST_VALUE`,
        values[index].toString()
      );
    }

    const completed = pageIndex === 7 && index === 7 ? true : false;
    if (index < 7) {
      props.onFinished(null, completed, goNextSlide);
    } else {
      const lastValue = localStorage.getItem(`${variableName}_LAST_VALUE`);
      const value = [
        {
          variableName,
          valueInt: lastValue ? lastValue : 0,
        },
      ];

      props.onFinished(value, completed, props.goNext);
    }
  };

  const { pageIndex } = props;
  return (
    <CarouselProvider
      naturalSlideWidth={"100%"}
      naturalSlideHeight={"100%"}
      totalSlides={CASES}
      orientation="vertical"
      lockOnWindowScroll={true}
    >
      <Slider>
        {new Array(CASES).fill(0).map((opt, i) => {
          const vaRow =
            ContrastPerceptionConstants[parseInt(pageIndex / 2, 10) - 2][i];
          const values = [...vaRow.values];
          return (
            <Slide key={`slide-${i}`} index={i} style={SlideStyle}>
              <PageContainer className="pg-white">
                <VisualAcuityWrapper className="height-auto">
                  <div className="main between">
                    <div className="main">
                      <Title style={{ width: "100%" }} className="text-left">
                        ŒIL{" "}
                        {parseInt(pageIndex / 2, 10) === 2 ? "DROIT" : "GAUCHE"}
                      </Title>
                      <div className="main">
                        <FlexCenter style={{ flex: 1, minHeight: "auto" }}>
                          <div
                            className="luciole79 text-center full-width"
                            style={{
                              opacity: opacities[i],
                              letterSpacing: pageIndex === 7 && i <= 1 ? 0 : 6,
                              marginTop: 50,
                            }}
                          >
                            {vaRow.label}
                          </div>
                        </FlexCenter>
                      </div>
                    </div>
                    <div
                      className="main"
                      style={{
                        justifyContent: "flex-end",
                      }}
                    >
                      <div className="luciole18 text-center">
                        Sélectionnez ce que vous voyez :
                      </div>
                      <Flex className="full" style={{ marginTop: 8.5 }}>
                        <LetterButton
                          className="small four"
                          onClick={() => onSelect(values[0] === vaRow.label, i)}
                        >
                          {values[0]}
                        </LetterButton>
                        <div className="w10"></div>
                        <LetterButton
                          className="small four"
                          onClick={() => onSelect(values[1] === vaRow.label, i)}
                        >
                          {values[1]}
                        </LetterButton>
                      </Flex>
                      <div className="h10"></div>
                      <Flex className="full">
                        <LetterButton
                          className="small four"
                          onClick={() => onSelect(values[2] === vaRow.label, i)}
                        >
                          {values[2]}
                        </LetterButton>
                        <div className="w10"></div>
                        <LetterButton
                          className="small four"
                          onClick={() => onSelect(values[3] === vaRow.label, i)}
                        >
                          {values[3]}
                        </LetterButton>
                      </Flex>
                      <div className="h10"></div>
                      <LetterButton
                        className="small"
                        onClick={() => onSelect(false, i)}
                      >
                        Je ne sais pas
                      </LetterButton>
                    </div>
                  </div>
                </VisualAcuityWrapper>
              </PageContainer>
            </Slide>
          );
        })}
      </Slider>
      <ButtonNext id="next-btn">Next</ButtonNext>
    </CarouselProvider>
  );
}
