import React, { useState } from "react";
import { ThemeButton } from "../../components/_styles";
import {
  VisualAcuityWrapper,
  Title,
  RectButton,
} from "../visualAcuity/_styles";
import hartIcon from "../../assets/svg/BlueHart.svg";
import glassesIcon from "../../assets/svg/BlueGlasses.svg";
import mobileEyeIcon from "../../assets/svg/BlueMobileEye.svg";
import rightArrowIcon from "../../assets/svg/BlueRight.svg";
import seeingPersonIcon from "../../assets/svg/BlueSeeingPerson.svg";
import handEyeIcon from "../../assets/svg/BlueHandEye.svg";
import eyeHandIcon from "../../assets/svg/BlueEyeHand.svg";
import eyesIcon from "../../assets/svg/BlueEyes.svg";
// import phizumImg from "../../assets/svg/phizum.svg";
import { ReactComponent as ZSvg } from "../../assets/svg/ZJHL.svg";
import { ReactComponent as PhizumSvg } from "../../assets/svg/CP.svg";

import { Flex } from "../_styles";
import { Button } from "antd";

export default function CPStartPage(props) {
  const [glassType, setGlassType] = useState(1);
  const buttonLabels = [
    "Démarrer mon test de l’œil droit ouvert",
    "Continuer mon test de l’œil gauche !",
  ];

  const { pageIndex, goNext, goPrev } = props;
  return (
    <VisualAcuityWrapper className="height-auto">
      {pageIndex === 0 ? (
        <div className="main">
          <img src={hartIcon} alt="" />
          <p style={{ width: 243 }} className="bold">
            Votre médecin souhaite que vous testiez votre vision
          </p>
        </div>
      ) : pageIndex === 1 ? (
        <div className="main between" style={{ paddingBottom: 20 }}>
          <Title>Instructions pour le test</Title>
          <div className="main" style={{ marginBottom: 21 }}>
            <div className="luciole18 text-center">
              Regardez la série de lettres
            </div>
            <div className="bordered">
              <ZSvg />
            </div>
            <div
              className="luciole18 text-center"
              style={{ margin: "18px -30px 0", lineHeight: "22px" }}
            >
              Puis sélectionnez ce que vous voyez
            </div>
            <div className="bordered">
              <PhizumSvg />
            </div>
          </div>
          <Button type="text" className="more" onClick={() => goNext(true)}>
            <img src={rightArrowIcon} alt="" />
            <span>En savoir plus</span>
          </Button>
        </div>
      ) : pageIndex === 2 ? (
        <div className="main between" style={{ paddingBottom: 20 }}>
          <Title>Test de la sensibilité au contraste</Title>
          <div className="main">
            <img src={seeingPersonIcon} alt="" />
            <div className="luciole18">
              Le test de la sensibilité au contraste mesure votre capacité à
              faire la distinction entre des contrastes de plus en plus fins.
            </div>
          </div>
        </div>
      ) : pageIndex === 3 ? (
        <div className="main between">
          <Title>Instructions pour le test</Title>
          <div className="main">
            {glassType === 1 ? (
              <>
                <img src={glassesIcon} alt="" />
                <p className="small black">
                  Si vous avez des lentilles de contact ou des lunettes, vous
                  devez{" "}
                  <span className="bold underline">
                    les porter pendant le test
                  </span>
                </p>
              </>
            ) : (
              <>
                <img src={mobileEyeIcon} alt="" />
                <p className="small black">
                  Tenez votre téléphone à une distance de lecture confortable et
                  constante durant tout le test
                </p>
              </>
            )}
            <Flex className="h-center" style={{ marginTop: 20 }}>
              <RectButton
                className={glassType === 1 ? "selected" : ""}
                onClick={() => setGlassType(1)}
              >
                {" "}
              </RectButton>
              <div style={{ width: 10 }}></div>
              <RectButton
                className={glassType === 2 ? "selected" : ""}
                onClick={() => setGlassType(2)}
              >
                {" "}
              </RectButton>
            </Flex>
          </div>
        </div>
      ) : pageIndex >= 4 ? (
        <div className="main between" style={{ paddingBottom: 20 }}>
          <Title>{pageIndex === 6 ? "ŒIL GAUCHE" : "ŒIL DROIT"}</Title>
          <div className="main">
            <img
              src={[handEyeIcon, eyeHandIcon, eyesIcon][pageIndex / 2 - 2]}
              alt=""
            />
            <div className="luciole18 text-center" style={{ marginTop: 28 }}>
              {pageIndex === 6 ? (
                <span>
                  Maintenant cachez
                  <br />
                  votre œil droit
                </span>
              ) : (
                "Cachez votre œil gauche"
              )}
            </div>
          </div>
        </div>
      ) : null}
      {pageIndex < 4 ? (
        <ThemeButton
          shape="round"
          className="blue"
          onClick={() => {
            if (pageIndex === 2) {
              goPrev();
            } else if (pageIndex === 3 && glassType === 1) {
              setGlassType(2);
            } else {
              goNext(false);
            }
          }}
        >
          {(pageIndex === 2 ? "J’ai compris" : "Continuer").toUpperCase()}
        </ThemeButton>
      ) : (
        <ThemeButton
          shape="round"
          className="blue two-lines"
          onClick={() => goNext(false)}
        >
          {buttonLabels[pageIndex / 2 - 2]}
        </ThemeButton>
      )}
    </VisualAcuityWrapper>
  );
}
