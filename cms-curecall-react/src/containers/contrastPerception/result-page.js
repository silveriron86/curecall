import React from "react";
import { VisualAcuityWrapper } from "../visualAcuity/_styles";
import { ReactComponent as LogoIcon } from "../../assets/svg/LogoCurecall.svg";

export default function CPResultPage(props) {
  return (
    <VisualAcuityWrapper className="height-auto">
      <div className="main">
        <LogoIcon />
        {/* <Title className="text-center" style={{ marginTop: 36 }}>
          Merci à vous !<br />A bientôt !
        </Title> */}
        <div
          className="luciole18 text-center"
          style={{ marginTop: 36, padding: "0 15px" }}
        >
          Les résultats de votre test ont été envoyés à votre médecin. Vous
          pouvez effectuer n'importe quel test supplémentaire, une fois par
          semaine en envoyant {props.from}.
        </div>
      </div>
    </VisualAcuityWrapper>
  );
}
