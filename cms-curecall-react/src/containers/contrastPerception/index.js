import React, { useState, useEffect } from "react";
import LoadingOverlay from "../../components/Loading-overlay";
import { FormActions } from "../../actions";
import { connect } from "react-redux";
import CPTestPage from "./test-page";
import { PageContainer } from "../_styles";
import CPStartPage from "./start-page";
import CPResultPage from "./result-page";
import Utils from "../../utils";

function ContrastPerceptionPage(props) {
  const [loading, setLoading] = useState(false);
  const [pageIndex, setPage] = useState(0);

  useEffect(() => {
    const pin = localStorage.getItem("PIN_CODE");
    const type = localStorage.getItem("FORM_TYPE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }
    if (type !== "contrast-perception") {
      window.location.href = `/form/${type}`;
      return;
    }

    const index = localStorage.getItem("CONTRAST_PERCEPTION_INDEX");
    if (index !== null) {
      setPage(parseInt(index, 10));
    }
    Utils.setMobileViewPort();
  }, []);

  const _setPage = (index) => {
    setPage(index);
    localStorage.setItem("CONTRAST_PERCEPTION_INDEX", index.toString());
  };

  const _next = (isSkip) => {
    let index = pageIndex + 1;
    if (index === 2) {
      if (!isSkip) {
        index++;
      }
    }
    _setPage(index);
  };

  const goNext = (isSkip) => {
    if (pageIndex === 5 || pageIndex === 7) {
      setLoading(true);
      setTimeout(() => {
        setLoading(false);
        _next(isSkip);
      }, 2000);
    } else {
      _next(isSkip);
    }
  };

  const onFinished = (data, completed, callback) => {
    const pin = localStorage.getItem("PIN_CODE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }

    if (data === null) {
      callback();
      return;
    }

    setLoading(true);
    props.postForm({
      data: {
        pin,
        completed,
        variables: data,
      },
      cb: (res) => {
        if (typeof res.status !== "undefined") {
          setLoading(false);
          Utils.showProblemAlert();
        } else {
          if (completed) {
            localStorage.removeItem("PIN_CODE");
            localStorage.removeItem("CONTRAST_PERCEPTION_INDEX");
          }
          setLoading(false);
          callback();
        }
      },
    });
  };

  const goPrev = () => {
    let index = pageIndex - 1;
    if (index < 0) {
      return;
    }

    if (index === 2) {
      index--;
    }
    _setPage(index);
  };

  if (pageIndex === 5 || pageIndex === 7) {
    return (
      <>
        <CPTestPage
          {...props}
          pageIndex={pageIndex}
          goNext={goNext}
          onFinished={onFinished}
        />
        <LoadingOverlay loading={loading} />
      </>
    );
  }

  if (pageIndex === 8) {
    return (
      <PageContainer className="pg-white" style={{ paddingBottom: 60 }}>
        <CPResultPage {...props} pageIndex={pageIndex} from="Contraste" />
      </PageContainer>
    );
  }

  return (
    <PageContainer className="pg-white">
      <CPStartPage
        {...props}
        pageIndex={pageIndex}
        goPrev={goPrev}
        goNext={goNext}
      />
    </PageContainer>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.FormReducer.form,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getForm: (req) => {
      dispatch(FormActions.getForm(req.pinCode, req.cb));
    },
    postForm: (req) => {
      dispatch(FormActions.postForm(req.data, req.cb));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContrastPerceptionPage);
