import React from "react";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonNext,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import {
  VisualAcuityWrapper,
  Title,
  SlideStyle,
} from "../visualAcuity/_styles";
import { Flex, PageContainer } from "../_styles";
import jQuery from "jquery";

import gridIcon from "../../assets/svg/Grid.svg";
import { ThemeOutlineButton } from "../../components/_styles";
import FadeIn from "../../components/fade";

const questions = [
  "Les lignes sont-elles droites et régulières ?",
  "Tous les carrés ont-ils la même taille ?",
  "Y a-t-il des zones déformées ou obscurcies ?",
];

export default function AGTestPage(props) {
  const CASES = 3;
  const goNextSlide = () => {
    jQuery("#next-btn").trigger("click");
  };

  const onSelect = (val, index) => {
    const { pageIndex } = props;
    const variableNames = [
      "amslerGridStraightLinesR",
      "amslerGridSameSizeSquareR",
      "amslerGridDistortedObscuredR",
      "amslerGridStraightLinesL",
      "amslerGridSameSizeSquareL",
      "amslerGridDistortedObscuredL",
    ];
    const variableName = variableNames[(pageIndex === 3 ? 0 : 1) * 3 + index];
    let data = {
      variableName,
      valueBool: val,
    };

    const completed = pageIndex === 5 && index === 2 ? true : false;
    props.onFinished(
      [data],
      completed,
      index === 2 ? props.goNext : goNextSlide
    );
  };

  const { pageIndex } = props;

  return (
    <CarouselProvider
      naturalSlideWidth={"100%"}
      naturalSlideHeight={"100%"}
      totalSlides={CASES}
      orientation="vertical"
      lockOnWindowScroll={true}
    >
      <Slider>
        {new Array(CASES).fill(0).map((opt, i) => {
          const content = (
            <div>
              <Title className="black text-center">{questions[i]}</Title>
              <Flex
                className="space-between mt33"
                style={{ width: "100%", marginTop: 33 }}
              >
                <div style={{ flex: 1 }}>
                  <ThemeOutlineButton onClick={() => onSelect(true, i)}>
                    OUI
                  </ThemeOutlineButton>
                </div>
                <div style={{ width: 10 }}></div>
                <div style={{ flex: 1 }}>
                  <ThemeOutlineButton onClick={() => onSelect(false, i)}>
                    NON
                  </ThemeOutlineButton>
                </div>
              </Flex>
            </div>
          );

          return (
            <Slide key={`slide-${i}`} index={i} style={SlideStyle}>
              <PageContainer className="pg-white">
                <VisualAcuityWrapper className="height-auto">
                  <div className="main between">
                    <div className="main">
                      <Title className="text-left" style={{ width: "100%" }}>
                        {(pageIndex === 3
                          ? "ŒIL DROIT"
                          : "œIL gauche"
                        ).toUpperCase()}
                      </Title>
                      <div className="main">
                        <img
                          src={gridIcon}
                          alt=""
                          style={{ maxWidth: "100%" }}
                        />
                      </div>
                    </div>
                    {i === 0 ? (
                      <FadeIn delay={4000} duration={1000}>
                        {content}
                      </FadeIn>
                    ) : (
                      content
                    )}
                  </div>
                </VisualAcuityWrapper>
              </PageContainer>
            </Slide>
          );
        })}
      </Slider>
      <ButtonNext id="next-btn">Next</ButtonNext>
    </CarouselProvider>
  );
}
