import React, { useState } from "react";
import { ThemeButton } from "../../components/_styles";
import {
  VisualAcuityWrapper,
  Title,
  RectButton,
} from "../visualAcuity/_styles";
import { Flex } from "../_styles";

import hartIcon from "../../assets/svg/BlueHart.svg";
import glassesIcon from "../../assets/svg/BlueGlasses.svg";
import closeLeftEyeIcon from "../../assets/svg/BlueCloseLeftEye.svg";
import closeRightEyeIcon from "../../assets/svg/BlueCloseRightEye.svg";

export default function AGStartPage(props) {
  const [glassType, setGlassType] = useState(0);
  const buttonLabels = [
    "Commencer le test avec l’oeil gauche fermé",
    "Continuer le test avec l’oeil droit fermé",
  ];

  const { pageIndex, goNext } = props;
  console.log("glassType = ", glassType);
  return (
    <VisualAcuityWrapper className="height-auto">
      {pageIndex === 0 ? (
        <div className="main">
          <img src={hartIcon} alt="" />
          <p style={{ width: 243 }} className="bold">
            Votre médecin souhaite que vous testiez votre vision
          </p>
        </div>
      ) : pageIndex === 1 ? (
        <div className="main between">
          <Title>Instructions pour le test</Title>
          <div className="main">
            {glassType === 0 ? (
              <div className="luciole18 text-center">
                La grille d’Amsler est un quadrillage composé de lignes droites.
                Elle permet de détecter des anomalies dans votre vision centrale
              </div>
            ) : (
              <>
                <img src={glassesIcon} alt="" />
                <p className="small black">
                  Si vous avez des lentilles de contact ou des lunettes, vous
                  devez{" "}
                  <span className="bold underline">
                    les porter pendant le test
                  </span>
                </p>
              </>
            )}
            <Flex className="h-center" style={{ marginTop: 20 }}>
              <RectButton
                className={glassType === 0 ? "selected" : ""}
                onClick={() => setGlassType(0)}
              >
                {" "}
              </RectButton>
              <div style={{ width: 10 }}></div>
              <RectButton
                className={glassType === 1 ? "selected" : ""}
                onClick={() => setGlassType(1)}
              >
                {" "}
              </RectButton>
              {/* <div style={{ width: 10 }}></div>
              <RectButton
                className={glassType === 2 ? "selected" : ""}
                onClick={() => setGlassType(2)}
              >
                {" "}
              </RectButton> */}
            </Flex>
          </div>
        </div>
      ) : pageIndex >= 2 ? (
        <div className="main between" style={{ paddingBottom: 20 }}>
          <Title>
            {pageIndex === 2
              ? "Instructions pour le test"
              : "Instructions test de votre vision"}
          </Title>
          <div className="main">
            <img
              src={[closeLeftEyeIcon, closeRightEyeIcon][pageIndex / 2 - 1]}
              alt=""
            />
            <p className="small black">
              Une grille va s'afficher pendant 5 secondes,{" "}
              <span className="bold underline">
                {pageIndex === 2
                  ? "fermez l'oeil gauche et concentrez-vous sur le point noir au centre."
                  : "fermez l'oeil droit et concentrez-vous sur le point noir au centre."}
              </span>{" "}
              Rapprochez doucement votre téléphone à 15 cm tout en regardant le
              point central.
            </p>
          </div>
        </div>
      ) : null}
      {pageIndex < 2 ? (
        <ThemeButton
          shape="round"
          className="blue"
          onClick={() => {
            if (pageIndex === 1 && glassType === 0) {
              setGlassType(1);
            } else {
              goNext();
            }
          }}
        >
          {"Continuer".toUpperCase()}
        </ThemeButton>
      ) : (
        <ThemeButton
          shape="round"
          className="blue two-lines"
          onClick={() => goNext(false)}
          style={{ height: "auto" }}
        >
          {buttonLabels[pageIndex / 2 - 1]}
        </ThemeButton>
      )}
    </VisualAcuityWrapper>
  );
}
