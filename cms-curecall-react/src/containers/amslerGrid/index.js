import React, { useState, useEffect } from "react";
import LoadingOverlay from "../../components/Loading-overlay";
import { FormActions } from "../../actions";
import { connect } from "react-redux";
import AGTestPage from "./test-page";
import { PageContainer } from "../_styles";
import AGStartPage from "./start-page";
import Utils from "../../utils";
import CPResultPage from "../contrastPerception/result-page";

function AmslerGridPage(props) {
  const [loading, setLoading] = useState(false);
  const [pageIndex, setPage] = useState(0);

  useEffect(() => {
    const pin = localStorage.getItem("PIN_CODE");
    const type = localStorage.getItem("FORM_TYPE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }
    if (type !== "amsler-grid") {
      window.location.href = `/form/${type}`;
      return;
    }

    const index = localStorage.getItem("AMSLER_GRID_INDEX");
    if (index !== null) {
      setPage(parseInt(index, 10));
    }
    Utils.setMobileViewPort();
  }, []);

  const _setPage = (index) => {
    setPage(index);
    localStorage.setItem("AMSLER_GRID_INDEX", index.toString());
  };

  const _next = () => {
    let index = pageIndex + 1;
    _setPage(index);
  };

  const goNext = () => {
    if (pageIndex === 3 || pageIndex === 5) {
      setLoading(true);
      setTimeout(() => {
        setLoading(false);
        _next();
      }, 2000);
    } else {
      _next();
    }
  };

  const onFinished = (data, completed, callback) => {
    const pin = localStorage.getItem("PIN_CODE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }

    setLoading(true);
    props.postForm({
      data: {
        pin,
        completed,
        variables: data,
      },
      cb: (res) => {
        if (typeof res.status !== "undefined") {
          setLoading(false);
          Utils.showProblemAlert();
        } else {
          if (completed) {
            localStorage.removeItem("PIN_CODE");
            localStorage.removeItem("AMSLER_GRID_INDEX");
          }
          setLoading(false);
          callback();
        }
      },
    });
  };

  const goPrev = () => {
    let index = pageIndex - 1;
    if (index < 0) {
      return;
    }
    _setPage(index);
  };

  if (pageIndex === 3 || pageIndex === 5) {
    return (
      <>
        <AGTestPage
          {...props}
          pageIndex={pageIndex}
          goNext={goNext}
          onFinished={onFinished}
        />
        <LoadingOverlay loading={loading} />
      </>
    );
  }

  if (pageIndex === 6) {
    return (
      <PageContainer className="pg-white" style={{ paddingBottom: 60 }}>
        <CPResultPage {...props} pageIndex={pageIndex} from="Amsler" />
      </PageContainer>
    );
  }

  return (
    <PageContainer className="pg-white">
      <AGStartPage
        {...props}
        pageIndex={pageIndex}
        goPrev={goPrev}
        goNext={goNext}
      />
    </PageContainer>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.FormReducer.form,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getForm: (req) => {
      dispatch(FormActions.getForm(req.pinCode, req.cb));
    },
    postForm: (req) => {
      dispatch(FormActions.postForm(req.data, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AmslerGridPage);
