import React from "react";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonNext,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import {
  VisualAcuityWrapper,
  Title,
  LetterButton,
  SlideStyle,
} from "./_styles";
import { Flex, FlexCenter, PageContainer } from "../_styles";
import jQuery from "jquery";
import VisualAcuityConstants from "../../constants/VisualAcuityConstants";

const fontSizes = [79, 39, 20, 12, 8, 4, 3];
const values = [500, 250, 125, 80, 50, 30, 20];
const results = ["1/25", "1/12", "1,6/10", "2,5/10", "4/10", "6,3/10", "10/10"];

export default function VATestPage(props) {
  const CASES = 7;

  const goNextSlide = () => {
    jQuery("#next-btn").trigger("click");
  };

  const onSelect = (val, index) => {
    const { pageIndex } = props;
    const variableNames = ["visualAcuityR", "visualAcuityL", "visualAcuity"];
    const variableName = variableNames[(pageIndex - 5) / 2];
    const valueString = `20/${values[index]}`;

    if (val === true) {
      localStorage.setItem(`${variableName}_LAST_VALUE`, valueString);
      localStorage.setItem(`${variableName}_RESULT`, results[index]);
    }

    const completed = pageIndex === 9 && index === 6 ? true : false;
    if (index < 6) {
      props.onFinished(null, completed, goNextSlide);
    } else {
      const lastValue = localStorage.getItem(`${variableName}_LAST_VALUE`);
      const value = [
        {
          variableName,
          valueString: lastValue ? lastValue : "0",
        },
      ];
      props.onFinished(value, completed, props.goNext);
    }
  };

  const { pageIndex } = props;

  return (
    <CarouselProvider
      naturalSlideWidth={"100%"}
      naturalSlideHeight={"100%"}
      totalSlides={CASES}
      orientation="vertical"
      lockOnWindowScroll={true}
    >
      <Slider>
        {new Array(CASES).fill(0).map((opt, i) => {
          const vaRow =
            VisualAcuityConstants[parseInt(pageIndex / 2, 10) - 2][i];
          const values = [...vaRow.values];
          return (
            <Slide key={`slide-${i}`} index={i} style={SlideStyle}>
              <PageContainer className="pg-white">
                <VisualAcuityWrapper className="height-auto">
                  <div className="main between">
                    <Title>
                      {
                        ["ŒIL DROIT", "ŒIL GAUCHE", "LES DEUX YEUX"][
                          (pageIndex - 5) / 2
                        ]
                      }
                    </Title>
                    <div className="main between">
                      <FlexCenter style={{ flex: 1, minHeight: "auto" }}>
                        <div
                          className="luciole79 text-center full-width"
                          style={{ fontSize: `${fontSizes[i]}pt` }}
                        >
                          {vaRow.label}
                        </div>
                      </FlexCenter>
                      <div className="luciole18 text-center">
                        Sélectionnez la lettre que vous voyez ci-dessus :
                      </div>
                    </div>
                    <div
                      className="main"
                      style={{
                        justifyContent: "flex-end",
                      }}
                    >
                      <Flex className="full">
                        <LetterButton
                          onClick={() => onSelect(values[0] === vaRow.label, i)}
                        >
                          {values[0]}
                        </LetterButton>
                        <div className="w10"></div>
                        <LetterButton
                          onClick={() => onSelect(values[1] === vaRow.label, i)}
                        >
                          {values[1]}
                        </LetterButton>
                        <div className="w10"></div>
                        <LetterButton
                          onClick={() => onSelect(values[2] === vaRow.label, i)}
                        >
                          {values[2]}
                        </LetterButton>
                      </Flex>
                      <div className="h10"></div>
                      <Flex className="full">
                        <LetterButton
                          onClick={() => onSelect(values[3] === vaRow.label, i)}
                        >
                          {values[3]}
                        </LetterButton>
                        <div className="w10"></div>
                        <LetterButton
                          onClick={() => onSelect(values[4] === vaRow.label, i)}
                        >
                          {values[4]}
                        </LetterButton>
                        <div className="w10"></div>
                        <LetterButton
                          onClick={() => onSelect(values[5] === vaRow.label, i)}
                        >
                          {values[5]}
                        </LetterButton>
                      </Flex>
                      <div className="h10"></div>
                      <LetterButton
                        className="small"
                        onClick={() => onSelect(false, i)}
                      >
                        Je ne sais pas
                      </LetterButton>
                    </div>
                  </div>
                </VisualAcuityWrapper>
              </PageContainer>
            </Slide>
          );
        })}
      </Slider>
      <ButtonNext id="next-btn">Next</ButtonNext>
    </CarouselProvider>
  );
}
