import React, { useState } from "react";
import { ThemeButton } from "../../components/_styles";
import { VisualAcuityWrapper, Title, RectButton } from "./_styles";
import hartIcon from "../../assets/svg/BlueHart.svg";
import glassesIcon from "../../assets/svg/BlueGlasses.svg";
import mobileEyeIcon from "../../assets/svg/BlueMobileEye.svg";
import rightArrowIcon from "../../assets/svg/BlueRight.svg";
import seeingPersonIcon from "../../assets/svg/BlueSeeingPerson.svg";
import handEyeIcon from "../../assets/svg/BlueHandEye.svg";
import eyeHandIcon from "../../assets/svg/BlueEyeHand.svg";
import eyesIcon from "../../assets/svg/BlueEyes.svg";
// import phizumImg from "../../assets/svg/phizum.svg";
import { ReactComponent as ZSvg } from "../../assets/svg/z.svg";
import { ReactComponent as PhizumSvg } from "../../assets/svg/phizum.svg";

import { Flex } from "../_styles";
import { Button } from "antd";

export default function VAStartPage(props) {
  const [glassType, setGlassType] = useState(1);
  const buttonLabels = [
    "Démarrer mon test de l’œil droit !",
    "Démarrer mon test de l’œil gauche !",
    "Démarrer mon test avec les deux yeux !",
  ];

  const { pageIndex, goNext, goPrev } = props;
  return (
    <VisualAcuityWrapper className="height-auto">
      {pageIndex === 0 ? (
        <div className="main">
          <img src={hartIcon} alt="" />
          <p style={{ width: 243 }} className="bold">
            Votre médecin souhaite que vous testiez votre vision
          </p>
        </div>
      ) : pageIndex === 1 ? (
        <div className="main between" style={{ paddingBottom: 20 }}>
          <Title>Instructions pour le test</Title>
          <div className="main" style={{ marginBottom: 21 }}>
            <div className="luciole18 text-center">Regardez la lettre</div>
            <div className="bordered">
              <ZSvg />
            </div>
            <div className="luciole18 text-center mt18">
              Puis sélectionnez la lettre lue
            </div>
            <div className="bordered">
              <PhizumSvg />
            </div>
          </div>
          <Button type="text" className="more" onClick={() => goNext(true)}>
            <img src={rightArrowIcon} alt="" />
            <span>En savoir plus</span>
          </Button>
        </div>
      ) : pageIndex === 2 ? (
        <div className="main between" style={{ paddingBottom: 20 }}>
          <Title>Test de la vision de près</Title>
          <div className="main">
            <img src={seeingPersonIcon} alt="" />
            <div className="luciole18 text-center">
              La vision de près correspond à la vision obtenue à longueur de
              bras <span className="bold">(environ 30-40 cm)</span>. Elle
              correspond à la distance idéale pour la lecture, l’écriture et les
              travaux manuels.
            </div>
          </div>
        </div>
      ) : pageIndex === 3 ? (
        <div className="main between">
          <Title>Instructions pour le test</Title>
          <div className="main">
            {glassType === 1 ? (
              <>
                <img src={glassesIcon} alt="" />
                <p className="small black">
                  Si vous avez des lentilles de contact ou des lunettes, vous
                  devez{" "}
                  <span className="bold underline">
                    les porter pendant le test
                  </span>
                </p>
              </>
            ) : (
              <>
                <img src={mobileEyeIcon} alt="" />
                <p className="small black">
                  Tenez votre téléphone à une distance de lecture confortable et
                  constante durant tout le test
                </p>
              </>
            )}
            <Flex className="h-center" style={{ marginTop: 20 }}>
              <RectButton
                className={glassType === 1 ? "selected" : ""}
                onClick={() => setGlassType(1)}
              >
                {" "}
              </RectButton>
              <div style={{ width: 10 }}></div>
              <RectButton
                className={glassType === 2 ? "selected" : ""}
                onClick={() => setGlassType(2)}
              >
                {" "}
              </RectButton>
            </Flex>
          </div>
        </div>
      ) : pageIndex >= 4 ? (
        <div className="main between" style={{ paddingBottom: 20 }}>
          <Title>
            {["ŒIL DROIT", "ŒIL GAUCHE", "LES DEUX YEUX"][pageIndex / 2 - 2]}
          </Title>
          <div className="main">
            <img
              src={[handEyeIcon, eyeHandIcon, eyesIcon][pageIndex / 2 - 2]}
              alt=""
            />
            <div className="luciole18 text-center" style={{ marginTop: 28 }}>
              {pageIndex < 6
                ? "Cachez votre oeil gauche"
                : pageIndex === 6
                ? "Cachez votre oeil droit"
                : "Enfin refaites le test avec vos deux yeux."}
            </div>
          </div>
        </div>
      ) : null}
      {pageIndex < 4 ? (
        <ThemeButton
          shape="round"
          className="blue"
          onClick={() => {
            if (pageIndex === 2) {
              goPrev();
            } else if (pageIndex === 3 && glassType === 1) {
              setGlassType(2);
            } else {
              goNext(false);
            }
          }}
        >
          {(pageIndex === 2 ? "J’ai compris" : "Continuer").toUpperCase()}
        </ThemeButton>
      ) : (
        <ThemeButton
          shape="round"
          className="blue two-lines"
          onClick={() => goNext(false)}
        >
          {buttonLabels[pageIndex / 2 - 2]}
        </ThemeButton>
      )}
    </VisualAcuityWrapper>
  );
}
