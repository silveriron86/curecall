import styled from "@emotion/styled";
import { Button } from "antd";
import ColorConstants from "../../constants/ColorConstants";

export const VisualAcuityWrapper = styled("div")({
  height: "100%",
  display: "flex",
  flexDirection: "column",
  ".main": {
    width: "100%",
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    "&.between": {
      justifyContent: "space-between",
      alignItems: "flex-start",
    },
    p: {
      marginTop: 25,
      marginBottom: 0,
      textAlign: "center",
      fontFamily: "Luciole Regular",
      fontSize: 30,
      lineHeight: "40px",
      color: ColorConstants.BLUE,
      "&.small": {
        fontSize: 18,
        lineHeight: "26px",
        ".underline": {
          textDecoration: "underline",
        },
      },
      "&.black": {
        color: "#555555",
      },
    },
    ".ant-btn.more": {
      height: "fit-content",
      span: {
        fontFamily: "Luciole RegularItalic",
        fontSize: 16,
        lineHeight: "22px",
        color: ColorConstants.BLUE,
        marginLeft: 6,
        textDecoration: "underline",
      },
    },
    ".bordered": {
      marginTop: 4.3,
      position: "relative",
      // width: 270,
    },
    ".bold": {
      fontFamily: "Luciole Bold !important",
    },
    ".w10": {
      width: 10,
    },
    ".h10": {
      height: 10,
    },
  },
});

export const Title = styled("div")({
  fontFamily: "Luciole Bold",
  fontSize: 25,
  lineHeight: "33px",
  color: ColorConstants.BLUE,
  "&.black": {
    color: "black",
  },
});

export const RectButton = styled(Button)({
  borderRadius: 50,
  width: 42,
  height: 12,
  border: `1px solid ${ColorConstants.BLUE} !important`,
  "&.selected, &:hover, &:focus": {
    backgroundColor: `${ColorConstants.BLUE} !important`,
  },
});

export const LetterButton = styled(Button)({
  flex: 1,
  height: 80,
  borderRadius: 10,
  padding: 0,
  borderColor: ColorConstants.BLUE,
  borderWidth: 2,
  backgroundColor: "transparent",
  boxShadow: "none",
  "&:hover, &:focus": {
    borderColor: ColorConstants.BLUE,
    backgroundColor: `${ColorConstants.BLUE} !important`,
    span: {
      color: "white",
    },
  },
  span: {
    fontFamily: "Luciole Regular",
    fontSize: "79px",
    lineHeight: "92px",
    color: "black",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    maxWidth: "100%",
  },
  "&.small": {
    flex: "unset",
    width: "100%",
    height: 70,
    span: {
      fontFamily: "Luciole Bold",
      fontSize: "40px",
      lineHeight: "62px",
      marginTop: 2,
      letterSpacing: "-3px",
    },
    "&.four": {
      flex: 1,
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      paddingTop: 3,
      span: {
        fontFamily: "Luciole Bold",
        fontSize: "29px",
        lineHeight: "35px",
        letterSpacing: 1,
      },
    },
  },
});

export const ResultRow = styled("div")({
  width: "100%",
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-between",
  padding: "32px 0",
  borderBottom: "1px solid #707070",
  "&:first-of-type": {
    paddingTop: 0,
  },
  "&:last-child": {
    paddingBottom: 0,
    borderBottom: 0,
  },

  ".values": {
    width: 102,
    height: "90%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    ".luciole18": {
      lineHeight: "22px",
      marginBottom: 17.9,
    },
  },
});

export const SlideStyle = {
  flex: 1,
  height: "calc(var(--vh, 1vh) * 99.9)",
};
