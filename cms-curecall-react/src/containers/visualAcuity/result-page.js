import React from "react";
import { VisualAcuityWrapper, Title, ResultRow } from "./_styles";
import handEyeIcon from "../../assets/svg/BlueHandEye.svg";
import eyeHandIcon from "../../assets/svg/BlueEyeHand.svg";
import eyesIcon from "../../assets/svg/BlueEyes.svg";

export default function VAResultPage(props) {
  const visualAcuityR = localStorage.getItem("visualAcuityR_RESULT");
  const visualAcuityL = localStorage.getItem("visualAcuityL_RESULT");
  const visualAcuity = localStorage.getItem("visualAcuity_RESULT");

  return (
    <VisualAcuityWrapper className="height-auto">
      <div className="main">
        <Title>Votre vision de près</Title>
        <div className="main">
          <ResultRow>
            <img src={handEyeIcon} alt="" />
            <div className="values">
              <div className="luciole18 text-center">Œil Droit</div>
              <Title className="text-center">
                {visualAcuityR ? visualAcuityR : "Test échoué."}
              </Title>
            </div>
          </ResultRow>
          <ResultRow>
            <img src={eyeHandIcon} alt="" />
            <div className="values">
              <div className="luciole18 text-center">Œil Gauche</div>
              <Title className="text-center">
                {visualAcuityL ? visualAcuityL : "Test échoué."}
              </Title>
            </div>
          </ResultRow>
          <ResultRow>
            <img src={eyesIcon} alt="" />
            <div className="values">
              <div
                className="luciole18 text-center"
                style={{ marginBottom: 10.9 }}
              >
                Les deux yeux
              </div>
              <Title className="text-center">
                {visualAcuity ? visualAcuity : "Test échoué."}
              </Title>
            </div>
          </ResultRow>
        </div>
      </div>
      <div className="luciole18 text-center" style={{ paddingBottom: 40 }}>
        Les résultats de votre test ont été envoyés à votre médecin. Vous pouvez
        effectuer n'importe quel test supplémentaire, une fois par semaine en
        envoyant Vision.
      </div>
    </VisualAcuityWrapper>
  );
}
