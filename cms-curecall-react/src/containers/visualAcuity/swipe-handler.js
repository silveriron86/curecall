import React from "react";
export default class SwipeHandler extends React.Component {
  constructor(props) {
    super(props);
    this.xDown = null;
    this.yDown = null;
  }

  componentDidMount() {
    document.addEventListener("touchstart", this.handleTouchStart, false);
    document.addEventListener("touchmove", this.handleTouchMove, false);
  }

  componentWillUnmount() {
    document.removeEventListener("touchstart");
    document.removeEventListener("touchmove");
  }

  getTouches = (evt) => {
    return (
      evt.touches || // browser API
      evt.originalEvent.touches
    );
  };

  handleTouchStart = (evt) => {
    const firstTouch = this.getTouches(evt)[0];
    this.xDown = firstTouch.clientX;
    this.yDown = firstTouch.clientY;
  };

  handleTouchMove = (evt) => {
    if (!this.yDown) {
      return;
    }

    var yUp = evt.touches[0].clientY;

    var yDiff = this.yDown - yUp;

    if (yDiff > 0) {
      console.log("up swipe");
      this.props.goPrev();
    } else {
      /* down swipe */
      console.log("down swipe");
    }
    /* reset values */
    this.xDown = null;
    this.yDown = null;
  };

  render() {
    return null;
  }
}
