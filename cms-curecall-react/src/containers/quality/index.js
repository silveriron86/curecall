import React, { useState, useEffect } from "react";
import LoadingOverlay from "../../components/Loading-overlay";
import EndPage from "./end-page";
import SliderPage from "./slider-page";
import StartPage from "./start-page";
import { FormActions } from "../../actions";
import { connect } from "react-redux";
import { QualityVariables } from "../../constants";
import Utils from "../../utils";

function QualityPage(props) {
  const [loading, setLoading] = useState(false);
  const [pageIndex, setPage] = useState(0);
  const [variables, setVariables] = useState(QualityVariables);

  useEffect(() => {
    const pin = localStorage.getItem("PIN_CODE");
    const type = localStorage.getItem("FORM_TYPE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }
    if (type !== "quality-of-life") {
      window.location.href = `/form/${type}`;
      return;
    }

    const index = localStorage.getItem("QUALITY_INDEX");
    if (index !== null) {
      setPage(parseInt(index, 10));
    }
    const vs = localStorage.getItem("QUALITY_VARIABLES");
    if (vs) {
      setVariables(JSON.parse(vs));
    }
  }, []);

  const onVariablesChanged = (v) => {
    const vs = JSON.parse(JSON.stringify(variables));
    vs[pageIndex - 2].value = v;
    localStorage.setItem("QUALITY_VARIABLES", JSON.stringify(vs));
    setVariables(vs);
  };

  const onFinished = (callback) => {
    const pin = localStorage.getItem("PIN_CODE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }
    const variable = variables[pageIndex - 2];
    let data = [];
    data.push({
      variableName: variable.name,
      valueInt: variable.value,
    });
    console.log(data);
    setLoading(true);
    props.postForm({
      data: {
        pin,
        completed: pageIndex === 28 ? true : false,
        variables: data,
      },
      cb: (res) => {
        console.log(res.status);
        if (typeof res.status !== "undefined") {
          setLoading(false);
          Utils.showProblemAlert();
        } else {
          setLoading(false);
          if (pageIndex === 28) {
            localStorage.removeItem("QUALITY_INDEX");
            localStorage.removeItem("PIN_CODE");
            localStorage.removeItem("QUALITY_VARIABLES");
          }
          callback();
        }
      },
    });
  };

  console.log(pageIndex);
  return (
    <>
      {pageIndex < 2 ||
      pageIndex === 6 ||
      pageIndex === 9 ||
      pageIndex === 13 ||
      pageIndex === 17 ||
      pageIndex === 21 ||
      pageIndex === 25 ? (
        <StartPage pageIndex={pageIndex} onPageChanged={setPage} />
      ) : pageIndex === 29 ? (
        // end page
        <EndPage pageIndex={pageIndex} onPageChanged={setPage} />
      ) : (
        <SliderPage
          pageIndex={pageIndex}
          variables={variables}
          onVariablesChanged={onVariablesChanged}
          onPageChanged={setPage}
          onFinished={onFinished}
        />
      )}
      <LoadingOverlay loading={loading} />
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.FormReducer.form,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getForm: (req) => {
      dispatch(FormActions.getForm(req.pinCode, req.cb));
    },
    postForm: (req) => {
      dispatch(FormActions.postForm(req.data, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(QualityPage);
