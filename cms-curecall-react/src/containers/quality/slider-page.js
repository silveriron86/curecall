import React from "react";
import { PageContainer } from "../_styles";
import { HeaderWrapper } from "../../components/_styles";

import Footer from "./footer";
// import ListenBtn from "../../components/listen-btn";
import TickSlider from "../../components/tick-slider";

function SliderPage(props) {
  const {
    pageIndex,
    onFinished,
    onPageChanged,
    variables,
    onVariablesChanged,
  } = props;

  const titles = [
    "Avez-vous des difficultés pour lire ce qui est écrit sur les étiquettes des produits que vous achetez au supermarché ?",
    "Quel est votre niveau de difficulté à regarder les séries, films ou programmes que vous aimez sur une télévision ou un écran d’ordinateur ?",
    "Quand vous croisez des gens que vous connaissez, vous arrive t-il de ne pas les voir ?",
    "Avez-vous le sentiment de devoir consacrer plus de temps à réaliser vos activités quotidiennes ?",
    "",
    "Avez vous des difficultés à conduire le jour ?",
    "Avez vous des difficultés à conduire la nuit ?",
    "",
    "Vous diriez que votre tension oculaire, vous préoccupe ?",
    "Vous arrive-t-il de penser au risque de perdre la vue ?",
    "L'idée d'être opéré, vous fait-elle peur ?",
    "",
    "“Au départ, j’aurais eu besoin d’être plus soutenu”",
    "“Les problèmes de vue, on évite d’en parler”",
    "“On me considère comme quelqu’un de différent”",
    "",
    "Du fait de vos problèmes de vue ou du traitement que vous prenez, vous sentez-vous découragé(e) ?",
    "Vous sentez-vous fragile ?",
    "Vous sentez-vous incompris(e)?",
    "",
    "Prendre son traitement tous les jours",
    "Mettre mes gouttes sans en faire couler",
    "S’adapter au changement de traitement",
    "",
    "Mon traitement est efficace",
    "Aujourd’hui, j’ai suffisamment d’information sur mes problèmes de vue",
    "J’ai l’habitude de suivre ce que mon ophtalmologiste me dit",
  ];

  const hasQuote = pageIndex >= 22 && pageIndex <= 28;
  console.log(variables, pageIndex);
  const variable = variables[pageIndex - 2];

  return (
    <PageContainer style={{ overflow: pageIndex === 23 ? "hidden" : "auto" }}>
      <div
        className={`wrapper more ${
          pageIndex >= 3 ? "quality-has-progress" : ""
        }`}
      >
        <div className="main">
          <HeaderWrapper
            style={{ minHeight: 160 }}
            className={hasQuote ? "has-quote" : ""}
          >
            {hasQuote && <span className="f50 block">“</span>}
            <span className="full">
              {titles[pageIndex - 2]}

              {hasQuote && <span className="f50 absolute">”</span>}
            </span>
          </HeaderWrapper>
          {/* <ListenBtn /> */}
          <div className="content">
            {pageIndex === 7 || pageIndex === 8 ? (
              <p className="f20">
                Si vous n'avez pas de permis de conduire répondez Non, pas du
                tout
              </p>
            ) : pageIndex === 26 ? (
              <p className="f20">
                Si vous n'êtes pas concerné, répondez Tout à fait vrai pour moi
              </p>
            ) : null}

            <div
              style={{
                marginTop:
                  pageIndex === 7 || pageIndex === 8 || pageIndex === 26
                    ? 10
                    : 50,
              }}
            >
              <TickSlider
                pageIndex={pageIndex - 2}
                value={variable.value}
                onChange={onVariablesChanged}
              />
            </div>
          </div>
        </div>
      </div>

      <Footer
        pageIndex={pageIndex}
        percent={variable.percent}
        onPageChanged={onPageChanged}
        onFinished={onFinished}
      />
    </PageContainer>
  );
}

export default SliderPage;
