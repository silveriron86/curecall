import React from "react";
import { ThemeButton, ThemeIconButton } from "../../components/_styles";
import nextIcon from "../../assets/svg/Come.svg";
import prevIcon from "../../assets/svg/Back.svg";
import { Flex } from "../_styles";

function Footer(props) {
  const { pageIndex, percent, onPageChanged, onFinished } = props;
  const goNext = () => {
    if (typeof percent !== "undefined") {
      onFinished(_next);
    } else {
      _next();
    }
  };

  const _next = () => {
    const index = pageIndex + 1;
    onPageChanged(pageIndex + 1);
    localStorage.setItem("QUALITY_INDEX", index.toString());
  };

  const goPrev = () => {
    const index = pageIndex < 1 ? 0 : pageIndex - 1;
    onPageChanged(index);
    localStorage.setItem("QUALITY_INDEX", index.toString());
  };

  const lastPL = localStorage.getItem("LAST_PAGE_INDEX");
  const lastPageIndex = lastPL ? parseInt(lastPL, 10) : 0;

  return (
    <div className="footer block">
      {pageIndex > 1 && percent > 0 && (
        <div className="content">
          <div className="percentage">
            <div className="filled" style={{ width: `${percent}%` }} />
          </div>
          <p className="f16 bold-italic text-center">
            <span className="blue">{percent}%</span>
          </p>
        </div>
      )}
      {pageIndex === 1 && (
        <Flex className="h-center">
          <ThemeButton shape="round" onClick={goNext}>
            Commencer
          </ThemeButton>
        </Flex>
      )}

      <Flex className="space-between pd-b-36" style={{ width: "100%" }}>
        {pageIndex > lastPageIndex ? (
          <ThemeIconButton onClick={goPrev}>
            <img src={prevIcon} alt="" />
          </ThemeIconButton>
        ) : (
          <div />
        )}

        {pageIndex !== 1 && pageIndex !== 29 && (
          <ThemeIconButton onClick={goNext}>
            <img src={nextIcon} alt="" />
          </ThemeIconButton>
        )}
      </Flex>
    </div>
  );
}

export default Footer;
