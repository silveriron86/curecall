import React from "react";
import { PageContainer } from "../_styles";
import { HeaderWrapper } from "../../components/_styles";
import Footer from "./footer";
// import closeIcon from "../../assets/svg/Close.svg";
// import ListenBtn from "../../components/listen-btn";

function EndPage(props) {
  const { pageIndex, onPageChanged } = props;

  // const onClose = () => {
  //   const index = 0;
  //   onPageChanged(index);
  //   localStorage.setItem("QUALITY_INDEX", index.toString());
  // };

  return (
    <PageContainer>
      <div className="wrapper">
        <HeaderWrapper
          className="full-center"
          style={{ height: "calc(100vh - 130px)" }}
        >
          <span>
            Merci pour
            <br />
            vos réponses qui aident votre ophtalmologue à mieux vous connaitre.
            Vous allez recevoir des conseils personnalisés qui respectent votre
            mode de vie.
          </span>
        </HeaderWrapper>
        {/* <ThemeIconButton className="close-btn" onClick={onClose}>
          <img src={closeIcon} alt="" />
        </ThemeIconButton> */}
        {/* <ListenBtn /> */}
        <Footer pageIndex={pageIndex} onPageChanged={onPageChanged} />
      </div>
    </PageContainer>
  );
}

export default EndPage;
