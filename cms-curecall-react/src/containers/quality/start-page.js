import React from "react";
import { Flex, PageContainer } from "../_styles";
import { HeaderWrapper } from "../../components/_styles";

import welcomeIcon from "../../assets/svg/Welcome.svg";
import eyeIcon from "../../assets/svg/Eye.svg";
import logoIcon from "../../assets/svg/LogoBlanc.svg";
import Footer from "./footer";
// import ListenBtn from "../../components/listen-btn";

function StartPage(props) {
  const { pageIndex, onPageChanged } = props;

  const headerTexts = [
    "Comprenons ensemble quel est l’impact de votre traitement sur votre qualité de vie.",
    "Les questions suivantes permettent de mesurer le niveau d’anxiété que peut générer votre traitement et maladie.",
    "Maintenant, dites nous comment vous réagissez à vos problèmes de vue.",
    "Maintenant, apprenons à mieux connaître comment vous vous sentez vis-à-vis de votre pathologie.",
    "Comprenons ensemble quel est l’impact de votre traitement sur votre qualité de vie.",
    "Pour finir, voyons ensemble comment vous percevez votre suivi et l’efficacité de votre traitement",
  ];

  const spanTexts = [
    "Voici ce que des patients nous ont déclaré à propos de leurs problèmes de vue et leur pathologie.<br/>Pour chacune des affirmations suivantes, merci de choisir la réponse qui s’applique le mieux à votre cas.",
    "Pour répondre à ces questions, décrivez vos sentiments au cours de ces dernières semaines.",
    "Pour chaque phrase indiquez si c’est un problème pour vous.",
    "Voici ce que des patients vivants avec une pathologie oculaire nous ont déclaré à propos de leur façon de voir leur suivi. Pour chacune des affirmations suivantes, merci de choisir la réponse qui s’applique le mieux à votre cas.",
  ];

  let headerHTML = "";
  let spanHTML = "";
  switch (pageIndex) {
    case 6:
      headerHTML = headerTexts[0];
      break;
    case 9:
      headerHTML = headerTexts[1];
      break;
    case 13:
      headerHTML = headerTexts[2];
      spanHTML = spanTexts[0];
      break;
    case 17:
      headerHTML = headerTexts[3];
      spanHTML = spanTexts[1];
      break;
    case 21:
      headerHTML = headerTexts[4];
      spanHTML = spanTexts[2];
      break;
    case 25:
      headerHTML = headerTexts[5];
      spanHTML = spanTexts[3];
      break;
    default:
      spanHTML = "";
  }

  return (
    <PageContainer>
      <div
        className={`wrapper more ${
          pageIndex === 1 ? "footer-has-commencer" : ""
        }`}
      >
        <div className="main">
          {pageIndex > 1 ? (
            <>
              <Flex className="v-center">
                <img src={logoIcon} width={82} height={75} alt="" />
              </Flex>
              <HeaderWrapper
                style={{
                  marginTop: pageIndex === 6 || pageIndex === 9 ? 82 : 25,
                }}
              >
                <span
                  className="full"
                  dangerouslySetInnerHTML={{ __html: headerHTML }}
                ></span>
              </HeaderWrapper>
              {/* <ListenBtn /> */}
            </>
          ) : (
            <>
              <HeaderWrapper>
                <span className="full">
                  Evaluation de
                  <br />
                  la qualité de vie
                </span>
              </HeaderWrapper>
              {/* <ListenBtn /> */}

              <HeaderWrapper style={{ marginTop: 24 }} className="center">
                {pageIndex === 0 && (
                  <img src={eyeIcon} width="50" height="56" alt="" />
                )}
                {pageIndex === 1 && (
                  <img src={welcomeIcon} width="50" height="56" alt="" />
                )}
              </HeaderWrapper>
            </>
          )}
          <div className="content" style={{ marginTop: 13 }}>
            {pageIndex === 0 && (
              <p className="f18">
                Ces différentes questions visent à évaluer la façon dont vous
                vivez au quotidien votre maladie et votre traitement.
                <br />
                Ces réponses sont précieuses pour votre ophtalmologue afin de le
                guider dans la prise en charge de votre pathologie.
                <br />
                Les réponses serviront également à personnaliser votre suivi
                CureCall.
              </p>
            )}
            {pageIndex === 1 && (
              <p className="f18">
                Commençons à mieux connaître l’impact de votre pathologie sur
                votre qualité de vie au quotidien.
                <br />
                Choisissez la réponse qui correspond le mieux à votre ressenti,
                il n’y a pas de bonne ou mauvaise réponse. Le but est d’avoir
                votre ressenti.
              </p>
            )}
            {pageIndex > 1 && (
              <p
                className="f18"
                dangerouslySetInnerHTML={{ __html: spanHTML }}
              ></p>
            )}
          </div>
        </div>
      </div>

      <Footer pageIndex={pageIndex} onPageChanged={onPageChanged} />
    </PageContainer>
  );
}

export default StartPage;
