import React from "react";
import { PageContainer, FlexCenter } from "../_styles";
import logo from "../../assets/svg/LogoCurecall.svg";

function HomePage() {
  return (
    <PageContainer className="login-page">
      <FlexCenter>
        <img src={logo} width="124" height="113" alt="" />
      </FlexCenter>
    </PageContainer>
  );
}

export default HomePage;
