import React, { useState, useEffect } from "react";
import { Select, Modal, Button } from "antd";
import {
  HeaderWrapper,
  ThemeIconButton,
  // ThemeInput,
  ThemeButton,
} from "../../components/_styles";
import {
  PageContainer,
  Flex,
  DottedBorder,
  ThemeHowManyInput,
  TakeTimeSelect,
} from "../_styles";
// import AnswersList from "../../components/answers-list";

// import listenOpenIcon from "../../assets/svg/Listen Open.svg";
// import listenCloseIcon from "../../assets/svg/Listen close.svg";
// import closeIcon from "../../assets/svg/Close.svg";
import prevIcon from "../../assets/svg/Back.svg";
import nextIcon from "../../assets/svg/Come.svg";
import addIcon from "../../assets/svg/Add.svg";
import blueAddIcon from "../../assets/svg/BlueAdd.svg";
// import modifyIcon from "../../assets/svg/Modify.svg";
// import borderBg from "../../assets/svg/Dotted.svg";

import { questionStyle } from "../login/_styles";
import { AddWidget, YellowBlock } from "./_styles";
import LoadingOverlay from "../../components/Loading-overlay";
import Utils from "../../utils";

const { Option } = Select;

function OptionsPage(props) {
  const pageIndex = props.pageIndex - 1;
  const [loading, setLoading] = useState(false);
  const [treatmentId, setTreatmentId] = useState(-1);
  // const [selectedAnswer, selectAnswer] = useState(-1);
  // const [treatmentIndex, setTreatmentIndex] = useState(0);
  const [visibleConfirm, setVisibleConfirm] = useState(false);
  const [inputting, setInputting] = useState(false);
  const [visibleMsg, setVisibleMsg] = useState(true);

  const onFocusInput = () => {
    setInputting(true);
  };

  const onBlurInput = () => {
    setInputting(false);
  };

  // const toggleListen = () => {
  //   setListen(!isListenOpened);
  // };

  useEffect(() => {
    // setListen(false);
    console.log(pageIndex);
    if (pageIndex === 5) {
      setVisibleMsg(true);
      setTimeout(() => {
        setVisibleMsg(false);
      }, 3000);
      setLoading(true);
      props.getTreatments({
        cb: (res) => {
          console.log(res);
          setLoading(false);
        },
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageIndex]);

  const questions = [
    ["Combien de fois par jour devez vous le prendre ?"],
    ["Heure de la prise de ce médicament :"],
    [],
    "Avez-vous la même posologie à chaque prise?",
    "Si les prises de la journée sont différentes, indiquez la plus importante.",
  ];

  const options1 = [
    ["Collyre", "Eye drops"],
    ["Comprimé", "Tablet"],
    ["Gélule", "Capsule"],
    ["Injection", "Injection"],
    ["Cuillère à café", "Coffee spoon"],
    ["Pipette", "Pipette"],
  ];

  const options2 = [
    ["ml", "ml"],
    ["mg", "mg"],
    ["gouttle(s)", "drop(s)"],
    ["unité(s)", "unit(s)"],
  ];

  const handleDelete = (treatment) => {
    // setTreatmentIndex(idx);
    setTreatmentId(treatment.id);
    setVisibleConfirm(true);
  };

  const handleModify = (treatment) => {
    console.log("*** handleModify = ", treatment);
    localStorage.setItem("TREATMENT_ID", treatment.id.toString());
    props.selectAnswer("modify", treatment);
    setTreatmentId(treatment.id);
    props.setPage(2);
  };

  const handleAdd = () => {
    setTreatmentId(-1);
    localStorage.removeItem("TREATMENT_ID");
    props.selectAnswer("add", null);
    props.setPage(1);
  };

  const setTakeTimeValue = (value, i) => {
    setVariableByNames([[`timeIntake${i + 1}`, value]]);
  };

  const isAllFilled = () => {
    const howManyCount = variables[7].valueInt;
    let notFilled = false;
    for (let i = 0; i < howManyCount; i++) {
      if (variables[8 + i] === "") {
        notFilled = true;
      }
    }
    return !notFilled;
  };

  const handleDeleteTreatment = () => {
    setVisibleConfirm(false);
    setLoading(true);
    props.deleteTreatment({
      id: treatmentId,
      cb: (res) => {
        props.getTreatments({
          cb: (_res) => {
            setLoading(false);
          },
        });
      },
    });
  };

  if (pageIndex === 5) {
    let treatmentName = "";
    if (treatmentId >= 0) {
      const found = props.treatments.find((t) => t.id === treatmentId);
      if (found) {
        treatmentName = found.drugName;
      }
    }

    return (
      <PageContainer>
        <Modal
          wrapClassName="confirm-modal"
          centered
          maskClosable={false}
          closable={false}
          title={null}
          footer={null}
          width={302}
          visible={visibleConfirm}
          onOk={() => setVisibleConfirm(false)}
          onCancel={() => setVisibleConfirm(false)}
        >
          <h1>Voulez vous supprimer ce traitement ?</h1>
          <p>{treatmentName}</p>
          <Flex className="space-between">
            <Button type="text" onClick={() => setVisibleConfirm(false)}>
              Non
            </Button>
            <Button type="text" onClick={handleDeleteTreatment}>
              Oui
            </Button>
          </Flex>
        </Modal>

        <div className="wrapper more">
          {visibleMsg && (
            <YellowBlock>
              <p className="blue-italic f14">
                Votre traitement a bien été enregistré.
              </p>
            </YellowBlock>
          )}
          <HeaderWrapper>
            <span style={{ width: "100%" }}>Vos traitement en cours</span>
          </HeaderWrapper>
          <div
            className="content no-left"
            style={{ marginTop: 0, marginBottom: 20 }}
          >
            <p style={{ marginTop: 15, marginBottom: -10 }}>
              <span className="f16">
                Vous pouvez modifier ou ajouter un traitement à tout moment en
                envoyant <span className="blue-italic">”Traitement”</span> à
                CureCall
              </span>
            </p>
            <AddWidget onClick={handleAdd}>
              <img src={blueAddIcon} alt="" />
              <p className="f12">
                Ajouter
                <br />
                un autre
                <br />
                médicament
              </p>
            </AddWidget>
            {props.treatments.map((treatment, idx) => {
              return (
                <DottedBorder key={`border-${idx}`}>
                  {/* <img className="border" src={borderBg} alt="" /> */}
                  <div>
                    <p className="f18">{treatment.drugName}</p>
                    <p
                      className="f14"
                      style={{ marginTop: 7, display: "block", height: 46 }}
                    >
                      {/* {idx === 2
                        ? "Commencé le 11/11/2020"
                        : "A partir du 11/11/2020 jusqu’au 04/12/2020"} */}
                      {treatment.endOfTreatment
                        ? `A partir du ${Utils.formatDate(
                            treatment.startOfTreatment
                          )} jusqu’au ${Utils.formatDate(
                            treatment.endOfTreatment
                          )}`
                        : `Commencé le ${Utils.formatDate(
                            treatment.startOfTreatment
                          )}`}
                    </p>
                  </div>

                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                    }}
                  >
                    <ThemeButton
                      shape="round"
                      style={{ width: 192 }}
                      className="modifier"
                      onClick={() => handleModify(treatment)}
                    >
                      Modifier
                    </ThemeButton>
                    <ThemeButton
                      shape="round"
                      style={{ marginTop: 24 }}
                      className="pink supprimer"
                      onClick={() => handleDelete(treatment)}
                    >
                      Supprimer
                    </ThemeButton>
                  </div>
                </DottedBorder>
              );
            })}

            <DottedBorder style={{ height: 306 }}>
              {/* <img className="border" src={borderBg} alt="" /> */}
              <img src={addIcon} alt="" />
              <p style={{ marginTop: 12 }}>
                <span className="f2530">Ajouter un autre médicament</span>
              </p>
              <ThemeButton
                style={{ marginTop: 37 }}
                shape="round"
                onClick={handleAdd}
              >
                Ajouter
              </ThemeButton>
            </DottedBorder>
          </div>
        </div>
        <LoadingOverlay loading={loading} />
      </PageContainer>
    );
  }

  const { variables, setVariableByNames } = props;
  const howManyCount = variables[7].valueInt;
  console.log("howManyCount =  ", howManyCount);
  const selects = new Array(howManyCount).fill(0);

  const lastPL = localStorage.getItem("LAST_PAGE_INDEX");
  const lastPageIndex = lastPL ? parseInt(lastPL, 10) : 0;
  console.log("treatments = ", props.treatments);

  return (
    <PageContainer>
      <div className="wrapper more">
        <HeaderWrapper>
          <span style={{ width: 235 }}>Votre traitement médical</span>
        </HeaderWrapper>
        {/* <ThemeIconButton className="listen-btn" onClick={toggleListen}>
          <img src={isListenOpened ? listenOpenIcon : listenCloseIcon} alt="" />
        </ThemeIconButton> */}
        <div className="content" style={{ marginTop: 10, paddingLeft: 0 }}>
          <p style={questionStyle}>{variables[2].valueString}</p>
          <p className="f18" style={{ marginTop: 29, marginBottom: 20 }}>
            {pageIndex < 3 ? "FREQUENCE" : "POSOLOGIE"}
          </p>
          {(pageIndex === 2 || pageIndex === 3) && (
            <Flex className="space-between" style={{ marginBottom: 19 }}>
              <p className="f16" style={{ width: 193 }}>
                Combien de fois par jour devez vous le prendre ?
              </p>
              <ThemeHowManyInput
                min={0}
                max={5}
                value={howManyCount}
                onChange={(v) => setVariableByNames([["frequencyIntake", v]])}
                placeholder="0"
                onFocus={onFocusInput}
                onBlur={onBlurInput}
              />
            </Flex>
          )}
          {pageIndex === 3 &&
            selects.map((q, i) => {
              return (
                <Flex
                  key={`treatment-options-q-${i}`}
                  className="space-between"
                  style={{ marginBottom: 19 }}
                >
                  <p className="f16">
                    {howManyCount === 1
                      ? "Heure de la prise de ce médicament :"
                      : `Heure de la ${i + 1}ère prise :`}
                  </p>
                  <TakeTimeSelect
                    // className="take-select"
                    // style={{ marginTop: 8, display: "block" }}
                    placeholder="00h"
                    value={variables[8 + i].valueInt}
                    onChange={(e) => setTakeTimeValue(e, i)}
                  >
                    {new Array(24).fill(0).map((opt, ii) => {
                      return (
                        <Option
                          // value={`${Utils.pad(ii, 2)}:00:00`}
                          value={ii * 3600 * 1000}
                          key={`opt-${i}-${ii}`}
                        >
                          {`${ii.toString().padStart(2, "0")}h`}
                        </Option>
                      );
                    })}
                  </TakeTimeSelect>
                </Flex>
              );
            })}
          {pageIndex === 4 && (
            <>
              <p className="f16">{questions[pageIndex]}</p>
              <Flex className="space-between" style={{ marginTop: 16 }}>
                <div style={{ minWidth: 112 }}>
                  <span style={questionStyle}>Le type</span>
                  <Select
                    className="treatment-select"
                    style={{ marginTop: 8, display: "block" }}
                    defaultValue={options1[0][1]}
                  >
                    {options1.map((opt, i) => {
                      return (
                        <Option value={opt[1]} key={`opt-${i}`}>
                          {opt[0]}
                        </Option>
                      );
                    })}
                  </Select>
                </div>

                <div style={{ minWidth: 112 }}>
                  <span style={questionStyle}>Dosage</span>
                  <Select
                    className="treatment-select"
                    style={{ marginTop: 8, display: "block" }}
                    defaultValue={options2[0][1]}
                  >
                    {options2.map((opt, i) => {
                      return (
                        <Option value={opt[1]} key={`opt-${i}`}>
                          {opt[0]}
                        </Option>
                      );
                    })}
                  </Select>
                </div>
              </Flex>
              <Flex className="h-center">
                <ThemeButton
                  shape="round"
                  style={{ position: "fixed", bottom: 57 }}
                  onClick={props.goNext}
                >
                  Enregistrer
                </ThemeButton>
              </Flex>
            </>
          )}
        </div>
      </div>

      {!inputting && (
        <div className={`footer ${pageIndex === 2 ? "treatment-first" : ""}`}>
          {pageIndex > lastPageIndex ? (
            <ThemeIconButton onClick={props.goPrev}>
              <img src={prevIcon} alt="" />
            </ThemeIconButton>
          ) : (
            <div />
          )}
          {((pageIndex === 2 && howManyCount >= 1) ||
            (pageIndex === 3 && isAllFilled())) && (
            <ThemeIconButton onClick={props.goNext}>
              <img src={nextIcon} alt="" />
            </ThemeIconButton>
          )}
        </div>
      )}
      <LoadingOverlay loading={loading} />
    </PageContainer>
  );
}

export default OptionsPage;
