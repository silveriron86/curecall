import React, { useState } from "react";
import { DateRange } from "react-date-range";
import moment from "moment";
import locale from "date-fns/locale/fr";
import InputMask from "react-input-mask";
import { DatesPickerModal, DatesPickerInput } from "../_styles";
import {
  ThemeInput,
  ThemeIconButton,
  ThemeButton,
} from "../../../components/_styles";

import calendarIcon from "../../../assets/svg/Calendar.svg";
import calendarWhiteIcon from "../../../assets/svg/Calendar_white.svg";
import closeIcon from "../../../assets/svg/Close.svg";
import editIcon from "../../../assets/svg/Pencil.svg";

import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import { Flex } from "../../_styles";

function DatesPicker(props) {
  moment.locale("fr");

  const [modalVisible, setModalVisible] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [state, setState] = useState([
    {
      startDate: moment(props.value[0]).toDate(),
      endDate: props.value[1] ? moment(props.value[1]).toDate() : null,
      key: "selection",
    },
  ]);
  const [startDate, setStartDate] = useState(
    moment(props.value[0]).format("DD/MM/YYYY")
  );
  const [endDate, setEndDate] = useState(
    props.value[1] ? moment(props.value[1]).format("DD/MM/YYYY") : null
  );

  const onClickInput = () => {
    console.log("onClickInput");
    setModalVisible(true);
  };

  const onCloseModal = () => {
    setModalVisible(false);
  };

  const onEditDate = () => {
    setStartDate(
      state[0].startDate
        ? moment(state[0].startDate).format("DD/MM/YYYY")
        : null
    );
    setEndDate(
      state[0].endDate ? moment(state[0].endDate).format("DD/MM/YYYY") : null
    );
    setEditMode(true);
  };

  const onChangeStartDate = (e) => {
    setStartDate(e.target.value);
  };

  const onChangeEndDate = (e) => {
    setEndDate(e.target.value);
  };

  const onCancelEdit = () => {
    setEditMode(false);
  };

  const onSelectDates = (item) => {
    console.log(item);
    if (item.selection.startDate === item.selection.endDate) {
      item.selection.endDate = null;
    }
    setState([item.selection]);
    setStartDate(moment(item.selection.startDate));
    setEndDate(item.selection.endDate ? moment(item.selection.endDate) : null);
  };

  const onConfirmEdit = () => {
    const start = moment(startDate, "DD/MM/YYYY");
    const end = endDate ? moment(endDate, "DD/MM/YYYY") : null;

    console.log(startDate, endDate);
    console.log(start, end);

    if (!start.isValid()) {
      // invalid
      return;
    }

    if (start && start.isValid() && end && end.isValid()) {
      const diff = start.diff(end);
      console.log("*diff = ", diff);
      if (diff > 0) {
        // invalid
        return;
      }
    }

    setState([
      {
        startDate: start.isValid() ? start.toDate() : null,
        endDate: end && end.isValid() ? end.toDate() : null,
        key: "selection",
      },
    ]);

    setEditMode(false);
    onCloseModal();
    props.onChange({
      startDate: typeof startDate === "string" ? start : startDate,
      endDate: typeof endDate === "string" ? end : endDate,
    });
  };

  let date1 = "";
  let date2 = "";
  if (startDate !== null) {
    date1 = (typeof startDate === "string"
      ? moment(startDate, "DD/MM/YYYY")
      : startDate
    ).format("DD/MM/YYYY");
  }
  if (endDate !== null) {
    date2 = (typeof endDate === "string"
      ? moment(endDate, "DD/MM/YYYY")
      : endDate
    ).format("DD/MM/YYYY");
  }
  let datesStr = `${date1} ~ ${date2}`;

  return (
    <>
      <p className="f18 white" style={{ marginTop: 24, marginBottom: 8 }}>
        Durée de votre traitement
      </p>
      <DatesPickerInput onClick={onClickInput}>
        <ThemeInput
          className="date"
          placeholder={`A partir du ${(startDate
            ? moment(startDate)
            : moment()
          ).format("DD/MM/YYYY")} au JJ/MM/AAAA`}
          value={datesStr}
          readOnly={true}
        />
        <img src={calendarIcon} alt="" />
      </DatesPickerInput>
      <DatesPickerModal
        centered
        maskClosable={false}
        closable={false}
        title={null}
        footer={null}
        width={325}
        visible={modalVisible}
        onOk={onCloseModal}
        onCancel={onCloseModal}
      >
        {editMode ? (
          <div style={{ height: 28 }}></div>
        ) : (
          <div style={{ height: 100 }}>
            <ThemeIconButton className="close-btn" onClick={onCloseModal}>
              <img src={closeIcon} alt="" />
            </ThemeIconButton>
            <ThemeButton className="valider" onClick={onConfirmEdit}>
              Valider
            </ThemeButton>
          </div>
        )}
        <span className="f16 white label">Durée du traitement</span>
        <Flex
          className="space-between"
          style={{ padding: "9px 25.5px 38px 27px" }}
        >
          <div className="selected-dates">
            {moment(state[0].startDate).format("DD MMM")} -{" "}
            {state[0].endDate
              ? moment(state[0].endDate).format("DD MMM")
              : "MM/JJ"}
          </div>
          <ThemeIconButton className="edit-btn" onClick={onEditDate}>
            <img src={editMode ? calendarWhiteIcon : editIcon} alt="" />
          </ThemeIconButton>
        </Flex>
        {editMode === false ? (
          <div style={{ padding: "20px 0px 0", backgroundColor: "white" }}>
            <DateRange
              locale={locale}
              // editableDateInputs={true}
              // startDate={startDate}
              // endDate={endDate}
              onChange={(item) => {
                onSelectDates(item);
              }}
              moveRangeOnFirstSelection={false}
              // direction="vertical"
              // scroll={{ enabled: true }}
              ranges={state}
            />
          </div>
        ) : (
          <div style={{ backgroundColor: "white", padding: "10px 0" }}>
            <Flex
              className="space-between"
              style={{
                padding: "0 27px 20px",
              }}
            >
              <div className="dates-col start">
                <span className="label">Début</span>
                <InputMask
                  mask="99/99/9999"
                  maskPlaceholder="JJ/MM/AAAA"
                  value={startDate}
                  onChange={onChangeStartDate}
                />
              </div>
              <div className="dates-col end">
                <span className="label">Fin du traitement</span>
                <InputMask
                  mask="99/99/9999"
                  maskPlaceholder="JJ/MM/AAAA"
                  placeholder="JJ/MM/AAAA"
                  value={endDate}
                  onChange={onChangeEndDate}
                />
              </div>
            </Flex>
            <Flex className="v-center h-center">
              <ThemeButton className="text pink" onClick={onCancelEdit}>
                Annuler
              </ThemeButton>
              <ThemeButton className="text blue" onClick={onConfirmEdit}>
                Valider
              </ThemeButton>
            </Flex>
          </div>
        )}
      </DatesPickerModal>
    </>
  );
}

export default DatesPicker;
