import React, { useState } from "react";
import { Select } from "antd";
import { HeaderWrapper, ThemeIconButton } from "../../components/_styles";
import { PageContainer, Flex, ThemeHowManyInput } from "../_styles";
import prevIcon from "../../assets/svg/Back.svg";
import nextIcon from "../../assets/svg/Come.svg";

import { questionStyle } from "../login/_styles";

const { Option } = Select;

function B12Page(props) {
  const [inputting, setInputting] = useState(false);
  // const [takeTimes, setTakeTimes] = useState([]);

  const onFocusInput = () => {
    setInputting(true);
  };

  const onBlurInput = () => {
    setInputting(false);
  };

  const options = [
    ["Goutte(s) de collyre", "Goutte(s) de collyre"],
    ["Comprimé(s) / Gélule(s)", "Comprimé(s) / Gélule(s)"],
    ["Unité(s) / autre", "Unité(s) / autre"],
  ];

  const { variables, setVariableByNames, pageIndex } = props;

  const lastPL = localStorage.getItem("LAST_PAGE_INDEX");
  const lastPageIndex = lastPL ? parseInt(lastPL, 10) : 0;

  return (
    <PageContainer>
      <div className="wrapper more">
        <HeaderWrapper>
          <span style={{ width: 235 }}>Votre traitement médical</span>
        </HeaderWrapper>
        {/* <ThemeIconButton className="listen-btn" onClick={toggleListen}>
          <img src={isListenOpened ? listenOpenIcon : listenCloseIcon} alt="" />
        </ThemeIconButton> */}
        <div className="content" style={{ marginTop: 10, paddingLeft: 0 }}>
          <p style={questionStyle}>{variables[2].valueString}</p>
          <p className="f18" style={{ marginTop: 27, marginBottom: 4 }}>
            POSOLOGIE
          </p>
          <p className="f16 yellow" style={{ marginBottom: 27 }}>
            Si les prises de la journée sont différentes, indiquez la plus
            importante.
          </p>
          <p style={questionStyle}>Pour une prise, je dois prendre :</p>
          <Flex
            className="space-between"
            style={{ marginTop: 9, marginBottom: 19 }}
          >
            <ThemeHowManyInput
              min={1}
              max={5}
              value={variables[5].valueInt}
              onChange={(v) => setVariableByNames([["drugIntake", v]])}
              placeholder="0"
              onFocus={onFocusInput}
              onBlur={onBlurInput}
            />
            <div style={{ width: 21.5 }}></div>
            <Select
              className="treatment-select"
              style={{ flex: 1 }}
              value={variables[6].valueString}
              onChange={(v) => setVariableByNames([["drugForm", v]])}
            >
              {options.map((opt, i) => {
                return (
                  <Option value={opt[1]} key={`opt-${i}`}>
                    {opt[0]}
                  </Option>
                );
              })}
            </Select>
          </Flex>
        </div>
      </div>

      {!inputting && (
        <div className="footer treatment-first">
          {pageIndex > lastPageIndex ? (
            <ThemeIconButton onClick={props.goPrev}>
              <img src={prevIcon} alt="" />
            </ThemeIconButton>
          ) : (
            <div />
          )}
          {variables[5].valueInt > 0 && (
            <ThemeIconButton onClick={props.goNext}>
              <img src={nextIcon} alt="" />
            </ThemeIconButton>
          )}
        </div>
      )}
    </PageContainer>
  );
}

export default B12Page;
