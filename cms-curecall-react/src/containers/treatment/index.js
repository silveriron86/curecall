import React, { useState, useEffect } from "react";
import moment from "moment";
import {
  HeaderWrapper,
  ThemeIconButton,
  ThemeInput,
  ThemeButton,
} from "../../components/_styles";
import { PageContainer, Flex } from "../_styles";
import { CommencerWrapper } from "./_styles";
import AnswersList from "../../components/answers-list";
import OptionsPage from "./options-page";

import prevIcon from "../../assets/svg/Back.svg";
import nextIcon from "../../assets/svg/Come.svg";
// import logoIcon from "../../assets/svg/LogoBlanc.svg";
import DatesPicker from "./modal/dates-picker";
import B12Page from "./b12-page";
import LoadingOverlay from "../../components/Loading-overlay";
import { FormActions, AccountActions } from "../../actions";
import { connect } from "react-redux";
import Utils from "../../utils";

const initVariables = JSON.stringify([
  { variableName: "hasDrug", valueBool: true },
  { variableName: "cip", valueString: "" },
  { variableName: "drugName", valueString: "" },
  {
    variableName: "startOfTreatment",
    valueString: moment().format("YYYY-MM-DD"),
  },
  { variableName: "endOfTreatment", valueString: null },
  { variableName: "drugIntake", valueInt: null },
  { variableName: "drugForm", valueString: "Goutte(s) de collyre" },
  { variableName: "frequencyIntake", valueInt: "" },
  { variableName: "timeIntake1", valueInt: "" },
  {
    variableName: "timeIntake2",
    valueInt: "",
  },
  { variableName: "timeIntake3", valueInt: "" },
  { variableName: "timeIntake4", valueInt: "" },
  { variableName: "timeIntake5", valueInt: "" },
]);

function TreatmentPage(props) {
  const [loading, setLoading] = useState(false);
  const [pageIndex, setPage] = useState(0);
  const [inputting, setInputting] = useState(false);
  const [selectedAnswer, selectAnswer] = useState(-1);

  const [variables, setVariables] = useState(JSON.parse(initVariables));

  const setVariableByNames = (names) => {
    const vs = JSON.parse(JSON.stringify(variables));
    names.forEach((row, idx) => {
      const variableName = row[0];
      const value = row[1];
      const index = vs.findIndex((v) => v.variableName === variableName);
      if (index > -1) {
        if (typeof vs[index].valueBool !== "undefined") {
          vs[index].valueBool = value;
        } else if (typeof vs[index].valueInt !== "undefined") {
          vs[index].valueInt = value;
        } else if (typeof vs[index].valueString !== "undefined") {
          vs[index].valueString = value;
        }
      }
    });
    localStorage.setItem("TREATMENT_VARIABLES", JSON.stringify(vs));
    setVariables(vs);
  };

  useEffect(() => {
    const pin = localStorage.getItem("PIN_CODE");
    const type = localStorage.getItem("FORM_TYPE");
    if (!pin) {
      window.location.href = "/form";
      return;
    }
    if (type !== "treatment") {
      window.location.href = `/form/${type}`;
      return;
    }

    const index = localStorage.getItem("TREATMENT_INDEX");
    if (index !== null) {
      setPage(parseInt(index, 10));
    }

    const vs = localStorage.getItem("TREATMENT_VARIABLES");
    if (vs) {
      const oVS = JSON.parse(vs);
      selectAnswer(oVS[0].valueBool === true ? 0 : 1);
      setVariables(oVS);
    }
  }, []);

  const goNext = () => {
    if (pageIndex === 4) {
      onFinished(_next);
    } else {
      _next();
    }
  };

  const _next = () => {
    const index =
      selectedAnswer === 1 // when choose OUI, it will go end page
        ? 6
        : pageIndex === 4
        ? pageIndex + 2
        : pageIndex + 1;
    setPage(index);
    localStorage.setItem("TREATMENT_INDEX", index.toString());
  };

  const onFinished = (callback) => {
    const pin = localStorage.getItem("PIN_CODE");
    const token = localStorage.getItem("TOKEN");
    if (!pin || !token) {
      window.location.href = "/form";
      return;
    }

    let data = {};
    variables.forEach((v, i) => {
      data[v.variableName] =
        typeof v.valueBool !== "undefined"
          ? v.valueBool
          : typeof v.valueString !== "undefined"
          ? v.valueString
          : v.valueInt;
    });
    if (data.frequencyIntake < 5) {
      for (let i = 5; i > data.frequencyIntake; i--) {
        delete data[`timeIntake${i}`];
      }
    }
    if (data.endOfTreatment === null) {
      delete data.endOfTreatment;
    }
    // ""frequencyIntake" must be a string"
    data.frequencyIntake = data.frequencyIntake.toString();
    delete data.cip;
    console.log(data);

    setLoading(true);
    const treatmentId = localStorage.getItem("TREATMENT_ID");
    if (treatmentId) {
      // update
      props.updateTreatment({
        id: treatmentId,
        data,
        cb: (res) => {
          _after(res, callback);
        },
      });
    } else {
      // create
      props.postTreatment({
        data,
        cb: (res) => {
          _after(res, callback);
        },
      });
    }
  };

  const _after = (res, callback) => {
    console.log(res);
    if (typeof res.status !== "undefined") {
      setLoading(false);
      if (res.status === 401) {
        // Unauthorized
        localStorage.removeItem("TOKEN");
        localStorage.removeItem("TREATMENT_INDEX");
        localStorage.removeItem("TREATMENT_VARIABLES");
        localStorage.removeItem("PIN_CODE");
        window.location.href = "/form";
      } else {
        Utils.showProblemAlert();
      }
    } else {
      // localStorage.removeItem("PIN_CODE");
      // localStorage.removeItem("SECONDARIES_INDEX");
      // localStorage.removeItem("TREATMENT_VARIABLES");

      setLoading(false);
      callback();
    }
  };

  const goPrev = () => {
    let index = pageIndex === 5 ? pageIndex - 2 : pageIndex - 1;
    setPage(index);
    localStorage.setItem("TREATMENT_INDEX", index.toString());
  };

  const onSelectAnswer = (index) => {
    selectAnswer(index);
    setVariableByNames([["hasDrug", index === 0 ? true : false]]);
  };

  const onDoAction = (action, treatment) => {
    selectAnswer(0);
    if (action === "add") {
      // add
      setVariables(JSON.parse(initVariables));
    } else {
      // modify
      let vs = JSON.parse(initVariables);
      vs[0].valueBool = treatment.hasDrug;
      vs[2].valueString = treatment.drugName;
      if (treatment.startOfTreatment) {
        vs[3].valueString = treatment.startOfTreatment;
      }
      if (treatment.endOfTreatment) {
        vs[4].valueString = treatment.endOfTreatment;
      }
      if (treatment.drugIntake !== null) {
        vs[5].valueInt = treatment.drugIntake;
      }
      if (treatment.drugForm) {
        vs[6].valueString = treatment.drugForm;
      }
      if (treatment.frequencyIntake) {
        vs[7].valueInt = treatment.frequencyIntake;
      }
      for (let i = 0; i < 5; i++) {
        if (treatment[`timeIntake${i + 1}`] !== null) {
          vs[8 + i].valueInt = treatment[`timeIntake${i + 1}`];
        }
      }
      setVariables(vs);
    }
  };

  const onChangeDates = (dates) => {
    const startDate = moment(dates.startDate).format("YYYY-MM-DD");
    const endDate = dates.endDate
      ? moment(dates.endDate).format("YYYY-MM-DD")
      : null;
    setVariableByNames([
      ["startOfTreatment", startDate],
      ["endOfTreatment", startDate === endDate ? null : endDate],
    ]);
  };

  const onFocusInput = () => {
    setInputting(true);
  };

  const onBlurInput = () => {
    setInputting(false);
  };

  console.log("pageIndex = ", pageIndex);
  console.log(variables);

  console.log("TOKE = ", localStorage.getItem("TOKEN"));

  // if (pageIndex === 1 && selectedAnswer === 1) {
  //   return (
  //     <PageContainer>
  //       <div className="wrapper more">
  //         <div className="main">
  //           <Flex className="v-center h-center">
  //             <ThemeIconButton style={{ marginTop: -15 }} onClick={goPrev}>
  //               <img src={logoIcon} width={82} height={75} alt="" />
  //             </ThemeIconButton>
  //           </Flex>
  //           <HeaderWrapper
  //             className="full-center"
  //             style={{ height: "calc(100vh - 260px)" }}
  //           >
  //             <span style={{ textAlign: "left" }}>
  //               Très bien merci pour cette information, vous pouvez à tout
  //               moment ajouter un traitement en envoyant
  //               <br />
  //               <span className="bold-italic blue">“Traitement”</span>
  //               <br />
  //               par SMS à Curecall.
  //             </span>
  //           </HeaderWrapper>
  //         </div>
  //       </div>
  //     </PageContainer>
  //   );
  // }

  if (pageIndex === 2) {
    return (
      <B12Page
        pageIndex={pageIndex}
        variables={variables}
        setVariableByNames={setVariableByNames}
        setPage={setPage}
        goNext={goNext}
        goPrev={goPrev}
      />
    );
  }

  if (pageIndex >= 3) {
    return (
      <OptionsPage
        pageIndex={pageIndex}
        variables={variables}
        setVariableByNames={setVariableByNames}
        setPage={setPage}
        selectAnswer={onDoAction}
        goNext={goNext}
        goPrev={goPrev}
        getTreatments={props.getTreatments}
        deleteTreatment={props.deleteTreatment}
        treatments={props.treatments}
      />
    );
  }

  const lastPL = localStorage.getItem("LAST_PAGE_INDEX");
  const lastPageIndex = lastPL ? parseInt(lastPL, 10) : 0;
  console.log("lastPageIndex = ", lastPageIndex);

  return (
    <PageContainer className={pageIndex === 1 ? "treatment-home" : ""}>
      <div className="wrapper more">
        <HeaderWrapper>
          <span style={{ width: "100%" }}>
            {pageIndex === 0
              ? "Votre ophtalmologue vous a t-il prescrit un traitement ?"
              : pageIndex === 1
              ? "Saisissez le nom du traitement que votre médecin vous a prescrit"
              : ""}
          </span>
        </HeaderWrapper>
        {pageIndex === 0 && (
          <div style={{ marginTop: 14 }}>
            <AnswersList
              data={["OUI", "NON"]}
              index={0}
              answer={selectedAnswer}
              onSelect={onSelectAnswer}
            />
          </div>
        )}
        {pageIndex === 1 && (
          <>
            <ThemeInput
              style={{ marginTop: 30 }}
              value={variables[2].valueString}
              placeholder="Saisissez le nom du médicament"
              onChange={(e) => {
                setVariableByNames([["drugName", e.target.value]]);
              }}
              maxLength={255}
              onFocus={onFocusInput}
              onBlur={onBlurInput}
            />

            {variables[3].valueString !== null && (
              <DatesPicker
                value={[variables[3].valueString, variables[4].valueString]}
                onChange={onChangeDates}
              />
            )}
          </>
        )}
      </div>
      {pageIndex === 1 && (
        <CommencerWrapper className="commencer">
          {variables[2].valueString !== "" && !inputting && (
            <Flex className="h-center">
              <ThemeButton shape="round" onClick={goNext}>
                Commencer
              </ThemeButton>
            </Flex>
          )}
        </CommencerWrapper>
      )}
      {pageIndex === 0 && selectedAnswer >= 0 && (
        <div className={`footer right treatment-first`}>
          <ThemeIconButton onClick={goNext}>
            <img src={nextIcon} alt="" />
          </ThemeIconButton>
        </div>
      )}
      {pageIndex === 1 && !inputting && pageIndex > lastPageIndex && (
        <div className="footer treatment-first">
          <ThemeIconButton onClick={goPrev}>
            <img src={prevIcon} alt="" />
          </ThemeIconButton>
        </div>
      )}
      <LoadingOverlay loading={loading} />
    </PageContainer>
  );
}

const mapStateToProps = (state) => {
  return {
    form: state.FormReducer.form,
    treatments: state.AccountReducer.treatments,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getForm: (req) => {
      dispatch(FormActions.getForm(req.pinCode, req.cb));
    },
    postForm: (req) => {
      dispatch(FormActions.postForm(req.data, req.cb));
    },
    postTreatment: (req) => {
      dispatch(AccountActions.postTreatment(req.data, req.cb));
    },
    getTreatments: (req) => {
      dispatch(AccountActions.getTreatments(req.cb));
    },
    deleteTreatment: (req) => {
      dispatch(AccountActions.deleteTreatment(req.id, req.cb));
    },
    updateTreatment: (req) => {
      dispatch(AccountActions.updateTreatment(req.id, req.data, req.cb));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TreatmentPage);
