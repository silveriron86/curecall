import React from "react";
import QrReader from "react-qr-scanner";
// import QrScanner from "qr-scanner";
// QrScanner.WORKER_PATH = "/qr-scanner-worker.min.js";

function QrCodePage(props) {
  // useEffect(() => {
  //   const video = document.getElementById("qr-video");
  //   const scanner = new QrScanner(
  //     video,
  //     (result) => {
  //       props.onSuccess(scanner.$canvas.toDataURL());
  //     },
  //     (error) => {
  //       // console.log(error);
  //     }
  //   );
  //   scanner.start().then(() => {
  //     console.log("Start qr scanner");
  //   });
  // });

  // const height = document.body.clientHeight - 74;
  // return <video id="qr-video" style={{ height }}></video>;

  const handleScan = (data) => {
    console.log("scanned");
    console.log(data);
    if (data) {
      const els = document.getElementsByTagName("Canvas");
      console.log(els[0].toDataURL());
      props.onSuccess(els[0].toDataURL());
    }
  };
  const handleError = (err) => {
    console.error(err);
  };

  const height = document.body.clientHeight - 74;
  return (
    <QrReader
      style={{ width: "100%", height }}
      delay={500}
      onError={handleError}
      onScan={handleScan}
    />
  );
}

export default QrCodePage;
