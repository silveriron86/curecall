import styled from "@emotion/styled";
import { Modal, Button } from "antd";

export const DatesPickerModal = styled(Modal)({
  ".ant-modal-content": {
    backgroundColor: "#0800FF",
    borderRadius: 20,
    overflow: "hidden",
    ".ant-modal-body": {
      padding: 0,
      ".close-btn": {
        top: 23,
        left: 28.5,
      },
      ".valider": {
        background: "transparent !important",
        float: "right",
        padding: "0 24px",
        marginTop: 20,
        span: {
          color: "white",
        },
      },
      ".label": {
        fontFamily: "Nexa Heavy",
        marginTop: 40,
        marginLeft: 27,
      },
      ".selected-dates": {
        fontFamily: "Nexa Heavy",
        fontSize: 25,
        lineHeight: "38px",
        color: "#FDFF00",
      },
      ".edit-btn": {},
      ".rdrMonthAndYearWrapper": {
        // display: "none",
        ".rdrPprevButton i, .rdrNextPrevButton i": {
          transform: "translate(-3px, 0px) scale(2)",
        },
        ".rdrNextPrevButton.rdrNextButton": {
          paddingLeft: 8,
          marginLeft: 0,
        },
      },
      ".rdrDateDisplayWrapper": {
        display: "none",
        backgroundColor: "#0800FF",
      },
      ".rdrDateRangeWrapper": {
        textTransform: "capitalize",
        width: "100%",
        ".rdrMonth": {
          height: "auto !important",
        },
        ".rdrDayPassive": {
          visibility: "hidden",
        },
        ".rdrMonthAndYearPickers": {
          select: {
            fontFamily: "Nexa Heavy",
            fontSize: 17,
            lineHeight: "26px",
            color: "#A5A5A5",
          },
        },
        ".rdrDay": {
          width: 43,
          height: 43,
          marginTop: 2,
          marginBottom: 2,
          "&.rdrDayEndOfWeek": {
            ".rdrInRange": {
              marginRight: -10.75,
              borderRadius: 21.5,
            },
          },
          "&.rdrDayStartOfWeek": {
            ".rdrInRange": {
              marginLeft: -10.75,
              borderRadius: 21.5,
            },
          },
        },
        ".rdrWeekDay": {
          fontFamily: "Nexa Heavy",
          fontSize: 13,
          lineHeight: "20px",
          color: "#A5A5A5",
        },
        ".rdrDayNumber": {
          zIndex: 2,
          span: {
            fontFamily: "Nexa Regular",
            fontSize: 18,
            lineHeight: "27px",
            color: "#0800FF",
          },
        },
        ".rdrInRange": {
          color: "#E5E5FF !important",
          height: 43,
          top: 0,
          marginLeft: -21.5,
          marginRight: -21.5,
          zIndex: 0,
        },
        ".rdrStartEdge, .rdrEndEdge": {
          width: 43,
          height: 43,
          borderRadius: 21.5,
          top: 0,
          color: "#0800FF !important",
          zIndex: 1,
          "&+span": {},
          "&+.rdrDayNumber, &+span+.rdrDayNumber": {
            span: {
              color: "white !important",
            },
          },
        },
        ".rdrDayToday .rdrDayNumber span:after": {
          display: "none",
        },
        ".rdrDayEndOfMonth": {
          ".rdrInRange": {
            marginRight: -10,
            borderRadius: 21.5,
          },
        },
        ".rdrDayStartOfMonth": {
          ".rdrInRange": {
            marginLeft: -10,
            borderRadius: 21.5,
          },
        },
        ".rdrMonthName": {
          fontFamily: "Nexa Heavy",
          fontSize: 17,
          lineHeight: "26px",
          color: "#A5A5A5",
        },
      },
      ".dates-col": {
        width: 130,
        ".label": {
          fontFamily: "Nexa Heavy",
          fontSize: 18,
          lineHeight: "27px",
          color: "#0800FF",
          marginLeft: 0,
          display: "flex",
          height: 30,
          alignItems: "flex-end",
          marginTop: 5,
          marginBottom: 3,
        },
        input: {
          fontFamily: "Nexa Regular",
          fontSize: 17,
          lineHeight: "27px",
          textAlign: "center",
          width: 130,
          height: 47,
          borderRadius: 8,
          borderColor: "#0800FF",
          borderWidth: 1,
          outline: "none",
        },
        "&.end": {
          ".label": {
            color: "#A5A5A5",
            fontSize: 15,
            lineHeight: "22px",
          },
          input: {
            borderColor: "#A5A5A5",
            color: "#A5A5A5",
          },
        },
      },
    },
  },
});

export const DatesPickerInput = styled("div")({
  position: "relative",
  img: {
    position: "absolute",
    top: 17.8,
    right: 15.5,
  },
});

export const CommencerWrapper = styled("div")({
  position: "absolute",
  bottom: 60,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
});

export const AddWidget = styled(Button)({
  backgroundColor: "white",
  width: 108,
  height: 70,
  borderTopRightRadius: 17,
  borderBottomLeftRadius: 17,
  float: "right",
  marginBottom: 10,
  right: 0,
  padding: "7px 7px 0 11px",
  position: "fixed",
  zIndex: 1,
  img: {
    position: "absolute",
    top: 7,
    left: 11,
  },
  p: {
    color: "#0800FF !important",
    textAlign: "right",
    fontSize: "12px !important",
    lineHeight: "16px",
    marginTop: "-2px !important",
  },
});

export const YellowBlock = styled("div")({
  backgroundColor: "#FDFF00",
  // height: 27,
  padding: "7px 0",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  marginBottom: 13,
  p: {
    color: "#0800FF !important",
    margin: 0,
    padding: 0,
    marginTop: -3,
  },
});
