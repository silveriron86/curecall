import { combineReducers } from "redux";
import { routerReducer as routing } from "react-router-redux";
import AccountReducer from "./AccountReducer";
import FormReducer from './FormReducer';

const rootReducer = combineReducers({
  AccountReducer,
  FormReducer,
  routing,
});

export default rootReducer;
