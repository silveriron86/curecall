import AccountConstants from "../constants/AccountConstants";
// import Immutable from "immutable";

const initialState = {
  userData: {},
  error: null,
  treatments: [],
};

function AccountReducer(state = initialState, action) {
  switch (action.type) {
    case AccountConstants.LGOIN:
      return Object.assign({}, state, {
        userData: {},
        error: null,
      });
    case AccountConstants.LOGIN_SUCCESS:
      return Object.assign({}, state, {
        userData: action.response,
        error: null,
      });
    case AccountConstants.LOGIN_ERROR:
      return Object.assign({}, state, {
        userData: {},
        error: action.error,
      });

    case AccountConstants.GET_TREATMENTS_SUCCESS:
      return Object.assign({}, state, {
        treatments: action.response,
        error: null,
      });
    case AccountConstants.GET_TREATMENTS_ERROR:
      return Object.assign({}, state, {
        treatments: [],
        error: action.error,
      });

    default:
      return state;
  }
}

export default AccountReducer;
