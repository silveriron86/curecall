import FormConstants from "../constants/FormConstants";
import Immutable from "immutable";

const initialState = new Immutable.Map({
  form: null,
  error: null,
});

function FormReducer(state = initialState, action) {
  switch (action.type) {
    case FormConstants.GET_FORM_SUCCESS:
      return Object.assign({}, state, {
        form: action.response,
        error: null,
      });
    case FormConstants.GET_FORM_ERROR:
      return Object.assign({}, state, {
        form: null,
        error: action.error,
      });

    default:
      return state;
  }
}

export default FormReducer;
