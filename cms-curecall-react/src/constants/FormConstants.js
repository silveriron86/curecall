module.exports = {
	GET_FORM_SUCCESS: 'GET_FORM_SUCCESS',
	GET_FORM_ERROR: 'GET_FORM_ERROR',
	POST_FORM_SUCCESS: 'POST_FORM_SUCCESS',
	POST_FORM_ERROR: 'POST_FORM_ERROR'
};
