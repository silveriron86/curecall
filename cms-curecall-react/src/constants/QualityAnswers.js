module.exports = [
  ["Pas du tout", "Un petit peu", "Moyennement", "Beaucoup", "Enormément"],
  ["Pas du tout", "Un petit peu", "Moyennement", "Beaucoup", "Enormément"],
  ["Jamais", "Rarement", "De temps en temps", "Souvent", "Trés souvent"],
  ["Pas du tout", "Un petit peu", "Moyennement", "Beaucoup", "Enormément"],
  [], // empty
  [
    // "Je n’ai pas le permis",
    "Non, pas du tout",
    "Un petit peu",
    "Moyennement",
    "Beaucoup",
    "Enormement, je ne conduis plus",
  ],
  [
    // "Je n’ai pas le permis",
    "Non, pas du tout",
    "Un petit peu",
    "Moyennement",
    "Beaucoup",
    "Enormement, je ne conduis plus",
  ],
  [], // empty
  ["Pas du tout", "Un petit peu", "Moyennement", "Beaucoup", "Enormément"],
  ["Jamais", "Rarement", "Parfois", "Souvent", "Toujours"],
  ["Pas du tout", "Un petit peu", "Moyennement", "Beaucoup", "Enormément"],
  [], // empty
  [
    "Pas du tout vrai pour moi",
    "Plutôt pas vrai pour moi",
    "Plutôt vrai pour moi",
    "Tout à fait vrai pour moi",
  ],
  [
    "Pas du tout vrai pour moi",
    "Plutôt pas vrai pour moi",
    "Plutôt vrai pour moi",
    "Tout à fait vrai pour moi",
  ],
  [
    "Pas du tout vrai pour moi",
    "Plutôt pas vrai pour moi",
    "Plutôt vrai pour moi",
    "Tout à fait vrai pour moi",
  ],
  [], // empty
  ["Jamais", "Rarement", "De temps en temps", "Souvent", "Trés souvent"],
  ["Jamais", "Rarement", "De temps en temps", "Souvent", "Trés souvent"],
  ["Jamais", "Rarement", "De temps en temps", "Souvent", "Trés souvent"],
  [], // empty
  [
    "Normal, ça fait partie de mon quotidien",
    "Normal avec quelques oublis",
    "Un problème",
    "Un gros problème",
    "Un très gros problème",
  ],
  [
    "Pas de problème, j’ai l’habitude",
    "J’ai l’habitude mais parfois ça tombe à côté",
    "C’est un problème",
    "C’est un gros problème",
    "C’est un enfer",
  ],
  [
    "Normal, ça fait partie de mon quotidien",
    "Normal avec quelques oublis",
    "Un problème",
    "Un gros problème",
    "Un très gros problème",
  ],
  [], // empty
  [
    // "Je suis pas concerné(e)",
    "Tout à fait vrai pour moi",
    "Plutôt vrai pour moi",
    "Plutôt pas vrai pour moi",
    "Pas du tout vrai pour moi",
  ],
  [
    "Tout à fait vrai pour moi",
    "Plutôt vrai pour moi",
    "Plutôt pas vrai pour moi",
    "Pas du tout vrai pour moi",
  ],
  [
    "Tout à fait vrai pour moi",
    "Plutôt vrai pour moi",
    "Plutôt pas vrai pour moi",
    "Pas du tout vrai pour moi",
  ],
];
