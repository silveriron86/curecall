// New quality-life form

module.exports = [
  {
    question: "Avez-vous des difficultés pour lire ou pour écrire ?",
    variableName: "difficultyReadAndWrite",
    answers: ["OUI", "NON", "J’ai besoin d’un aidant"],
  },
  {
    question:
      "Quand vous croisez des gens que vous connaissez, vous arrive- t-il de ne pas les voir ?",
    variableName: "difficultySeeingKnownPeople",
    answers: ["OUI", "NON"],
  },
  {
    question:
      "Avez-vous le sentiment de devoir consacrer plus de temps à réaliser vos activités quotidiennes (préparation, prise des repas, conduire...) ?",
    variableName: "difficultyDoingDailyActivities",
    answers: ["OUI", "NON", "J’ai besoin d’un aidant"],
  },
  {
    question:
      "Avez-vous des difficultés à utiliser votre téléphone, votre ordinateur ou tout appareil de communication ?",
    variableName: "difficultyLookingScreens",
    answers: ["OUI", "NON", "J’ai besoin d’un aidant"],
  },
  {
    question:
      "Avez-vous tendance à casser des choses, verser à côté, renverser ou à vous cogner ?",
    variableName: "difficultyGesturalSkill",
    answers: ["OUI", "NON", "J’ai besoin d’un aidant"],
  },
  {
    question: "Avez-vous des difficultés à vous déplacer chez vous ?",
    variableName: "difficultyMoveInside",
    answers: ["OUI", "NON", "J’ai besoin d’un aidant"],
  },
  {
    question: "Avez-vous des difficultés à vous déplacer en extérieur ?",
    variableName: "difficultyMoveOutside",
    answers: ["OUI", "NON", "J’ai besoin d’un aidant"],
  },
  {
    question:
      "Avez-vous des difficultés à prendre votre traitement tous les jours ?",
    variableName: "difficultyTakeTreatmentEveryDay",
    answers: ["OUI", "NON", "J’ai besoin d’un aidant"],
  },
  {
    question:
      "Avez-vous des difficultés à utiliser votre traitement correctement (j’en fais tomber à côté…)?",
    variableName: "difficultyUsingTreatment",
    answers: ["OUI", "NON", "J’ai besoin d’un aidant"],
  },
  {
    question:
      "Avez-vous des difficultés à vous adapter aux changements de traitement ?",
    variableName: "difficultyAdaptingTreatmentChange",
    answers: ["OUI", "NON", "J’ai besoin d’un aidant"],
  },
  {
    question: "Avez -vous le sentiment que votre traitement est efficace ?",
    variableName: "effectiveTreatment",
    answers: [
      "OUI",
      "NON",
      "J’ai besoin de plus d’information sur mon traitement",
    ],
  },
  {
    question:
      "Avez-vous le sentiment que les gens autour de vous ne comprennent pas vos difficultés visuelles ?",
    variableName: "difficultyToBeUnderstoodByOthers",
    answers: ["OUI", "NON"],
  },
  {
    question:
      "Vous sentez-vous découragé(e), déprimé(e) ou fragile à cause de vos problèmes de vue ?",
    variableName: "feelDiscouragedFragile",
    answers: ["OUI", "NON"],
  },
  {
    question: "Êtes vous satisfait de votre prise en charge ?",
    variableName: "careSatisfaction",
    answers: ["OUI", "NON"],
  },
];
