module.exports = [
  [
    {
      label: "VRSK",
      values: ["RTYH", "APDF", "VRSK", "VPDF"],
    },
    {
      label: "NHCS",
      values: ["NHCS", "AODF", "VRSK", "VPDF"],
    },
    {
      label: "CNHZ",
      values: ["CTYH", "CNHZ", "DRSK", "VPDF"],
    },
    {
      label: "YTRI",
      values: ["YTRI", "MHGR", "NODV", "GVFS"],
    },
    {
      label: "CDNZ",
      values: ["CDNZ", "TTXC", "FTRZ", "LKNB"],
    },
    {
      label: "KCHO",
      values: ["NODV", "CNHZ", "CDNZ", "KCHO"],
    },
    {
      label: "SVUD",
      values: ["NODV", "SVUD", "RSMH", "VPDF"],
    },
    {
      label: "RSZH",
      values: ["RSZH", "CNHZ", "CDNZ", "KCHO"],
    },
  ],
  [
    {
      label: "GAUN",
      values: ["RTYH", "GAUN", "OCRM", "VPDF"],
    },
    {
      label: "OCRM",
      values: ["OCRM", "AODF", "VRSK", "VPDF"],
    },
    {
      label: "KTIE",
      values: ["CTYH", "CNHZ", "KTIE", "VPSK"],
    },
    {
      label: "AELS",
      values: ["AELS", "CNHZ", "DRSK", "VPDF"],
    },
    {
      label: "YPTU",
      values: ["YPTU", "CNHZ", "CDNZ", "VPDF"],
    },
    {
      label: "RVZO",
      values: ["NODV", "CNHZ", "RVZO", "KCHO"],
    },
    {
      label: "HCOR",
      values: ["NODV", "RSZH", "HCOR", "VPDF"],
    },
    {
      label: "OKSV",
      values: ["NODV", "RSZH", "OKSV", "VPDF"],
    },
  ],
];
