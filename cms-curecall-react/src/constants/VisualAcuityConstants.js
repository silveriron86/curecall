module.exports = [
  [
    {
      label: "A",
      values: "HJBAUH",
    },
    {
      label: "U",
      values: "HJKAUP",
    },
    {
      label: "A",
      values: "PYQBAH",
    },
    {
      label: "U",
      values: "WLDPJU",
    },
    {
      label: "K",
      values: "EIDAJK",
    },
    {
      label: "P",
      values: "OPFAYC",
    },
    {
      label: "H",
      values: "LVHTVB",
    },
  ],
  [
    {
      label: "B",
      values: "HJBAUH",
    },
    {
      label: "P",
      values: "HJKAUP",
    },
    {
      label: "H",
      values: "HJLAUM",
    },
    {
      label: "S",
      values: "ZJKASP",
    },
    {
      label: "H",
      values: "HJKAUP",
    },
    {
      label: "U",
      values: "HJKAUP",
    },
    {
      label: "K",
      values: "HJKAUI",
    },
  ],
  [
    {
      label: "H",
      values: "HJBAUP",
    },
    {
      label: "J",
      values: "HJKAUP",
    },
    {
      label: "I",
      values: "HJLIUT",
    },
    {
      label: "O",
      values: "DJKAUO",
    },
    {
      label: "J",
      values: "HJKAUP",
    },
    {
      label: "I",
      values: "HJKIUP",
    },
    {
      label: "X",
      values: "XJKAUP",
    },
  ],
];
