import axios from "axios";
import { ApiPathConstants, FormConstants } from "../constants";

let FormActions = {
  getFormError: function (error) {
    return {
      error,
      type: FormConstants.GET_FORM_ERROR,
    };
  },
  getFormSuccess: function (response) {
    return {
      response,
      type: FormConstants.GET_FORM_SUCCESS,
    };
  },
  getForm: function (pinCode, cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}public/patients/form?pin=${pinCode}`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          dispatch(this.getFormSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.getFormError(error.response));
          cb(error.response);
        });
    };
  },

  postFormError: function (error) {
    return {
      error,
      type: FormConstants.POST_FORM_ERROR,
    };
  },
  postFormSuccess: function (response) {
    return {
      response,
      type: FormConstants.POST_FORM_SUCCESS,
    };
  },
  postForm: function (data, cb) {
    console.log(`${ApiPathConstants.getApiPath()}public/patients/form`, data);
    return (dispatch) => {
      axios({
        method: "POST",
        url: `${ApiPathConstants.getApiPath()}public/patients/form`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        data,
      })
        .then((response) => {
          dispatch(this.postFormSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.postFormError(error.response));
          cb(error.response);
        });
    };
  },
};

export default FormActions;
