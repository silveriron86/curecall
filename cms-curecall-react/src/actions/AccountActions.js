import axios from "axios";
import { ApiPathConstants, AccountConstants } from "../constants";

let AccountActions = {
  loginError: function (error) {
    return {
      error,
      type: AccountConstants.LOGIN_ERROR,
    };
  },
  loginSuccess: function (response) {
    return {
      response,
      type: AccountConstants.LOGIN_SUCCESS,
    };
  },
  login: function (data, cb) {
    return (dispatch) => {
      axios({
        method: "POST",
        url: `${ApiPathConstants.getApiPath()}users/login`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        data,
      })
        .then((response) => {
          dispatch(this.loginSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.loginError(error.response));
          cb(error.response);
        });
    };
  },

  tradePinError: function (error) {
    return {
      error,
      type: AccountConstants.TRADE_PIN_ERROR,
    };
  },
  tradePinSuccess: function (response) {
    return {
      response,
      type: AccountConstants.TRADE_PIN_SUCCESS,
    };
  },
  tradePin: function (pin, cb) {
    return (dispatch) => {
      axios({
        method: "POST",
        url: `${ApiPathConstants.getApiPath()}patients/trade-pin`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        data: { pin },
      })
        .then((response) => {
          dispatch(this.tradePinSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.tradePinError(error.response));
          cb(error.response);
        });
    };
  },

  postTreatmentError: function (error) {
    return {
      error,
      type: AccountConstants.POST_TREATMENT_ERROR,
    };
  },
  postTreatmentSuccess: function (response) {
    return {
      response,
      type: AccountConstants.POST_TREATMENT_SUCCESS,
    };
  },
  postTreatment: function (data, cb) {
    return (dispatch) => {
      axios({
        method: "POST",
        url: `${ApiPathConstants.getApiPath()}patients/treatments`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("TOKEN")}`,
        },
        data,
      })
        .then((response) => {
          dispatch(this.postTreatmentSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.postTreatmentError(error.response));
          cb(error.response);
        });
    };
  },

  getTreatmentsError: function (error) {
    return {
      error,
      type: AccountConstants.GET_TREATMENTS_ERROR,
    };
  },
  getTreatmentsSuccess: function (response) {
    return {
      response,
      type: AccountConstants.GET_TREATMENTS_SUCCESS,
    };
  },
  getTreatments: function (cb) {
    return (dispatch) => {
      axios({
        method: "GET",
        url: `${ApiPathConstants.getApiPath()}patients/treatments`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("TOKEN")}`,
        },
      })
        .then((response) => {
          dispatch(this.getTreatmentsSuccess(response.data.patientTreatments));
          cb(response.data.patientTreatments);
        })
        .catch((error) => {
          dispatch(this.getTreatmentsError(error.response));
          cb(error.response);
        });
    };
  },

  deleteTreatmentError: function (error) {
    return {
      error,
      type: AccountConstants.DELETE_TREATMENT_ERROR,
    };
  },
  deleteTreatmentSuccess: function (response) {
    return {
      response,
      type: AccountConstants.DELETE_TREATMENT_SUCCESS,
    };
  },
  deleteTreatment: function (id, cb) {
    return (dispatch) => {
      axios({
        method: "DELETE",
        url: `${ApiPathConstants.getApiPath()}patients/treatments/${id}`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("TOKEN")}`,
        },
      })
        .then((response) => {
          dispatch(this.deleteTreatmentSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.deleteTreatmentError(error.response));
          cb(error.response);
        });
    };
  },

  updateTreatmentError: function (error) {
    return {
      error,
      type: AccountConstants.UPDATE_TREATMENT_ERROR,
    };
  },
  updateTreatmentSuccess: function (response) {
    return {
      response,
      type: AccountConstants.UPDATE_TREATMENT_SUCCESS,
    };
  },
  updateTreatment: function (id, data, cb) {
    return (dispatch) => {
      axios({
        method: "PATCH",
        url: `${ApiPathConstants.getApiPath()}patients/treatments/${id}`,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("TOKEN")}`,
        },
        data,
      })
        .then((response) => {
          dispatch(this.updateTreatmentSuccess(response.data));
          cb(response.data);
        })
        .catch((error) => {
          dispatch(this.updateTreatmentError(error.response));
          cb(error.response);
        });
    };
  },
};

export default AccountActions;
