import React from "react";
import { PageNoWrapper } from "./_styles";
import LineIcon from "../assets/svg/Line 2.svg";

function PageNo(props) {
  const { value, total } = props;
  return (
    <PageNoWrapper>
      <div className="relative">
        <span className="value">{value}</span>
        <img src={LineIcon} alt="" />
        <span className="total">{total}</span>
      </div>
    </PageNoWrapper>
  );
}

export default PageNo;
