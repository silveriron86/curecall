import React from "react";
import { HeaderWrapper } from "./_styles";
import securityIcon from "../assets/svg/SecurityBlue.svg";

function Header() {
  return (
    <HeaderWrapper>
      <img src={securityIcon} width="50" height="56" alt="" />
      <span className="theme-blue">Connexion à CureCall</span>
    </HeaderWrapper>
  );
}

export default Header;
