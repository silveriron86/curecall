import styled from "@emotion/styled";
import { Button, Input } from "antd";
import Modal from "antd/lib/modal/Modal";
import ColorConstants from "../constants/ColorConstants";

export const HeaderWrapper = styled("div")({
  display: "flex",
  "&.center": {
    alignItems: "center",
  },
  "&.full-center": {
    width: "100%",
    height: "100vh",
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    span: {
      width: 302,
      textAlign: "center",
    },
  },
  img: {
    marginRight: 17.4,
  },
  span: {
    width: 189,
    fontFamily: "Luciole Bold",
    // fontWeight: 900,
    fontSize: 25,
    lineHeight: "33px",
    textAlign: "left",
    color: "#fff",
    "&.full": {
      width: "fit-content",
    },
  },
  ".block": {
    display: "block",
  },
  "&.has-quote": {
    display: "block",
    ".full": {
      display: "block",
      marginTop: -20,
    },
    ".f50.absolute": {
      marginTop: 10,
      marginLeft: 10,
    },
  },
});

export const ThemeOutlineButton = styled(Button)({
  border: `2px solid ${ColorConstants.BLUE} !important`,
  borderRadius: 30,
  height: 65,
  width: "100%",
  span: {
    color: ColorConstants.BLUE,
    fontSize: 25,
    lineHeight: "33px",
    fontFamily: "Luciole Bold",
    marginTop: 3,
  },
  "&:hover, &:focus": {
    background: `${ColorConstants.BLUE} !important`,
    span: {
      color: "white",
    },
  },
  "&.narrow": {
    span: {
      letterSpacing: "-2px",
    },
  },
  "&.two-lines": {
    span: {
      fontSize: 17,
      lineHeight: "22px",
      whiteSpace: "normal",
    },
  },
  "&.error": {
    height: 65,
    minHeight: 65,
    width: "100%",
    // height: 42.64,
    // width: "auto",
    // padding: "0 22.6px",
    span: {
      marginTop: 6,
      // fontFamily: "Luciole Bold",
      // fontSize: 18,
      // lineHeight: "26px",
    },
  },
  "&:disabled": {
    background: `white !important`,
    span: {
      color: ColorConstants.BLUE,
    },
  },
});

export const ThemeButton = styled(Button)({
  height: 56,
  maxWidth: "100%",
  background: "#00fffd",
  outline: "none",
  border: "none",
  boxShadow: "none",
  padding: "0 32px",
  span: {
    fontFamily: "Nexa Heavy",
    fontWeight: 900,
    fontSize: 25,
    lineHeight: "25px",
    textAlign: "center",
    color: "#0800ff",
    whiteSpace: "normal",
    maxWidth: "100%",
  },
  "&.blue": {
    background: ColorConstants.BLUE,
    height: 65,
    minHeight: 65,
    width: "100%",
    "&.ant-btn-round": {
      borderRadius: 33,
    },
    span: {
      marginTop: 6,
      fontFamily: "Luciole Bold",
      color: "white",
      lineHeight: "33px",
      // textTransform: "uppercase",
    },
    "&:hover, &:focus": {
      background: `${ColorConstants.BLUE} !important`,
    },
    "&.two-lines": {
      height: 65,
      padding: "0 31px",
      span: {
        fontSize: 18,
        lineHeight: "22px",
      },
    },
  },
  "&.error": {
    backgroundColor: "#FDFF00",
    span: {
      color: "#0800FF",
    },
  },
  "&.pink": {
    backgroundColor: "#DE53FF",
    span: {
      color: "#0800FF",
    },
    "&:hover, &:focus": {
      background: "#DE53FF !important",
    },
  },
  "&.supprimer": {
    width: 118,
    height: 35,
    padding: 0,
    span: {
      fontSize: 18,
      lineHeight: "27px",
    },
  },
  "&.ant-btn-round": {
    borderRadius: 28,
  },
  "&:hover, &:focus": {
    background: "#00fffd !important",
  },
  "&.text": {
    backgroundColor: "transparent !important",
    span: {
      fontSize: 18,
      lineHeight: "27px",
    },
    "&.pink": {
      span: {
        color: "#DE53FF",
      },
    },
    "&.blue": {
      span: {
        color: "#0800FF",
      },
    },
  },
});

export const ThemeInput = styled(Input)({
  height: 66,
  borderRadius: 5,
  fontFamily: "Nexa Heavy",
  fontWeight: 900,
  fontSize: 18,
  lineHeight: "25px",
  color: "#0800ff",
  outline: "none",
  border: "none",
  "&::placeholder": {
    color: "#0800FF30",
  },
  "&.question": {
    width: 75,
    height: 50,
    textAlign: "center",
    fontSize: 25,
  },
  "&.drug-name": {
    marginTop: 108,
    paddingLeft: 21,
    "&::placeholder": {
      fontSize: 15,
      lineHeight: "25px",
    },
  },
  "&.date": {
    "&::placeholder": {
      fontSize: "11px !important",
    },
  },
});

export const ThemeIconButton = styled(Button)({
  padding: 0,
  backgroundColor: "transparent",
  boxShadow: "none",
  outline: "none",
  border: "none",
  display: "flex",
  alignItems: "center",
  height: "fit-content",
  "&::after": {
    display: "none !important",
  },
  span: {
    fontFamily: "Nexa BoldItalic",
    fontWeight: "bold",
    fontStyle: "italic",
    fontSize: 16,
    lineHeight: "22px",
    textAlign: "left",
    color: "#00fffd",
    marginLeft: 6.6,
  },
  "&.laugh": {
    width: 126,
    display: "block",
    margin: "0 auto",
    marginTop: 46,
    backgroundColor: "transparent !important",
    p: {
      marginTop: "9px !important",
      display: "block",
      whiteSpace: "normal",
      lineHeight: "18px !important",
    },
  },
  "&.close-btn": {
    position: "absolute",
    top: 68,
    left: 40,
  },
  "&.listen-btn": {
    position: "absolute",
    top: 70,
    right: 37,
  },
  "&.in-footer": {
    position: "fixed",
    bottom: 23.5,
    top: "auto",
    right: "auto",
    left: "calc(50% - 21px)",
    "&.in-content": {
      top: "auto",
    },
  },
  "&.in-content": {
    position: "relative",
    top: 20.8,
    bottom: "auto",
    right: "auto",
    left: "auto",
  },
});

export const PageNoWrapper = styled("div")({
  width: 59,
  height: 72,
  position: "absolute",
  top: 39,
  right: 37,
  ".relative": {
    position: "relative",
    img: {
      position: "absolute",
      marginTop: 10.5,
      right: 12.5,
    },
    span: {
      color: "white",
      fontFamily: "Nexa Heavy",
      fontWeight: 900,
      fontSize: 35,
      position: "absolute",
      "&.value": {},
      "&.total": {
        marginTop: 27,
        right: 0,
      },
    },
  },
});

export const TickCol = styled("div")({
  flex: 1,
  height: 6,
  borderLeft: "2px solid white",
  "&.last": {
    borderRight: "2px solid white",
  },
});

export const answerTextAreaStyle = {
  width: 250,
  height: 84,
  border: "1px solid white",
  backgroundColor: "transparent",
  marginLeft: 11,
  color: "white",
  fontFamily: "Nexa Heavy",
  fontSize: 16,
  lineHeight: "25px",
};

export const LoadingWrapper = styled(Modal)({
  ".ant-modal-content": {
    width: "100%",
    overflow: "hidden",
    backgroundColor: "transparent",
    boxShadow: "none",
    padding: 0,
    ".ant-modal-body": {
      padding: 0,
      img: {
        margin: "0 auto",
        display: "block",
        width: "100%",
      },
    },
  },
  ".ant-modal-close": {
    display: "none",
  },
});

export const BlurWrapper = styled("div")({
  width: "100%",
  height: "100%",
  position: "absolute",
  zIndex: "1000",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  background: " rgba(255,255,255,0.3)",
  backdropFilter: "blur(1px)",
});
