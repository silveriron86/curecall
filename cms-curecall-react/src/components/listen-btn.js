import React, { useState } from "react";
import { ThemeIconButton } from "./_styles";
import listenCloseIcon from "../assets/svg/Listen close.svg";
import listenOpenIcon from "../assets/svg/Listen Open.svg";

function ListenBtn(props) {
  const [isListenOpened, setListen] = useState(false);
  const { inFooter, inContent, onListen } = props;

  const toggleListen = () => {
    setListen(!isListenOpened);
    if (onListen) {
      onListen(!isListenOpened);
    }
  };

  return (
    <ThemeIconButton
      className={`listen-btn ${inFooter ? "in-footer" : ""} ${
        inContent ? "in-content" : ""
      }`}
      onClick={toggleListen}
    >
      <img src={isListenOpened ? listenOpenIcon : listenCloseIcon} alt="" />
    </ThemeIconButton>
  );
}

export default ListenBtn;
