import React from "react";
import { Slider } from "antd";
import { Flex } from "../containers/_styles";
import { TickCol } from "./_styles";
import { QualityAnswers } from "../constants";

function TickSlider(props) {
  const { pageIndex } = props;
  const max = QualityAnswers[pageIndex].length - 1;
  console.log("slider index = ", pageIndex);
  console.log("max = ", max);
  let cols = [];
  for (let i = 0; i < max; i++) {
    cols.push(
      <TickCol key={`tick-${i}`} className={i === max - 1 ? "last" : ""} />
    );
  }

  const { value } = props;

  return (
    <>
      <p className="f18">{QualityAnswers[pageIndex][value]}</p>
      <div
        style={{
          padding: "0 10px",
        }}
      >
        <Slider
          className="yellow-slider"
          value={value}
          min={0}
          max={max}
          tooltipVisible={false}
          onChange={props.onChange}
        />
      </div>
      <Flex style={{ margin: "4px -1px 0 -1px" }}>{cols}</Flex>
    </>
  );
}

export default TickSlider;
