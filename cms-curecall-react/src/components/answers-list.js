import React from "react";
import { useState, useEffect } from "react";
import { answers } from "../constants/answers";
import { Button, Input } from "antd";
import { Answer } from "../containers/_styles";
import { answerTextAreaStyle } from "./_styles";
const { TextArea } = Input;

function AnswersList(props) {
  const [selectedAnswer, selectAnswer] = useState([]);
  const {
    isListenOpened,
    defaultValue,
    category,
    index,
    data,
    layout,
    multiple,
    answer,
    strings,
    onChangeString,
  } = props;

  const onSelect = (answerIndex) => {
    const selected = [...selectedAnswer];
    const found = selected.indexOf(answerIndex);
    if (found >= 0) {
      selected.splice(found, 1);
    } else {
      if (multiple || selected.length === 0) {
        selected.push(answerIndex);
      } else {
        selected[0] = answerIndex;
      }
    }
    selectAnswer(selected);
    if (props.onSelect) {
      props.onSelect(
        multiple ? selected : selected.length > 0 ? selected[0] : -1
      );
    }
  };

  useEffect(() => {
    selectAnswer([]);
    if (defaultValue >= 0) {
      selectAnswer([defaultValue]);
    }
  }, [index, defaultValue]);

  useEffect(() => {
    selectAnswer(Array.isArray(answer) ? answer : [answer]);
  }, [answer]);

  // console.log("selected naswer = ", selectedAnswer);
  // console.log(answers, category, index);
  const answersList = data ? data : answers[category][index];
  // console.log("********", answersList);
  if (answersList.length === 0) {
    return null;
  }

  return (
    <div
      className={`answers ${isListenOpened ? "large" : ""} ${
        layout === "horizontal" ? "flex space-between" : ""
      }`}
    >
      {answersList.map((a, i) => {
        const value = category === 7 && index === 0 ? [1, 0][i] : i;
        let charCode = 65 + i;
        if (category === 8 && index === 0 && i >= 10) {
          charCode -= 6;
        }
        return (
          <Answer
            key={`answer-${i}`}
            className={`${a.length < 35 ? "one" : ""} ${
              isListenOpened ? "large" : ""
            }
              ${
                category === 8 && index === 0 && i >= 4 && i < 10
                  ? "obsolete"
                  : ""
              }
            `}
          >
            <Button
              onClick={() => onSelect(value)}
              className={selectedAnswer.indexOf(value) >= 0 ? "selected" : ""}
            >
              {String.fromCharCode(charCode)}
            </Button>
            {category === 8 &&
            selectedAnswer.indexOf(i) >= 0 &&
            i === answersList.length - 1 ? (
              <TextArea
                style={answerTextAreaStyle}
                value={strings[index]}
                onChange={onChangeString}
              />
            ) : (
              <div className="text">
                {a}
                {((i === 16 && index === 0) || (i === 17 && index === 1)) &&
                strings[index] !== ""
                  ? ` : ${strings[index]}`
                  : ""}
              </div>
            )}
          </Answer>
        );
      })}
    </div>
  );
}

export default AnswersList;
