import React from "react";
import { Router, Route /*, Redirect*/ } from "react-router";
// import Utils from "./utils";
import history from "./store/history";
import LoginPage from "./containers/login";
import EvaluationPage from "./containers/evaluation";
import TreatmentPage from "./containers/treatment";
import QualityPage from "./containers/quality";
import SecondariesPage from "./containers/secondaires";
import NotionPage from "./containers/notion";
import HomePage from "./containers/home";
import VisualAcuityPage from "./containers/visualAcuity";
import ContrastPerceptionPage from "./containers/contrastPerception";
import AmslerGridPage from "./containers/amslerGrid";
import QualityLifePage from "./containers/quality-life";

export default (
  <Router history={history}>
    <Route exact path="/" component={HomePage} />
    {/* <Route
      path="/main"
      render={() =>
        Utils.AuthHelper.isLoggedIn() ? <MainPage /> : <Redirect to="/login" />
      }
    /> */}
    <Route exact path="/form" component={LoginPage} />
    <Route path="/form/visual-function" component={EvaluationPage} />
    <Route path="/form/treatment" component={TreatmentPage} />
    <Route path="/form/quality-of-life" component={QualityPage} />
    <Route path="/form/side-effects" component={SecondariesPage} />
    <Route path="/form/visual-acuity" component={VisualAcuityPage} />
    <Route path="/form/quality-life" component={QualityLifePage} />
    <Route
      path="/form/contrast-perception"
      component={ContrastPerceptionPage}
    />
    <Route path="/form/amsler-grid" component={AmslerGridPage} />

    <Route path="/notion/:pageId" component={NotionPage} />
  </Router>
);
