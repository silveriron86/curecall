import moment from "moment";
import { notification } from "antd";

let Utils = {
  AuthHelper: {
    isLoggedIn: () => {
      console.log(localStorage.getItem("TOKEN"));
      return localStorage.getItem("TOKEN");
    },
  },

  capitalize: (s) => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.toLowerCase().slice(1);
  },

  getCurrentUser: () => {
    const USER = localStorage.getItem("USER");
    let user = null;
    if (USER) {
      user = JSON.parse(USER);
    }
    return user;
  },

  getPhone: () => {
    const user = Utils.getCurrentUser();
    let phone = "";
    if (user && user.phone) {
      phone = user.phone.replace("+33", "0");
      const tmp = phone.match(/.{1,2}/g);
      phone = tmp.join(" ");
    }
    return phone;
  },

  getAge: (birthdate) => {
    return moment().diff(moment(birthdate), "years");
  },

  getHeader: () => {
    return {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("TOKEN")}`,
    };
  },

  pad: (num, size) => {
    num = num.toString();
    while (num.length < size) num = "0" + num;
    return num;
  },

  formatDate: (val) => {
    return moment(val).format("DD/MM/YY");
  },

  showProblemAlert: () => {
    notification.error({
      duration: 300000,
      message: "Erreur",
      style: { width: 290 },
      description: "Un problème est survenu.",
    });
  },

  setMobileViewPort: () => {
    setTimeout(() => {
      // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
      let vh = window.innerHeight * 0.01;
      // Then we set the value in the --vh custom property to the root of the document
      document.documentElement.style.setProperty("--vh", `${vh}px`);
      window.addEventListener("resize", () => {
        // We execute the same script as before
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty("--vh", `${vh}px`);
      });
    }, 100);
  },
};

export default Utils;
