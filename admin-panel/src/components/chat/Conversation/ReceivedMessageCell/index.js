import React from "react";
import { Avatar } from "antd";

const ReceivedMessageCell = ({ conversation, user }) => {
  return (
    <div className="gx-chat-item">
      {user && (
        <Avatar
          className="gx-size-40 gx-align-self-end"
          src={user.thumb}
          alt=""
        />
      )}

      <div className="gx-bubble-block">
        <div
          className="gx-bubble"
          style={{ background: conversation.senderId ? "#d7e6f5" : "white" }}
        >
          {conversation.senderId && (
            <div
              className="gx-time gx-text-muted"
              style={{ marginBottom: ".5rem" }}
            >
              Canal: {conversation.senderId}
            </div>
          )}
          <div className="gx-message">{conversation.message}</div>
          <div className="gx-time gx-text-muted gx-text-right gx-mt-2">
            {conversation.sentAt}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReceivedMessageCell;
