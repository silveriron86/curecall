export const notifications = [

  {
    image: require('assets/images/placeholder.jpg'),
    title: "Stella Johnson has recently posted an album",
    time: "4:10 PM",
    icon: "thumb-up gx-text-blue",
  }
];
