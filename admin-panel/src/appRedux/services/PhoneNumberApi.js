import http from "../../util/http";
import { ApiPathConstants } from "../../constants";

export const PhoneNumberApi = {
  getAll: () => {
    return http.get(`${ApiPathConstants.getApiPath()}admin/phone-numbers`);
  },
  getById: (id) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}admin/phone-numbers/${id}`
    );
  },
  create: (data) =>
    http.post(`${ApiPathConstants.getApiPath()}admin/phone-numbers`, data),
  delete: (id) =>
    http.delete(`${ApiPathConstants.getApiPath()}admin/phone-numbers/${id}`),
  update: (id, data) =>
    http.patch(
      `${ApiPathConstants.getApiPath()}admin/phone-numbers/${id}`,
      data
    ),
};
