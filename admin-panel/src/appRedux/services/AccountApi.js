import http from "../../util/http";
import { ApiPathConstants } from "../../constants";
import qs from "qs";

export const AccountApi = {
  login: (data) =>
    http.post(
      `${ApiPathConstants.getApiPath()}users/login`,
      qs.stringify(data)
    ),

  getSMSCode: (phone) =>
    http.get(`${ApiPathConstants.getApiPath()}users/login/sms/${phone}`),

  verifySMSCode: (data) =>
    http.post(`${ApiPathConstants.getApiPath()}users/login/sms`, data),

  resetPassword: (data) => {
    const id = localStorage.getItem("user_id");
    return http.patch(`${ApiPathConstants.getApiPath()}users/${id}`, data);
  },
};
