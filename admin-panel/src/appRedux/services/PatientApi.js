import http from "../../util/http";
import { ApiPathConstants } from "../../constants";

export const PatientApi = {
  getAll: (projectId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients`
    );
  },
  getById: (projectId, patientId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}`
    );
  },
  getPatientMatrixes: (projectId, patientId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/matrices`
    );
  },
  getPatientSMS: (projectId, patientId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/sms`
    );
  },
  postPatientSMS: (projectId, patientId, data) => {
    return http.post(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/sms/one-way`,
      data
    );
  },
  getMatrixVariables: (projectId, patientId, patientMatrixId) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}/matrices/${patientMatrixId}/variables`
    );
  },
  update: (projectId, patientId, data) => {
    return http.patch(
      `${ApiPathConstants.getApiPath()}projects/${projectId}/patients/${patientId}`,
      data
    );
  },
};
