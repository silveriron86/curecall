import http from "../../util/http";
import { ApiPathConstants } from "../../constants";

export const UserApi = {
  getAll: () => {
    return http.get(`${ApiPathConstants.getApiPath()}admin/users`);
  },
  getById: (id) => {
    return http.get(`${ApiPathConstants.getApiPath()}admin/users/${id}`);
  },
  create: (data) =>
    http.post(`${ApiPathConstants.getApiPath()}admin/users`, data),
  update: (id, data) =>
    http.patch(`${ApiPathConstants.getApiPath()}admin/users/${id}`, data),
  addProject: (id, data) =>
    http.post(
      `${ApiPathConstants.getApiPath()}admin/users/${id}/add-project`,
      data
    ),
  deleteProject: (id, data) =>
    http.delete(
      `${ApiPathConstants.getApiPath()}admin/users/${id}/remove-project?projectId=${
        data.projectId
      }`
    ),
};
