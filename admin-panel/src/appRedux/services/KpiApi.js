import http from "../../util/http";
import { ApiPathConstants } from "../../constants";

export const KpiApi = {
  getLastMonthUsers: () => {
    return http.get(`${ApiPathConstants.getApiPath()}admin/kpi/last-month-users`);
  },
  getStats: () => {
    return http.get(
      `${ApiPathConstants.getApiPath()}admin/kpi/stats`
    );
  },
};
