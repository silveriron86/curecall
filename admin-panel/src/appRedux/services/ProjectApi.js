import http from "../../util/http";
import { ApiPathConstants } from "../../constants";

export const ProjectApi = {
  getAll: () => {
    return http.get(`${ApiPathConstants.getApiPath()}admin/projects`);
  },
  getTypes: () => {
    return http.get(
      `${ApiPathConstants.getApiPath()}admin/projects/project-types`
    );
  },
  getPathologies: (id) => {
    return http.get(
      `${ApiPathConstants.getApiPath()}projects/${id}/pathologies`
    );
  },
  getById: (id) => {
    return http.get(`${ApiPathConstants.getApiPath()}admin/projects/${id}`);
  },
  create: (data) =>
    http.post(`${ApiPathConstants.getApiPath()}admin/projects`, data),
  delete: (id) =>
    http.delete(`${ApiPathConstants.getApiPath()}admin/projects/${id}`),
  update: (id, data) =>
    http.patch(`${ApiPathConstants.getApiPath()}admin/projects/${id}`, data),
};
