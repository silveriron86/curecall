import {
  HIDE_MESSAGE,
  INIT_URL,
  ON_HIDE_LOADER,
  ON_SHOW_LOADER,
  SHOW_MESSAGE,
  SIGNIN_USER,
  SIGNIN_USER_SUCCESS,
  SIGNOUT_USER,
  SIGNOUT_USER_SUCCESS,
  GET_SMS_CODE,
  GET_SMS_CODE_SUCCESS,
  VERIFY_SMS_CODE,
  VERIFY_SMS_CODE_SUCCESS,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
} from "constants/ActionTypes";

export const userSignIn = (user) => {
  return {
    type: SIGNIN_USER,
    payload: user,
  };
};
export const userSignOut = () => {
  return {
    type: SIGNOUT_USER,
  };
};

export const userSignInSuccess = (authUser) => {
  return {
    type: SIGNIN_USER_SUCCESS,
    payload: authUser,
  };
};
export const userSignOutSuccess = () => {
  return {
    type: SIGNOUT_USER_SUCCESS,
  };
};

export const showAuthMessage = (message) => {
  return {
    type: SHOW_MESSAGE,
    payload: message,
  };
};

export const setInitUrl = (url) => {
  return {
    type: INIT_URL,
    payload: url,
  };
};

export const showAuthLoader = () => {
  return {
    type: ON_SHOW_LOADER,
  };
};

export const hideMessage = () => {
  return {
    type: HIDE_MESSAGE,
  };
};
export const hideAuthLoader = () => {
  return {
    type: ON_HIDE_LOADER,
  };
};

export const getSMSCode = (phone) => {
  return {
    type: GET_SMS_CODE,
    payload: phone,
  };
};
export const getSMSCodeSuccess = (data) => {
  return {
    type: GET_SMS_CODE_SUCCESS,
    payload: data,
  };
};

export const verifySMSCode = (data) => {
  return {
    type: VERIFY_SMS_CODE,
    payload: data,
  };
};
export const verifySMSCodeSuccess = (data) => {
  return {
    type: VERIFY_SMS_CODE_SUCCESS,
    payload: data,
  };
};

export const resetPassword = (data) => {
  return {
    type: RESET_PASSWORD,
    payload: data,
  };
};
export const resetPasswordSuccess = (data) => {
  return {
    type: RESET_PASSWORD_SUCCESS,
    payload: data,
  };
};
