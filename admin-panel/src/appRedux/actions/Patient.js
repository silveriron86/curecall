import {
  GET_ALL_PATIENTS,
  GET_ALL_PATIENTS_SUCCESS,
  GET_PATIENT,
  GET_PATIENT_SUCCESS,
  GET_PATIENT_MATRIXES,
  GET_PATIENT_MATRIXES_SUCCESS,
  GET_MATRIX_VARIABLES,
  GET_MATRIX_VARIABLES_SUCCESS,
  GET_PATIENT_SMS,
  GET_PATIENT_SMS_SUCCESS,
  POST_PATIENT_SMS,
  POST_PATIENT_SMS_SUCCESS,
  UPDATE_PATIENT,
  UPDATE_PATIENT_SUCCESS,
} from "../../constants/ActionTypes";

export const getAllPatients = (data) => {
  return {
    type: GET_ALL_PATIENTS,
    payload: data,
  };
};

export const getAllPatientsSuccess = (data) => {
  return {
    type: GET_ALL_PATIENTS_SUCCESS,
    payload: data,
  };
};

export const getPatient = (data) => {
  return {
    type: GET_PATIENT,
    payload: data,
  };
};
export const getPatientSuccess = (data) => {
  return {
    type: GET_PATIENT_SUCCESS,
    payload: data,
  };
};

export const getPatientMatrixes = (data) => {
  return {
    type: GET_PATIENT_MATRIXES,
    payload: data,
  };
};
export const getPatientMatrixesSuccess = (data) => {
  return {
    type: GET_PATIENT_MATRIXES_SUCCESS,
    payload: data,
  };
};

export const getMatrixVariables = (data) => {
  return {
    type: GET_MATRIX_VARIABLES,
    payload: data,
  };
};
export const getMatrixVariablesSuccess = (data) => {
  return {
    type: GET_MATRIX_VARIABLES_SUCCESS,
    payload: data,
  };
};

export const getPatientSMS = (data) => {
  return {
    type: GET_PATIENT_SMS,
    payload: data,
  };
};
export const getPatientSMSSuccess = (data) => {
  return {
    type: GET_PATIENT_SMS_SUCCESS,
    payload: data,
  };
};

export const postPatientSMS = (data) => {
  return {
    type: POST_PATIENT_SMS,
    payload: data,
  };
};
export const postPatientSMSSuccess = (data) => {
  return {
    type: POST_PATIENT_SMS_SUCCESS,
    payload: data,
  };
};

export const updatePatient = (data) => {
  return {
    type: UPDATE_PATIENT,
    payload: data,
  };
};
export const updatePatientSuccess = (data) => {
  return {
    type: UPDATE_PATIENT_SUCCESS,
    payload: data,
  };
};
