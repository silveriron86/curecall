import {
  GET_ALL_PHONE_NUMBERS,
  GET_ALL_PHONE_NUMBERS_SUCCESS,
  GET_PHONE_NUMBER,
  GET_PHONE_NUMBER_SUCCESS,
  DELETE_PHONE_NUMBER,
  DELETE_PHONE_NUMBER_SUCCESS,
  UPDATE_PHONE_NUMBER,
  UPDATE_PHONE_NUMBER_SUCCESS,
  CREATE_PHONE_NUMBER,
  CREATE_PHONE_NUMBER_SUCCESS,
} from "../../constants/ActionTypes";

export const getAllPhoneNumbers = () => {
  return {
    type: GET_ALL_PHONE_NUMBERS,
  };
};
export const getAllPhoneNumbersSuccess = (data) => {
  return {
    type: GET_ALL_PHONE_NUMBERS_SUCCESS,
    payload: data,
  };
};

export const getPhoneNumber = () => {
  return {
    type: GET_PHONE_NUMBER,
  };
};
export const getPhoneNumberSuccess = (data) => {
  return {
    type: GET_PHONE_NUMBER_SUCCESS,
    payload: data,
  };
};

export const createPhoneNumber = (data) => {
  return {
    type: CREATE_PHONE_NUMBER,
    payload: data,
  };
};
export const createPhoneNumberSuccess = (data) => {
  return {
    type: CREATE_PHONE_NUMBER_SUCCESS,
    payload: data,
  };
};

export const deletePhoneNumber = (data) => {
  return {
    type: DELETE_PHONE_NUMBER,
    payload: data,
  };
};
export const deletePhoneNumberSuccess = (data) => {
  return {
    type: DELETE_PHONE_NUMBER_SUCCESS,
    payload: data,
  };
};
export const updatePhoneNumber = (data) => {
  return {
    type: UPDATE_PHONE_NUMBER,
    payload: data,
  };
};
export const updatePhoneNumberSuccess = (data) => {
  return {
    type: UPDATE_PHONE_NUMBER_SUCCESS,
    payload: data,
  };
};
