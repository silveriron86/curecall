import {
  GET_LAST_MONTH_USERS,
  GET_LAST_MONTH_USERS_SUCCESS,
  GET_STATS,
  GET_STATS_SUCCESS
} from "../../constants/ActionTypes";

export const getLastMonthUsers = () => {
  return {
    type: GET_LAST_MONTH_USERS,
  };
};
export const getLastMonthUsersSuccess = (data) => {
  return {
    type: GET_LAST_MONTH_USERS_SUCCESS,
    payload: data,
  };
};

export const getStats = () => {
  return {
    type: GET_STATS,
  };
};
export const getStatsSuccess = (data) => {
  return {
    type: GET_STATS_SUCCESS,
    payload: data,
  };
};
