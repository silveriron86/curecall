import {
  GET_ALL_PROJECTS,
  GET_ALL_PROJECTS_SUCCESS,
  GET_PROJECT,
  GET_PROJECT_SUCCESS,
  GET_PROJECT_TYPES,
  GET_PROJECT_TYPES_SUCCESS,
  CREATE_PROJECT,
  CREATE_PROJECT_SUCCESS,
  GET_ALL_PATHOLOGIES,
  GET_ALL_PATHOLOGIES_SUCCESS,
  UPDATE_PROJECT,
  UPDATE_PROJECT_SUCCESS,
  DELETE_PROJECT
} from "../../constants/ActionTypes";

export const getAllProjects = () => {
  return {
    type: GET_ALL_PROJECTS,
  };
};
export const getAllProjectsSuccess = (data) => {
  return {
    type: GET_ALL_PROJECTS_SUCCESS,
    payload: data,
  };
};

export const getProjectTypes = () => {
  return {
    type: GET_PROJECT_TYPES,
  };
};
export const getProjectTypesSuccess = (data) => {
  return {
    type: GET_PROJECT_TYPES_SUCCESS,
    payload: data,
  };
};

export const getProject = (data) => {
  return {
    type: GET_PROJECT,
    payload: data,
  };
};
export const getProjectSuccess = (data) => {
  return {
    type: GET_PROJECT_SUCCESS,
    payload: data,
  };
};

export const createProject = (data) => {
  return {
    type: CREATE_PROJECT,
    payload: data,
  };
};
export const createProjectSuccess = (data) => {
  return {
    type: CREATE_PROJECT_SUCCESS,
    payload: data,
  };
};

export const updateProject = (data) => {
  return {
    type: UPDATE_PROJECT,
    payload: data,
  };
};
export const updateProjectSuccess = (data) => {
  return {
    type: UPDATE_PROJECT_SUCCESS,
    payload: data,
  };
};

export const getAllPathologies = (data) => {
  return {
    type: GET_ALL_PATHOLOGIES,
    payload: data,
  };
};
export const getPathologiesSuccess = (data) => {
  return {
    type: GET_ALL_PATHOLOGIES_SUCCESS,
    payload: data,
  };
};

export const deleteProject = (data) => {
  return {
    type: DELETE_PROJECT,
    payload: data,
  };
};
