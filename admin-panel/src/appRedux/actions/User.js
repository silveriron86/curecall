import {
  GET_ALL_USERS,
  GET_ALL_USERS_SUCCESS,
  GET_USER,
  GET_USER_SUCCESS,
  CREATE_USER,
  CREATE_USER_SUCCESS,
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  ADD_USER_PROJECT,
  ADD_USER_PROJECT_SUCCESS,
  DELETE_USER_PROJECT,
  DELETE_USER_PROJECT_SUCCESS,
} from "../../constants/ActionTypes";

export const getAllUsers = () => {
  return {
    type: GET_ALL_USERS,
  };
};
export const getAllUsersSuccess = (data) => {
  return {
    type: GET_ALL_USERS_SUCCESS,
    payload: data,
  };
};

export const getUser = (data) => {
  return {
    type: GET_USER,
    payload: data,
  };
};
export const getUserSuccess = (data) => {
  return {
    type: GET_USER_SUCCESS,
    payload: data,
  };
};

export const createUser = (data) => {
  return {
    type: CREATE_USER,
    payload: data,
  };
};
export const createUserSuccess = (data) => {
  return {
    type: CREATE_USER_SUCCESS,
    payload: data,
  };
};

export const updateUser = (data) => {
  return {
    type: UPDATE_USER,
    payload: data,
  };
};
export const updateUserSuccess = (data) => {
  return {
    type: UPDATE_USER_SUCCESS,
    payload: data,
  };
};

export const deleteUserProject = (data) => {
  return {
    type: DELETE_USER_PROJECT,
    payload: data,
  };
};
export const deleteUserProjectSuccess = (data) => {
  return {
    type: DELETE_USER_PROJECT_SUCCESS,
    payload: data,
  };
};

export const addUserProject = (data) => {
  return {
    type: ADD_USER_PROJECT,
    payload: data,
  };
};
export const addUserProjectSuccess = (data) => {
  return {
    type: ADD_USER_PROJECT_SUCCESS,
    payload: data,
  };
};
