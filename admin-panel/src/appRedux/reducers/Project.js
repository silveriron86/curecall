import {
  GET_ALL_PROJECTS,
  GET_ALL_PROJECTS_SUCCESS,
  GET_PROJECT,
  UPDATE_PROJECT,
  DELETE_PROJECT,
  GET_PROJECT_SUCCESS,
  GET_PROJECT_TYPES_SUCCESS,
  GET_ALL_PATHOLOGIES_SUCCESS,
  UPDATE_PROJECT_SUCCESS,
  FETCH_ERROR,
} from "../../constants/ActionTypes";

const INIT_STATE = {
  projects: [],
  projectTypes: [],
  pathologies: [],
  project: null,
  loading: false,
  loadingProject: false,
  errorMessage: "",
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_ERROR: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
      };
    }

    case GET_ALL_PROJECTS:
    case DELETE_PROJECT: {
      return {
        ...state,
        loading: true,
        errorMessage: "",
      };
    }
    case GET_ALL_PROJECTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        projects: action.payload,
      };
    }
    case GET_PROJECT_TYPES_SUCCESS: {
      return {
        ...state,
        projectTypes: action.payload,
      };
    }
    case GET_PROJECT: {
      return {
        ...state,
        loadingProject: true,
        project: null,
      };
    }
    case GET_PROJECT_SUCCESS: {
      return {
        ...state,
        loadingProject: false,
        project: action.payload,
      };
    }
    case UPDATE_PROJECT: {
      return {
        ...state,
        loadingProject: true,
        project: null,
      };
    }
    case UPDATE_PROJECT_SUCCESS: {
      return {
        ...state,
        loadingProject: false,
        project: action.payload,
      };
    }
    case GET_ALL_PATHOLOGIES_SUCCESS: {
      return {
        ...state,
        pathologies: action.payload,
      };
    }
    default:
      return state;
  }
};
