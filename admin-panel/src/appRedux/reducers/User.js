import {
  GET_ALL_USERS,
  GET_ALL_USERS_SUCCESS,
  GET_USER,
  GET_USER_SUCCESS,
  CREATE_USER,
  CREATE_USER_SUCCESS,
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  FETCH_ERROR,
  DELETE_USER_PROJECT,
  ADD_USER_PROJECT,
  DELETE_USER_PROJECT_SUCCESS,
  ADD_USER_PROJECT_SUCCESS,
} from "../../constants/ActionTypes";

const INIT_STATE = {
  users: [],
  userTypes: [],
  user: null,
  createdUser: null,
  loading: false,
  errorMessage: "",
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_ERROR:
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
      };
    case GET_ALL_USERS: {
      return {
        ...state,
        loading: true,
      };
    }
    case GET_ALL_USERS_SUCCESS: {
      return {
        ...state,
        users: action.payload,
        loading: false,
      };
    }
    case GET_USER: {
      return {
        ...state,
        user: null,
        loading: true,
        errorMessage: "",
      };
    }
    case GET_USER_SUCCESS: {
      return {
        ...state,
        user: action.payload,
        loading: false,
      };
    }
    case CREATE_USER: {
      return {
        ...state,
        createdUser: null,
      };
    }
    case CREATE_USER_SUCCESS: {
      console.log(action.payload);
      return {
        ...state,
        createdUser: action.payload,
      };
    }
    case UPDATE_USER: {
      return {
        ...state,
        loading: true,
        errorMessage: "",
      };
    }
    case UPDATE_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
      };
    }

    case DELETE_USER_PROJECT:
    case ADD_USER_PROJECT: {
      return {
        ...state,
        loading: true,
      };
    }
    case DELETE_USER_PROJECT_SUCCESS:
    case ADD_USER_PROJECT_SUCCESS: {
      return {
        ...state,
        loading: false,
      };
    }
    default:
      return state;
  }
};
