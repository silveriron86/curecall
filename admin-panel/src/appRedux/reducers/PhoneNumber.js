import {
  GET_ALL_PHONE_NUMBERS,
  GET_ALL_PHONE_NUMBERS_SUCCESS,
  CREATE_PHONE_NUMBER,
  UPDATE_PHONE_NUMBER,
  FETCH_ERROR,
  DELETE_PHONE_NUMBER,
  // GET_PHONE_NUMBER,
  // GET_PHONE_NUMBER_SUCCESS,
} from "../../constants/ActionTypes";

const INIT_STATE = {
  phoneNumbers: [],
  phoneNumber: null,
  loading: false,
  errorMessage: "",
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_ERROR:
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
      };

    case GET_ALL_PHONE_NUMBERS:
      return {
        ...state,
        loading: true,
        errorMessage: "",
      };

    case GET_ALL_PHONE_NUMBERS_SUCCESS: {
      return {
        ...state,
        loading: false,
        phoneNumbers: action.payload,
      };
    }

    case CREATE_PHONE_NUMBER:
    case UPDATE_PHONE_NUMBER:
    case DELETE_PHONE_NUMBER:
      return {
        ...state,
        loading: true,
        errorMessage: "",
      };

    default:
      return state;
  }
};
