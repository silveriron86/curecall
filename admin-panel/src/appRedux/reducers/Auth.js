import {
  HIDE_MESSAGE,
  INIT_URL,
  ON_HIDE_LOADER,
  ON_SHOW_LOADER,
  SHOW_MESSAGE,
  SIGNIN_USER_SUCCESS,
  SIGNOUT_USER_SUCCESS,
  GET_SMS_CODE,
  GET_SMS_CODE_SUCCESS,
  VERIFY_SMS_CODE,
  VERIFY_SMS_CODE_SUCCESS,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
} from "constants/ActionTypes";

const loggedInUser = localStorage.getItem("logged_user");
const INIT_STATE = {
  loader: false,
  alertMessage: "",
  showMessage: false,
  initURL: "",
  authUser: loggedInUser ? JSON.parse(loggedInUser) : null,
  smsUser: null,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SIGNIN_USER_SUCCESS: {
      return {
        ...state,
        loader: false,
        authUser: action.payload,
      };
    }
    case INIT_URL: {
      return {
        ...state,
        initURL: action.payload,
      };
    }
    case SIGNOUT_USER_SUCCESS: {
      return {
        ...state,
        authUser: null,
        initURL: "/",
        loader: false,
      };
    }

    case SHOW_MESSAGE: {
      return {
        ...state,
        alertMessage: action.payload,
        showMessage: true,
        loader: false,
      };
    }
    case HIDE_MESSAGE: {
      return {
        ...state,
        alertMessage: "",
        showMessage: false,
        loader: false,
      };
    }

    case ON_SHOW_LOADER: {
      return {
        ...state,
        loader: true,
      };
    }
    case ON_HIDE_LOADER: {
      return {
        ...state,
        loader: false,
      };
    }

    case GET_SMS_CODE: {
      return {
        ...state,
        loader: true,
        sentSMS: false,
      };
    }
    case VERIFY_SMS_CODE: {
      return {
        ...state,
        loader: true,
        smsUser: null,
      };
    }
    case RESET_PASSWORD: {
      return {
        ...state,
        loader: true,
      };
    }
    case GET_SMS_CODE_SUCCESS: {
      return {
        ...state,
        loader: false,
        sentSMS: true,
      };
    }
    case VERIFY_SMS_CODE_SUCCESS: {
      return {
        ...state,
        loader: false,
        smsUser: action.payload,
      };
    }
    case RESET_PASSWORD_SUCCESS: {
      return {
        ...state,
        loader: false,
      };
    }
    default:
      return state;
  }
};
