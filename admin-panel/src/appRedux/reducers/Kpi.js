import {
  GET_LAST_MONTH_USERS,
  GET_LAST_MONTH_USERS_SUCCESS,
  GET_STATS,
  GET_STATS_SUCCESS,
} from "../../constants/ActionTypes";

const INIT_STATE = {
  loadingUsers: false,
  loadingStats: false,
  kpiUsers: [],
  kpiStats: null,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_LAST_MONTH_USERS: {
      return {
        ...state,
        loadingUsers: true,
      };
    }
    case GET_LAST_MONTH_USERS_SUCCESS: {
      return {
        ...state,
        loadingUsers: false,
        kpiUsers: action.payload,
      };
    }
    case GET_STATS: {
      return {
        ...state,
        loadingStats: true,
      };
    }
    case GET_STATS_SUCCESS: {
      return {
        ...state,
        loadingStats: false,
        kpiStats: action.payload,
      };
    }

    default:
      return state;
  }
};
