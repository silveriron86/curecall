import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import Settings from "./Settings";
import Auth from "./Auth";
import Project from "./Project";
import Common from "./Common";
import User from "./User";
import Kpi from "./Kpi";
import PhoneNumber from "./PhoneNumber";
import Patient from "./Patient";

const createRootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    settings: Settings,
    auth: Auth,
    project: Project,
    common: Common,
    user: User,
    kpi: Kpi,
    phoneNumber: PhoneNumber,
    patient: Patient
  });

export default createRootReducer;
