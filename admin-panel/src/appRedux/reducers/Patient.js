import {
  FETCH_ERROR,
  GET_ALL_PATIENTS,
  GET_ALL_PATIENTS_SUCCESS,
  GET_PATIENT,
  GET_PATIENT_SUCCESS,
  GET_PATIENT_MATRIXES,
  GET_PATIENT_MATRIXES_SUCCESS,
  GET_MATRIX_VARIABLES,
  GET_MATRIX_VARIABLES_SUCCESS,
  GET_PATIENT_SMS,
  GET_PATIENT_SMS_SUCCESS,
  POST_PATIENT_SMS,
  POST_PATIENT_SMS_SUCCESS,
  UPDATE_PATIENT,
  UPDATE_PATIENT_SUCCESS
} from "../../constants/ActionTypes";

const INIT_STATE = {
  patients: [],
  matrixes: [],
  variables: [],
  patient: null,
  loading: false,
  loadingMatrixes: false,
  loadingVariables: false,
  loadingSMS: false,
  errorMessage: "",
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_ERROR:
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
      };

    case GET_ALL_PATIENTS:
      return {
        ...state,
        loading: true,
        errorMessage: "",
      };

    case GET_ALL_PATIENTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        patients: action.payload,
      };
    }

    case GET_PATIENT:
      return {
        ...state,
        loading: true,
        errorMessage: "",
        patient: null,
      };

    case GET_PATIENT_SUCCESS: {
      return {
        ...state,
        loading: false,
        patient: action.payload,
      };
    }

    case GET_PATIENT_MATRIXES:
      return {
        ...state,
        loadingMatrixes: true,
        errorMessage: "",
        matrixes: [],
      };

    case GET_PATIENT_MATRIXES_SUCCESS: {
      return {
        ...state,
        loadingMatrixes: false,
        matrixes: action.payload,
      };
    }

    case GET_MATRIX_VARIABLES:
      return {
        ...state,
        loadingVariables: true,
        errorMessage: "",
        variables: [],
      };

    case GET_MATRIX_VARIABLES_SUCCESS: {
      return {
        ...state,
        loadingVariables: false,
        variables: action.payload,
      };
    }

    case GET_PATIENT_SMS:
      return {
        ...state,
        loadingSMS: true,
        errorMessage: "",
        sms: [],
      };

    case GET_PATIENT_SMS_SUCCESS: {
      return {
        ...state,
        loadingSMS: false,
        sms: action.payload,
      };
    }

    case GET_PATIENT_SMS:
      return {
        ...state,
        loadingSMS: true,
        errorMessage: "",
        sms: [],
      };

    case GET_PATIENT_SMS_SUCCESS: {
      return {
        ...state,
        loadingSMS: false,
        sms: action.payload,
      };
    }

    case POST_PATIENT_SMS:
      return {
        ...state,
        loadingSMS: true,
        errorMessage: "",
      };

    case POST_PATIENT_SMS_SUCCESS: {
      return {
        ...state,
        loadingSMS: false,
      };
    }

    case UPDATE_PATIENT:
      return {
        ...state,
        loading: true,
      };

    case UPDATE_PATIENT_SUCCESS: {
      return {
        ...state,
        loading: false,
      };
    }

    default:
      return state;
  }
};
