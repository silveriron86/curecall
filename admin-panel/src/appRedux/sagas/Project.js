import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import {
  getAllProjectsSuccess,
  getProjectSuccess,
  getProjectTypesSuccess,
  getPathologiesSuccess,
  updateProjectSuccess,
} from "../actions/Project";
import {
  GET_ALL_PROJECTS,
  GET_PROJECT_TYPES,
  GET_PROJECT,
  CREATE_PROJECT,
  UPDATE_PROJECT,
  DELETE_PROJECT,
  GET_ALL_PATHOLOGIES,
} from "../../constants/ActionTypes";
import { fetchError } from "../actions/Common";
import { ProjectApi } from "../services";

function* getAllProjectsRequest() {
  try {
    const result = yield call(() => ProjectApi.getAll());
    yield put(getAllProjectsSuccess(result.data.projects));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getAllProjects() {
  yield takeEvery(GET_ALL_PROJECTS, getAllProjectsRequest);
}

function* getProjectTypesRequest() {
  try {
    const result = yield call(() => ProjectApi.getTypes());
    console.log("*** getProjectTypesRequest", result);
    yield put(getProjectTypesSuccess(result.data.projectTypes));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getProjectTypes() {
  yield takeEvery(GET_PROJECT_TYPES, getProjectTypesRequest);
}

function* getProjectRequest({ payload }) {
  try {
    const result = yield call(() => ProjectApi.getById(payload));
    yield put(getProjectSuccess(result.data.project));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getProject() {
  yield takeEvery(GET_PROJECT, getProjectRequest);
}

function* createProjectRequest({ payload }) {
  try {
    const result = yield call(() => ProjectApi.create(payload));
    console.log(result);
    window.location.href = "/projects/list";
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* createProject() {
  yield takeEvery(CREATE_PROJECT, createProjectRequest);
}

function* deleteProjectRequest({payload}) {
  try {
    const result = yield call(() => ProjectApi.delete(payload));
    console.log(result);
    const res = yield call(() => ProjectApi.getAll());
    yield put(getAllProjectsSuccess(res.data.projects));
  } catch (error) {
    yield put(fetchError(error.response.data.msg));
  }
}
export function* deleteProject() {
  yield takeEvery(DELETE_PROJECT, deleteProjectRequest);
}

function* getPathologiesRequest({ payload }) {
  try {
    const result = yield call(() => ProjectApi.getPathologies(payload));
    yield put(getPathologiesSuccess(result.data));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getPathologies() {
  yield takeEvery(GET_ALL_PATHOLOGIES, getPathologiesRequest);
}

function* updateProjectRequest({ payload }) {
  try {
    const { id, data } = payload;
    const result = yield call(() => ProjectApi.update(id, data));
    yield put(updateProjectSuccess(result.data));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* updateProject() {
  yield takeEvery(UPDATE_PROJECT, updateProjectRequest);
}

export default function* rootSaga() {
  yield all([
    fork(getAllProjects),
    fork(getProjectTypes),
    fork(getProject),
    fork(createProject),
    fork(updateProject),
    fork(deleteProject),
    fork(getPathologies),
  ]);
}
