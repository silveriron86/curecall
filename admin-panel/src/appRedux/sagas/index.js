import { all } from "redux-saga/effects";
import authSagas from "./Auth";
import projectSagas from "./Project";
import userSaga from "./User";
import kpiSaga from "./Kpi";
import phoneNumberSaga from "./PhoneNumber";
import patientSaga from "./Patient";

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    projectSagas(),
    userSaga(),
    kpiSaga(),
    phoneNumberSaga(),
    patientSaga(),
  ]);
}
