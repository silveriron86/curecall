import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import {
  GET_ALL_PHONE_NUMBERS,
  CREATE_PHONE_NUMBER,
  UPDATE_PHONE_NUMBER,
  DELETE_PHONE_NUMBER,
} from "../../constants/ActionTypes";
import { getAllPhoneNumbersSuccess, fetchError } from "../actions";
import { PhoneNumberApi } from "../services";

function* getAllPhoneNumbersRequest() {
  try {
    const result = yield call(() => PhoneNumberApi.getAll());
    yield put(getAllPhoneNumbersSuccess(result.data.phoneNumbers));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getAllPhoneNumbers() {
  yield takeEvery(GET_ALL_PHONE_NUMBERS, getAllPhoneNumbersRequest);
}

function* createPhoneNumberRequest({ payload }) {
  try {
    yield call(() => PhoneNumberApi.create(payload));
    const result = yield call(() => PhoneNumberApi.getAll());
    yield put(getAllPhoneNumbersSuccess(result.data.phoneNumbers));
  } catch (error) {
    yield put(fetchError(error.response.data.errors.msg));
  }
}
export function* createPhoneNumber() {
  yield takeEvery(CREATE_PHONE_NUMBER, createPhoneNumberRequest);
}

function* updatePhoneNumberRequest({ payload }) {
  try {
    const { id, data } = payload;
    yield call(() => PhoneNumberApi.update(id, data));
    const result = yield call(() => PhoneNumberApi.getAll());
    yield put(getAllPhoneNumbersSuccess(result.data.phoneNumbers));
  } catch (error) {
    yield put(fetchError(error.response.data.errors.msg));
  }
}
export function* updatePhoneNumber() {
  yield takeEvery(UPDATE_PHONE_NUMBER, updatePhoneNumberRequest);
}

function* deletePhoneNumberRequest({ payload }) {
  try {
    yield call(() => PhoneNumberApi.delete(payload));
    const result = yield call(() => PhoneNumberApi.getAll());
    yield put(getAllPhoneNumbersSuccess(result.data.phoneNumbers));
  } catch (error) {
    yield put(fetchError(error.response.data.errors.msg));
  }
}
export function* deletePhoneNumber() {
  yield takeEvery(DELETE_PHONE_NUMBER, deletePhoneNumberRequest);
}

export default function* rootSaga() {
  yield all([
    fork(getAllPhoneNumbers),
    fork(createPhoneNumber),
    fork(updatePhoneNumber),
    fork(deletePhoneNumber),
  ]);
}
