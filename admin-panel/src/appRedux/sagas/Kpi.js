import { all, call, fork, put, takeEvery } from "redux-saga/effects";

import { GET_LAST_MONTH_USERS, GET_STATS } from "../../constants/ActionTypes";
import { getLastMonthUsersSuccess, getStatsSuccess } from "../actions";
import { fetchError } from "../actions/Common";
import { KpiApi } from "../services";

function* getLastMonthUsersRequest() {
  try {
    const result = yield call(() => KpiApi.getLastMonthUsers());
    yield put(getLastMonthUsersSuccess(result.data.users));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getLastMonthUsers() {
  yield takeEvery(GET_LAST_MONTH_USERS, getLastMonthUsersRequest);
}

function* getStatsRequest() {
  try {
    const result = yield call(() => KpiApi.getStats());
    yield put(getStatsSuccess(result.data));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getStats() {
  yield takeEvery(GET_STATS, getStatsRequest);
}

export default function* rootSaga() {
  yield all([fork(getLastMonthUsers), fork(getStats)]);
}
