import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import {
  getAllUsersSuccess,
  getUserSuccess,
  createUserSuccess,
  addUserProjectSuccess,
  deleteUserProjectSuccess,
} from "../actions/User";
import {
  GET_ALL_USERS,
  GET_USER,
  UPDATE_USER,
  ADD_USER_PROJECT,
  DELETE_USER_PROJECT,
  CREATE_USER,
} from "../../constants/ActionTypes";
import { fetchError } from "../actions/Common";
import { UserApi } from "../services";

function* getAllUsersRequest() {
  try {
    const result = yield call(() => UserApi.getAll());
    yield put(getAllUsersSuccess(result.data.users));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getAllUsers() {
  yield takeEvery(GET_ALL_USERS, getAllUsersRequest);
}

function* getUserRequest({ payload }) {
  try {
    const result = yield call(() => UserApi.getById(payload));
    yield put(getUserSuccess(result.data.user));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getUser() {
  yield takeEvery(GET_USER, getUserRequest);
}

function* updateUserRequest({ payload }) {
  const { id, data } = payload;
  try {
    const result = yield call(() => UserApi.update(id, data));
    console.log(result);
    const res = yield call(() => UserApi.getAll());
    yield put(getAllUsersSuccess(res.data.users));
    window.location.href = "/users";
  } catch (error) {
    yield put(fetchError(error.response.data.errors.msg));
  }
}
export function* updateUser() {
  yield takeEvery(UPDATE_USER, updateUserRequest);
}

function* createUserRequest({ payload }) {
  try {
    const created = yield call(() => UserApi.create(payload));
    console.log("* created ", created.data.user);
    yield put(createUserSuccess(created.data.user));
    const res = yield call(() => UserApi.getAll());
    yield put(getAllUsersSuccess(res.data.users));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* createUser() {
  yield takeEvery(CREATE_USER, createUserRequest);
}

function* addUserProjectRequest({ payload }) {
  try {
    const { userId, data } = payload;
    const result = yield call(() => UserApi.addProject(userId, data));
    yield put(addUserProjectSuccess(result.data));
    // const res = yield call(() => UserApi.getAll());
    // yield put(getAllUsersSuccess(res.data.users));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* addUserProject() {
  yield takeEvery(ADD_USER_PROJECT, addUserProjectRequest);
}

function* deleteUserProjectRequest({ payload }) {
  try {
    const { userId, data } = payload;
    const result = yield call(() => UserApi.deleteProject(userId, data));
    yield put(deleteUserProjectSuccess(result.data));
    // const res = yield call(() => UserApi.getAll());
    // yield put(getAllUsersSuccess(res.data.users));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* deleteUserProject() {
  yield takeEvery(DELETE_USER_PROJECT, deleteUserProjectRequest);
}

export default function* rootSaga() {
  yield all([
    fork(getAllUsers),
    fork(getUser),
    fork(updateUser),
    fork(createUser),
    fork(addUserProject),
    fork(deleteUserProject),
  ]);
}
