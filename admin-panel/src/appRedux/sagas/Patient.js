import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import {
  GET_ALL_PATIENTS,
  GET_PATIENT,
  GET_PATIENT_MATRIXES,
  GET_MATRIX_VARIABLES,
  GET_PATIENT_SMS,
  POST_PATIENT_SMS,
  UPDATE_PATIENT
} from "../../constants/ActionTypes";
import {
  getAllPatientsSuccess,
  getPatientSuccess,
  fetchError,
  getPatientMatrixesSuccess,
  getMatrixVariablesSuccess,
  getPatientSMSSuccess,
  updatePatientSuccess
} from "../actions";
import { PatientApi } from "../services";

function* getAllPatientsRequest({ payload }) {
  try {
    const result = yield call(() => PatientApi.getAll(payload));
    yield put(getAllPatientsSuccess(result.data));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getAllPatients() {
  yield takeEvery(GET_ALL_PATIENTS, getAllPatientsRequest);
}

function* getPatientRequest({ payload }) {
  try {
    const { projectId, patientId } = payload;
    const result = yield call(() => PatientApi.getById(projectId, patientId));
    yield put(getPatientSuccess(result.data));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getPatient() {
  yield takeEvery(GET_PATIENT, getPatientRequest);
}

function* getPatientMatrixesRequest({ payload }) {
  try {
    const { projectId, patientId } = payload;
    const result = yield call(() =>
      PatientApi.getPatientMatrixes(projectId, patientId)
    );
    yield put(getPatientMatrixesSuccess(result.data.patientMatrixHistory));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getPatientMatrixes() {
  yield takeEvery(GET_PATIENT_MATRIXES, getPatientMatrixesRequest);
}

function* getMatrixVariablesRequest({ payload }) {
  try {
    const { projectId, patientId, patientMatrixId } = payload;
    const result = yield call(() =>
      PatientApi.getMatrixVariables(projectId, patientId, patientMatrixId)
    );
    yield put(getMatrixVariablesSuccess(result.data.patientProfile));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getMatrixVariables() {
  yield takeEvery(GET_MATRIX_VARIABLES, getMatrixVariablesRequest);
}

function* getPatientSMSRequest({ payload }) {
  try {
    const { projectId, patientId } = payload;
    const result = yield call(() =>
      PatientApi.getPatientSMS(projectId, patientId)
    );
    yield put(getPatientSMSSuccess(result.data.messages));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* getPatientSMS() {
  yield takeEvery(GET_PATIENT_SMS, getPatientSMSRequest);
}

function* postPatientSMSRequest({ payload }) {
  try {
    const { projectId, patientId, data } = payload;
    yield call(() => PatientApi.postPatientSMS(projectId, patientId, data));
    const result = yield call(() =>
      PatientApi.getPatientSMS(projectId, patientId)
    );
    yield put(getPatientSMSSuccess(result.data.messages));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* postPatientSMS() {
  yield takeEvery(POST_PATIENT_SMS, postPatientSMSRequest);
}

function* updatePatientRequest({ payload }) {
  try {
    const { projectId, patientId, data } = payload;
    yield call(() => PatientApi.update(projectId, patientId, data));
    const result = yield call(() => PatientApi.getById(projectId, patientId));
    yield put(getPatientSuccess(result.data));
  } catch (error) {
    yield put(fetchError(error));
  }
}
export function* updatePatient() {
  yield takeEvery(UPDATE_PATIENT, updatePatientRequest);
}

export default function* rootSaga() {
  yield all([
    fork(getAllPatients),
    fork(getPatient),
    fork(getPatientMatrixes),
    fork(getMatrixVariables),
    fork(getPatientSMS),
    fork(postPatientSMS),
    fork(updatePatient),
  ]);
}
