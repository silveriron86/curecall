import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import {
  SIGNIN_USER,
  SIGNOUT_USER,
  GET_SMS_CODE,
  VERIFY_SMS_CODE,
  RESET_PASSWORD,
} from "constants/ActionTypes";
import {
  showAuthMessage,
  userSignInSuccess,
  userSignOutSuccess,
  getSMSCodeSuccess,
  verifySMSCodeSuccess,
} from "../../appRedux/actions";

import { AccountApi } from "../services/AccountApi";

function* signInUserWithEmailPassword({ payload }) {
  const { email, password } = payload;
  try {
    const response = yield call(() => AccountApi.login({ email, password }));
    console.log("* signInUser = ", response);
    if (response.data.role === "admin") {
      localStorage.setItem("access_token", response.data.token);
      localStorage.setItem("logged_user", JSON.stringify(response.data));
      yield put(userSignInSuccess(response.data));
    } else {
      yield put(
        showAuthMessage(
          "It's not allowed for you to use the admin panel because your role is not admin"
        )
      );
    }
  } catch (error) {
    const { error_description, errors } = error.response.data;
    yield put(showAuthMessage(errors ? errors.msg : error_description));
  }
}

function* signOut() {
  localStorage.removeItem("access_token");
  localStorage.removeItem("logged_user");
  yield put(userSignOutSuccess(signOutUser));
}

export function* signInUser() {
  yield takeEvery(SIGNIN_USER, signInUserWithEmailPassword);
}

export function* signOutUser() {
  yield takeEvery(SIGNOUT_USER, signOut);
}

function* getSMSCodeRequest({ payload }) {
  try {
    const response = yield call(() => AccountApi.getSMSCode(payload));
    yield put(getSMSCodeSuccess(response.data));
  } catch (error) {
    const { error_description, errors } = error.response.data;
    yield put(showAuthMessage(errors ? errors.msg : error_description));
  }
}

export function* getSMSCode() {
  yield takeEvery(GET_SMS_CODE, getSMSCodeRequest);
}

function* verifySMSCodeRequest({ payload }) {
  try {
    const response = yield call(() => AccountApi.verifySMSCode(payload));
    yield put(verifySMSCodeSuccess(response.data));
    localStorage.setItem("user_id", response.data.id);
    localStorage.setItem("access_token", response.data.token);
    localStorage.setItem("logged_user", JSON.stringify(response.data));
  } catch (error) {
    const { error_description, errors } = error.response.data;
    yield put(showAuthMessage(errors ? errors.msg : error_description));
  }
}

export function* verifySMSCode() {
  yield takeEvery(VERIFY_SMS_CODE, verifySMSCodeRequest);
}

function* resetPasswordRequest({ payload }) {
  try {
    const response = yield call(() => AccountApi.resetPassword(payload));
    console.log(response);
    localStorage.removeItem("access_token");
    localStorage.removeItem("logged_user");
    localStorage.removeItem("user_id");
    window.location.href = "/signin";
    // yield put(resetPasswordSuccess(response.data));
  } catch (error) {
    const { error_description, errors } = error.response.data;
    yield put(showAuthMessage(errors ? errors.msg : error_description));
  }
}

export function* resetPassword() {
  yield takeEvery(RESET_PASSWORD, resetPasswordRequest);
}

export default function* rootSaga() {
  yield all([
    fork(signInUser),
    fork(signOutUser),
    fork(getSMSCode),
    fork(verifySMSCode),
    fork(resetPassword),
  ]);
}
