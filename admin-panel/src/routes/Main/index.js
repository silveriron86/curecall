import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, Table, Row, Col, Spin } from "antd";
import IntlMessages from "util/IntlMessages";
import IconWithTextCard from "components/Metrics/IconWithTextCard";
import { Link } from "react-router-dom";
import SearchBox from "components/SearchBox";
import { getLastMonthUsers, getStats } from "../../appRedux/actions";
import Utils from "util/common";

const MainPage = () => {
  const [searchText, setSearchText] = useState("");
  const { kpiUsers, kpiStats, loadingUsers, loadingStats } = useSelector(
    ({ kpi }) => kpi
  );
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getLastMonthUsers());
    dispatch(getStats());
  }, [dispatch]);

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "User name",
      dataIndex: "title",
      render: (title, record) => {
        return `${title} ${record.firstname} ${record.lastname}`;
      },
    },
    // {
    //   title: "Age",
    //   dataIndex: "birthdate",
    //   render: (birthdate) => {
    //     return Utils.getAge(birthdate);
    //   },
    // },
    {
      title: "Phone number",
      dataIndex: "phone",
    },
    {
      title: "Email address",
      dataIndex: "email",
    },
    {
      title: "National Register ID",
      dataIndex: "nationalRegisterID",
    },
    {
      title: "Registered date",
      dataIndex: "createdAt",
      render: (createdAt) => Utils.formatDateTime(createdAt),
    },
    {
      title: "",
      key: "view",
      dataIndex: "id",
      render: (id, record) => {
        return <Link to={`/users/${id}`}>View</Link>;
      },
    },
  ];

  const projectColumns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Creation date",
      dataIndex: "createdAt",
      render: (createdAt) => Utils.formatDateTime(createdAt),
    },
    {
      title: "",
      dataIndex: "id",
      width: 100,
      render: (id) => {
        return <Link to={`/projects/${id}`}>View Project</Link>;
      },
    },
  ];

  const filteredItems = kpiUsers.filter((record) => {
    return (
      `${record.id} ${record.title} ${record.firstname} ${record.lastname} ${
        record.phone
      } ${record.email} ${record.email} ${Utils.formatDateTime(
        record.createdAt
      )}`
        .toLowerCase()
        .indexOf(searchText.toLowerCase()) >= 0
    );
  });

  return (
    <div>
      <h2 className="title gx-mb-4">
        <IntlMessages id="sidebar.home" />
      </h2>

      <Card
        title="New Registered users of the last 30 days"
        className="card-no-bottom"
      >
        <Spin spinning={loadingUsers}>
          <SearchBox
            styleName="gx-d-none gx-d-lg-block gx-lt-icon-search-bar-lg"
            placeholder="Search by ID, phone number, ..."
            onChange={(e) => setSearchText(e.target.value)}
            value={searchText}
          />
          <Table
            className="gx-table-responsive"
            dataSource={filteredItems}
            columns={columns}
            size="middle"
            expandable={{
              expandedRowRender: (record) => (
                <div style={{ marginLeft: 30, width: "50%" }}>
                  <p style={{ margin: 0 }}>
                    list of projects associated with the doctor
                  </p>
                  <Table
                    className="gx-table-responsive"
                    dataSource={record.Projects}
                    columns={projectColumns}
                    pagination={{ hideOnSinglePage: true }}
                    size="small"
                  />
                </div>
              ),
              rowExpandable: (record) => record.Projects.length > 0,
            }}
            rowKey="id"
          />
        </Spin>
      </Card>

      <Spin spinning={loadingStats}>
        <Card title="Few statistics" className="card-no-bottom">
          <Row>
            <Col xl={6} lg={12} md={12} sm={12} xs={12} className="gx-col-full">
              <IconWithTextCard
                // icon="orders"
                // iconColor="geekblue"
                title={kpiStats ? kpiStats.numberOfUsers : 0}
                subTitle={<IntlMessages id="app.home.numberOfUsers" />}
              />
            </Col>

            <Col xl={6} lg={12} md={12} sm={12} xs={12} className="gx-col-full">
              <IconWithTextCard
                // icon="revenue-new"
                // iconColor="primary"
                title={kpiStats ? kpiStats.numberOfPatients : 0}
                subTitle={<IntlMessages id="app.home.numberOfPatients" />}
              />
            </Col>

            <Col xl={6} lg={12} md={12} sm={12} xs={12} className="gx-col-full">
              <IconWithTextCard
                // icon="visits"
                // iconColor="geekblue"
                title={kpiStats ? kpiStats.numberOfMessageSent : 0}
                subTitle={<IntlMessages id="app.home.numberOfMessages" />}
              />
            </Col>

            <Col xl={6} lg={12} md={12} sm={12} xs={12} className="gx-col-full">
              <IconWithTextCard
                // icon="queries"
                // iconColor="geekblue"
                title={kpiStats ? kpiStats.numberOfMessageReceived : 0}
                subTitle={<IntlMessages id="app.home.numberOfMessagesPer" />}
              />
            </Col>
          </Row>
        </Card>
      </Spin>
    </div>
  );
};

export default MainPage;
