import React from "react";
import { Route, Switch } from "react-router-dom";
import asyncComponent from "util/asyncComponent";
import Patients from "./Patients";
import Projects from "./Projects";
import Users from "./Users";

const App = ({ match }) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route
        path={`${match.url}main`}
        component={asyncComponent(() => import("./Main"))}
      />
      <Route path={`${match.url}patients`} component={Patients} />
      <Route path={`${match.url}projects`} component={Projects} />
      <Route path={`${match.url}users`} component={Users} />
    </Switch>
  </div>
);

export default App;
