import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table, Select, Spin, Card } from "antd";
import IntlMessages from "util/IntlMessages";
import Utils from "util/common";
import {
  getAllProjects,
  getAllPatients,
  getAllPathologies,
} from "../../appRedux/actions";
import { Link } from "react-router-dom";
import SearchBox from "components/SearchBox";
const { Option } = Select;

const PatientsPage = () => {
  const [projectId, setProjectId] = useState(null);
  const [searchText, setSearchText] = useState("");
  const dispatch = useDispatch();
  const { projects, pathologies } = useSelector(({ project }) => project);
  const { patients, loading } = useSelector(({ patient }) => patient);

  useEffect(() => {
    dispatch(getAllProjects());
  }, [dispatch]);

  useEffect(() => {
    if (projects.length > 0) {
      const pid = projects[0].id;
      setProjectId(pid);
    }
  }, [projects]);

  useEffect(() => {
    if (projectId) {
      dispatch(getAllPatients(projectId));
      dispatch(getAllPathologies(projectId));
    }
  }, [projectId]);

  const onChooseProject = (pid) => {
    setProjectId(pid);
  };

  const getPathologyName = (PathologyId) => {
    const found = pathologies
      ? pathologies.find((p) => p.id === PathologyId)
      : null;
    return found ? found.name : "";
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "First name",
      dataIndex: "firstname",
    },
    {
      title: "Phone",
      dataIndex: "phone",
    },
    // {
    //   title: "Email",
    //   dataIndex: "email",
    // },
    // {
    //   title: "Birthdate",
    //   dataIndex: "birthdate",
    //   render: (birthdate) => Utils.formatDate(birthdate),
    // },
    {
      title: "Creation date",
      dataIndex: "createdAt",
      render: (createdAt) => Utils.formatDate(createdAt),
    },
    {
      title: "Pathology",
      dataIndex: "PathologyId",
      render: (PathologyId) => getPathologyName(PathologyId),
    },
    // {
    //   title: "Zipcode",
    //   dataIndex: "zipcode",
    // },
    // {
    //   title: "Gender",
    //   dataIndex: "gender",
    // },
    // {
    //   title: "Has caregiver",
    //   dataIndex: "hasCaregiver",
    //   render: (hasCaregiver) => (hasCaregiver ? "OUI" : "NON"),
    // },
    // {
    //   title: "General Terms",
    //   dataIndex: "generalTerms",
    // },
    // {
    //   title: "Research Terms",
    //   dataIndex: "researchTerms",
    // },
    // {
    //   title: "Events",
    //   dataIndex: "status",
    //   key: "events",
    //   render: (status) => {
    //     return (
    //       <div style={{ display: "flex" }}>
    //         {status.map((value, index) => {
    //           return (
    //             <div
    //               style={{
    //                 margin: "0 11px 1px 0",
    //                 color: index === 0 ? "blue" : "#545454",
    //               }}
    //               key={`event-${index}`}
    //             >
    //               {value}
    //             </div>
    //           );
    //         })}
    //       </div>
    //     );
    //   },
    // },
    {
      title: "",
      width: 100,
      dataIndex: "id",
      key: "action",
      render: (id) => (
        <Link to={`/patients/${projectId}.${id}`}>
          <span className="gx-link">View</span>
        </Link>
      ),
    },
  ];

  const filteredItems = patients.filter((record) => {
    let statusLabel = "";
    record.status.forEach((value, index) => {
      statusLabel += " " + value;
    });
    return (
      `${record.id} ${record.firstname} ${record.lastname} ${
        record.phone
      } ${Utils.formatDate(record.createdAt)} ${Utils.formatDate(
        record.birthdate
      )} ${record.phone} ${record.email} ${record.generalTerms} ${
        record.researchTerms
      } ${record.gender} ${getPathologyName(
        record.PathologyId
      )} ${statusLabel} ${record.zipcode} ${
        record.hasCaregiver ? "OUI" : "NON"
      }`
        .toLowerCase()
        .indexOf(searchText.toLowerCase()) >= 0
    );
  });

  return (
    <div>
      <div style={{ display: "flex", alignItems: "center", marginBottom: 10 }}>
        <h2 className="title gx-mb-0">
          <IntlMessages id="sidebar.patients" />
        </h2>

        {/* <label>Project: </label> */}
        <Select
          placeholder="Project"
          dropdownClassName="full-width"
          value={projectId}
          onChange={onChooseProject}
          style={{ marginLeft: 10 }}
        >
          {projects.map((p, i) => {
            return (
              <Option key={`p-${i}`} value={p.id}>
                {p.name}
              </Option>
            );
          })}
        </Select>
      </div>
      <Card className="card-no-bottom">
        <SearchBox
          styleName="gx-d-none gx-d-lg-block gx-lt-icon-search-bar-lg"
          placeholder="Search by ID, phone number, ..."
          onChange={(e) => setSearchText(e.target.value)}
          value={searchText}
        />
        <Spin spinning={loading}>
          <Table
            className="gx-table-responsive"
            dataSource={filteredItems}
            columns={columns}
            size="middle"
            rowKey="id"
            style={{ marginTop: 10 }}
          />
        </Spin>
      </Card>
    </div>
  );
};

export default PatientsPage;
