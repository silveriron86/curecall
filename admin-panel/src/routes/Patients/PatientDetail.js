import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, Card, Spin, Button } from "antd";
import { EditOutlined } from "@ant-design/icons";
import {
  getPatient,
  getAllPathologies,
  getPatientMatrixes,
  getMatrixVariables,
  getPatientSMS,
  postPatientSMS,
  updatePatient,
} from "../../appRedux/actions";
import Utils from "../../util/common";
import PatientSMSExchanges from "./detail/Exchanges";
import PatientMatrix from "./detail/Matrix";
import PatientForm from "./detail/PatientForm";

const PatientDetailPage = (props) => {
  const dispatch = useDispatch();
  const [editMode, setEditMode] = useState(false);
  const { pathologies } = useSelector(({ project }) => project);
  const {
    patient,
    matrixes,
    variables,
    loading,
    loadingMatrixes,
    loadingVariables,
    loadingSMS,
    sms,
  } = useSelector(({ patient }) => patient);
  const { id } = props.match.params;
  let ids = id.split(".");
  const projectId = ids[0];
  const patientId = ids[1];

  useEffect(() => {
    dispatch(getPatient({ projectId, patientId }));
    dispatch(getAllPathologies(projectId));
    dispatch(getPatientMatrixes({ projectId, patientId }));
    dispatch(getPatientSMS({ projectId, patientId }));
  }, [dispatch]);

  const onSendSMS = (message) => {
    dispatch(
      postPatientSMS({
        projectId,
        patientId,
        data: {
          message,
          senderId: "CURECALL",
        },
      })
    );
  };

  const onSelectMatrix = (patientMatrixId) => {
    dispatch(getMatrixVariables({ projectId, patientId, patientMatrixId }));
  };

  const getValue = (v) => {
    return v ? v : "--";
  };

  const getPathologyName = (PathologyId) => {
    const found = pathologies
      ? pathologies.find((p) => p.id === PathologyId)
      : null;
    return found ? found.name : "";
  };

  if (!patient) {
    return (
      <Spin>
        <div style={{ height: 500 }}>
          <h2 className="title gx-mb-4">Patient Information</h2>
        </div>
      </Spin>
    );
  }

  const toggleEditPatient = () => {
    setEditMode(!editMode);
  };

  const onSavePatient = (data) => {
    console.log(data);
    setEditMode(false);
    dispatch(updatePatient({ projectId, patientId, data }));
  };

  const {
    firstname,
    lastname,
    generalTerms,
    researchTerms,
    createdAt,
    birthdate,
    hasCaregiver,
    phone,
    mail,
    zipcode,
    PathologyId,
  } = patient;
  const pathologyName = getPathologyName(PathologyId);

  return (
    <div>
      <h2 className="title gx-mb-4">Patient Information</h2>
      <Row>
        <Col span={16}>
          <Card title="About the patient">
            <Spin spinning={loading}>
              {editMode ? (
                <PatientForm
                  patient={patient}
                  pathologies={pathologies}
                  onSubmit={onSavePatient}
                  onCancel={toggleEditPatient}
                />
              ) : (
                <>
                  <Row>
                    <Col span={6}>
                      <h1>#{patient.id}</h1>
                      <span>Date de creation</span>
                      <div>
                        <span className="p-value">
                          {Utils.formatDate(createdAt)}
                        </span>
                      </div>
                    </Col>
                    <Col span={12}>
                      <div>
                        <span className="p-value">
                          {firstname} {lastname}
                        </span>
                      </div>
                      <div style={{ marginTop: 5 }}>
                        Date de noissance:{" "}
                        <span className="p-value">
                          {getValue(Utils.formatDate(birthdate))}
                        </span>
                      </div>
                      {pathologyName && (
                        <div style={{ marginTop: 5 }}>
                          <span className="p-value">{pathologyName}</span>
                        </div>
                      )}
                      <div style={{ marginTop: 5 }}>
                        Le patient dispose d'un aidant:{" "}
                        <span className="p-value">
                          {hasCaregiver ? "OUI" : "NON"}
                        </span>
                      </div>
                    </Col>
                    <Col span={6}>
                      <div>
                        General Term:{" "}
                        <span className="p-value">
                          {getValue(generalTerms)}
                        </span>
                      </div>
                      <div>
                        Research Term:{" "}
                        <span className="p-value">
                          {getValue(researchTerms)}
                        </span>
                      </div>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: 15 }}>
                    <Col span={24}>
                      {phone && (
                        <div>
                          <span className="p-value">{getValue(phone)}</span>
                        </div>
                      )}
                      {mail && (
                        <div>
                          <span className="p-value">{getValue(mail)}</span>
                        </div>
                      )}
                      <div>
                        Code postal:{" "}
                        <span className="p-value">{getValue(zipcode)}</span>
                      </div>
                    </Col>
                  </Row>
                </>
              )}
              {!editMode && (
                <Button
                  className="gx-mb-0"
                  onClick={toggleEditPatient}
                  type="primary"
                  icon={<EditOutlined style={{ marginTop: 2 }} />}
                  style={{ position: "absolute", bottom: 0, right: 5 }}
                >
                  Edit Patient
                </Button>
              )}
            </Spin>
          </Card>
          <PatientMatrix
            matrixes={matrixes}
            variables={variables}
            loadingMatrixes={loadingMatrixes}
            loadingVariables={loadingVariables}
            loadingSMS={loadingSMS}
            onSelectMatrix={onSelectMatrix}
          />
        </Col>
        <Col span={8}>
          <PatientSMSExchanges
            sms={sms}
            loading={loadingSMS}
            onSend={onSendSMS}
          />
        </Col>
      </Row>
    </div>
  );
};

export default PatientDetailPage;
