import React from "react";
import { Form, Select, Input, Col, Row, Button, Spin, DatePicker } from "antd";
import moment from "moment";

const { Option } = Select;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
};

const PatientForm = (props) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    let data = { ...values };
    data.phone = `+${data.prefix}${data.phone}`;
    data.birthdate = data.birthdate
      ? data.birthdate.format("YYYY-MM-DD HH:mm:ss")
      : "";
    delete data.prefix;
    props.onSubmit(data);
  };
  const { patient, pathologies } = props;
  const prefix = patient.phone ? patient.phone.substring(1, 3) : "33";
  const phone = patient.phone ? patient.phone.substring(3) : "";
  const birthdate = patient.birthdate ? moment(patient.birthdate) : null;
  const initialValues = { ...patient, prefix, phone, birthdate };

  return (
    <div>
      <h2 className="title gx-mb-4">User Detail</h2>
      <Form
        form={form}
        name="userForm"
        {...formItemLayout}
        onFinish={onFinish}
        initialValues={initialValues}
      >
        <Row>
          <Col span={6} className="ant-form-item-label">
            <label
              for="userForm_title"
              class="ant-form-item-required"
              title="Name"
            >
              Name
            </label>
          </Col>
          <Col span={18}>
            <div style={{ display: "flex" }}>
              <Form.Item
                name="firstname"
                rules={[
                  {
                    required: true,
                    message: "Please input first name",
                  },
                ]}
                autoComplete="off"
                style={{ marginRight: 0 }}
              >
                <Input placeholder="First name" style={{ width: 200 }} />
              </Form.Item>
              <Form.Item
                name="lastname"
                rules={[
                  {
                    required: true,
                    message: "Please input last name",
                  },
                ]}
                autoComplete="off"
                style={{ marginLeft: 10, marginRight: 0 }}
              >
                <Input placeholder="Last name" style={{ width: 200 }} />
              </Form.Item>
            </div>
          </Col>
        </Row>
        <Form.Item
          name="phone"
          label="Phone"
          rules={[
            {
              required: true,
              message: "Please input a mobile phone",
            },
          ]}
        >
          <Input
            addonBefore={
              <Form.Item name="prefix" noStyle>
                <Select style={{ width: 70 }}>
                  <Option value="33">+33</Option>
                </Select>
              </Form.Item>
            }
            style={{ width: 300 }}
          />
        </Form.Item>
        <Form.Item name="mail" label="Email" autoComplete="off">
          <Input type="email" placeholder="Email" style={{ width: 300 }} />
        </Form.Item>
        <Form.Item name="PathologyId" label="Pathology">
          <Select style={{ width: 300 }}>
            {pathologies.map((pathology, index) => {
              return (
                <Option key={`pathology-${index}`} value={pathology.id}>
                  {pathology.name}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item name="birthdate" label="Birthday">
          <DatePicker
            className="gx-module-date gx-my-1"
            format="DD/MM/YYYY"
            animateYearScrolling={false}
          />
        </Form.Item>
        <Form.Item name="zipcode" label="Zipcode">
          <Input placeholder="Zip code" style={{ width: 80 }} />
        </Form.Item>
        <Form.Item name="gender" label="Gender">
          <Select placeholder="Gender" style={{ width: 120 }}>
            <Option value={1}>Homme</Option>
            <Option value={2}>Femme</Option>
            <Option value={0}>NC</Option>
            <Option value={9}>NB</Option>
          </Select>
        </Form.Item>

        <Form.Item name="hasCaregiver" label="Has Caregiver">
          <Select placeholder="Has Caregiver" style={{ width: 80 }}>
            <Option value={false}>Non</Option>
            <Option value={true}>Oui</Option>
          </Select>
        </Form.Item>
        <Form.Item name="generalTerms" label="General Terms">
          <Input placeholder="General Terms" style={{ width: 300 }} />
        </Form.Item>
        <Form.Item name="researchTerms" label="Research Terms">
          <Input placeholder="Research Terms" style={{ width: 300 }} />
        </Form.Item>
        <Row style={{ alignItems: "center" }}>
          <Col span={6}></Col>
          <Button type="primary" className="gx-mb-0" htmlType="submit">
            Submit
          </Button>{" "}
          <Button type="link" className="gx-mb-0" onClick={props.onCancel}>
            Cancel
          </Button>
        </Row>
      </Form>
    </div>
  );
};

export default PatientForm;
