import React, { useState } from "react";
import { Card, Button, Spin } from "antd";
import { SendOutlined } from "@ant-design/icons";
import moment from "moment";
import CustomScrollbars from "util/CustomScrollbars";
import Conversation from "components/chat/Conversation/index";

const PatientSMSExchanges = (props) => {
  const [message, setMessage] = useState("");
  const updateMessageValue = (evt) => {
    setMessage(evt.target.value);
  };

  const _handleKeyPress = (e) => {
    if (e.key === "Enter") {
      submitComment();
    }
  };

  const submitComment = () => {
    if (message !== "") {
      // send
      props.onSend(message);
      setMessage("");
    }
  };

  const { sms, loading } = props;
  let formattedSMS = [];
  sms.forEach((s) => {
    let row = { ...s };
    row.sentAt = row.sentAt
      ? moment(row.sentAt, "YYYY-MM-DDTHH:mm:ssZ").format("DD/MM/YYYY HH:mm:ss")
      : "";
    row.type = row.inbound ? "sent" : "received";
    formattedSMS.push(row);
  });

  return (
    <Spin spinning={loading}>
      <Card title="SMS exchanges" className="card-no-bottom">
        <CustomScrollbars className="gx-chat-list-scroll">
          <Conversation conversationData={formattedSMS} selectedUser={null} />
        </CustomScrollbars>
        <div className="gx-chat-main-footer">
          <div
            className="gx-flex-row gx-align-items-center"
            // style={{ maxHeight: 51 }}
          >
            <div className="gx-col">
              <div className="gx-form-group">
                <textarea
                  id="required"
                  className="ant-input gx-chat-textarea"
                  onKeyUp={_handleKeyPress}
                  onChange={updateMessageValue}
                  value={message}
                  rows={10}
                  maxLength={160}
                  placeholder="Type and hit enter to send message"
                  style={{
                    height: 100,
                    border: "1px solid #eee",
                  }}
                />
              </div>
            </div>
            {/* <i
            className="gx-icon-btn icon icon-sent"
            onClick={submitComment}
            title="Send"
          /> */}
            <Button
              onClick={submitComment}
              type="primary"
              icon={<SendOutlined style={{ marginTop: 2 }} />}
            >
              Send
            </Button>
          </div>
        </div>
      </Card>
    </Spin>
  );
};

export default PatientSMSExchanges;
