import React, { useState, useEffect } from "react";
import { Card, Table, Spin } from "antd";
import Utils from "util/common";

const PatientMatrix = (props) => {
  const [rowId, setRowId] = useState(-1);
  const { matrixes, variables, loadingMatrixes, loadingVariables } = props;

  useEffect(() => {
    if (matrixes.length > 0) {
      setRowId(matrixes[0].id);
      props.onSelectMatrix(matrixes[0].id);
    }
  }, [matrixes]);

  const columns = [
    {
      title: "ID patient matrix",
      dataIndex: "id",
    },
    {
      title: "Name Title",
      dataIndex: "Matrix",
      render: (Matrix) => Matrix.title,
    },
    {
      title: "Sent at",
      dataIndex: "sentAt",
      render: (sentAt) => Utils.formatDate(sentAt),
    },
    {
      title: "Completed",
      dataIndex: "completed",
      render: (value) => (value ? "yes" : "no"),
    },
    {
      title: "Aborted",
      dataIndex: "aborted",
      render: (value) => (value ? "yes" : "no"),
    },
  ];

  const variableColumns = [
    {
      title: "Variable name",
      dataIndex: "Variable",
      render: (Variable) => Variable.name,
    },
    {
      title: "Absolute value",
      dataIndex: "absoluteValue",
    },
  ];

  const matrix = matrixes.find((m) => m.id === rowId);

  return (
    <Card title="Matrix" className="card-no-bottom">
      <Spin spinning={loadingMatrixes}>
        <Table
          className="gx-table-responsive"
          dataSource={matrixes}
          columns={columns}
          size="middle"
          rowClassName={(record) => (record.id === rowId ? "pinned-row" : "")}
          onRow={(record, rowIndex) => {
            return {
              onClick: (event) => {
                setRowId(record.id);
                props.onSelectMatrix(record.id);
              },
            };
          }}
        />
      </Spin>

      {rowId > 0 && (
        <>
          <h4>Details 2 {matrix.name}</h4>
          <Spin spinning={loadingVariables}>
            <Table
              className="gx-table-responsive"
              dataSource={variables}
              columns={variableColumns}
              size="middle"
            />
          </Spin>
        </>
      )}
    </Card>
  );
};

export default PatientMatrix;
