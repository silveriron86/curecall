import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import PatientsPage from "./PatientsList";
import AddPatient from "./AddPatient";
import PatientDetailPage from "./PatientDetail";

const Patients = ({ match }) => (
  <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/list`} />
    <Route path={`${match.url}/list`} component={PatientsPage} />
    <Route path={`${match.url}/add`} component={AddPatient} />
    <Route path={`${match.url}/:id`} component={PatientDetailPage} />
  </Switch>
);

export default Patients;
