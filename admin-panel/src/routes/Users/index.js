import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import UsersPage from "./UsersList";
import AddUser from "./AddUser";
import UserDetailPage from "./UserDetail";

const Users = ({ match }) => (
  <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/list`} />
    <Route path={`${match.url}/list`} component={UsersPage} />
    <Route path={`${match.url}/add`} component={AddUser} />
    <Route path={`${match.url}/:id`} component={UserDetailPage} />
  </Switch>
);

export default Users;
