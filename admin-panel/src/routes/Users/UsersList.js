import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table, Spin, Card } from "antd";
import SearchBox from "components/SearchBox";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";
import { getAllUsers } from "../../appRedux/actions/User";
import Utils from "util/common";

const UsersPage = () => {
  const dispatch = useDispatch();
  const [searchText, setSearchText] = useState("");
  const { users, loading } = useSelector(({ user }) => user);

  useEffect(() => {
    dispatch(getAllUsers());
  }, [dispatch]);
  console.log(users);

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Name",
      dataIndex: "title",
      render: (title, record, index) => {
        return `${title} ${record.firstname} ${record.lastname}`;
      },
    },
    {
      title: "Mobile phone",
      dataIndex: "phone",
      render: (phone, record) => {
        return record.verified ? <font color="green">{phone}</font> : phone;
      },
    },
    {
      title: "Creation date",
      dataIndex: "createdAt",
      render: (createdAt) => Utils.formatDate(createdAt),
    },
    {
      title: "National Register ID",
      dataIndex: "nationalRegisterID",
    },
    {
      title: "General Terms",
      dataIndex: "generalTerms",
    },
    {
      title: "Specialty",
      dataIndex: "speciality",
    },
    {
      title: "Business ID",
      dataIndex: "businessID",
    },
    {
      title: "Company Name",
      dataIndex: "companyName",
    },
    {
      title: "Address",
      dataIndex: "street",
      render: (street, record, index) => {
        const { zipcode, city } = record;
        return `${street ? street : ""} ${zipcode ? zipcode : ""} ${
          city ? city : ""
        }`.trim();
      },
    },
    {
      title: "Membership",
      dataIndex: "membership",
    },
    {
      title: "",
      width: 30,
      dataIndex: "id",
      key: "action",
      render: (id) => (
        <a href={`${id}`}>
          <span className="gx-link">View</span>
        </a>
      ),
    },
  ];

  const projectColumns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Creation date",
      dataIndex: "createdAt",
      render: (createdAt) => Utils.formatDate(createdAt),
    },
    {
      title: "",
      dataIndex: "id",
      width: 100,
      render: (id) => {
        return <Link to={`/projects/${id}`}>View Project</Link>;
      },
    },
  ];

  const filteredItems = users.filter((record) => {
    return (
      `${record.id} ${record.title} ${record.firstname} ${record.lastname} ${
        record.phone
      } ${Utils.formatDate(record.createdAt)} ${record.nationalRegisterID} ${
        record.generalTerms
      } ${record.speciality} ${record.businessID} ${record.companyName} ${
        record.membership
      } ${record.street} ${record.zipcode} ${record.city}`
        .toLowerCase()
        .indexOf(searchText.toLowerCase()) >= 0
    );
  });

  return (
    <div>
      <h2 className="title gx-mb-4">
        <IntlMessages id="sidebar.users" />
      </h2>
      <Card className="card-no-bottom">
        <SearchBox
          styleName="gx-d-none gx-d-lg-block gx-lt-icon-search-bar-lg"
          placeholder="Search by ID, phone number, ..."
          onChange={(e) => setSearchText(e.target.value)}
          value={searchText}
        />
        <Spin spinning={loading}>
          <Table
            className="gx-table-responsive"
            dataSource={filteredItems}
            columns={columns}
            size="middle"
            expandable={{
              expandedRowRender: (record) => (
                <div style={{ marginLeft: 30, width: "50%" }}>
                  <p style={{ margin: 0 }}>
                    list of projects associated with the doctor
                  </p>
                  <Table
                    className="gx-table-responsive"
                    dataSource={record.Projects}
                    columns={projectColumns}
                    pagination={{ hideOnSinglePage: true }}
                    size="small"
                  />
                </div>
              ),
              rowExpandable: (record) => record.Projects.length > 0,
            }}
            rowKey="id"
          />
        </Spin>
      </Card>
    </div>
  );
};

export default UsersPage;
