import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Form,
  Select,
  Input,
  Col,
  Row,
  Button,
  notification,
  Spin,
} from "antd";
import { Link } from "react-router-dom";
import Utils from "util/common";
import { getUser, updateUser } from "../../appRedux/actions/User";
const { Option } = Select;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
};

const UserDetailPage = (props) => {
  const { id } = props.match.params;
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { user, errorMessage, loading } = useSelector(({ user }) => user);

  useEffect(() => {
    console.log("*** user id = ", id);
    dispatch(getUser(id));
  }, [dispatch]);

  useEffect(() => {
    if (errorMessage) {
      notification.error({
        message: "Erreur",
        description: errorMessage,
      });
    }
  }, [errorMessage]);

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    dispatch(updateUser({ id, data: Utils.cleanObject(values) }));
  };

  console.log("user = ", user);

  return (
    <Spin spinning={loading}>
      <div>
        <h2 className="title gx-mb-4">User Detail</h2>
        {user && (
          <Form
            form={form}
            name="userForm"
            {...formItemLayout}
            onFinish={onFinish}
            initialValues={user}
          >
            <Row>
              <Col span={6} className="ant-form-item-label">
                <label
                  for="userForm_title"
                  class="ant-form-item-required"
                  title="Name"
                >
                  Name
                </label>
              </Col>
              <Col span={18}>
                <div style={{ display: "flex" }}>
                  <Form.Item
                    name="title"
                    rules={[
                      {
                        required: true,
                        message: "Please select a title!",
                      },
                    ]}
                    style={{ marginRight: 0 }}
                  >
                    <Select placeholder="Title" style={{ width: 80 }}>
                      <Option value="Dr.">Dr.</Option>
                      <Option value="M.">M.</Option>
                      <Option value="Mme.">Mme.</Option>
                      <Option value="Pr.">Pr.</Option>
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="firstname"
                    rules={[
                      {
                        required: true,
                        message: "Please input first name",
                      },
                    ]}
                    autoComplete="off"
                    style={{ marginLeft: 10, marginRight: 0 }}
                  >
                    <Input placeholder="First name" style={{ width: 200 }} />
                  </Form.Item>
                  <Form.Item
                    name="lastname"
                    rules={[
                      {
                        required: true,
                        message: "Please input last name",
                      },
                    ]}
                    autoComplete="off"
                    style={{ marginLeft: 10, marginRight: 0 }}
                  >
                    <Input placeholder="Last name" style={{ width: 200 }} />
                  </Form.Item>
                </div>
              </Col>
            </Row>
            <Form.Item
              name="email"
              label="Email"
              rules={[
                {
                  required: true,
                  type: "email",
                  message: "Please input email",
                },
              ]}
              autoComplete="off"
            >
              <Input type="email" placeholder="Email" style={{ width: 300 }} />
            </Form.Item>
            <Form.Item name="nationalRegisterID" label="National Register ID">
              <Input
                placeholder="National Register ID"
                style={{ width: 300 }}
              />
            </Form.Item>
            <Form.Item name="generalTerms" label="General Terms">
              <Input placeholder="General Terms" style={{ width: 300 }} />
            </Form.Item>
            <Form.Item name="speciality" label="Speciality">
              <Input placeholder="Speciality" style={{ width: 300 }} />
            </Form.Item>
            <Form.Item name="businessID" label="Business ID">
              <Input placeholder="Business ID" style={{ width: 300 }} />
            </Form.Item>
            <Form.Item name="companyName" label="Company Name">
              <Input placeholder="Company Name" style={{ width: 300 }} />
            </Form.Item>
            <Form.Item name="street" label="Street">
              <Input placeholder="Street" style={{ width: 300 }} />
            </Form.Item>
            <Row>
              <Col span={6} className="ant-form-item-label">
                <label for="userForm_title" title="Name">
                  Zipcode/City
                </label>
              </Col>
              <Col span={18}>
                <div style={{ display: "flex" }}>
                  <Form.Item name="zipcode" style={{ marginRight: 0 }}>
                    <Input placeholder="Zip code" style={{ width: 80 }} />
                  </Form.Item>
                  <Form.Item
                    name="city"
                    style={{ marginLeft: 10, marginRight: 0 }}
                  >
                    <Input placeholder="City" style={{ width: 300 }} />
                  </Form.Item>
                </div>
              </Col>
            </Row>
            <Form.Item label="Membership" className="gx-mb-0">
              <span className="ant-form-text">{user.membership}</span>
              {/* <Select placeholder="Membership" style={{ width: "auto" }}>
              <Option value="WAITING_LIST">WAITING_LIST</Option>
              <Option value="FREEMIUM">FREEMIUM</Option>
              <Option value="PREMIUM">PREMIUM</Option>
              <Option value="NONE">NONE</Option>
            </Select> */}
            </Form.Item>
            <Form.Item label="Created Date">
              <span className="ant-form-text">
                {Utils.formatDateTime(user.createdAt)}
              </span>
            </Form.Item>
            <Row style={{ alignItems: "center" }}>
              <Col span={6}></Col>
              <Button type="primary" className="gx-mb-0" htmlType="submit">
                Submit
              </Button>{" "}
              <Link to="/users">Cancel</Link>
            </Row>
          </Form>
        )}
      </div>
    </Spin>
  );
};

export default UserDetailPage;
