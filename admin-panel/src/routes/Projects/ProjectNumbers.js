import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Table,
  Button,
  Modal,
  Input,
  Select,
  notification,
  Spin,
  Popconfirm,
  Form,
  Checkbox,
  Card,
} from "antd";
import { PlusCircleOutlined } from "@ant-design/icons";
import SearchBox from "components/SearchBox";
import {
  getAllPhoneNumbers,
  updatePhoneNumber,
  createPhoneNumber,
  deletePhoneNumber,
} from "../../appRedux/actions";
import { Link } from "react-router-dom";
import Utils from "util/common";

const { Option } = Select;

const ProjectNumbersPage = (props) => {
  const [form] = Form.useForm();
  const [id, setId] = useState(-1);
  const [searchText, setSearchText] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [readOnly, setReadOnly] = useState(false);
  const dispatch = useDispatch();
  const { phoneNumbers, errorMessage, loading } = useSelector(
    ({ phoneNumber }) => phoneNumber
  );

  useEffect(() => {
    dispatch(getAllPhoneNumbers());
  }, [dispatch]);

  useEffect(() => {
    if (errorMessage) {
      notification.error({
        message: "Erreur",
        description: errorMessage,
      });
    }
  }, [errorMessage]);

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Phone number",
      dataIndex: "phone",
    },
    {
      title: "Shared or not",
      dataIndex: "shared",
      render: (shared) => (shared ? "Yes" : "No"),
    },
    {
      title: "Linked Projects",
      dataIndex: "Projects",
      render: (Projects) => Projects.length,
    },
    // {
    //   title: "Lien vers le projet",
    //   dataIndex: "Projects",
    //   key: "action",
    //   render: (Projects, record, index) => {
    //     // (uniquement pour les numéros de téléphone not shared)
    //     // record.shared ||
    //     if (!Projects || Projects.length === 0) {
    //       return null;
    //     }
    //     let rows = [];
    //     console.log(Projects);
    //     Projects.forEach((p, i) => {
    //       rows.push(
    //         <Link
    //           key={`p-${p.id}-index`}
    //           to={`/projects/${p.id}`}
    //           style={{ display: "inline-block" }}
    //         >
    //           [{p.name}]
    //         </Link>
    //       );
    //     });
    //     console.log(rows);
    //     return rows;
    //   },
    // },
    {
      title: "Action",
      dataIndex: "id",
      key: "action",
      align: "center",
      width: 60,
      render: (id, record) => {
        return (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Button
              type="link"
              className="no-margin"
              style={{ marginRight: 0 }}
              onClick={() => openEditModal(id)}
            >
              Edit
            </Button>
            {record.Projects.length < 1 && (
              <Popconfirm
                title="Are you sure？"
                okText="Yes"
                cancelText="No"
                onConfirm={() => onRemoveNumber(id)}
              >
                <Button type="link" className="no-margin">
                  Remove
                </Button>
              </Popconfirm>
            )}
          </div>
        );
      },
    },
  ];

  const projectColumns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Creation date",
      dataIndex: "createdAt",
      render: (createdAt) => Utils.formatDateTime(createdAt),
    },
    {
      title: "",
      dataIndex: "id",
      width: 100,
      render: (id) => {
        return <Link to={`/projects/${id}`}>Link</Link>;
      },
    },
  ];

  const openEditModal = (id) => {
    setId(id);
    const item = phoneNumbers.find((record) => record.id === id);
    const { phone, shared, Projects } = item;

    form.setFieldsValue({
      prefix: phone ? phone.substring(1, 3) : "33",
      phone: phone ? phone.substring(3) : "",
      shared,
    });

    setReadOnly(Projects.length > 0);
    setModalVisible(true);
  };

  const openAddModal = () => {
    setId(-1);
    form.resetFields();
    form.setFieldsValue({
      prefix: "33",
      phone: "",
      shared: false,
    });
    setModalVisible(true);
  };

  const toggleAddModal = () => {
    setModalVisible(!modalVisible);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const onFinish = (values) => {
    console.log("Received values of form", values);
    let data = { ...values };
    data.phone = `+${data.prefix}${data.phone}`;
    delete data.prefix;
    if (id >= 0) {
      dispatch(updatePhoneNumber({ id, data }));
    } else {
      dispatch(createPhoneNumber(data));
    }
    setModalVisible(false);
  };

  const onRemoveNumber = (id) => {
    dispatch(deletePhoneNumber(id));
  };

  const filteredItems = phoneNumbers.filter((record) => {
    return (
      `${record.id} ${record.phone} ${record.shared ? "Yes" : "No"}`
        .toLowerCase()
        .indexOf(searchText.toLowerCase()) >= 0
    );
  });

  return (
    <div>
      <div style={{ display: "flex", alignItems: "center", marginBottom: 10 }}>
        <h2 className="title" style={{ marginBottom: 0 }}>
          Management of Mobile Numbers
        </h2>
        <Button
          type="primary"
          style={{ marginBottom: 0, marginLeft: 20 }}
          onClick={openAddModal}
        >
          <PlusCircleOutlined />
          Add
        </Button>
      </div>

      <Spin spinning={loading}>
        <Card className="card-no-bottom">
          <SearchBox
            styleName="gx-d-none gx-d-lg-block gx-lt-icon-search-bar-lg"
            placeholder="Search by ID, phone number, ..."
            onChange={(e) => setSearchText(e.target.value)}
            value={searchText}
          />
          <Table
            className="gx-table-responsive"
            dataSource={filteredItems}
            columns={columns}
            size="middle"
            expandable={{
              expandedRowRender: (record) => (
                <div style={{ marginLeft: 30, width: "50%" }}>
                  <p style={{ margin: 0 }}>
                    list of projects associated with the number
                  </p>
                  <Table
                    className="gx-table-responsive"
                    dataSource={record.Projects}
                    columns={projectColumns}
                    pagination={{ hideOnSinglePage: true }}
                    size="small"
                  />
                </div>
              ),
              rowExpandable: (record) => record.Projects.length > 0,
            }}
          />
        </Card>
      </Spin>

      <Modal
        title={`${id >= 0 ? "Edit" : "New"} number`}
        visible={modalVisible}
        footer={false}
        onCancel={toggleAddModal}
      >
        <Form
          name="basic"
          form={form}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          className="gx-signin-form gx-form-row0"
        >
          <Form.Item
            name={"phone"}
            rules={[
              {
                required: true,
                message: "Please input a mobile phone",
              },
            ]}
            style={{ marginBottom: 5 }}
          >
            <Input
              readOnly={readOnly}
              placeholder="Mobile number"
              addonBefore={
                <Form.Item name={"prefix"} noStyle>
                  <Select style={{ width: 70 }} disabled={readOnly}>
                    <Option value="33">+33</Option>
                  </Select>
                </Form.Item>
              }
              style={{ width: 300 }}
            />
          </Form.Item>
          <Form.Item
            name="shared"
            valuePropName="checked"
            style={{ marginBottom: 5 }}
          >
            <Checkbox>Shared or not</Checkbox>
          </Form.Item>
          <Button type="primary" className="gx-mb-0" htmlType="submit">
            Submit
          </Button>{" "}
          <Button type="link" className="gx-mb-0" onClick={toggleAddModal}>
            Cancel
          </Button>
        </Form>
      </Modal>
    </div>
  );
};

export default ProjectNumbersPage;
