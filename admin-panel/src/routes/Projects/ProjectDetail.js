import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Card,
  Form,
  Select,
  Input,
  Button,
  Col,
  Row,
  Popconfirm,
  Spin,
} from "antd";
import Utils from "util/common";
import {
  getProjectTypes,
  createProject,
  getProject,
  updateProject,
} from "../../appRedux/actions/Project";
import {
  getAllUsers,
  createUser,
  deleteUserProject,
  addUserProject,
} from "../../appRedux/actions/User";

const { Option } = Select;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
};
const ProjectDetailPage = (props) => {
  const [form] = Form.useForm();
  const [ownerForm] = Form.useForm();
  const { id } = props.match.params;
  const [owners, setOwners] = useState([]);
  const dispatch = useDispatch();
  const { projectTypes, project, loadingProject } = useSelector(
    ({ project }) => project
  );
  const { users, createdUser, loading } = useSelector(({ user }) => user);

  useEffect(() => {
    dispatch(getProjectTypes());
    dispatch(getAllUsers());
    if (id) {
      dispatch(getProject(id));
    }
  }, [dispatch]);

  useEffect(() => {
    if (project) {
      const { name, ProjectType, Users } = project;
      form.setFieldsValue({
        name,
        projectTypeId: ProjectType.id,
      });
      setOwners([...Users]);
    }
  }, [project]);

  useEffect(() => {
    if (owners.length > 0) {
      owners.map((o) => {
        if (o.phone) {
          o.prefix = o.phone.substring(1, 3);
          o.phone = o.phone.substring(3);
        }
        console.log(o);
        return o;
      });
      ownerForm.setFieldsValue({
        users: owners,
      });
      console.log(owners);
    } else {
      ownerForm.setFieldsValue({
        users: [],
      });
    }
  }, [owners]);

  useEffect(() => {
    console.log(createdUser);
    if (createdUser) {
      let curList = [...owners];
      for (let i = 0; i < curList.length; i++) {
        if (typeof curList[i].id === "undefined") {
          curList[i] = { ...createdUser };
        }
      }
      setOwners(curList);
      dispatch(
        addUserProject({
          userId: createdUser.id,
          data: {
            projectId: id,
            role: "owner",
          },
        })
      );
    }
  }, [createdUser]);

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    const { name, projectTypeId } = values;

    if (id) {
      dispatch(
        updateProject({
          id,
          data: {
            name,
            projectTypeId,
          },
        })
      );
    }

    let doctors = [];
    owners.forEach((o, _i) => {
      doctors.push({
        userId: o.id,
        role: "owner",
      });
    });
    dispatch(
      createProject({
        name,
        projectTypeId,
        users: doctors,
      })
    );
  };

  const onSelectUser = (userId) => {
    dispatch(
      addUserProject({
        userId,
        data: {
          projectId: id,
          role: "owner",
        },
      })
    );

    const foundUser = users.find((u) => u.id === userId);
    let curList = [...owners];
    curList.push(foundUser);
    setOwners(curList);
  };

  const onAddNewUser = () => {
    let curList = [...owners];
    curList.push({ prefix: "33" });
    setOwners(curList);
  };

  const onRemoveOwner = (index) => {
    if (id) {
      // edit
      console.log(index, owners[index].id, id);
      dispatch(
        deleteUserProject({
          userId: owners[index].id,
          data: {
            projectId: id,
          },
        })
      );
    }
    let curList = [...owners];
    curList.splice(index, 1);
    setOwners(curList);
  };

  const onOwnerFinish = (values) => {
    console.log("owner = ", values);
    const newUser = values.users.find((v) => typeof v.id === "undefined");
    let data = { ...newUser };
    data.phone = `+${data.prefix}${data.phone}`;
    data.password = Utils.generateRandomStr(8);
    delete data.prefix;
    dispatch(createUser(data));
  };

  const isNew = typeof id === "undefined";
  let canSaveProject = owners.length === 0;
  if (canSaveProject) {
    const found = owners.find((o) => typeof o.id === "undefined");
    if (found) canSaveProject = false;
  }

  const filteredUsers = users.filter((u) => {
    if (!owners.length) return true;
    const found = owners.find((o) => o.id === u.id);
    return found ? false : true;
  });

  console.log("*** owners: ", owners);
  return (
    <div>
      <h2 className="title gx-mb-4">
        {isNew ? "Add a new project" : `Edit project`}
      </h2>
      <Spin spinning={loadingProject || loading}>
        <Form
          form={form}
          name="validate_other"
          {...formItemLayout}
          onFinish={onFinish}
        >
          <Button
            type="primary"
            htmlType="submit"
            style={{ marginTop: -50, marginRight: 0, float: "right" }}
            disabled={canSaveProject}
          >
            Save Project
          </Button>
          <Card title="About the project" className="card-no-bottom gx-mb-0">
            <Form.Item
              name="name"
              label="Name of the project"
              rules={[
                {
                  required: true,
                  message: "Please input the name of the project",
                },
              ]}
              autoComplete="off"
            >
              <Input placeholder="Name of the project" style={{ width: 300 }} />
            </Form.Item>
            <Form.Item
              name="projectTypeId"
              label="Type"
              // hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please select a type!",
                },
              ]}
            >
              <Select placeholder="Type" style={{ width: "auto" }}>
                {projectTypes.map((type, i) => {
                  return (
                    <Option key={`type-${i}`} value={type.id}>
                      {type.name}
                    </Option>
                  );
                })}
              </Select>
            </Form.Item>
          </Card>
          <div>
            <Select
              placeholder="Select User"
              dropdownClassName="full-width"
              onChange={onSelectUser}
              value={null}
            >
              {filteredUsers.map((user, i) => {
                return (
                  <Option key={`user-${i}`} value={user.id}>
                    {`${user.title} ${user.firstname} ${user.lastname}`.trim()}
                  </Option>
                );
              })}
            </Select>
            <Button
              type="link"
              htmlType="button"
              style={{ marginTop: 15 }}
              onClick={onAddNewUser}
            >
              Add a new user
            </Button>
          </div>
          <Form form={ownerForm} {...formItemLayout} onFinish={onOwnerFinish}>
            <Form.List name="users">
              {(fields, { add, remove }) => (
                <>
                  {fields.map((field, index) => (
                    <Card
                      key={field.key}
                      title={
                        <>
                          <span>
                            {`Owner - ${Utils.getOrdinalSuffix(
                              index + 1
                            )} user details`}
                          </span>
                          <Popconfirm
                            title="Are you sure？"
                            okText="Yes"
                            cancelText="No"
                            onConfirm={() => onRemoveOwner(index)}
                          >
                            <Button
                              type="link"
                              style={{
                                position: "absolute",
                                right: 10,
                                marginTop: -5,
                              }}
                            >
                              Remove the user
                            </Button>
                          </Popconfirm>
                        </>
                      }
                      className="card-no-bottom"
                    >
                      <Form.Item
                        {...field}
                        name={[field.name, "nationalRegisterID"]}
                        fieldKey={[field.fieldKey, "nationalRegisterID"]}
                        label="RPPS"
                      >
                        <Input placeholder="RPPS" style={{ width: 300 }} />
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "title"]}
                        fieldKey={[field.fieldKey, "title"]}
                        label="Title"
                        rules={[
                          {
                            required: true,
                            message: "Please select a title!",
                          },
                        ]}
                      >
                        <Select placeholder="Type" style={{ width: "auto" }}>
                          <Option value="Dr.">Dr.</Option>
                          <Option value="M.">M.</Option>
                          <Option value="Mme.">Mme.</Option>
                          <Option value="Pr.">Pr.</Option>
                        </Select>
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "firstname"]}
                        fieldKey={[field.fieldKey, "firstname"]}
                        label="First name"
                        rules={[
                          {
                            required: true,
                            message: "Please input a first name",
                          },
                        ]}
                      >
                        <Input
                          placeholder="First name"
                          style={{ width: 300 }}
                        />
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "lastname"]}
                        fieldKey={[field.fieldKey, "lastname"]}
                        label="Last name"
                        rules={[
                          {
                            required: true,
                            message: "Please input a last name",
                          },
                        ]}
                      >
                        <Input placeholder="Last name" style={{ width: 300 }} />
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "phone"]}
                        fieldKey={[field.fieldKey, "phone"]}
                        label="Mobile phone"
                        rules={[
                          {
                            required: true,
                            message: "Please input a mobile phone",
                          },
                        ]}
                      >
                        <Input
                          addonBefore={
                            <Form.Item
                              {...field}
                              name={[field.name, "prefix"]}
                              fieldKey={[field.fieldKey, "prefix"]}
                              noStyle
                            >
                              <Select style={{ width: 70 }}>
                                <Option value="33">+33</Option>
                              </Select>
                            </Form.Item>
                          }
                          style={{ width: 300 }}
                        />
                      </Form.Item>
                      <Form.Item
                        {...field}
                        name={[field.name, "email"]}
                        fieldKey={[field.fieldKey, "email"]}
                        label="Email"
                        rules={[
                          {
                            required: true,
                            type: "email",
                            message: "Please input an email",
                          },
                        ]}
                      >
                        <Input
                          type="email"
                          placeholder="Email"
                          style={{ width: 300 }}
                        />
                      </Form.Item>
                      <Row>
                        <Col span={6}></Col>
                        {owners[index] &&
                          typeof owners[index].id === "undefined" && (
                            <Button
                              type="primary"
                              htmlType="submit"
                              className="login-form-button gx-mt-1"
                            >
                              Add the owner
                            </Button>
                          )}
                        <Button
                          type="link"
                          htmlType="button"
                          onClick={onAddNewUser}
                          style={{ marginTop: 5 }}
                        >
                          + Add another user
                        </Button>
                      </Row>
                    </Card>
                  ))}
                </>
              )}
            </Form.List>
          </Form>
          <Button
            type="primary"
            htmlType="submit"
            style={{ marginTop: -20, float: "right" }}
            disabled={owners.length === 0}
          >
            Save Project
          </Button>
        </Form>
      </Spin>
    </div>
  );
};

export default ProjectDetailPage;
