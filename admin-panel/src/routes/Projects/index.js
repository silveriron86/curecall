import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import ProjectsPage from "./ProjectsList";
import ProjectDetailPage from "./ProjectDetail";
import ProjectNumbersPage from "./ProjectNumbers";

const Projects = ({match}) => (
  <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/list`} />
    <Route path={`${match.url}/list`} component={ProjectsPage}/>
    <Route path={`${match.url}/add`} component={ProjectDetailPage}/>
    <Route path={`${match.url}/numbers`} component={ProjectNumbersPage}/>
    <Route path={`${match.url}/:id`} component={ProjectDetailPage}/>
  </Switch>
);

export default Projects;
