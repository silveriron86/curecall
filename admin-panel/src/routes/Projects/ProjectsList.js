import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table, Button, Tooltip, Spin, Card, Popconfirm, notification } from "antd";
import IntlMessages from "util/IntlMessages";
import { PlusCircleOutlined, PushpinOutlined } from "@ant-design/icons";
import SearchBox from "components/SearchBox";
import {
  deleteProject,
  getAllProjects,
  getProjectTypes,
} from "../../appRedux/actions/Project";
import Utils from "util/common";

const ProjectsPage = () => {
  const [searchText, setSearchText] = useState("");
  const [pinId, setPinId] = useState(-1);
  const dispatch = useDispatch();
  const { projects, projectTypes, loading, errorMessage } = useSelector(
    ({ project }) => project
  );

  useEffect(() => {
    dispatch(getAllProjects());
    dispatch(getProjectTypes());
  }, [dispatch]);

  useEffect(() => {
    console.log(errorMessage)
    if (errorMessage !== '') {
      notification.error({
        message: "Erreur",
        description: errorMessage,
      });
    }
  }, [errorMessage]);

  console.log(projects, projectTypes);

  const onSortString = (key, a, b) => {
    const stringA = a[key] ? a[key] : "";
    const stringB = b[key] ? b[key] : "";
    return stringA.localeCompare(stringB);
  };

  const getRowClassName = (record, index) => {
    return record.id === pinId ? "pinned-row" : "";
  };

  const onTogglePin = (id) => {
    setPinId(id !== pinId ? id : -1);
  };

  const onDeleteProject = (id) => {
    dispatch(deleteProject(id))
  }

  // By default the table is sorted in descending order on the number of patients, but we can sort by Name, Type and Creation date.
  const columns = [
    {
      title: "",
      dataIndex: "id",
      key: "pin",
      width: 30,
      align: "center",
      render: (id) => {
        return (
          <a href="javascript:void(0)" onClick={() => onTogglePin(id)}>
            {id === pinId ? (
              <Tooltip title="Unpin this project">
                <PushpinOutlined style={{ color: "white" }} />
              </Tooltip>
            ) : (
              <Tooltip title="Pin this project">
                <PushpinOutlined />
              </Tooltip>
            )}
          </a>
        );
      },
    },
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Name of the project",
      dataIndex: "name",
      sorter: (a, b) => onSortString("name", a, b),
    },
    {
      title: "Type",
      dataIndex: "ProjectType",
      sorter: (a, b) => onSortString("ProjectType.name", a, b),
      render: (ProjectType) => ProjectType.name,
    },
    // {
    //   title: "List of the users",
    //   dataIndex: "users",
    // },
    {
      title: "Number of patients",
      dataIndex: "patientsCount",
      defaultSortOrder: "descend",
      sorter: (a, b) => a.patientsCount - b.patientsCount,
    },
    {
      title: "Creation date",
      dataIndex: "createdAt",
      sorter: (a, b) => onSortString("createdAt", a, b),
      render: (createdAt) => Utils.formatDateTime(createdAt),
    },
    {
      title: "Action",
      width: 100,
      dataIndex: "id",
      key: "action",
      render: (id) => (
        <div style={{display: 'flex'}}>
          <a href={`${id}`}>
            <span className="gx-link">View</span>
          </a>
          <Popconfirm
            title="Are you sure？"
            okText="Yes"
            cancelText="No"
            onConfirm={() => onDeleteProject(id)}
          >
            <Button type="link" className="no-margin">
              Delete
            </Button>
          </Popconfirm>
        </div>
      ),
    },
  ];

  const usersColumns = [
    {
      title: "User ID",
      dataIndex: "id",
    },
    {
      title: "User Name",
      dataIndex: "title",
      render: (title, record) =>
        `${title} ${record.firstname} ${record.lastname}`,
    },
    {
      title: "",
      width: 100,
      dataIndex: "id",
      key: "action",
      render: (id) => (
        <a href={`/users/${id}`}>
          <span className="gx-link">View User</span>
        </a>
      ),
    },
  ];

  const onExpandUsers = (record) => {
    return (
      <div style={{ marginLeft: 30, width: "50%" }}>
        {/* <p style={{ margin: 0 }}>list of the users</p> */}
        <Table
          className="gx-table-responsive"
          dataSource={record.Users}
          columns={usersColumns}
          pagination={{ hideOnSinglePage: true }}
          size="small"
        />
      </div>
    );
  };

  const handleFilter = (record) => {
    return (
      `${record.id} ${record.name} ${
        record.ProjectType.name
      } ${Utils.formatDateTime(record.createdAt)}`
        .toLowerCase()
        .indexOf(searchText.toLowerCase()) >= 0
    );
  };

  const items = projects.filter((p) => p.id !== pinId);
  const pinnedItem = projects.filter((p) => p.id === pinId);
  const filteredItems = items.filter(handleFilter);
  const filteredPinnedItem = pinnedItem.filter(handleFilter);

  return (
    <div>
      <div style={{ display: "flex", alignItems: "center", marginBottom: 10 }}>
        <h2 className="title" style={{ marginBottom: 0 }}>
          <IntlMessages id="sidebar.projects" />
        </h2>
        <Button
          type="primary"
          style={{ marginBottom: 0, marginLeft: 20 }}
          href="/projects/add"
        >
          <PlusCircleOutlined />
          Add
        </Button>
      </div>
      <Card className="card-no-bottom">
        <SearchBox
          styleName="gx-d-none gx-d-lg-block gx-lt-icon-search-bar-lg"
          placeholder="Search by ID, name of the project, ..."
          onChange={(e) => setSearchText(e.target.value)}
          value={searchText}
        />
        {filteredPinnedItem.length > 0 && (
          <Spin spinning={loading}>
            <Table
              className="gx-table-responsive pinned-table"
              dataSource={filteredPinnedItem}
              columns={columns}
              size="middle"
              rowClassName={getRowClassName}
              pagination={{ hideOnSinglePage: true }}
              showSorterTooltip={false}
              showHeader={false}
              expandable={{
                expandedRowRender: onExpandUsers,
                rowExpandable: (record) => record.Users.length > 0,
              }}
              rowKey="id"
            />
          </Spin>
        )}

        <Spin spinning={loading}>
          <Table
            className="gx-table-responsive"
            dataSource={filteredItems}
            columns={columns}
            size="middle"
            rowClassName={getRowClassName}
            expandable={{
              expandedRowRender: onExpandUsers,
              rowExpandable: (record) => record.Users.length > 0,
            }}
            rowKey="id"
          />
        </Spin>
      </Card>
    </div>
  );
};

export default ProjectsPage;
