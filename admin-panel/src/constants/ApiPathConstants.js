let _inv = {
  DEV: "http://localhost:3000/",
  BETA: "https://dapi.curecall.com/",
  QA: "https://dapi.curecall.com/",
  PRODUCTION: "https://api.curecall.com/",
};

module.exports = {
  getApiPath: () => {
    switch (process.env.REACT_APP_API_ENV || process.env.NODE_ENV) {
      case "development":
        return _inv.DEV;
      case "beta":
        return _inv.BETA;
      case "quality":
        return _inv.QA;
      case "production":
        return _inv.PRODUCTION;
      default:
        return _inv.DEV;
    }
  },
};
