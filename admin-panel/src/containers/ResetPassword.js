import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, Input, message } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";
import { verifySMSCode, hideMessage, resetPassword } from "../appRedux/actions";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

const ResetPassword = (props) => {
  const dispatch = useDispatch();
  const { loader, alertMessage, showMessage, smsUser } = useSelector(
    ({ auth }) => auth
  );
  const [sent, setSent] = useState(false);
  useEffect(() => {
    if (smsUser) {
      setSent(true);
    }
  }, [smsUser]);

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const onFinish = (values) => {
    console.log("Received values of form", values);
    dispatch(resetPassword({ password: values.password }));
  };

  const onSubmitCode = (values) => {
    const { code } = values;
    console.log("* code = ", code);
    dispatch(hideMessage());
    const phone = localStorage.getItem("REQUESTED_SMS_PHONE");
    console.log({ phone, code });
    dispatch(verifySMSCode({ phone, code }));
  };

  const layout = {
    labelCol: { span: 10 },
    wrapperCol: { span: 14 },
  };

  return (
    <div className="gx-login-container" id="reset-code-page">
      <div className="gx-login-content">
        <div className="gx-login-header">
          <img
            src={require("assets/images/Logo color.svg")}
            alt="Curecall"
            title="Curecall"
            width="100"
          />
        </div>
        <div className="gx-mb-4">
          <h2>Reset Password</h2>
          {!sent && <IntlMessages id="app.userAuth.enterCode" />}
        </div>
        {sent ? (
          <Form
            {...layout}
            initialValues={{ remember: true }}
            name="basic"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            className="gx-signin-form gx-form-row0"
          >
            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input!",
                },
                // ({ getFieldValue }) => ({
                //   validator(rule, value) {
                //     const isValid =
                //       value &&
                //       value.match(
                //         "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{1,100}$"
                //       );
                //     if (isValid) {
                //       return Promise.resolve();
                //     }
                //     return Promise.reject("INVALID_PASSWORD");
                //   },
                // }),
              ]}
            >
              <Input type="password" placeholder="Password" />
            </Form.Item>

            <Form.Item
              label="Confirm Password"
              name="confirm"
              dependencies={["password"]}
              rules={[
                {
                  required: true,
                  message: "Please confirm!",
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject("Dismatch");
                  },
                }),
              ]}
            >
              <Input type="password" placeholder="Confirm Password" />
            </Form.Item>
            <FormItem>
              <Button type="primary" className="gx-mb-0" htmlType="submit">
                Change
              </Button>{" "}
              <Link onClick={() => setSent(false)}>Cancel</Link>
            </FormItem>
          </Form>
        ) : (
          <>
            <Form
              name="codeForm"
              onFinish={onSubmitCode}
              onFinishFailed={onFinishFailed}
              className="gx-signin-form gx-form-row0"
            >
              <Form.Item
                name="code"
                rules={[
                  {
                    required: true,
                    message: "Please input the SMS code!",
                  },
                ]}
              >
                <Input placeholder="Pin Code" />
              </Form.Item>
              <div style={{ marginTop: 20, marginBottom: 10 }}>
                <Button type="primary" className="gx-mb-0" htmlType="submit">
                  Submit
                </Button>{" "}
                <Link to="/signin">
                  <IntlMessages id="app.userAuth.seConnecter" />
                </Link>
              </div>
            </Form>
          </>
        )}
      </div>

      {loader ? (
        <div
          className="gx-loader-view"
          style={{ position: "absolute", width: "100%" }}
        >
          <CircularProgress />
        </div>
      ) : null}
      {showMessage ? message.error(alertMessage.toString()) : null}
    </div>
  );
};

export default ResetPassword;
