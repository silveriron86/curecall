import React from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";
import UserProfile from "./UserProfile";
// import AppsNavigation from "./AppsNavigation";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE,
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";
import { useSelector } from "react-redux";

const SubMenu = Menu.SubMenu;
// const MenuItemGroup = Menu.ItemGroup;

const SidebarContent = ({ sidebarCollapsed, setSidebarCollapsed }) => {
  let { navStyle, themeType } = useSelector(({ settings }) => settings);
  let { pathname } = useSelector(({ common }) => common);

  const getNoHeaderClass = (navStyle) => {
    if (
      navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR ||
      navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR
    ) {
      return "gx-no-header-notifications";
    }
    return "";
  };
  const getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };
  const selectedKeys = pathname.substr(1);
  const defaultOpenKeys = selectedKeys.split("/")[0];
  console.log(selectedKeys, defaultOpenKeys);
  return (
    <>
      <SidebarLogo
        sidebarCollapsed={sidebarCollapsed}
        setSidebarCollapsed={setSidebarCollapsed}
      />
      <div className="gx-sidebar-content">
        <div
          className={`gx-sidebar-notifications ${getNoHeaderClass(navStyle)}`}
        >
          <UserProfile />
          {/* <AppsNavigation /> */}
        </div>
        <CustomScrollbars className="gx-layout-sider-scrollbar">
          <Menu
            defaultOpenKeys={[defaultOpenKeys]}
            selectedKeys={[selectedKeys]}
            theme={themeType === THEME_TYPE_LITE ? "lite" : "dark"}
            mode="inline"
          >
            <Menu.Item key="main">
              <Link to="/main">
                <i className="icon icon-widgets" />
                <span>
                  <IntlMessages id="sidebar.home" />
                </span>
              </Link>
            </Menu.Item>

            <SubMenu
              key="projects"
              popupClassName={getNavStyleSubMenuClass(navStyle)}
              title={
                <span>
                  {" "}
                  <i className="icon icon-apps" />
                  <span>
                    <IntlMessages id="sidebar.projects" />
                  </span>
                </span>
              }
            >
              <Menu.Item key="projects/list">
                <Link to="/projects/list">
                  <i className="icon icon-table" />
                  <span>
                    <IntlMessages id="sidebar.projects.list" />
                  </span>
                </Link>
              </Menu.Item>
              <Menu.Item key="projects/add">
                <Link to="/projects/add">
                  <i className="icon icon-add-circle" />
                  <span>
                    <IntlMessages id="sidebar.projects.add" />
                  </span>
                </Link>
              </Menu.Item>
              <Menu.Item key="projects/numbers">
                <Link to="/projects/numbers">
                  <i className="icon icon-all-contacts" />
                  <span>
                    <IntlMessages id="sidebar.projects.numbers" />
                  </span>
                </Link>
              </Menu.Item>
            </SubMenu>

            <SubMenu
              key="users"
              popupClassName={getNavStyleSubMenuClass(navStyle)}
              title={
                <span>
                  {" "}
                  <i className="icon icon-auth-screen" />
                  <span>
                    <IntlMessages id="sidebar.users" />
                  </span>
                </span>
              }
            >
              <Menu.Item key="users/list">
                <Link to="/users/list">
                  <i className="icon icon-table" />
                  <span>
                    <IntlMessages id="sidebar.users.list" />
                  </span>
                </Link>
              </Menu.Item>
              {/* <Menu.Item key="users/add">
                <Link to="/users/add">
                  <i className="icon icon-add-circle" />
                  <span>
                    <IntlMessages id="sidebar.users.add" />
                  </span>
                </Link>
              </Menu.Item> */}
            </SubMenu>

            <SubMenu
              key="patients"
              popupClassName={getNavStyleSubMenuClass(navStyle)}
              title={
                <span>
                  {" "}
                  <i className="icon icon-profile" />
                  <span>
                    <IntlMessages id="sidebar.patients" />
                  </span>
                </span>
              }
            >
              <Menu.Item key="patients/list">
                <Link to="/patients/list">
                  <i className="icon icon-table" />
                  <span>
                    <IntlMessages id="sidebar.patients.list" />
                  </span>
                </Link>
              </Menu.Item>
              {/* <Menu.Item key="patients/add">
                <Link to="/patients/add">
                  <i className="icon icon-add-circle" />
                  <span>
                    <IntlMessages id="sidebar.patients.add" />
                  </span>
                </Link>
              </Menu.Item> */}
            </SubMenu>

            {/* <Menu.Item key="content">
              <Link to="/content">
                <i className="icon icon-product-list" />
                <span>
                  <IntlMessages id="sidebar.content" />
                </span>
              </Link>
            </Menu.Item>

            <Menu.Item key="tools">
              <Link to="/tools">
                <i className="icon icon-setting" />
                <span>
                  <IntlMessages id="sidebar.tools" />
                </span>
              </Link>
            </Menu.Item> */}
          </Menu>
        </CustomScrollbars>
      </div>
    </>
  );
};

SidebarContent.propTypes = {};
export default SidebarContent;
