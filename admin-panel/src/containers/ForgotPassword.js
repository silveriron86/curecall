import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, Input, Select, message } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link, useHistory } from "react-router-dom";
import { getSMSCode, hideMessage } from "../appRedux/actions";
import CircularProgress from "components/CircularProgress/index";

const { Option } = Select;
const FormItem = Form.Item;

const ForgotPassword = () => {
  const dispatch = useDispatch();
  const { loader, alertMessage, showMessage, sentSMS } = useSelector(
    ({ auth }) => auth
  );
  const history = useHistory();
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  useEffect(() => {
    if (sentSMS) {
      history.push("/reset");
    }
  }, [sentSMS]);

  const onFinish = (values) => {
    dispatch(hideMessage());
    console.log("Received values of form", values);
    const { prefix, phone } = values;
    const mobile = `+${prefix}${phone}`;
    localStorage.setItem("REQUESTED_SMS_PHONE", mobile);
    console.log("mobile = ", localStorage.getItem("REQUESTED_SMS_PHONE"));
    dispatch(getSMSCode(mobile));
  };

  return (
    <div className="gx-app-login-wrap">
      <div className="gx-login-container">
        <div className="gx-login-content">
          <div className="gx-login-header">
            <img
              src={require("assets/images/Logo color.svg")}
              alt="Curecall"
              title="Curecall"
              width="100"
            />
          </div>
          <div className="gx-mb-4">
            <h2>Forgot Your Password ?</h2>
          </div>

          <Form
            initialValues={{ prefix: "33" }}
            name="basic"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            className="gx-signin-form gx-form-row0"
          >
            {/* <FormItem
            name="email"
            rules={[
              { required: true, message: "Please input your email or mobile" },
            ]}
          >
            <Input
              className="gx-input-lineheight"
              // type="email"
              placeholder="Email / Mobile"
            />
          </FormItem> */}
            <Form.Item
              name={"phone"}
              rules={[
                {
                  required: true,
                  message: "Please input a mobile phone",
                },
              ]}
            >
              <Input
                className="gx-input-lineheight"
                placeholder="Mobile number"
                addonBefore={
                  <Form.Item name={"prefix"} noStyle>
                    <Select style={{ width: 70 }}>
                      <Option value="33">+33</Option>
                    </Select>
                  </Form.Item>
                }
                // style={{ width: 300 }}
              />
            </Form.Item>

            <FormItem>
              <Button type="primary" className="gx-mb-0" htmlType="submit">
                <IntlMessages id="app.userAuth.receiveSMS" />
              </Button>{" "}
              <Link to="/signin">
                <IntlMessages id="app.userAuth.seConnecter" />
              </Link>
            </FormItem>
          </Form>
        </div>
      </div>
      {loader ? (
        <div
          className="gx-loader-view"
          style={{ position: "absolute", width: "100%" }}
        >
          <CircularProgress />
        </div>
      ) : null}
      {showMessage ? message.error(alertMessage.toString()) : null}
    </div>
  );
};

export default ForgotPassword;
