import moment from "moment";

let Utils = {
  getOrdinalSuffix: (i) => {
    var j = i % 10,
      k = i % 100;
    if (j === 1 && k !== 11) {
      return i + "st";
    }
    if (j === 2 && k !== 12) {
      return i + "nd";
    }
    if (j === 3 && k !== 13) {
      return i + "rd";
    }
    return i + "th";
  },
  formatDateTime: (v) => {
    return v ? moment(v).format("DD/MM/YYYY HH:mm:ss") : "";
  },
  formatDate: (v) => {
    return v ? moment(v).format("DD/MM/YYYY") : "";
  },
  generateRandomStr: (length) => {
    let result = "";
    const characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  },
  getAge: (birthdate) => {
    return birthdate ? moment().diff(moment(birthdate), "years") : 0;
  },
  cleanObject: (obj) => {
    for (let propName in obj) {
      if (obj[propName] === null) {
        delete obj[propName];
      }
    }
    return obj;
  },
};

export default Utils;
